using System;
using UnityEngine;

public class PID_controller
{
	public string name;

	public uint inputValue;
	public uint outputValue;

	private int error;

	public int SetPoint;
	public float T;
	public float Kp;
	public float Kd;
	public float Ki;

	private float K1;
	private float K2;
	private float K3;

	private float E;
	private float Em1;
	private float Em2;
	private uint Um1;
	private uint Um2;

	public PID_controller ( float _Kp, float _Kd, float _Ki, int _setPoint, float _T, string _name )
	{
		SetPoint = _setPoint;
		//T = _T;
		Kp = _Kp;
		Kd = _Kd;
		Ki = _Ki;
		name = "pid_" + _name;
		Debug.Log ("PID created: " + name);

		Um1 = 0;
		Um2 = 0;
		E = 0;
		Em1 = 0;
		Em2 = 0;

	}

	public uint processInput() {

		Debug.Log ("SP: " + SetPoint);

		uint _value = 0;

		T = Time.deltaTime;

		K1 = Kp + Kd / T + Ki * T;
		K2 = Ki * T - 2 * Kd / T;
		K3 = Kd / T - Kp;

		E = (float)SetPoint - (float)inputValue;

		float u = Um2 + K1 * E + K2 * Em1 + K3 * Em2;

		_value = (uint)Math.Round(u, 0);

		Um2 = Um1;
		Um1 = _value;

		Em2 = Em1;
		Em1 = E;

		Debug.Log ("inputValue: " + inputValue);
		Debug.Log ("E: " + E );
		Debug.Log ("output: " + _value);

		return _value;
	}
}