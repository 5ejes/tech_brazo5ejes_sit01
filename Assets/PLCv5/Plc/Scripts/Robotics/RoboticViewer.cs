﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RoboticViewer : MonoBehaviour {

    public bool[] digitalInputs;
    public bool[] digitalOutputs;
    public uint[] memoryData;
    public byte[] analogInputs;

    public counterElement[] countersData;
    public timerElement[] timersData;

    public Button buttonStart;
    public Button buttonGrafcet;
    public Button I2Bt;
    public Button I3Bt;
    public Button I4Bt;
    public Button I5Bt;
    public Button I6Bt;
    public Button I7Bt;
    public Button I8Bt;
    public Button I9Bt;
    public Button I10Bt;
    public Button I11Bt;
    public Button I12Bt;
    public Button I13Bt;
    public Button I14Bt;
    public Button I15Bt;

    public Image O2Ind;
    public Image O3Ind;
    public Image O4Ind;
    public Image O5Ind;
    public Image O6Ind;
    public Image O7Ind;
    public Image O8Ind;
    public Image O9Ind;
    public Image O10Ind;
    public Image O11Ind;
    public Image O12Ind;
    public Image O13Ind;
    public Image O14Ind;
    public Image O15Ind;

    public Text inputsTitle;
    public Text outputsTitle;
    public Text title1;
    public Text title2;
    public Text title3;
    public Text title4;
    public Text title5;
    public Text title6;
    public Text title7;

    public selectorComponent selectorArea1A;
    public selectorComponent selectorArea1B;

    public selectorComponent selectorArea2A;
    public selectorComponent selectorArea2B;

    public selectorComponent selectorArea3A;
    public selectorComponent selectorArea3B;

    public selectorComponent selectorArea4A;
    public Button btArea4A;
    public Button btArea4B;
    public Button btArea4C;
    public Button btArea4D;

    public selectorComponent selectorArea5A;
    public Image imageArea5A;
    public Button btArea5A;
    public Button btArea5B;
    public Button btArea5C;
    public Button btArea5D;
    public Button btArea5E;
    public Button btArea5F;
    public Button btArea5G;
    public Button btArea5H;

    public selectorComponent selectorArea6A;
    public Image imageArea6A;
    public Button btArea6A;
    public Button btArea6B;
    public Button btArea6C;
    public Button btArea6D;
    public Button btArea6E;
    public Button btArea6F;
    public Button btArea6G;
    public Button btArea6H;

    public Button btarea7A;

    public Text textArea8A;
    public Text textArea8B;

    public Text textArea9A;
    public Text textArea9B;

    public Text textArea10A;
    public Text textArea10B;

    public Text textArea11A;
    public Text textArea11B;

    public Text textArea12A;
    public Text textArea13A;
    public Text textArea14A;
    public Text textArea15A;
    public Text textArea16A;
    public Text textArea17A;
    public Text textArea18A;

    public Text tmp0A;
    public Text tmp0B;
    public Text tmp0C;

    public Text tmp1A;
    public Text tmp1B;
    public Text tmp1C;

    public Text tmp2A;
    public Text tmp2B;
    public Text tmp2C;

    public Text tmp3A;
    public Text tmp3B;
    public Text tmp3C;

    public Text tmp4A;
    public Text tmp4B;
    public Text tmp4C;

    public Text tmp5A;
    public Text tmp5B;
    public Text tmp5C;

    public Text tmp6A;
    public Text tmp6B;
    public Text tmp6C;

    public Text tmp7A;
    public Text tmp7B;
    public Text tmp7C;

    public Text tmp8A;
    public Text tmp8B;
    public Text tmp8C;

    public Text tmp9A;
    public Text tmp9B;
    public Text tmp9C;

    public Text tmp10A;
    public Text tmp10B;
    public Text tmp10C;

    public Text tmp11A;
    public Text tmp11B;
    public Text tmp11C;

    public Text tmp12A;
    public Text tmp12B;
    public Text tmp12C;

    public Text tmp13A;
    public Text tmp13B;
    public Text tmp13C;

    public Text tmp14A;
    public Text tmp14B;
    public Text tmp14C;

    public Text tmp15A;
    public Text tmp15B;
    public Text tmp15C;

    public Text tmp16A;
    public Text tmp16B;
    public Text tmp16C;

    public Text tmp17A;
    public Text tmp17B;
    public Text tmp17C;

    public Text tmp18A;
    public Text tmp18B;
    public Text tmp18C;

    public Text tmp19A;
    public Text tmp19B;
    public Text tmp19C;

    public Text cnt0A;
    public Text cnt0B;
    public Text cnt0C;

    public Text cnt1A;
    public Text cnt1B;
    public Text cnt1C;

    public Text cnt2A;
    public Text cnt2B;
    public Text cnt2C;

    public Text cnt3A;
    public Text cnt3B;
    public Text cnt3C;

    public Text cnt4A;
    public Text cnt4B;
    public Text cnt4C;

    public Text cnt5A;
    public Text cnt5B;
    public Text cnt5C;

    public Text cnt6A;
    public Text cnt6B;
    public Text cnt6C;

    public Text cnt7A;
    public Text cnt7B;
    public Text cnt7C;

    public Text cnt8A;
    public Text cnt8B;
    public Text cnt8C;

    public Text cnt9A;
    public Text cnt9B;
    public Text cnt9C;

    public InputField ai0;
    public InputField ai1;
    public InputField ai2;
    public InputField ai3;
    public InputField ai4;


    public GameObject _situation;

    public Image Fundido_;

	// Use this for initialization
	void Start () {

        digitalInputs = new bool[16];
        digitalOutputs = new bool[16];
        memoryData = new uint[20];
        analogInputs = new byte[5];

        buttonGrafcet.onClick.AddListener(GoToController);

        I2Bt.gameObject.AddComponent<EventTrigger>();
        I3Bt.gameObject.AddComponent<EventTrigger>();
        I4Bt.gameObject.AddComponent<EventTrigger>();
        I5Bt.gameObject.AddComponent<EventTrigger>();
        I6Bt.gameObject.AddComponent<EventTrigger>();
        I7Bt.gameObject.AddComponent<EventTrigger>();
        I8Bt.gameObject.AddComponent<EventTrigger>();
        I9Bt.gameObject.AddComponent<EventTrigger>();
        I10Bt.gameObject.AddComponent<EventTrigger>();
        I11Bt.gameObject.AddComponent<EventTrigger>();
        I12Bt.gameObject.AddComponent<EventTrigger>();
        I13Bt.gameObject.AddComponent<EventTrigger>();
        I14Bt.gameObject.AddComponent<EventTrigger>();
        I15Bt.gameObject.AddComponent<EventTrigger>();

        EventTrigger trigger_Object_I2Bt = I2Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I3Bt = I3Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I4Bt = I4Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I5Bt = I5Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I6Bt = I6Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I7Bt = I7Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I8Bt = I8Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I9Bt = I9Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I10Bt = I10Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I11Bt = I11Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I12Bt = I12Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I13Bt = I13Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I14Bt = I14Bt.gameObject.GetComponent<EventTrigger>();
        EventTrigger trigger_Object_I15Bt = I15Bt.gameObject.GetComponent<EventTrigger>();

        EventTrigger.Entry entry_I2Bt_Down = new EventTrigger.Entry();
        entry_I2Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I2Bt_Down.callback.AddListener((data) => { I2BtDownAction(); });

        EventTrigger.Entry entry_I3Bt_Down = new EventTrigger.Entry();
        entry_I3Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I3Bt_Down.callback.AddListener((data) => { I3BtDownAction(); });

        EventTrigger.Entry entry_I4Bt_Down = new EventTrigger.Entry();
        entry_I4Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I4Bt_Down.callback.AddListener((data) => { I4BtDownAction(); });

        EventTrigger.Entry entry_I5Bt_Down = new EventTrigger.Entry();
        entry_I5Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I5Bt_Down.callback.AddListener((data) => { I5BtDownAction(); });

        EventTrigger.Entry entry_I6Bt_Down = new EventTrigger.Entry();
        entry_I6Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I6Bt_Down.callback.AddListener((data) => { I6BtDownAction(); });

        EventTrigger.Entry entry_I7Bt_Down = new EventTrigger.Entry();
        entry_I7Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I7Bt_Down.callback.AddListener((data) => { I7BtDownAction(); });

        EventTrigger.Entry entry_I8Bt_Down = new EventTrigger.Entry();
        entry_I8Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I8Bt_Down.callback.AddListener((data) => { I8BtDownAction(); });

        EventTrigger.Entry entry_I9Bt_Down = new EventTrigger.Entry();
        entry_I9Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I9Bt_Down.callback.AddListener((data) => { I9BtDownAction(); });

        EventTrigger.Entry entry_I10Bt_Down = new EventTrigger.Entry();
        entry_I10Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I10Bt_Down.callback.AddListener((data) => { I10BtDownAction(); });

        EventTrigger.Entry entry_I11Bt_Down = new EventTrigger.Entry();
        entry_I11Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I11Bt_Down.callback.AddListener((data) => { I11BtDownAction(); });

        EventTrigger.Entry entry_I12Bt_Down = new EventTrigger.Entry();
        entry_I12Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I12Bt_Down.callback.AddListener((data) => { I12BtDownAction(); });

        EventTrigger.Entry entry_I13Bt_Down = new EventTrigger.Entry();
        entry_I13Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I13Bt_Down.callback.AddListener((data) => { I13BtDownAction(); });

        EventTrigger.Entry entry_I14Bt_Down = new EventTrigger.Entry();
        entry_I14Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I14Bt_Down.callback.AddListener((data) => { I14BtDownAction(); });

        EventTrigger.Entry entry_I15Bt_Down = new EventTrigger.Entry();
        entry_I15Bt_Down.eventID = EventTriggerType.PointerDown;
        entry_I15Bt_Down.callback.AddListener((data) => { I15BtDownAction(); });

        EventTrigger.Entry entry_I2Bt_Up = new EventTrigger.Entry();
        entry_I2Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I2Bt_Up.callback.AddListener((data) => { I2BtUpAction(); });

        EventTrigger.Entry entry_I3Bt_Up = new EventTrigger.Entry();
        entry_I3Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I3Bt_Up.callback.AddListener((data) => { I3BtUpAction(); });

        EventTrigger.Entry entry_I4Bt_Up = new EventTrigger.Entry();
        entry_I4Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I4Bt_Up.callback.AddListener((data) => { I4BtUpAction(); });

        EventTrigger.Entry entry_I5Bt_Up = new EventTrigger.Entry();
        entry_I5Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I5Bt_Up.callback.AddListener((data) => { I5BtUpAction(); });

        EventTrigger.Entry entry_I6Bt_Up = new EventTrigger.Entry();
        entry_I6Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I6Bt_Up.callback.AddListener((data) => { I6BtUpAction(); });

        EventTrigger.Entry entry_I7Bt_Up = new EventTrigger.Entry();
        entry_I7Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I7Bt_Up.callback.AddListener((data) => { I7BtUpAction(); });

        EventTrigger.Entry entry_I8Bt_Up = new EventTrigger.Entry();
        entry_I8Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I8Bt_Up.callback.AddListener((data) => { I8BtUpAction(); });

        EventTrigger.Entry entry_I9Bt_Up = new EventTrigger.Entry();
        entry_I9Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I9Bt_Up.callback.AddListener((data) => { I9BtUpAction(); });

        EventTrigger.Entry entry_I10Bt_Up = new EventTrigger.Entry();
        entry_I10Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I10Bt_Up.callback.AddListener((data) => { I10BtUpAction(); });

        EventTrigger.Entry entry_I11Bt_Up = new EventTrigger.Entry();
        entry_I11Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I11Bt_Up.callback.AddListener((data) => { I11BtUpAction(); });

        EventTrigger.Entry entry_I12Bt_Up = new EventTrigger.Entry();
        entry_I12Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I12Bt_Up.callback.AddListener((data) => { I12BtUpAction(); });

        EventTrigger.Entry entry_I13Bt_Up = new EventTrigger.Entry();
        entry_I13Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I13Bt_Up.callback.AddListener((data) => { I13BtUpAction(); });

        EventTrigger.Entry entry_I14Bt_Up = new EventTrigger.Entry();
        entry_I14Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I14Bt_Up.callback.AddListener((data) => { I14BtUpAction(); });

        EventTrigger.Entry entry_I15Bt_Up = new EventTrigger.Entry();
        entry_I15Bt_Up.eventID = EventTriggerType.PointerUp;
        entry_I15Bt_Up.callback.AddListener((data) => { I15BtUpAction(); });

        trigger_Object_I2Bt.triggers.Add(entry_I2Bt_Down);
        trigger_Object_I2Bt.triggers.Add(entry_I2Bt_Up);

        trigger_Object_I3Bt.triggers.Add(entry_I3Bt_Down);
        trigger_Object_I3Bt.triggers.Add(entry_I3Bt_Up);

        trigger_Object_I4Bt.triggers.Add(entry_I4Bt_Down);
        trigger_Object_I4Bt.triggers.Add(entry_I4Bt_Up);

        trigger_Object_I5Bt.triggers.Add(entry_I5Bt_Down);
        trigger_Object_I5Bt.triggers.Add(entry_I5Bt_Up);

        trigger_Object_I6Bt.triggers.Add(entry_I6Bt_Down);
        trigger_Object_I6Bt.triggers.Add(entry_I6Bt_Up);

        trigger_Object_I7Bt.triggers.Add(entry_I7Bt_Down);
        trigger_Object_I7Bt.triggers.Add(entry_I7Bt_Up);

        trigger_Object_I8Bt.triggers.Add(entry_I8Bt_Down);
        trigger_Object_I8Bt.triggers.Add(entry_I8Bt_Up);

        trigger_Object_I9Bt.triggers.Add(entry_I9Bt_Down);
        trigger_Object_I9Bt.triggers.Add(entry_I9Bt_Up);

        trigger_Object_I10Bt.triggers.Add(entry_I10Bt_Down);
        trigger_Object_I10Bt.triggers.Add(entry_I10Bt_Up);

        trigger_Object_I11Bt.triggers.Add(entry_I11Bt_Down);
        trigger_Object_I11Bt.triggers.Add(entry_I11Bt_Up);

        trigger_Object_I12Bt.triggers.Add(entry_I12Bt_Down);
        trigger_Object_I12Bt.triggers.Add(entry_I12Bt_Up);

        trigger_Object_I13Bt.triggers.Add(entry_I13Bt_Down);
        trigger_Object_I13Bt.triggers.Add(entry_I13Bt_Up);

        trigger_Object_I14Bt.triggers.Add(entry_I14Bt_Down);
        trigger_Object_I14Bt.triggers.Add(entry_I14Bt_Up);

        trigger_Object_I15Bt.triggers.Add(entry_I15Bt_Down);
        trigger_Object_I15Bt.triggers.Add(entry_I15Bt_Up);

        SetDataElements();

        buttonStart.gameObject.AddComponent<EventTrigger>();
       
        EventTrigger trigger_Object_Start = buttonStart.gameObject.GetComponent<EventTrigger>();

        EventTrigger.Entry entry_Start_Down = new EventTrigger.Entry();
        entry_Start_Down.eventID = EventTriggerType.PointerDown;
        entry_Start_Down.callback.AddListener((data) => { ActionStartDown((PointerEventData)data); });

        EventTrigger.Entry entry_Start_Up = new EventTrigger.Entry();
        entry_Start_Up.eventID = EventTriggerType.PointerUp;
        entry_Start_Up.callback.AddListener((data) => { ActionStartUp((PointerEventData)data); });

        trigger_Object_Start.triggers.Add(entry_Start_Down);
        trigger_Object_Start.triggers.Add(entry_Start_Up);

        selectorArea1A.setBtAction(ActionComponent1A);
        selectorArea2A.setBtAction(ActionComponent2A);
        selectorArea3A.setBtAction(ActionComponent3A);

        selectorArea1B.setBtAction(ActionComponent1B);
        selectorArea2B.setBtAction(ActionComponent2B);
        selectorArea3B.setBtAction(ActionComponent3B);

        selectorArea4A.setBtAction(ActionComponent4A);
        btArea4A.onClick.AddListener(ActionBtArea4A);
        btArea4B.onClick.AddListener(ActionBtArea4B);
        btArea4C.onClick.AddListener(ActionBtArea4C);
        btArea4D.onClick.AddListener(ActionBtArea4D);

        selectorArea5A.setBtAction(ActionComponent5A);
        btArea5A.onClick.AddListener(ActionBtArea5A);
        btArea5B.onClick.AddListener(ActionBtArea5B);
        btArea5C.onClick.AddListener(ActionBtArea5C);
        btArea5D.onClick.AddListener(ActionBtArea5D);
        btArea5E.onClick.AddListener(ActionBtArea5E);
        btArea5F.onClick.AddListener(ActionBtArea5F);
        btArea5G.onClick.AddListener(ActionBtArea5G);
        btArea5H.onClick.AddListener(ActionBtArea5H);

        selectorArea6A.setBtAction(ActionComponent6A);
        btArea6A.onClick.AddListener(ActionBtArea6A);
        btArea6B.onClick.AddListener(ActionBtArea6B);
        btArea6C.onClick.AddListener(ActionBtArea6C);
        btArea6D.onClick.AddListener(ActionBtArea6D);
        btArea6E.onClick.AddListener(ActionBtArea6E);
        btArea6F.onClick.AddListener(ActionBtArea6F);
        btArea6G.onClick.AddListener(ActionBtArea6G);
        btArea6H.onClick.AddListener(ActionBtArea6H);

        btarea7A.onClick.AddListener(ActionBtArea7A);
	}
	
    public void InitialAction(){
        _situation = GameObject.Find("Canvas/OnlyControllerMain");
        SetTexts();
    }

    private void SetDataElements(){
        List<string> _data1 = new List<string>();
        List<string> _data2 = new List<string>();
        List<string> _data3 = new List<string>();
        List<string> _data4 = new List<string>();

        for (int i = 0; i <= 100; i++)
        {
            _data1.Add(i.ToString());
        }
        for (int i = 0; i <= 255; i++)
        {
            _data2.Add(i.ToString());
        }

        for (int i = 0; i <= 15; i++)
        {
            _data3.Add(i.ToString());
        }

        for (int i = 0; i <= 7; i++)
        {
            _data4.Add(i.ToString());
        }

        selectorArea1A.initializeElement(_data1);
        selectorArea2A.initializeElement(_data1);
        selectorArea3A.initializeElement(_data1);

        selectorArea1B.initializeElement(_data2);
        selectorArea2B.initializeElement(_data2);
        selectorArea3B.initializeElement(_data2);

        selectorArea4A.initializeElement(_data3);

        selectorArea5A.initializeElement(_data4);
        selectorArea6A.initializeElement(_data4);
    }

    private void SetTexts(){
        buttonStart.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='robotic_start']").InnerText;
        buttonGrafcet.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='robotic_grafcet']").InnerText;
        inputsTitle.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='robotic_inputs']").InnerText;
        outputsTitle.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='robotic_outputs']").InnerText;

        title1.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='robotic_title_1']").InnerText;
        title2.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='robotic_title_2']").InnerText;
        title3.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='robotic_title_3']").InnerText;
        title4.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='robotic_title_4']").InnerText;
        title5.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='robotic_title_5']").InnerText;
        title6.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='robotic_title_6']").InnerText;
        title7.text = Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='robotic_title_7']").InnerText;
    }

    public void GoToController(){
        Fundido_.transform.SetAsLastSibling();
        Fundido_.CrossFadeAlpha(1, 0.5f, false);
        Fundido_.raycastTarget = true;

        GameObject.Find("SystemController").GetComponent<baseSystem>().goToController();
    }

    private void ActionStartDown(PointerEventData data){
        digitalInputs[0] = true;
    }

    private void ActionStartUp(PointerEventData data)
    {
        digitalInputs[0] = false;
    }

    private void I2BtDownAction(){
        digitalInputs[2] = true;
    }
    private void I2BtUpAction()
    {
        digitalInputs[2] = false;
    }

    private void I3BtDownAction()
    {
        digitalInputs[3] = true;
    }
    private void I3BtUpAction()
    {
        digitalInputs[3] = false;
    }

    private void I4BtDownAction()
    {
        digitalInputs[4] = true;
    }
    private void I4BtUpAction()
    {
        digitalInputs[4] = false;
    }

    private void I5BtDownAction()
    {
        digitalInputs[5] = true;
    }
    private void I5BtUpAction()
    {
        digitalInputs[5] = false;
    }

    private void I6BtDownAction()
    {
        digitalInputs[6] = true;
    }
    private void I6BtUpAction()
    {
        digitalInputs[6] = false;
    }

    private void I7BtDownAction()
    {
        digitalInputs[7] = true;
    }
    private void I7BtUpAction()
    {
        digitalInputs[7] = false;
    }

    private void I8BtDownAction()
    {
        digitalInputs[8] = true;
    }
    private void I8BtUpAction()
    {
        digitalInputs[8] = false;
    }

    private void I9BtDownAction()
    {
        digitalInputs[9] = true;
    }
    private void I9BtUpAction()
    {
        digitalInputs[9] = false;
    }

    private void I10BtDownAction()
    {
        digitalInputs[10] = true;
    }
    private void I10BtUpAction()
    {
        digitalInputs[10] = false;
    }

    private void I11BtDownAction()
    {
        digitalInputs[11] = true;
    }
    private void I11BtUpAction()
    {
        digitalInputs[11] = false;
    }

    private void I12BtDownAction()
    {
        digitalInputs[12] = true;
    }
    private void I12BtUpAction()
    {
        digitalInputs[12] = false;
    }

    private void I13BtDownAction()
    {
        digitalInputs[13] = true;
    }
    private void I13BtUpAction()
    {
        digitalInputs[13] = false;
    }

    private void I14BtDownAction()
    {
        digitalInputs[14] = true;
    }
    private void I14BtUpAction()
    {
        digitalInputs[14] = false;
    }

    private void I15BtDownAction()
    {
        digitalInputs[15] = true;
    }
    private void I15BtUpAction()
    {
        digitalInputs[15] = false;
    }

    private void ActionComponent1A(){
        int intVal = Mathf.RoundToInt((float)selectorArea1A.getIndex() * 2.55f);
        selectorArea1B.setValuePerIndex(intVal);

        memoryData[15] = (uint)intVal;
    }
    private void ActionComponent1B()
    {
        int cenVal = Mathf.RoundToInt((float)selectorArea1B.getIndex() / 2.55f);
        selectorArea1A.setValuePerIndex(cenVal);

        memoryData[15] = (uint)selectorArea1B.getIndex();
    }

    private void ActionComponent2A()
    {
        int intVal = Mathf.RoundToInt((float)selectorArea2A.getIndex() * 2.55f);
        selectorArea2B.setValuePerIndex(intVal);

        memoryData[16] = (uint)intVal;
    }
    private void ActionComponent2B()
    {
        int cenVal = Mathf.RoundToInt((float)selectorArea2B.getIndex() / 2.55f);
        selectorArea2A.setValuePerIndex(cenVal);

        memoryData[16] = (uint)selectorArea2B.getIndex();
    }

    private void ActionComponent3A()
    {
        int intVal = Mathf.RoundToInt((float)selectorArea3A.getIndex() * 2.55f);
        selectorArea3B.setValuePerIndex(intVal);

        memoryData[17] = (uint)intVal;
    }
    private void ActionComponent3B()
    {
        int cenVal = Mathf.RoundToInt((float)selectorArea3B.getIndex() / 2.55f);
        selectorArea3A.setValuePerIndex(cenVal);

        memoryData[17] = (uint)selectorArea3B.getIndex();
    }

    private void ActionComponent4A()
    { 
        int value = selectorArea4A.getIndex();

        BitArray valueBits = new BitArray(new int[] { value });

        btArea4A.GetComponentInChildren<Text>().text = (valueBits[0] == true) ? "1" : "0";
        btArea4B.GetComponentInChildren<Text>().text = (valueBits[1] == true) ? "1" : "0";
        btArea4C.GetComponentInChildren<Text>().text = (valueBits[2] == true) ? "1" : "0";
        btArea4D.GetComponentInChildren<Text>().text = (valueBits[3] == true) ? "1" : "0";

        ConfigureButton(btArea4A);
        ConfigureButton(btArea4B);
        ConfigureButton(btArea4C);
        ConfigureButton(btArea4D);

        memoryData[13] = (uint)value;
    }

    private void ActionBtArea4A(){
        int valueToAdd = 0;
        if (btArea4A.GetComponentInChildren<Text>().text == "0")
        {
            btArea4A.GetComponentInChildren<Text>().text = "1";
            valueToAdd = 1;
        }
        else
        {
            btArea4A.GetComponentInChildren<Text>().text = "0";
            valueToAdd = -1;
        }

        ConfigureButton(btArea4A);

        //change values in selector
        int value = selectorArea4A.getIndex();
        value = value + valueToAdd * (int)Mathf.Pow(2, 0);

        selectorArea4A.setValuePerIndex(value);

        memoryData[13] = (uint)value;
    }

    private void ActionBtArea4B()
    {
        int valueToAdd = 0;
        if (btArea4B.GetComponentInChildren<Text>().text == "0")
        {
            btArea4B.GetComponentInChildren<Text>().text = "1";
            valueToAdd = 1;
        }
        else
        {
            btArea4B.GetComponentInChildren<Text>().text = "0";
            valueToAdd = -1;
        }

        ConfigureButton(btArea4B);

        //change values in selector
        int value = selectorArea4A.getIndex();
        value = value + valueToAdd * (int)Mathf.Pow(2, 1);

        selectorArea4A.setValuePerIndex(value);

        memoryData[13] = (uint)value;
    }

    private void ActionBtArea4C()
    {
        int valueToAdd = 0;
        if (btArea4C.GetComponentInChildren<Text>().text == "0")
        {
            btArea4C.GetComponentInChildren<Text>().text = "1";
            valueToAdd = 1;
        }
        else
        {
            btArea4C.GetComponentInChildren<Text>().text = "0";
            valueToAdd = -1;
        }

        ConfigureButton(btArea4C);

        //change values in selector
        int value = selectorArea4A.getIndex();
        value = value + valueToAdd * (int)Mathf.Pow(2, 2);

        selectorArea4A.setValuePerIndex(value);

        memoryData[13] = (uint)value;
    }

    private void ActionBtArea4D()
    {
        int valueToAdd = 0;
        if (btArea4D.GetComponentInChildren<Text>().text == "0")
        {
            btArea4D.GetComponentInChildren<Text>().text = "1";
            valueToAdd = 1;
        }
        else
        {
            btArea4D.GetComponentInChildren<Text>().text = "0";
            valueToAdd = -1;
        }

        ConfigureButton(btArea4D);

        //change values in selector
        int value = selectorArea4A.getIndex();
        value = value + valueToAdd * (int)Mathf.Pow(2, 3);

        selectorArea4A.setValuePerIndex(value);

        memoryData[13] = (uint)value;
    }

    private void ActionComponent5A(){
        switch(selectorArea5A.getIndex()){
            case 0:
                imageArea5A.transform.localPosition = btArea5A.transform.localPosition;
                imageArea6A.transform.localPosition = btArea6A.transform.localPosition;
                break;
            case 1:
                imageArea5A.transform.localPosition = btArea5B.transform.localPosition;
                imageArea6A.transform.localPosition = btArea6B.transform.localPosition;
                break;
            case 2:
                imageArea5A.transform.localPosition = btArea5C.transform.localPosition;
                imageArea6A.transform.localPosition = btArea6C.transform.localPosition;
                break;
            case 3:
                imageArea5A.transform.localPosition = btArea5D.transform.localPosition;
                imageArea6A.transform.localPosition = btArea6D.transform.localPosition;
                break;
            case 4:
                imageArea5A.transform.localPosition = btArea5E.transform.localPosition;
                imageArea6A.transform.localPosition = btArea6E.transform.localPosition;
                break;
            case 5:
                imageArea5A.transform.localPosition = btArea5F.transform.localPosition;
                imageArea6A.transform.localPosition = btArea6F.transform.localPosition;
                break;
            case 6:
                imageArea5A.transform.localPosition = btArea5G.transform.localPosition;
                imageArea6A.transform.localPosition = btArea6G.transform.localPosition;
                break;
            case 7:
                imageArea5A.transform.localPosition = btArea5H.transform.localPosition;
                imageArea6A.transform.localPosition = btArea6H.transform.localPosition;
                break;
        }

        selectorArea6A.setValuePerIndex(selectorArea5A.getIndex());

        memoryData[14] = (uint)selectorArea5A.getIndex();
    }

    private void ActionBtArea5A(){
        imageArea5A.transform.localPosition = btArea5A.transform.localPosition;
        imageArea6A.transform.localPosition = btArea6A.transform.localPosition;
        selectorArea5A.setValuePerIndex(0);
        selectorArea6A.setValuePerIndex(0);

        memoryData[14] = 0;
    }

    private void ActionBtArea5B()
    {
        imageArea5A.transform.localPosition = btArea5B.transform.localPosition;
        imageArea6A.transform.localPosition = btArea6B.transform.localPosition;
        selectorArea5A.setValuePerIndex(1);
        selectorArea6A.setValuePerIndex(1);

        memoryData[14] = 1;
    }

    private void ActionBtArea5C()
    {
        imageArea5A.transform.localPosition = btArea5C.transform.localPosition;
        imageArea6A.transform.localPosition = btArea6C.transform.localPosition;
        selectorArea5A.setValuePerIndex(2);
        selectorArea6A.setValuePerIndex(2);

        memoryData[14] = 2;
    }

    private void ActionBtArea5D()
    {
        imageArea5A.transform.localPosition = btArea5D.transform.localPosition;
        imageArea6A.transform.localPosition = btArea6D.transform.localPosition;
        selectorArea5A.setValuePerIndex(3);
        selectorArea6A.setValuePerIndex(3);

        memoryData[14] = 3;
    }

    private void ActionBtArea5E()
    {
        imageArea5A.transform.localPosition = btArea5E.transform.localPosition;
        imageArea6A.transform.localPosition = btArea6E.transform.localPosition;
        selectorArea5A.setValuePerIndex(4);
        selectorArea6A.setValuePerIndex(3);

        memoryData[14] = 4;
    }

    private void ActionBtArea5F()
    {
        imageArea5A.transform.localPosition = btArea5F.transform.localPosition;
        imageArea6A.transform.localPosition = btArea6F.transform.localPosition;
        selectorArea5A.setValuePerIndex(5);
        selectorArea6A.setValuePerIndex(5);

        memoryData[14] = 5;
    }

    private void ActionBtArea5G()
    {
        imageArea5A.transform.localPosition = btArea5G.transform.localPosition;
        imageArea6A.transform.localPosition = btArea6G.transform.localPosition;
        selectorArea5A.setValuePerIndex(6);
        selectorArea6A.setValuePerIndex(6);

        memoryData[14] = 6;
    }

    private void ActionBtArea5H()
    {
        imageArea5A.transform.localPosition = btArea5H.transform.localPosition;
        imageArea6A.transform.localPosition = btArea6H.transform.localPosition;
        selectorArea5A.setValuePerIndex(7);
        selectorArea6A.setValuePerIndex(7);

        memoryData[14] = 7;
    }

    private void ActionComponent6A()
    {
        switch (selectorArea6A.getIndex())
        {
            case 0:
                imageArea6A.transform.localPosition = btArea6A.transform.localPosition;
                imageArea5A.transform.localPosition = btArea5A.transform.localPosition;
                break;
            case 1:
                imageArea6A.transform.localPosition = btArea6B.transform.localPosition;
                imageArea5A.transform.localPosition = btArea5B.transform.localPosition;
                break;
            case 2:
                imageArea6A.transform.localPosition = btArea6C.transform.localPosition;
                imageArea5A.transform.localPosition = btArea5C.transform.localPosition;
                break;
            case 3:
                imageArea6A.transform.localPosition = btArea6D.transform.localPosition;
                imageArea5A.transform.localPosition = btArea5D.transform.localPosition;
                break;
            case 4:
                imageArea6A.transform.localPosition = btArea6E.transform.localPosition;
                imageArea5A.transform.localPosition = btArea5E.transform.localPosition;
                break;
            case 5:
                imageArea6A.transform.localPosition = btArea6F.transform.localPosition;
                imageArea5A.transform.localPosition = btArea5F.transform.localPosition;
                break;
            case 6:
                imageArea6A.transform.localPosition = btArea6G.transform.localPosition;
                imageArea5A.transform.localPosition = btArea5G.transform.localPosition;
                break;
            case 7:
                imageArea6A.transform.localPosition = btArea6H.transform.localPosition;
                imageArea5A.transform.localPosition = btArea5H.transform.localPosition;
                break;
        }

        selectorArea5A.setValuePerIndex(selectorArea6A.getIndex());

        memoryData[14] = (uint)selectorArea6A.getIndex();
    }

    private void ActionBtArea6A()
    {
        imageArea6A.transform.localPosition = btArea6A.transform.localPosition;
        imageArea5A.transform.localPosition = btArea5A.transform.localPosition;
        selectorArea6A.setValuePerIndex(0);
        selectorArea5A.setValuePerIndex(0);

        memoryData[14] = 0;
    }

    private void ActionBtArea6B()
    {
        imageArea6A.transform.localPosition = btArea6B.transform.localPosition;
        imageArea5A.transform.localPosition = btArea5B.transform.localPosition;
        selectorArea6A.setValuePerIndex(1);
        selectorArea5A.setValuePerIndex(1);

        memoryData[14] = 1;
    }

    private void ActionBtArea6C()
    {
        imageArea6A.transform.localPosition = btArea6C.transform.localPosition;
        imageArea5A.transform.localPosition = btArea5C.transform.localPosition;
        selectorArea6A.setValuePerIndex(2);
        selectorArea5A.setValuePerIndex(2);

        memoryData[14] = 2;
    }

    private void ActionBtArea6D()
    {
        imageArea6A.transform.localPosition = btArea6D.transform.localPosition;
        imageArea5A.transform.localPosition = btArea5D.transform.localPosition;
        selectorArea6A.setValuePerIndex(3);
        selectorArea5A.setValuePerIndex(3);

        memoryData[14] = 3;
    }

    private void ActionBtArea6E()
    {
        imageArea6A.transform.localPosition = btArea6E.transform.localPosition;
        imageArea5A.transform.localPosition = btArea5E.transform.localPosition;
        selectorArea6A.setValuePerIndex(4);
        selectorArea5A.setValuePerIndex(4);

        memoryData[14] = 4;
    }

    private void ActionBtArea6F()
    {
        imageArea6A.transform.localPosition = btArea6F.transform.localPosition;
        imageArea5A.transform.localPosition = btArea5F.transform.localPosition;
        selectorArea6A.setValuePerIndex(5);
        selectorArea5A.setValuePerIndex(5);

        memoryData[14] = 5;
    }

    private void ActionBtArea6G()
    {
        imageArea6A.transform.localPosition = btArea6G.transform.localPosition;
        imageArea5A.transform.localPosition = btArea5G.transform.localPosition;
        selectorArea6A.setValuePerIndex(6);
        selectorArea5A.setValuePerIndex(6);

        memoryData[14] = 6;
    }

    private void ActionBtArea6H()
    {
        imageArea6A.transform.localPosition = btArea6H.transform.localPosition;
        imageArea5A.transform.localPosition = btArea5H.transform.localPosition;
        selectorArea6A.setValuePerIndex(7);
        selectorArea5A.setValuePerIndex(7);

        memoryData[14] = 7;
    }

    private void ActionBtArea7A(){
        if(btarea7A.GetComponentInChildren<Text>().text == "DESACTIVADO"){
            btarea7A.GetComponentInChildren<Text>().text = "ACTIVADO";
            digitalInputs[1] = true;
        } else {
            btarea7A.GetComponentInChildren<Text>().text = "DESACTIVADO";
            digitalInputs[1] = false;
        }
    }


    private void SetOutput(Image element, bool value){
        if(value == true){
            element.color = new Color(0, 1, 38f / 255f);
        } else {
            element.color = new Color(1, 1, 1);
        }
    }

    private void ConfigureButton(Button button)
    {
        ColorBlock cb = button.colors;
        if (button.GetComponentInChildren<Text>().text == "1")
        {
            cb.normalColor = Color.gray;
            cb.highlightedColor = Color.gray;
        }
        else
        {
            cb.normalColor = Color.white;
            cb.highlightedColor = Color.white;
        }
        button.colors = cb;
    }

	// Update is called once per frame
	void Update () {

        //put outputs
        if(digitalOutputs[1] == true){
            textArea18A.text = "1";
        } else {
            textArea18A.text = "0";
        }

        if (digitalOutputs[0] == true)
        {
            textArea15A.text = "1";
        }
        else
        {
            textArea15A.text = "0";
        }

        SetOutput(O2Ind, digitalOutputs[2]);
        SetOutput(O3Ind, digitalOutputs[3]);
        SetOutput(O4Ind, digitalOutputs[4]);
        SetOutput(O5Ind, digitalOutputs[5]);
        SetOutput(O6Ind, digitalOutputs[6]);
        SetOutput(O7Ind, digitalOutputs[7]);
        SetOutput(O8Ind, digitalOutputs[8]);
        SetOutput(O9Ind, digitalOutputs[9]);
        SetOutput(O10Ind, digitalOutputs[10]);
        SetOutput(O11Ind, digitalOutputs[11]);
        SetOutput(O12Ind, digitalOutputs[12]);
        SetOutput(O13Ind, digitalOutputs[13]);
        SetOutput(O14Ind, digitalOutputs[14]);
        SetOutput(O15Ind, digitalOutputs[15]);


        //output memory
        switch((int)memoryData[4]){
            case 0:
                textArea8A.text = "ADELANTE";
                break;
            case 1:
                textArea8A.text = "ATRAS";
                break;
            case 2:
                textArea8A.text = "DETENER";
                break;
        }

        textArea8B.text = memoryData[0].ToString();

        switch ((int)memoryData[5])
        {
            case 0:
                textArea9A.text = "ADELANTE";
                break;
            case 1:
                textArea9A.text = "ATRAS";
                break;
            case 2:
                textArea9A.text = "DETENER";
                break;
        }

        textArea9B.text = memoryData[1].ToString();

        switch (memoryData[6])
        {
            case 0:
                textArea10A.text = "ADELANTE";
                break;
            case 1:
                textArea10A.text = "ATRAS";
                break;
            case 2:
                textArea10A.text = "DETENER";
                break;
        }

        textArea10B.text = memoryData[2].ToString();

        switch (memoryData[7])
        {
            case 0:
                textArea11A.text = "ADELANTE";
                break;
            case 1:
                textArea11A.text = "ATRAS";
                break;
            case 2:
                textArea11A.text = "DETENER";
                break;
        }

        textArea11B.text = memoryData[3].ToString();

        textArea12A.text = memoryData[8].ToString();
        textArea13A.text = memoryData[9].ToString();
        textArea14A.text = memoryData[10].ToString();

        textArea16A.text = memoryData[11].ToString();
        textArea17A.text = memoryData[12].ToString();


        //Temp data:
        if(timersData != null){
            tmp0A.text = timersData[0].TP.ToString();
            tmp0C.text = timersData[0].tipo.ToString();

            tmp1A.text = timersData[1].TP.ToString();
            tmp1C.text = timersData[1].tipo.ToString();

            tmp2A.text = timersData[2].TP.ToString();
            tmp2C.text = timersData[2].tipo.ToString();

            tmp3A.text = timersData[3].TP.ToString();
            tmp3C.text = timersData[3].tipo.ToString();

            tmp4A.text = timersData[4].TP.ToString();
            tmp4C.text = timersData[4].tipo.ToString();

            tmp5A.text = timersData[5].TP.ToString();
            tmp5C.text = timersData[5].tipo.ToString();

            tmp6A.text = timersData[6].TP.ToString();
            tmp6C.text = timersData[6].tipo.ToString();

            tmp7A.text = timersData[7].TP.ToString();
            tmp7C.text = timersData[7].tipo.ToString();

            tmp8A.text = timersData[8].TP.ToString();
            tmp8C.text = timersData[8].tipo.ToString();

            tmp9A.text = timersData[9].TP.ToString();
            tmp9C.text = timersData[9].tipo.ToString();

            tmp10A.text = timersData[10].TP.ToString();
            tmp10C.text = timersData[10].tipo.ToString();

            tmp11A.text = timersData[11].TP.ToString();
            tmp11C.text = timersData[11].tipo.ToString();

            tmp12A.text = timersData[12].TP.ToString();
            tmp12C.text = timersData[12].tipo.ToString();

            tmp13A.text = timersData[13].TP.ToString();
            tmp13C.text = timersData[13].tipo.ToString();

            tmp14A.text = timersData[14].TP.ToString();
            tmp14C.text = timersData[14].tipo.ToString();

            tmp15A.text = timersData[15].TP.ToString();
            tmp15C.text = timersData[15].tipo.ToString();

            tmp16A.text = timersData[16].TP.ToString();
            tmp16C.text = timersData[16].tipo.ToString();

            tmp17A.text = timersData[17].TP.ToString();
            tmp17C.text = timersData[17].tipo.ToString();

            tmp18A.text = timersData[18].TP.ToString();
            tmp18C.text = timersData[18].tipo.ToString();

            tmp19A.text = timersData[19].TP.ToString();
            tmp19C.text = timersData[19].tipo.ToString();

            try { tmp0B.text = timersData[0]._timer.ElaspedTime.ToString(); } catch (Exception e) {}
            try { tmp1B.text = timersData[1]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp2B.text = timersData[2]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp3B.text = timersData[3]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp4B.text = timersData[4]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp5B.text = timersData[5]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp6B.text = timersData[6]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp7B.text = timersData[7]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp8B.text = timersData[8]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp9B.text = timersData[9]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp10B.text = timersData[10]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp11B.text = timersData[11]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp12B.text = timersData[12]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp13B.text = timersData[13]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp14B.text = timersData[14]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp15B.text = timersData[15]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp16B.text = timersData[16]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp17B.text = timersData[17]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp18B.text = timersData[18]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
            try { tmp19B.text = timersData[19]._timer.ElaspedTime.ToString(); } catch (Exception e) { }
        }

        if(countersData != null){
            cnt0A.text = countersData[0].CVal.ToString();
            cnt0B.text = countersData[0].PV.ToString();
            cnt0C.text = countersData[0].tipo.ToString();

            cnt1A.text = countersData[1].CVal.ToString();
            cnt1B.text = countersData[1].PV.ToString();
            cnt1C.text = countersData[1].tipo.ToString();

            cnt2A.text = countersData[2].CVal.ToString();
            cnt2B.text = countersData[2].PV.ToString();
            cnt2C.text = countersData[2].tipo.ToString();

            cnt3A.text = countersData[3].CVal.ToString();
            cnt3B.text = countersData[3].PV.ToString();
            cnt3C.text = countersData[3].tipo.ToString();

            cnt4A.text = countersData[4].CVal.ToString();
            cnt4B.text = countersData[4].PV.ToString();
            cnt4C.text = countersData[4].tipo.ToString();

            cnt5A.text = countersData[5].CVal.ToString();
            cnt5B.text = countersData[5].PV.ToString();
            cnt5C.text = countersData[5].tipo.ToString();

            cnt6A.text = countersData[6].CVal.ToString();
            cnt6B.text = countersData[6].PV.ToString();
            cnt6C.text = countersData[6].tipo.ToString();

            cnt7A.text = countersData[7].CVal.ToString();
            cnt7B.text = countersData[7].PV.ToString();
            cnt7C.text = countersData[7].tipo.ToString();

            cnt8A.text = countersData[8].CVal.ToString();
            cnt8B.text = countersData[8].PV.ToString();
            cnt8C.text = countersData[8].tipo.ToString();

            cnt9A.text = countersData[9].CVal.ToString();
            cnt9B.text = countersData[9].PV.ToString();
            cnt9C.text = countersData[9].tipo.ToString();
        }

        if (ai0.text != "")
        {
            analogInputs[0] = Convert.ToByte(int.Parse(ai0.text));
        }
        else
        {
            analogInputs[0] = Convert.ToByte(0);
        }

        if (ai1.text != "")
        {
            analogInputs[1] = Convert.ToByte(int.Parse(ai1.text));
        }
        else
        {
            analogInputs[1] = Convert.ToByte(0);
        }

        if (ai2.text != "")
        {
            analogInputs[2] = Convert.ToByte(int.Parse(ai2.text));
        }
        else
        {
            analogInputs[2] = Convert.ToByte(0);
        }

        if (ai3.text != "")
        {
            analogInputs[3] = Convert.ToByte(int.Parse(ai3.text));
        }
        else
        {
            analogInputs[3] = Convert.ToByte(0);
        }

        if (ai4.text != "")
        {
            analogInputs[4] = Convert.ToByte(int.Parse(ai4.text));
        }
        else
        {
            analogInputs[4] = Convert.ToByte(0);
        }
    }
}
