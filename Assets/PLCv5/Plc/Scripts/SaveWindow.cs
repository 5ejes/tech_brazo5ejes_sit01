﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SaveWindow : MonoBehaviour {

	public Button ButtonAccept;
	public Button ButtonCancel;
	public Text labelMsg;
	public InputField nameFile;

	private string dataToSave;
	private GameObject parentSituation;
	// Use this for initialization
	void Start () {
		ButtonAccept.onClick.AddListener (saveAction);
		ButtonCancel.onClick.AddListener (cancelAction);
	}

	public void setTexts(){
		ButtonAccept.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText;
		ButtonCancel.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='cancel']").InnerText;
		labelMsg.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='save_as_message']").InnerText;
		nameFile.text = "";
	}

	public void showWindow(GameObject parentGObject, string programData){
		parentSituation = parentGObject;
		dataToSave = programData;
		switch (Manager.Instance.globalController) {
		case baseSystem.Controllers.Grafcet:
			parentGObject.GetComponent<GrafcetMainClass> ().activateBackground ();
			nameFile.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='fileDefaultName']").InnerText + ".vgraf";
			break;
		case baseSystem.Controllers.Ladder:
			parentGObject.GetComponent<LadderMainClass> ().activateBackground ();
			parentGObject.GetComponent<LadderMainClass> ().bgEditorActive = true;
			nameFile.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='fileDefaultName']").InnerText + ".vplc";
			break;
		}
		gameObject.transform.SetAsLastSibling ();

		iTween.MoveTo (
			gameObject,
			iTween.Hash (
				"position", new Vector3 (0, 0, 0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal", true
			)
		);
	}

	private void saveAction(){

		if (!Directory.Exists (Manager.Instance.globalAndroidUrl + "CloudLabs")) { 
			System.IO.Directory.CreateDirectory(Manager.Instance.globalAndroidUrl + "CloudLabs");
		}

		string path = Manager.Instance.globalAndroidUrl + "CloudLabs/";

		switch (Manager.Instance.globalController) {
		case baseSystem.Controllers.Grafcet:
			parentSituation.GetComponent<GrafcetMainClass> ().deactivateBackground ();
			if (nameFile.text.Contains (".vgraf")) {
				path = path + nameFile.text;
			} else {
				path = path + nameFile.text + ".vgraf";
			}
			break;
		case baseSystem.Controllers.Ladder:
			parentSituation.GetComponent<LadderMainClass> ().deactivateBackground ();
			if (nameFile.text.Contains (".vplc")) {
				path = path + nameFile.text;
			} else {
				path = path + nameFile.text + ".vplc";
			}
			break;
		}

		byte[] bytes = System.Text.Encoding.UTF8.GetBytes (dataToSave);
		File.WriteAllBytes (path, bytes);

		iTween.MoveTo (
			gameObject,
			iTween.Hash (
				"position", new Vector3 (0, 1400f, 0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal", true
			)
		);
	}

	private void cancelAction(){
		switch (Manager.Instance.globalController) {
		case baseSystem.Controllers.Grafcet:
			parentSituation.GetComponent<GrafcetMainClass> ().deactivateBackground ();
			break;
		case baseSystem.Controllers.Ladder:
			parentSituation.GetComponent<LadderMainClass> ().deactivateBackground ();
			break;
		}

		iTween.MoveTo (
			gameObject,
			iTween.Hash (
				"position", new Vector3 (0, 1400f, 0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal", true
			)
		);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
