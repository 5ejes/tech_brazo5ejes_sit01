﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class BaseSituation : MonoBehaviour {

	[Header("GENERAL SITUATION SETTINGS", order = 1)]
	public string situationTag;
	public bool developerMode = false;
	public bool userIndicatorWithAttempts = true;
	public bool userIndicatorOnTop = true;

	[Header("GENERAL SITUATION PREFABS", order = 2)]
	public GameObject _evaluationPrefab;
	public GameObject _upperIndicatorPrefab;
	public GameObject _notePadPrefab;
	public GameObject _alertPrefab;

	[Header("GENERAL SITUATION GRAPHIC COMPONENTS", order = 3)]
	public Camera _cam1;
	public Camera _cam2;
	public Canvas _reportCanvas;

	[HideInInspector]
	public GameObject _alert;
	[HideInInspector]
	public int[] orderOptions;
	[HideInInspector]
	public GameObject _evaluation;
	[HideInInspector]
	public GameObject _upperIndicator;
	[HideInInspector]
	public GameObject _notePad;

	// Use this for initialization
	public void StartBaseSituation () {
		if (developerMode == true) {
			TextAsset _xmlTexts = Resources.Load("SpanishTexts") as TextAsset;
			TextAsset _xmlInfo = Resources.Load("SpanishInfo") as TextAsset;

			XmlDocument xmlTexts = new XmlDocument ();
			xmlTexts.LoadXml (_xmlTexts.text);
			Manager.Instance.globalTexts = xmlTexts;

			XmlDocument xmlInfo = new XmlDocument();
			xmlInfo.LoadXml(_xmlInfo.text);
			Manager.Instance.globalInfo = xmlInfo;

			Manager.Instance.globalNumericScore = new int[2]{ 0, 5 };
		}

		string _urlQuestions = "";
		switch (Manager.Instance.globalLanguage) {
		case BaseSimulator.LanguageList.English:
			_urlQuestions = "EnglishQuestions";
			break;
		case BaseSimulator.LanguageList.Spanish:
			_urlQuestions = "SpanishQuestions";
			break;
		case BaseSimulator.LanguageList.Portuguese:
			_urlQuestions = "PortugueseQuestions";
			break;
		}

		TextAsset _xmlQuestions = Resources.Load(_urlQuestions) as TextAsset;

		XmlDocument xmlQuestions = new XmlDocument();
		xmlQuestions.LoadXml(_xmlQuestions.text);

        Debug.Log("situationTag");
        Debug.Log(situationTag);

		Manager.Instance.globalQuestions = xmlQuestions.SelectNodes ("/data/" + situationTag + "/statement");
		Manager.Instance.globalComplementaryQuestions = xmlQuestions.SelectNodes ("/data/" + situationTag + "/complementaries/question");

        Manager.Instance.currentSituationName = Manager.Instance.globalInfo.SelectSingleNode ("/data/" + situationTag + "/name_practice_").InnerText;
		Manager.Instance.currentAulaCode = Manager.Instance.globalInfo.SelectSingleNode ("/data/" + situationTag + "/aula_code").InnerText;

		orderOptions = new int[4];
		orderOptions [0] = Mathf.RoundToInt(Random.Range(0.0f,3.0f));
		int _tempValue = Mathf.RoundToInt (Random.Range(0.0f,3.0f));
		while(_tempValue == orderOptions [0]){
			_tempValue = Mathf.RoundToInt (Random.Range(0.0f,3.0f));
		}
		orderOptions [1] = _tempValue;
		while(_tempValue == orderOptions [0] || _tempValue == orderOptions [1]){
			_tempValue = Mathf.RoundToInt (Random.Range(0.0f,3.0f));
		}
		orderOptions [2] = _tempValue;

		while(_tempValue == orderOptions [0] || _tempValue == orderOptions [1] || _tempValue == orderOptions [2]){
			_tempValue = Mathf.RoundToInt (Random.Range(0.0f,3.0f));
		}
		orderOptions [3] = _tempValue;

		//float _scaleUpper = _cam1.pixelWidth / 1920f;
		float _aspectRatio = (float)_cam1.pixelWidth / (float)_cam1.pixelHeight;
		float posY = -337.457f * _aspectRatio + 1090;

		if (userIndicatorOnTop == true) {

			_upperIndicator = Instantiate (_upperIndicatorPrefab, new Vector3 (540, -40, 0), Quaternion.identity) as GameObject;
			_upperIndicator.transform.SetParent (GameObject.Find ("Canvas").transform, false);
			_upperIndicator.name = "upperIndicator";
			_upperIndicator.transform.SetSiblingIndex (5);
			if (userIndicatorWithAttempts == true) {
				_upperIndicator.GetComponent<UpperIndicator> ().configureIndicator (1);
			} else {
				_upperIndicator.GetComponent<UpperIndicator> ().configureIndicator (2);
			}
			_upperIndicator.GetComponent<UpperIndicator> ().startTimer ();	

		} else {

			_upperIndicatorPrefab.GetComponent<RectTransform>().anchorMin = new Vector2(1f, 0);
			_upperIndicatorPrefab.GetComponent<RectTransform>().anchorMax = new Vector2(1f, 0);
			_upperIndicatorPrefab.GetComponent<RectTransform>().pivot = new Vector2(1f, 0);

			_upperIndicator = Instantiate (_upperIndicatorPrefab, new Vector3 (270, -25, 0), Quaternion.identity) as GameObject;
			_upperIndicator.transform.SetParent (GameObject.Find ("Canvas").transform, false);
			_upperIndicator.name = "upperIndicator";
			_upperIndicator.transform.SetSiblingIndex (5);
			if (userIndicatorWithAttempts == true) {
				_upperIndicator.GetComponent<UpperIndicator> ().configureIndicator (1);
			} else {
				_upperIndicator.GetComponent<UpperIndicator> ().configureIndicator (2);
			}
			_upperIndicator.GetComponent<UpperIndicator> ().startTimer ();	
		
		}


		_notePad = Instantiate (_notePadPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
		_notePad.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		_notePad.transform.localPosition = new Vector3 (0, 1200, 0);
		_notePad.name = "notePad";

		string _complementary = "";
		for(int i = 0; i < Manager.Instance.globalComplementaryQuestions.Count; i++){
			if(Manager.Instance.globalComplementaryQuestions [i].InnerText == ""){
				_notePad.GetComponent<NotePad> ().addPage ("", false, Manager.Instance.globalComplementaryQuestions [i].InnerText, true);
			}
			else {
				_notePad.GetComponent<NotePad> ().addPage ("", false, Manager.Instance.globalComplementaryQuestions [i].InnerText);
			}
		}
		_notePad.GetComponent<NotePad> ().goToFirstPage ();

		_reportCanvas.enabled = false;
	}
}
