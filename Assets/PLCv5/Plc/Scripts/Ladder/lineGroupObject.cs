﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using UnityEngine.EventSystems;

public class lineGroupObject : MonoBehaviour {

	public GameObject lineObject1;
	public GameObject lineObject2;
	public GameObject lineObject3;
	public GameObject lineObject4;
	public GameObject lineObject5;
	public GameObject lineObject6;
	public GameObject lineObject7;
	public GameObject lineObject8;
	public GameObject lineObject9;
	public GameObject lineObject10;
	public GameObject lineObject11;
	public GameObject lineObject12;
	public GameObject lineObject13;

	public GameObject editorContainer;
	public GameObject addPrevContainer;
	public Button deleteLineBt;
	public Button moveDownLineBt;
	public Button moveUpLineBt;
	public Button copyLineBt;
	public Button pasteLineBt;
	public Button addPrevLineBt;

	public List<GameObject> branchLines;

	public GameObject lineObjectPrefab;

	public float initialPosition;

	private GameObject _situation;
	// Use this for initialization
	void Start () {

		_situation = GameObject.Find ("Canvas/Ladder");

		if(lineObject1 == null){
			lineObject1 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject1.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject1.GetComponent<lineObject> ().lineNumber = 0;
			lineObject1.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject1.transform.localPosition = new Vector3 (0, 0, 0);
		}
		if(lineObject2 == null){
			lineObject2 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject2.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject2.GetComponent<lineObject> ().lineNumber = 1;
			lineObject2.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject2.transform.localPosition = new Vector3 (124, 0, 0);
		}
		if(lineObject3 == null){
			lineObject3 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject3.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject3.GetComponent<lineObject> ().lineNumber = 2;
			lineObject3.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject3.transform.localPosition = new Vector3 (248, 0, 0);
		}
		if(lineObject4 == null){
			lineObject4 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject4.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject4.GetComponent<lineObject> ().lineNumber = 3;
			lineObject4.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject4.transform.localPosition = new Vector3 (372, 0, 0);
		}
		if(lineObject5 == null){
			lineObject5 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject5.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject5.GetComponent<lineObject> ().lineNumber = 4;
			lineObject5.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject5.transform.localPosition = new Vector3 (496, 0, 0);
		}
		if(lineObject6 == null){
			lineObject6 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject6.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject6.GetComponent<lineObject> ().lineNumber = 5;
			lineObject6.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject6.transform.localPosition = new Vector3 (620, 0, 0);
		}
		if(lineObject7 == null){
			lineObject7 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject7.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject7.GetComponent<lineObject> ().lineNumber = 6;
			lineObject7.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject7.transform.localPosition = new Vector3 (744, 0, 0);
		}
		if(lineObject8 == null){
			lineObject8 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject8.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject8.GetComponent<lineObject> ().lineNumber = 7;
			lineObject8.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject8.transform.localPosition = new Vector3 (868, 0, 0);
		}
		if(lineObject9 == null){
			lineObject9 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject9.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject9.GetComponent<lineObject> ().lineNumber = 8;
			lineObject9.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject9.transform.localPosition = new Vector3 (992, 0, 0);
		}
		if(lineObject10 == null){
			lineObject10 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject10.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject10.GetComponent<lineObject> ().lineNumber = 9;
			lineObject10.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject10.transform.localPosition = new Vector3 (1116, 0, 0);
		}
		if(lineObject11 == null){
			lineObject11 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject11.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject11.GetComponent<lineObject> ().lineNumber = 10;
			lineObject11.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject11.transform.localPosition = new Vector3 (1240, 0, 0);
		}
		if(lineObject12 == null){
			lineObject12 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject12.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Input;
			lineObject12.GetComponent<lineObject> ().lineNumber = 11;
			lineObject12.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject12.transform.localPosition = new Vector3 (1364, 0, 0);
		}
		if(lineObject13 == null){
			lineObject13 = Instantiate (lineObjectPrefab, gameObject.transform);
			lineObject13.GetComponent<lineObject> ().allowedType = dragElementLadder._typesIO.Output;
			lineObject13.GetComponent<lineObject> ().lineNumber = 12;
			lineObject13.GetComponent<lineObject> ().parentLineGroup = gameObject;
			lineObject13.transform.localPosition = new Vector3 (1488, 0, 0);
		}

		editorContainer.SetActive (false);
		addPrevContainer.SetActive (false);

		deleteLineBt.gameObject.AddComponent<EventTrigger> ();
		moveDownLineBt.gameObject.AddComponent<EventTrigger> ();
		moveUpLineBt.gameObject.AddComponent<EventTrigger> ();
		copyLineBt.gameObject.AddComponent<EventTrigger> ();
		pasteLineBt.gameObject.AddComponent<EventTrigger> ();
		addPrevLineBt.gameObject.AddComponent<EventTrigger> ();

		EventTrigger.Entry entry_1_0 = new EventTrigger.Entry();
		entry_1_0.eventID = EventTriggerType.PointerEnter;
		entry_1_0.callback.AddListener((data) => { _situation.GetComponent<LadderMainClass>().overButtonAction_((PointerEventData)data, "deleteLine"); });

		EventTrigger.Entry entry_1_1 = new EventTrigger.Entry();
		entry_1_1.eventID = EventTriggerType.PointerEnter;
		entry_1_1.callback.AddListener((data) => { _situation.GetComponent<LadderMainClass>().overButtonAction_((PointerEventData)data, "moveDownLine"); });

		EventTrigger.Entry entry_1_2 = new EventTrigger.Entry();
		entry_1_2.eventID = EventTriggerType.PointerEnter;
		entry_1_2.callback.AddListener((data) => { _situation.GetComponent<LadderMainClass>().overButtonAction_((PointerEventData)data, "moveUpLine"); });

		EventTrigger.Entry entry_1_3 = new EventTrigger.Entry();
		entry_1_3.eventID = EventTriggerType.PointerEnter;
		entry_1_3.callback.AddListener((data) => { _situation.GetComponent<LadderMainClass>().overButtonAction_((PointerEventData)data, "copyLine"); });

		EventTrigger.Entry entry_1_4 = new EventTrigger.Entry();
		entry_1_4.eventID = EventTriggerType.PointerEnter;
		entry_1_4.callback.AddListener((data) => { _situation.GetComponent<LadderMainClass>().overButtonAction_((PointerEventData)data, "pasteLine"); });

		EventTrigger.Entry entry_1_5 = new EventTrigger.Entry();
		entry_1_5.eventID = EventTriggerType.PointerEnter;
		entry_1_5.callback.AddListener((data) => { _situation.GetComponent<LadderMainClass>().overButtonAction_((PointerEventData)data, "addPrevLine"); });

		EventTrigger.Entry entry_2_0 = new EventTrigger.Entry();
		entry_2_0.eventID = EventTriggerType.PointerExit;
		entry_2_0.callback.AddListener((data) => { _situation.GetComponent<LadderMainClass>().outButtonAction_((PointerEventData)data, ""); });

		deleteLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_1_0);
		deleteLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_2_0);

		moveDownLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_1_1);
		moveDownLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_2_0);

		moveUpLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_1_2);
		moveUpLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_2_0);

		copyLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_1_3);
		copyLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_2_0);

		pasteLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_1_4);
		pasteLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_2_0);

		addPrevLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_1_5);
		addPrevLineBt.GetComponent<EventTrigger> ().triggers.Add (entry_2_0);

		deleteLineBt.onClick.AddListener (removeLineAction);
		moveDownLineBt.onClick.AddListener (moveDownLineAction);
		moveUpLineBt.onClick.AddListener (moveUpLineAction);
		copyLineBt.onClick.AddListener (copyLineAction);
		pasteLineBt.onClick.AddListener (pasteLineAction);
		addPrevLineBt.onClick.AddListener (addPrevLineAction);

	}

	private void removeLineAction(){
		int currentIndex = 0;
		for(int i = 0; i < _situation.GetComponent<LadderMainClass>().lineGroupObjects.Count; i++){
			if (_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].GetInstanceID () == gameObject.GetInstanceID ()) {
				currentIndex = i;
				_situation.GetComponent<LadderMainClass> ().lineGroupObjects.RemoveAt (i);
				break;
			}
		}

		if (_situation.GetComponent<LadderMainClass> ().lineGroupObjects.Count == 0) {
			_situation.GetComponent<LadderMainClass> ().createInitialGroupLine ();
			_situation.GetComponent<LadderMainClass> ().editAction ();
		} else {
			int maxBranch = getMaxBranchOffset ();
			for(int i = currentIndex; i < _situation.GetComponent<LadderMainClass>().lineGroupObjects.Count; i++ ){
				_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition = new Vector3 (_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.x, _situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.y + (140f + 140f * (float)maxBranch), _situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.z);
				_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].GetComponent<lineGroupObject> ().initialPosition += (140f + 140f * (float)maxBranch);
			}
		}

		_situation.GetComponent<LadderMainClass> ().updateWorkbenchArea ();

		Destroy (gameObject);
	}

	private void moveDownLineAction(){
		int currentIndex = 0;
		for(int i = 0; i < _situation.GetComponent<LadderMainClass>().lineGroupObjects.Count; i++){
			if (_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].GetInstanceID () == gameObject.GetInstanceID ()) {
				currentIndex = i;
				break;
			}
		}

		if ((_situation.GetComponent<LadderMainClass> ().lineGroupObjects.Count - 1) >= (currentIndex + 1)) {
			GameObject nextElement = _situation.GetComponent<LadderMainClass> ().lineGroupObjects [currentIndex + 1];

			Vector2 nextPosition = nextElement.GetComponent<RectTransform> ().anchoredPosition;
			float nextYInitialPosition = nextElement.GetComponent<lineGroupObject> ().initialPosition;

			int branchDif = nextElement.GetComponent<lineGroupObject> ().getMaxBranchOffset () - getMaxBranchOffset ();

			nextElement.GetComponent<lineGroupObject> ().initialPosition = initialPosition;
			nextElement.GetComponent<lineGroupObject> ().updateLineGroupHeight ();

			initialPosition = nextYInitialPosition - branchDif * 140f;
			updateLineGroupHeight ();

			_situation.GetComponent<LadderMainClass> ().lineGroupObjects.RemoveAt (currentIndex + 1);
			_situation.GetComponent<LadderMainClass> ().lineGroupObjects.Insert (currentIndex, nextElement);

			gameObject.transform.SetSiblingIndex (nextElement.transform.GetSiblingIndex ());
		}

		//_situation.GetComponent<LadderMainClass> ().editAction ();
	}

	private void moveUpLineAction(){
		int currentIndex = 0;
		for(int i = 0; i < _situation.GetComponent<LadderMainClass>().lineGroupObjects.Count; i++){
			if (_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].GetInstanceID () == gameObject.GetInstanceID ()) {
				currentIndex = i;
				break;
			}
		}

		if ((currentIndex - 1) >= 0) {
			GameObject nextElement = _situation.GetComponent<LadderMainClass> ().lineGroupObjects [currentIndex - 1];

			Vector2 nextPosition = nextElement.GetComponent<RectTransform> ().anchoredPosition;
			float nextYInitialPosition = nextElement.GetComponent<lineGroupObject> ().initialPosition;

			int branchDif = nextElement.GetComponent<lineGroupObject> ().getMaxBranchOffset () - getMaxBranchOffset ();

			nextElement.GetComponent<lineGroupObject> ().initialPosition = initialPosition + branchDif * 140f;
			nextElement.GetComponent<lineGroupObject> ().updateLineGroupHeight ();

			initialPosition = nextYInitialPosition;
			updateLineGroupHeight ();

			_situation.GetComponent<LadderMainClass> ().lineGroupObjects.RemoveAt (currentIndex - 1);
			_situation.GetComponent<LadderMainClass> ().lineGroupObjects.Insert (currentIndex, nextElement);

			gameObject.transform.SetSiblingIndex (nextElement.transform.GetSiblingIndex ());
		}
		//_situation.GetComponent<LadderMainClass> ().editAction ();
	}

	private void copyLineAction(){
		_situation.GetComponent<LadderMainClass> ().copiedLineXML = new XmlDocument ();
		_situation.GetComponent<LadderMainClass> ().copiedLineXML.AppendChild (getXMLLineNode (_situation.GetComponent<LadderMainClass> ().copiedLineXML, 0, 0));
		//Debug.Log (_situation.GetComponent<LadderMainClass> ().copiedLineXML.OuterXml);
		//_situation.GetComponent<LadderMainClass> ().editAction ();
	}

	private void pasteLineAction(){
		_situation.GetComponent<LadderMainClass> ().prevPasteLine (gameObject);
		//StartCoroutine (_situation.GetComponent<LadderMainClass> ().pasteCopiedLine (gameObject));
		_situation.GetComponent<LadderMainClass> ().editAction ();
	}

	private void addPrevLineAction(){
		int currentIndex = 0;
		for(int i = 0; i < _situation.GetComponent<LadderMainClass>().lineGroupObjects.Count; i++){
			if (_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].GetInstanceID () == gameObject.GetInstanceID ()) {
				//delete this
				currentIndex = i;
				break;
			}
		}

		for(int i = currentIndex; i < _situation.GetComponent<LadderMainClass>().lineGroupObjects.Count; i++ ){
			_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition = new Vector3 (_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.x, _situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.y - (140f), _situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].transform.localPosition.z);
			_situation.GetComponent<LadderMainClass> ().lineGroupObjects [i].GetComponent<lineGroupObject> ().initialPosition -= (140f);
		}

		GameObject newGroupLine = Instantiate(_situation.GetComponent<LadderMainClass> ().lineGroupPrefab, _situation.GetComponent<LadderMainClass> ()._workbenchArea.transform);
		float newPosition = gameObject.GetComponent<RectTransform> ().anchoredPosition.y + 140f;
		newGroupLine.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (11f, newPosition);
		newGroupLine.GetComponent<lineGroupObject> ().initialPosition = newPosition;
		newGroupLine.transform.SetSiblingIndex (currentIndex);
		_situation.GetComponent<LadderMainClass> ().lineGroupObjects.Insert (currentIndex, newGroupLine);

		_situation.GetComponent<LadderMainClass> ().updateWorkbenchArea ();

		_situation.GetComponent<LadderMainClass> ().editAction ();
	}

	public void showWorkbenchColliders(){
		if(lineObject1 != null){
			lineObject1.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject2 != null){
			lineObject2.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject3 != null){
			lineObject3.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject4 != null){
			lineObject4.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject5 != null){
			lineObject5.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject6 != null){
			lineObject6.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject7 != null){
			lineObject7.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject8 != null){
			lineObject8.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject9 != null){
			lineObject9.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject10 != null){
			lineObject10.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject11 != null){
			lineObject11.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject12 != null){
			lineObject12.GetComponent<lineObject> ().showCollider ();
		}
		if(lineObject13 != null){
			lineObject13.GetComponent<lineObject> ().showCollider ();
		}

		for(int i = 0; i < branchLines.Count; i++){
			branchLines[i].GetComponent<lineObject> ().showCollider ();
		}
	}

	public void hideWorkbenchColliders(){
		if(lineObject1 != null){
			lineObject1.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject2 != null){
			lineObject2.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject3 != null){
			lineObject3.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject4 != null){
			lineObject4.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject5 != null){
			lineObject5.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject6 != null){
			lineObject6.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject7 != null){
			lineObject7.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject8 != null){
			lineObject8.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject9 != null){
			lineObject9.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject10 != null){
			lineObject10.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject11 != null){
			lineObject11.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject12 != null){
			lineObject12.GetComponent<lineObject> ().hideCollider ();
		}
		if(lineObject13 != null){
			lineObject13.GetComponent<lineObject> ().hideCollider ();
		}

		for(int i = 0; i < branchLines.Count; i++){
			branchLines[i].GetComponent<lineObject> ().hideCollider ();
		}
	}

	public GameObject getElementPerLineAndLevel(int lineNumber, int branchLevel){
		GameObject resultObject = null;

		if (branchLevel == 0) {
			switch (lineNumber) {
			case 0:
				resultObject = lineObject1;
				break;
			case 1:
				resultObject = lineObject2;
				break;
			case 2:
				resultObject = lineObject3;
				break;
			case 3:
				resultObject = lineObject4;
				break;
			case 4:
				resultObject = lineObject5;
				break;
			case 5:
				resultObject = lineObject6;
				break;
			case 6:
				resultObject = lineObject7;
				break;
			case 7:
				resultObject = lineObject8;
				break;
			case 8:
				resultObject = lineObject9;
				break;
			case 9:
				resultObject = lineObject10;
				break;
			case 10:
				resultObject = lineObject11;
				break;
			case 11:
				resultObject = lineObject12;
				break;
			case 12:
				resultObject = lineObject13;
				break;
			}	
		} else {
			for(int i = 0; i < branchLines.Count; i++){
				if(branchLines[i].GetComponent<lineObject>().lineNumber == lineNumber && branchLines[i].GetComponent<lineObject>().branchLevel == branchLevel ){
					resultObject = branchLines [i];
				}
			}
		}

		return resultObject;
	}

	private XmlNode getDivElements(XmlDocument doc, GameObject divElement, int lineIndex, int precOffset, out int offsetRightBranch){
		XmlNode nodeDiv = doc.CreateNode (XmlNodeType.Element, "div", "");
		XmlAttribute att1_1 = doc.CreateAttribute ("cantidad");
		att1_1.InnerText = "2";
		nodeDiv.Attributes.Append (att1_1);

		XmlNode nodeSubDiv = doc.CreateNode (XmlNodeType.Element, "subdiv", "");

		GameObject whileDivElement = divElement;

		offsetRightBranch = -1;

		while(whileDivElement != null && whileDivElement.GetComponent<lineObject>().branchElement != null ){
			XmlNode whileDiv = doc.CreateNode (XmlNodeType.Element, "entrada", "");

			XmlAttribute att1_w = doc.CreateAttribute ("numero");
			att1_w.InnerText = whileDivElement.GetComponent<lineObject> ().lineNumber.ToString();
			whileDiv.Attributes.Append (att1_w);

			XmlAttribute att2_w = doc.CreateAttribute ("linea");
			att2_w.InnerText = (lineIndex + precOffset + whileDivElement.GetComponent<lineObject> ().branchLevel).ToString ();
			whileDiv.Attributes.Append (att2_w);

			whileDiv.InnerText = whileDivElement.GetComponent<lineObject> ().getXMLValue ();
			nodeSubDiv.AppendChild (whileDiv);

			whileDivElement = whileDivElement.GetComponent<lineObject> ().rightBranchElement;

			offsetRightBranch++;
		}

		XmlNode nodeBranchDiv = doc.CreateNode (XmlNodeType.Element, "subdiv", "");

		for(int i = 0; i <= offsetRightBranch; i++){
			GameObject branchNewElement = getElementPerLineAndLevel (divElement.GetComponent<lineObject> ().lineNumber + i, divElement.GetComponent<lineObject> ().branchLevel + 1);

			if (branchNewElement.GetComponent<lineObject> ().branchElement != null) {

				XmlNode nodeSectionDiv = nodeBranchDiv.AppendChild (doc.CreateNode (XmlNodeType.Element, "entrada", ""));

				XmlAttribute att1_Div = doc.CreateAttribute ("numero");
				att1_Div.InnerText = branchNewElement.GetComponent<lineObject> ().lineNumber.ToString();
				nodeSectionDiv.Attributes.Append (att1_Div);

				XmlAttribute att2_Div = doc.CreateAttribute ("linea");
				att2_Div.InnerText = (lineIndex + precOffset + branchNewElement.GetComponent<lineObject> ().branchLevel).ToString ();
				nodeSectionDiv.Attributes.Append (att2_Div);

				int lineSubOffset = 0;
				nodeSectionDiv.AppendChild (getDivElements(doc, branchNewElement, lineIndex, precOffset, out lineSubOffset));
				if(lineSubOffset > 0){
					i = i + lineSubOffset;
				}
			} else {
				XmlNode whileDiv = doc.CreateNode (XmlNodeType.Element, "entrada", "");

				XmlAttribute att1_w = doc.CreateAttribute ("numero");
				att1_w.InnerText = branchNewElement.GetComponent<lineObject> ().lineNumber.ToString();
				whileDiv.Attributes.Append (att1_w);

				XmlAttribute att2_w = doc.CreateAttribute ("linea");
				att2_w.InnerText = (lineIndex + precOffset + branchNewElement.GetComponent<lineObject> ().branchLevel).ToString ();
				whileDiv.Attributes.Append (att2_w);

				whileDiv.InnerText = branchNewElement.GetComponent<lineObject> ().getXMLValue ();
				nodeBranchDiv.AppendChild (whileDiv);
			}
		}

		nodeDiv.AppendChild (nodeSubDiv);
		nodeDiv.AppendChild (nodeBranchDiv);

		return nodeDiv;
	}

	public XmlNode getXMLLineNode(XmlDocument doc, int lineIndex, int precOffset){
		XmlNode nodeLine = doc.CreateNode (XmlNodeType.Element, "L" + (lineIndex + 1).ToString (), "");

		for (int i = 0; i < 12; i++) {
			GameObject lineElement = getElementPerLineAndLevel (i, 0);

			if(lineElement.GetComponent<lineObject>().typeElement != dragElementLadder._type_elements.none || lineElement.GetComponent<lineObject> ().branchElement != null){
				XmlNode nodeSection1_1 = nodeLine.AppendChild (doc.CreateNode (XmlNodeType.Element, "entrada", ""));
				XmlAttribute att1_1 = doc.CreateAttribute ("numero");
				att1_1.InnerText = lineElement.GetComponent<lineObject> ().lineNumber.ToString();
				nodeSection1_1.Attributes.Append (att1_1);

				XmlAttribute att1_2 = doc.CreateAttribute ("linea");
				att1_2.InnerText = (lineIndex + precOffset).ToString ();
				nodeSection1_1.Attributes.Append (att1_2);

				if (lineElement.GetComponent<lineObject> ().branchElement != null) {
					int offsetBranch = 0;
					nodeSection1_1.AppendChild (getDivElements (doc, lineElement, lineIndex, precOffset, out offsetBranch));
					if(offsetBranch > 0){
						i = i + offsetBranch;
					}
				} else {
					nodeSection1_1.InnerText = lineElement.GetComponent<lineObject> ().getXMLValue ();
				}
			}
		}

		if (lineObject13.GetComponent<lineObject> ().typeElement != dragElementLadder._type_elements.none) {
			XmlNode nodeSection1_1 = nodeLine.AppendChild (doc.CreateNode (XmlNodeType.Element, "salida", ""));

			if (lineObject13.GetComponent<lineObject> ().branchElement != null) {
				GameObject _branchComponent = lineObject13;
				int countBranchs = 0;

				while(_branchComponent != null){
					XmlNode nodeSection1_n = nodeSection1_1.AppendChild (doc.CreateNode (XmlNodeType.Element, "sal", ""));

					if(_branchComponent.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.open_coil){
						XmlAttribute att1_n = doc.CreateAttribute ("name");
						att1_n.InnerText = "1";
						nodeSection1_n.Attributes.Append (att1_n);
					} else if(_branchComponent.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.close_coil){
						XmlAttribute att1_n = doc.CreateAttribute ("name");
						att1_n.InnerText = "2";
						nodeSection1_n.Attributes.Append (att1_n);
					} else if(_branchComponent.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.set_coil){
						XmlAttribute att1_n = doc.CreateAttribute ("name");
						att1_n.InnerText = "3";
						nodeSection1_n.Attributes.Append (att1_n);
					} else if(_branchComponent.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.reset_coil){
						XmlAttribute att1_n = doc.CreateAttribute ("name");
						att1_n.InnerText = "4";
						nodeSection1_n.Attributes.Append (att1_n);
					}

					XmlAttribute att2_n = doc.CreateAttribute ("linea");
					att2_n.InnerText = (lineIndex + precOffset + _branchComponent.GetComponent<lineObject> ().branchLevel).ToString ();
					nodeSection1_n.Attributes.Append (att2_n);

					nodeSection1_n.InnerText = _branchComponent.GetComponent<lineObject> ().getXMLValue ();

					countBranchs++;

					_branchComponent = _branchComponent.GetComponent<lineObject> ().branchElement;

				}

				XmlAttribute att1_1 = doc.CreateAttribute ("cantidad");
				att1_1.InnerText = countBranchs.ToString ();
				nodeSection1_1.Attributes.Append (att1_1);

			} else {
				if(lineObject13.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.open_coil){
					XmlAttribute att1_1 = doc.CreateAttribute ("name");
					att1_1.InnerText = "1";
					nodeSection1_1.Attributes.Append (att1_1);
				} else if(lineObject13.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.close_coil){
					XmlAttribute att1_1 = doc.CreateAttribute ("name");
					att1_1.InnerText = "2";
					nodeSection1_1.Attributes.Append (att1_1);
				} else if(lineObject13.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.set_coil){
					XmlAttribute att1_1 = doc.CreateAttribute ("name");
					att1_1.InnerText = "3";
					nodeSection1_1.Attributes.Append (att1_1);
				} else if(lineObject13.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.reset_coil){
					XmlAttribute att1_1 = doc.CreateAttribute ("name");
					att1_1.InnerText = "4";
					nodeSection1_1.Attributes.Append (att1_1);
				}

				XmlAttribute att1_2 = doc.CreateAttribute ("linea");
				att1_2.InnerText = (lineIndex + precOffset).ToString ();
				nodeSection1_1.Attributes.Append (att1_2);

				nodeSection1_1.InnerText = lineObject13.GetComponent<lineObject> ().getXMLValue ();
			}
				
		}

		return nodeLine;
	}

	public int getMaxBranchOffset(){
		int branchOffset = 0;
		for (int i = 0; i < branchLines.Count; i++) {
			if (branchLines [i].GetComponent<lineObject> ().branchLevel > branchOffset) {
				branchOffset = branchLines [i].GetComponent<lineObject> ().branchLevel;
			}
		}

		return branchOffset;
	}

	public void addLineAtLineNumberAndBranch(int lineNumber, int branchLevel, string lineValue, dragElementLadder._typesIO typeInput, int outputNameAtt = -1){
		//Debug.Log ("lineNumber: " + lineNumber.ToString());
		//Debug.Log ("branchLevel: " + branchLevel.ToString());
		//Debug.Log ("lineValue: " + lineValue);

		GameObject lineToEdit = getElementPerLineAndLevel (lineNumber, branchLevel);

		if (lineToEdit != null) {
			lineToEdit.GetComponent<lineObject> ().setValue (lineValue, typeInput, outputNameAtt);	
		}
	}

	public void updateLineGroupHeight(){
		int maxOffset = getMaxBranchOffset ();
		float newHeight = 140f + (float)maxOffset * 140f;
		gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (gameObject.GetComponent<RectTransform> ().rect.width, newHeight);
		gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (gameObject.GetComponent<RectTransform> ().anchoredPosition.x, initialPosition - 70f * (float)maxOffset);

	}

	public void showMainEditor(){
		editorContainer.SetActive (true);
		addPrevContainer.SetActive (true);
		editorContainer.transform.SetAsLastSibling ();
		addPrevContainer.transform.SetAsLastSibling ();
	}

	public void hideMainEditor(){
		editorContainer.SetActive (false);
		addPrevContainer.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if(_situation.GetComponent<LadderMainClass> ().editorActive == true){
			if (_situation.GetComponent<LadderMainClass> ().lineGroupObjects [0].GetInstanceID () == gameObject.GetInstanceID ()) {
				moveUpLineBt.interactable = false;
			} else {
				moveUpLineBt.interactable = true;
			}

			if (_situation.GetComponent<LadderMainClass> ().lineGroupObjects [_situation.GetComponent<LadderMainClass> ().lineGroupObjects.Count - 1].GetInstanceID () == gameObject.GetInstanceID ()) {
				moveDownLineBt.interactable = false;
			} else {
				moveDownLineBt.interactable = true;
			}	
		}
	}
}
