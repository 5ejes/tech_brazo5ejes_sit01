﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class languageBt : MonoBehaviour {

	public enum typeButton
	{
		Left,
		Top
	}

	public typeButton _type;

	public Button _buttonLang1;
	public Button _buttonLang2;
	public Sprite _imageSpanish;
	public Sprite _imageEnglish;
	public Sprite _imagePortuguese;

	public Button _selector;

	public GameObject _bgLanguages;

	public delegate void BtAction();
	public BtAction bt_action;

	private Vector3 initialPosition;
	private bool menuActive = false;
	private bool isMoving = false;
	// Use this for initialization
	void Start () {

		switch (Manager.Instance.globalLanguageConf) {
		case BaseSimulator.ConfigureLanguageList.Spanish_English:
			_buttonLang1.GetComponent<Image> ().sprite = _imageEnglish;
			_buttonLang2.GetComponent<Image> ().sprite = _imageSpanish;
			_buttonLang1.onClick.AddListener(loadEnglish);
			_buttonLang2.onClick.AddListener(loadSpanish);
			break;
		case BaseSimulator.ConfigureLanguageList.Spanish_Portuguese:
			_buttonLang1.GetComponent<Image> ().sprite = _imageSpanish;
			_buttonLang2.GetComponent<Image> ().sprite = _imagePortuguese;
			_buttonLang1.onClick.AddListener(loadSpanish);
			_buttonLang2.onClick.AddListener(loadPortuguese);
			break;
		case BaseSimulator.ConfigureLanguageList.English_Portuguese:
			_buttonLang1.GetComponent<Image> ().sprite = _imageEnglish;
			_buttonLang2.GetComponent<Image> ().sprite = _imagePortuguese;
			_buttonLang1.onClick.AddListener(loadEnglish);
			_buttonLang2.onClick.AddListener(loadPortuguese);
			break;
		}

		_bgLanguages.SetActive (false);
		initialPosition = gameObject.transform.localPosition;
	}

	public void setInitialPosition(){
		initialPosition = gameObject.transform.localPosition;
	}

	public void showMenu(BtAction _btAction){

		bt_action = _btAction;

		if (isMoving == false) {
			if (menuActive == false) {
				menuActive = true;
				isMoving = true;

				Vector3 _newPosition = new Vector3(0,0,0);
				switch(_type){
				case typeButton.Left:
					_newPosition = new Vector3 (initialPosition.x + 190f, initialPosition.y, initialPosition.z);
					break;
				case typeButton.Top:
					_newPosition = new Vector3 (initialPosition.x, initialPosition.y - 190f, initialPosition.z);
					break;
				}

				iTween.MoveTo (
					gameObject,
					iTween.Hash (
						"position", _newPosition,
						"looktarget", Camera.main,
						"easeType", iTween.EaseType.easeOutExpo,
						"time", 1f,
						"islocal", true
					)
				);

				Invoke ("activateMenu", 0.8f);	
				Invoke ("activateInteraction", 1f);
			} else {
				menuActive = false;
				isMoving = true;
				hideMenu ();
			}
		}
	}

	public void hideMenu(){
		_bgLanguages.SetActive (false);

		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", initialPosition,
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);
		Invoke ("activateInteraction", 1f);
	}

	private void activateMenu(){
		_bgLanguages.SetActive (true);
	}

	private void activateInteraction(){
		isMoving = false;
	}
		
	void loadSpanish(){
		PlayerPrefs.SetString ("language","Spanish");

		Manager.Instance.globalLanguage = BaseSimulator.LanguageList.Spanish;
		bt_action.Invoke ();

		//menu _menu = GameObject.Find (Manager.Instance.globalMenuName).GetComponent<menu>();
		//_menu.Language = BaseSimulator.LanguageList.Spanish;

		//StartCoroutine(_menu.loadLanguageXML (false, false));

		hideMenu ();

	}

	void loadEnglish(){
		PlayerPrefs.SetString ("language","English");

		Manager.Instance.globalLanguage = BaseSimulator.LanguageList.English;
		bt_action.Invoke ();

		//menu _menu = GameObject.Find (Manager.Instance.globalMenuName).GetComponent<menu>();
		//_menu.Language = BaseSimulator.LanguageList.English;

		//StartCoroutine(_menu.loadLanguageXML (false, false));

		hideMenu ();

	}

	void loadPortuguese(){
		PlayerPrefs.SetString ("language","Portuguese");

		Manager.Instance.globalLanguage = BaseSimulator.LanguageList.Portuguese;
		bt_action.Invoke ();

		//menu _menu = GameObject.Find (Manager.Instance.globalMenuName).GetComponent<menu>();
		//_menu.Language = BaseSimulator.LanguageList.Portuguese;

		//StartCoroutine(_menu.loadLanguageXML (false, false));

		hideMenu ();
			
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
