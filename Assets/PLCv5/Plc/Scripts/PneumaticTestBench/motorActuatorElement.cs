﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class motorActuatorElement : MonoBehaviour {
	public string textTag;
	public Image actualImage;
	public Sprite _standImage;
	public Sprite _bench1Image;
	public Sprite _bench2Image;
	public Sprite _bench3Image;
	public Sprite _bench4Image;

	public Vector2[] rowsColumnsUsed;

	public int normalValue = 0;
	public int pressedValue = 0;

	public int actualValue = 0;

	public bool dragEnabled = true;
	public bool inBench = false;

	public GameObject _situation;

	public GameObject _doubleSolenoidPrefab;

	public GameObject _solenoidAttached;
	public GameObject _solenoidAttachedB;

	private GameObject _rightOvers;
	private GameObject _rightColliders;

	private Image right_over_1_1;
	private Image right_over_1_2;
	private Image right_over_1_3;
	private Image right_over_1_4;
	private Image right_over_1_5;
	private Image right_over_1_6;

	private Image right_over_2_1;
	private Image right_over_2_2;
	private Image right_over_2_3;
	private Image right_over_2_4;
	private Image right_over_2_5;
	private Image right_over_2_6;

	private Image right_over_3_1;
	private Image right_over_3_2;
	private Image right_over_3_3;
	private Image right_over_3_4;
	private Image right_over_3_5;
	private Image right_over_3_6;

	private Image right_over_4_1;
	private Image right_over_4_2;
	private Image right_over_4_3;
	private Image right_over_4_4;
	private Image right_over_4_5;
	private Image right_over_4_6;

	private Image right_collider_1_1;
	private Image right_collider_1_2;
	private Image right_collider_1_3;
	private Image right_collider_1_4;
	private Image right_collider_1_5;
	private Image right_collider_1_6;

	private Image right_collider_2_1;
	private Image right_collider_2_2;
	private Image right_collider_2_3;
	private Image right_collider_2_4;
	private Image right_collider_2_5;
	private Image right_collider_2_6;

	private Image right_collider_3_1;
	private Image right_collider_3_2;
	private Image right_collider_3_3;
	private Image right_collider_3_4;
	private Image right_collider_3_5;
	private Image right_collider_3_6;

	private Image right_collider_4_1;
	private Image right_collider_4_2;
	private Image right_collider_4_3;
	private Image right_collider_4_4;
	private Image right_collider_4_5;
	private Image right_collider_4_6;

	private bool objectDragged;
	private Vector3 initialPosition;
	private Vector3 startPosition;
	private int initialIndex;
	private int startIndex;

	private bool goForward = false;
	private bool goBack = false;
	private int actualStatus = 1;
	private bool animationEnabled = false;

	private bool _mouseOver = false;
	// Use this for initialization
	void Start () {
		actualValue = normalValue;

		initialPosition = gameObject.transform.localPosition;
		initialIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

		if (gameObject.GetComponent<EventTrigger> () != null) {
			EventTrigger _prev_trigger_Object = gameObject.GetComponent<EventTrigger> ();
			List<EventTrigger.Entry> entriesToRemove = new List<EventTrigger.Entry>();

			foreach (var entry in _prev_trigger_Object.triggers) {        
				entry.callback.RemoveAllListeners ();
				entriesToRemove.Add (entry);
			}

			foreach(var entry in entriesToRemove)
			{
				_prev_trigger_Object.triggers.Remove(entry);
			}
		} else {
			gameObject.AddComponent<EventTrigger> ();
		}

		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		EventTrigger.Entry entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.BeginDrag;
		entry_0.callback.AddListener((data) => { BeginDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.Drag;
		entry_1.callback.AddListener((data) => { ObjectDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.EndDrag;
		entry_2.callback.AddListener((data) => { ObjectDrop_((PointerEventData)data); });

		EventTrigger.Entry entry_5 = new EventTrigger.Entry();
		entry_5.eventID = EventTriggerType.PointerEnter;
		entry_5.callback.AddListener((data) => { ObjectMouseOver_((PointerEventData)data); });

		EventTrigger.Entry entry_6 = new EventTrigger.Entry();
		entry_6.eventID = EventTriggerType.PointerExit;
		entry_6.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);

		trigger_Object.triggers.Add(entry_5);
		trigger_Object.triggers.Add(entry_6);

		_situation = GameObject.Find ("PneumaticBench");

		_rightOvers = GameObject.Find ("Canvas/PneumaticBench/rightOvers");
		_rightColliders = GameObject.Find ("Canvas/PneumaticBench/rightColliders");

		right_over_1_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_1").GetComponent<Image>();
		right_over_1_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_2").GetComponent<Image>();
		right_over_1_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_3").GetComponent<Image>();
		right_over_1_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_4").GetComponent<Image>();
		right_over_1_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_5").GetComponent<Image>();
		right_over_1_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_6").GetComponent<Image>();

		right_over_2_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_1").GetComponent<Image>();
		right_over_2_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_2").GetComponent<Image>();
		right_over_2_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_3").GetComponent<Image>();
		right_over_2_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_4").GetComponent<Image>();
		right_over_2_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_5").GetComponent<Image>();
		right_over_2_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_6").GetComponent<Image>();

		right_over_3_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_1").GetComponent<Image>();
		right_over_3_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_2").GetComponent<Image>();
		right_over_3_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_3").GetComponent<Image>();
		right_over_3_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_4").GetComponent<Image>();
		right_over_3_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_5").GetComponent<Image>();
		right_over_3_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_6").GetComponent<Image>();

		right_over_4_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_1").GetComponent<Image>();
		right_over_4_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_2").GetComponent<Image>();
		right_over_4_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_3").GetComponent<Image>();
		right_over_4_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_4").GetComponent<Image>();
		right_over_4_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_5").GetComponent<Image>();
		right_over_4_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_6").GetComponent<Image>();

		right_collider_1_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_1").GetComponent<Image>();
		right_collider_1_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_2").GetComponent<Image>();
		right_collider_1_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_3").GetComponent<Image>();
		right_collider_1_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_4").GetComponent<Image>();
		right_collider_1_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_5").GetComponent<Image>();
		right_collider_1_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_6").GetComponent<Image>();

		right_collider_2_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_1").GetComponent<Image>();
		right_collider_2_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_2").GetComponent<Image>();
		right_collider_2_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_3").GetComponent<Image>();
		right_collider_2_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_4").GetComponent<Image>();
		right_collider_2_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_5").GetComponent<Image>();
		right_collider_2_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_6").GetComponent<Image>();

		right_collider_3_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_1").GetComponent<Image>();
		right_collider_3_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_2").GetComponent<Image>();
		right_collider_3_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_3").GetComponent<Image>();
		right_collider_3_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_4").GetComponent<Image>();
		right_collider_3_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_5").GetComponent<Image>();
		right_collider_3_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_6").GetComponent<Image>();

		right_collider_4_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_1").GetComponent<Image>();
		right_collider_4_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_2").GetComponent<Image>();
		right_collider_4_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_3").GetComponent<Image>();
		right_collider_4_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_4").GetComponent<Image>();
		right_collider_4_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_5").GetComponent<Image>();
		right_collider_4_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_6").GetComponent<Image>();

		right_over_1_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_6.CrossFadeAlpha(0, 0.01f, false);

		right_over_2_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_6.CrossFadeAlpha(0, 0.01f, false);

		right_over_3_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_6.CrossFadeAlpha(0, 0.01f, false);

		right_over_4_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_6.CrossFadeAlpha(0, 0.01f, false);

		right_collider_1_1.enabled = false;
		right_collider_1_2.enabled = false;
		right_collider_1_3.enabled = false;
		right_collider_1_4.enabled = false;
		right_collider_1_5.enabled = false;
		right_collider_1_6.enabled = false;

		right_collider_2_1.enabled = false;
		right_collider_2_2.enabled = false;
		right_collider_2_3.enabled = false;
		right_collider_2_4.enabled = false;
		right_collider_2_5.enabled = false;
		right_collider_2_6.enabled = false;

		right_collider_3_1.enabled = false;
		right_collider_3_2.enabled = false;
		right_collider_3_3.enabled = false;
		right_collider_3_4.enabled = false;
		right_collider_3_5.enabled = false;
		right_collider_3_6.enabled = false;

		right_collider_4_1.enabled = false;
		right_collider_4_2.enabled = false;
		right_collider_4_3.enabled = false;
		right_collider_4_4.enabled = false;
		right_collider_4_5.enabled = false;
		right_collider_4_6.enabled = false;
	}

	private void BeginDrag_(PointerEventData data)
	{
		if(dragEnabled == true){
			objectDragged = true;
			startPosition = gameObject.transform.localPosition;
			startIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

			gameObject.GetComponent<RectTransform> ().SetAsLastSibling();

			actualImage.sprite = _bench1Image;
			actualImage.SetNativeSize ();

			_rightColliders.transform.SetAsLastSibling ();
			activateRightColliders ();

			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + textTag + "']").InnerText;
		}
	}

	private void ObjectDrag_(PointerEventData data)
	{
		if (objectDragged == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			gameObject.GetComponent<RectTransform>().position = new Vector3(mousePos.x, mousePos.y, 0);

			if (data.pointerEnter) {
				switch (data.pointerEnter.name) {
				case "over_1_1":
					Vector2[] _vector11 = new Vector2[2];
					_vector11 [0] = new Vector2 (1, 1);
					_vector11 [1] = new Vector2 (2, 1);
					deactivateRightOversWithException (_vector11);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1)) {
						right_over_1_1.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_1_2":
					Vector2[] _vector12 = new Vector2[2];
					_vector12 [0] = new Vector2 (1, 2);
					_vector12 [1] = new Vector2 (2, 2);
					deactivateRightOversWithException (_vector12);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2)) {
						right_over_1_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_1_3":
					Vector2[] _vector13 = new Vector2[2];
					_vector13 [0] = new Vector2 (1, 3);
					_vector13 [1] = new Vector2 (2, 3);
					deactivateRightOversWithException (_vector13);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3)) {
						right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_1_4":
					Vector2[] _vector14 = new Vector2[2];
					_vector14 [0] = new Vector2 (1, 4);
					_vector14 [1] = new Vector2 (2, 4);
					deactivateRightOversWithException (_vector14);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4)) {
						right_over_1_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_1_5":
					Vector2[] _vector15 = new Vector2[2];
					_vector15 [0] = new Vector2 (1, 5);
					_vector15 [1] = new Vector2 (2, 5);
					deactivateRightOversWithException (_vector15);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5)) {
						right_over_1_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_5.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_1_6":
					Vector2[] _vector16 = new Vector2[2];
					_vector16 [0] = new Vector2 (1, 6);
					_vector16 [1] = new Vector2 (2, 6);
					deactivateRightOversWithException (_vector16);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 6)) {
						right_over_1_6.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_6.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_1":
					Vector2[] _vector21 = new Vector2[2];
					_vector21 [0] = new Vector2 (2, 1);
					_vector21 [1] = new Vector2 (3, 1);
					deactivateRightOversWithException (_vector21);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
						right_over_2_1.CrossFadeAlpha (1, 0.2f, false);
						right_over_3_1.CrossFadeAlpha (1, 0.2f, false);
					}
					break;
				case "over_2_2":
					Vector2[] _vector22 = new Vector2[2];
					_vector22 [0] = new Vector2 (2, 2);
					_vector22 [1] = new Vector2 (3, 2);
					deactivateRightOversWithException (_vector22);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2)) {
						right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_3":
					Vector2[] _vector23 = new Vector2[2];
					_vector23 [0] = new Vector2 (2, 3);
					_vector23 [1] = new Vector2 (3, 3);
					deactivateRightOversWithException (_vector23);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3)) {
						right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_4":
					Vector2[] _vector24 = new Vector2[2];
					_vector24 [0] = new Vector2 (2, 4);
					_vector24 [1] = new Vector2 (3, 4);
					deactivateRightOversWithException (_vector24);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4)) {
						right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_5":
					Vector2[] _vector25 = new Vector2[2];
					_vector25 [0] = new Vector2 (2, 5);
					_vector25 [1] = new Vector2 (3, 5);
					deactivateRightOversWithException (_vector25);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5)) {
						right_over_2_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_5.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_6":
					Vector2[] _vector26 = new Vector2[2];
					_vector26 [0] = new Vector2 (1, 6);
					_vector26 [1] = new Vector2 (2, 6);
					deactivateRightOversWithException (_vector26);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6)) {
						right_over_2_6.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_6.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_1":
				case "over_4_1":
					Vector2[] _vector31 = new Vector2[2];
					_vector31 [0] = new Vector2 (3, 1);
					_vector31 [1] = new Vector2 (4, 1);
					deactivateRightOversWithException (_vector31);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1)) {
						right_over_3_1.CrossFadeAlpha (1, 0.2f, false);
						right_over_4_1.CrossFadeAlpha (1, 0.2f, false);
					}
					break;
				case "over_3_2":
				case "over_4_2":
					Vector2[] _vector32 = new Vector2[2];
					_vector32 [0] = new Vector2 (3, 2);
					_vector32 [1] = new Vector2 (4, 2);
					deactivateRightOversWithException (_vector32);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2)) {
						right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_3":
				case "over_4_3":
					Vector2[] _vector33 = new Vector2[2];
					_vector33 [0] = new Vector2 (3, 3);
					_vector33 [1] = new Vector2 (4, 3);
					deactivateRightOversWithException (_vector33);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3)) {
						right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_4":
				case "over_4_4":
					Vector2[] _vector34 = new Vector2[2];
					_vector34 [0] = new Vector2 (3, 4);
					_vector34 [1] = new Vector2 (4, 4);
					deactivateRightOversWithException (_vector34);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4)) {
						right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_5":
				case "over_4_5":
					Vector2[] _vector35 = new Vector2[2];
					_vector35 [0] = new Vector2 (2, 5);
					_vector35 [1] = new Vector2 (3, 5);
					deactivateRightOversWithException (_vector35);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5)) {
						right_over_3_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_5.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_6":
				case "over_4_6":
					Vector2[] _vector36 = new Vector2[2];
					_vector36 [0] = new Vector2 (1, 6);
					_vector36 [1] = new Vector2 (2, 6);
					deactivateRightOversWithException (_vector36);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 6)) {
						right_over_3_6.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_6.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				default:
					deactivateOvers ();
					break;	
				}
			}
		}
	}

	private void ObjectDrop_(PointerEventData data)
	{
		if (objectDragged == true) {
			objectDragged = false;

			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = "";

			deactivateOvers ();
			deactivateRightColliders ();

			bool sensorsActuatorsAvailable = true;

			if (_situation.GetComponent<PnematicTestBenchClass> ().usedOutputs >= (_situation.GetComponent<PnematicTestBenchClass> ().simulatorOutputs - 1)) {
				sensorsActuatorsAvailable = false;
			}

			if (data.pointerEnter != null && sensorsActuatorsAvailable == true) {
				switch (data.pointerEnter.name) {
				case "over_1_1":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 1);
						gameObject.transform.position = right_over_1_1.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (1, 1);
						rowsColumnsUsed [1] = new Vector2 (2, 1);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_2_1.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_2_1.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_1_2":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 2);
						gameObject.transform.position = right_over_1_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (1, 2);
						rowsColumnsUsed [1] = new Vector2 (2, 2);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_2_2.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_2_2.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_1_3":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 3);
						gameObject.transform.position = right_over_1_3.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (1, 3);
						rowsColumnsUsed [1] = new Vector2 (2, 3);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_2_3.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_2_3.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_1_4":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 4);
						gameObject.transform.position = right_over_1_4.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (1, 4);
						rowsColumnsUsed [1] = new Vector2 (2, 4);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_2_4.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_2_4.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_1_5":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 5);
						gameObject.transform.position = right_over_1_5.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (1, 5);
						rowsColumnsUsed [1] = new Vector2 (2, 5);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_2_5.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_2_5.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_1_6":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 6)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 6);
						gameObject.transform.position = right_over_1_6.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (1, 6);
						rowsColumnsUsed [1] = new Vector2 (2, 6);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_2_6.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_2_6.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_1":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 1);
						gameObject.transform.position = right_over_2_1.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (2, 1);
						rowsColumnsUsed [1] = new Vector2 (3, 1);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_1.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_1.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_2":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 2);
						gameObject.transform.position = right_over_2_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (2, 2);
						rowsColumnsUsed [1] = new Vector2 (3, 2);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_2.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_2.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_3":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 3);
						gameObject.transform.position = right_over_2_3.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (2, 3);
						rowsColumnsUsed [1] = new Vector2 (3, 3);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_3.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_3.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_4":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 4);
						gameObject.transform.position = right_over_2_4.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (2, 4);
						rowsColumnsUsed [1] = new Vector2 (3, 4);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_4.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_4.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_5":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 5);
						gameObject.transform.position = right_over_2_5.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (2, 5);
						rowsColumnsUsed [1] = new Vector2 (3, 5);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_5.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_5.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_6":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 6);
						gameObject.transform.position = right_over_2_6.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (2, 6);
						rowsColumnsUsed [1] = new Vector2 (3, 6);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_6.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_6.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_1":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 1);
						gameObject.transform.position = right_over_3_1.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (3, 1);
						rowsColumnsUsed [1] = new Vector2 (4, 1);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_4_1.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_4_1.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_2":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 2);
						gameObject.transform.position = right_over_3_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (3, 2);
						rowsColumnsUsed [1] = new Vector2 (4, 2);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_4_2.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_4_2.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_3":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 3);
						gameObject.transform.position = right_over_3_3.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (3, 3);
						rowsColumnsUsed [1] = new Vector2 (4, 3);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_4_3.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_4_3.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_4":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 4);
						gameObject.transform.position = right_over_3_4.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (3, 4);
						rowsColumnsUsed [1] = new Vector2 (4, 4);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_4_4.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_4_4.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_5":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 5);
						gameObject.transform.position = right_over_3_5.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (3, 5);
						rowsColumnsUsed [1] = new Vector2 (4, 5);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_4_5.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_4_5.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_6":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 6)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 6);
						gameObject.transform.position = right_over_3_6.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (3, 6);
						rowsColumnsUsed [1] = new Vector2 (4, 6);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_4_6.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_4_6.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_1":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 1);
						gameObject.transform.position = right_over_4_1.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (4, 1);
						rowsColumnsUsed [1] = new Vector2 (3, 1);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_1.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_1.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_2":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 2);
						gameObject.transform.position = right_over_4_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (4, 2);
						rowsColumnsUsed [1] = new Vector2 (3, 2);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_2.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_2.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_3":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 3);
						gameObject.transform.position = right_over_4_3.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (4, 3);
						rowsColumnsUsed [1] = new Vector2 (3, 3);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_3.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_3.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_4":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 4);
						gameObject.transform.position = right_over_4_4.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (4, 4);
						rowsColumnsUsed [1] = new Vector2 (3, 4);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_4.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_4.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_5":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 5);
						gameObject.transform.position = right_over_4_5.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (4, 5);
						rowsColumnsUsed [1] = new Vector2 (3, 5);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_5.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_5.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_6":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 6)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 6);
						gameObject.transform.position = right_over_4_6.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, gameObject.transform.localPosition.y + 15, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[2];
						rowsColumnsUsed [0] = new Vector2 (4, 6);
						rowsColumnsUsed [1] = new Vector2 (3, 6);

						_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttached.transform.position = right_over_3_6.transform.position;
						_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

						_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
						_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
						_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
						_solenoidAttachedB.transform.position = right_over_3_6.transform.position;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
						_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
						_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

						int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
						int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
					} else {
						tweenToInitialPosition ();
					}
					break;
				default:
					tweenToInitialPosition ();
					break;
				}
			} else {
				tweenToInitialPosition ();
			}
		}
	}

	private void ObjectMouseOver_(PointerEventData data){
		if(objectDragged == false){
			_mouseOver = true;
			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + textTag + "']").InnerText;	
		}
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(_mouseOver == true){
			_mouseOver = false;
			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = "";
		}
	}

	private void tweenToInitialPosition(){
		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", initialPosition,
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 0.2f,
				"islocal",true
			)
		);

		Invoke ("goToInitialState", 0.1f);
	}

	private void goToInitialState(){
		actualImage.sprite = _standImage;
		actualImage.SetNativeSize ();
		gameObject.GetComponent<RectTransform> ().SetSiblingIndex (initialIndex);
	}

	public void activateRightOver(Vector2[] _rows_columns_elements = null, bool _hideAutomatically = true){
		bool feedbackActivated = true;

		if(_rows_columns_elements == null){
			feedbackActivated = false;
			_rows_columns_elements = rowsColumnsUsed;
		}

		int _row = 0;
		int _column = 0;
		for(int i = 0; i < _rows_columns_elements.Length; i++){
			_row = (int)_rows_columns_elements [i].x;
			_column = (int)_rows_columns_elements [i].y;

			switch(_row){
			case 1:
				switch(_column){
				case 1:
					right_over_1_1.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 2:
					right_over_1_2.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 3:
					right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 4:
					right_over_1_4.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 5:
					right_over_1_5.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 6:
					right_over_1_6.CrossFadeAlpha(1, 0.2f, false);
					break;
				}
				break;
			case 2:
				switch(_column){
				case 1:
					right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 2:
					right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 3:
					right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 4:
					right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 5:
					right_over_2_5.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 6:
					right_over_2_6.CrossFadeAlpha(1, 0.2f, false);
					break;
				}
				break;
			case 3:
				switch(_column){
				case 1:
					right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 2:
					right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 3:
					right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 4:
					right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 5:
					right_over_3_5.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 6:
					right_over_3_6.CrossFadeAlpha(1, 0.2f, false);
					break;
				}
				break;
			case 4:
				switch(_column){
				case 1:
					right_over_4_1.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 2:
					right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 3:
					right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 4:
					right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 5:
					right_over_4_5.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 6:
					right_over_4_6.CrossFadeAlpha(1, 0.2f, false);
					break;
				}
				break;
			}
		}

		if(_hideAutomatically == true){
			Invoke ("deactivateOvers", 0.3f);
			if(feedbackActivated == true){
				Invoke ("PostactivateRightOver", 0.7f);	
			}
		}
	}

	private void PostactivateRightOver(){
		activateRightOver ();
	}

	private void deactivateOvers(){
		right_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_6.CrossFadeAlpha(0, 0.2f, false);

		right_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_6.CrossFadeAlpha(0, 0.2f, false);

		right_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_6.CrossFadeAlpha(0, 0.2f, false);

		right_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_6.CrossFadeAlpha(0, 0.2f, false);
	}

	private void activateRightColliders(){
		right_collider_1_1.enabled = true;
		right_collider_1_2.enabled = true;
		right_collider_1_3.enabled = true;
		right_collider_1_4.enabled = true;
		right_collider_1_5.enabled = true;
		right_collider_1_6.enabled = true;

		right_collider_2_1.enabled = true;
		right_collider_2_2.enabled = true;
		right_collider_2_3.enabled = true;
		right_collider_2_4.enabled = true;
		right_collider_2_5.enabled = true;
		right_collider_2_6.enabled = true;

		right_collider_3_1.enabled = true;
		right_collider_3_2.enabled = true;
		right_collider_3_3.enabled = true;
		right_collider_3_4.enabled = true;
		right_collider_3_5.enabled = true;
		right_collider_3_6.enabled = true;

		right_collider_4_1.enabled = true;
		right_collider_4_2.enabled = true;
		right_collider_4_3.enabled = true;
		right_collider_4_4.enabled = true;
		right_collider_4_5.enabled = true;
		right_collider_4_6.enabled = true;
	}

	private void deactivateRightColliders(){
		right_collider_1_1.enabled = false;
		right_collider_1_2.enabled = false;
		right_collider_1_3.enabled = false;
		right_collider_1_4.enabled = false;
		right_collider_1_5.enabled = false;
		right_collider_1_6.enabled = false;

		right_collider_2_1.enabled = false;
		right_collider_2_2.enabled = false;
		right_collider_2_3.enabled = false;
		right_collider_2_4.enabled = false;
		right_collider_2_5.enabled = false;
		right_collider_2_6.enabled = false;

		right_collider_3_1.enabled = false;
		right_collider_3_2.enabled = false;
		right_collider_3_3.enabled = false;
		right_collider_3_4.enabled = false;
		right_collider_3_5.enabled = false;
		right_collider_3_6.enabled = false;

		right_collider_4_1.enabled = false;
		right_collider_4_2.enabled = false;
		right_collider_4_3.enabled = false;
		right_collider_4_4.enabled = false;
		right_collider_4_5.enabled = false;
		right_collider_4_6.enabled = false;
	}

	private void deactivateRightOversWithException(Vector2[] _positions){
		bool _used_1_1 = false;
		bool _used_1_2 = false;
		bool _used_1_3 = false;
		bool _used_1_4 = false;
		bool _used_1_5 = false;
		bool _used_1_6 = false;
		bool _used_2_1 = false;
		bool _used_2_2 = false;
		bool _used_2_3 = false;
		bool _used_2_4 = false;
		bool _used_2_5 = false;
		bool _used_2_6 = false;
		bool _used_3_1 = false;
		bool _used_3_2 = false;
		bool _used_3_3 = false;
		bool _used_3_4 = false;
		bool _used_3_5 = false;
		bool _used_3_6 = false;
		bool _used_4_1 = false;
		bool _used_4_2 = false;
		bool _used_4_3 = false;
		bool _used_4_4 = false;
		bool _used_4_5 = false;
		bool _used_4_6 = false;

		for(int i = 0; i < _positions.Length; i++){
			if(_positions[i].x == 1 && _positions[i].y == 1){
				_used_1_1 = true;
			} else if(_positions[i].x == 1 && _positions[i].y == 2){
				_used_1_2 = true;
			} else if(_positions[i].x == 1 && _positions[i].y == 3){
				_used_1_3 = true;
			} else if(_positions[i].x == 1 && _positions[i].y == 4){
				_used_1_4 = true;
			} else if(_positions[i].x == 1 && _positions[i].y == 5){
				_used_1_5 = true;
			} else if(_positions[i].x == 1 && _positions[i].y == 6){
				_used_1_6 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 1){
				_used_2_1 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 2){
				_used_2_2 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 3){
				_used_2_3 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 4){
				_used_2_4 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 5){
				_used_2_5 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 6){
				_used_2_6 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 1){
				_used_3_1 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 2){
				_used_3_2 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 3){
				_used_3_3 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 4){
				_used_3_4 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 5){
				_used_3_5 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 6){
				_used_3_6 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 1){
				_used_4_1 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 2){
				_used_4_2 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 3){
				_used_4_3 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 4){
				_used_4_4 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 5){
				_used_4_5 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 6){
				_used_4_6 = true;
			}
		}

		if(_used_1_1 == false){
			right_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_1_2 == false){
			right_over_1_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_1_3 == false){
			right_over_1_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_1_4 == false){
			right_over_1_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_1_5 == false){
			right_over_1_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_1_6 == false){
			right_over_1_6.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_1 == false){
			right_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_2 == false){
			right_over_2_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_3 == false){
			right_over_2_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_4 == false){
			right_over_2_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_5 == false){
			right_over_2_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_6 == false){
			right_over_2_6.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_1 == false){
			right_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_2 == false){
			right_over_3_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_3 == false){
			right_over_3_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_4 == false){
			right_over_3_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_5 == false){
			right_over_3_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_6 == false){
			right_over_3_6.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_1 == false){
			right_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_2 == false){
			right_over_4_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_3 == false){
			right_over_4_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_4 == false){
			right_over_4_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_5 == false){
			right_over_4_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_6 == false){
			right_over_4_6.CrossFadeAlpha(0, 0.2f, false);
		}
	}

	private void addNewElement (){
		GameObject newElement = Instantiate (gameObject, initialPosition, Quaternion.identity) as GameObject;
		newElement.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
		newElement.transform.SetSiblingIndex (initialIndex);
		newElement.GetComponent<motorActuatorElement> ().actualImage.sprite = newElement.GetComponent<motorActuatorElement> ()._standImage;
		newElement.GetComponent<motorActuatorElement> ().actualImage.SetNativeSize ();
	}

	IEnumerator goForwardAction(){
		animationEnabled = true;

		switch (actualStatus) {
		case 1:
			actualStatus = 2;
			actualImage.sprite = _bench2Image;
			break;
		case 2:
			actualStatus = 3;
			actualImage.sprite = _bench3Image;
			break;
		case 3:
			actualStatus = 4;
			actualImage.sprite = _bench4Image;
			break;
		case 4:
			actualStatus = 1;
			actualImage.sprite = _bench1Image;
			break;
		}

		yield return new WaitForSeconds (0.1f);

		if (goForward == true && goBack == false) {
			StartCoroutine (goForwardAction ());
		} else if (goForward == false && goBack == true) {
			StartCoroutine (goBackwardAction ());
		} else {
			animationEnabled = false;
		}

	}

	IEnumerator goBackwardAction(){
		switch (actualStatus) {
		case 1:
			actualStatus = 4;
			actualImage.sprite = _bench4Image;
			break;
		case 2:
			actualStatus = 1;
			actualImage.sprite = _bench1Image;
			break;
		case 3:
			actualStatus = 2;
			actualImage.sprite = _bench2Image;
			break;
		case 4:
			actualStatus = 3;
			actualImage.sprite = _bench3Image;
			break;
		}

		yield return new WaitForSeconds (0.1f);

		if (goForward == true && goBack == false) {
			StartCoroutine (goForwardAction ());
		} else if (goForward == false && goBack == true) {
			StartCoroutine (goBackwardAction ());
		} else {
			animationEnabled = false;
		}
	}

	// Update is called once per frame
	void Update () {
		if(inBench == true){
			if (_solenoidAttached != null && _solenoidAttachedB != null && _solenoidAttached.GetComponent<solenoidClass> ().actualValue == 1 && _solenoidAttachedB.GetComponent<solenoidClass> ().actualValue == 0) {
				//open
				goForward = true;
				goBack = false;
				if (animationEnabled == false) {
					StartCoroutine (goForwardAction ());
				}
			} else if (_solenoidAttached != null && _solenoidAttachedB != null && _solenoidAttached.GetComponent<solenoidClass> ().actualValue == 0 && _solenoidAttachedB.GetComponent<solenoidClass> ().actualValue == 1) {
				//close
				goForward = false;
				goBack = true;
				if (animationEnabled == false) {
					StartCoroutine (goBackwardAction ());
				}
			} else if (_solenoidAttached != null && _solenoidAttachedB == null && _solenoidAttached.GetComponent<solenoidClass> ().actualValue == 1) {
				goForward = true;
				goBack = false;
				if (animationEnabled == false) {
					StartCoroutine (goForwardAction ());
				}
			} else {
				goForward = false;
				goBack = false;
			}
		}
	}
}
