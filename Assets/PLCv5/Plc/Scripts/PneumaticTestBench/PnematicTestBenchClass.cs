﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PnematicTestBenchClass : BaseSituation {

	[Header("SITUATION GAMEOBJECTS")]
	public GameObject _upperBars;
	public GameObject _deleteGrid;
	public GameObject _backgroundPref;
	public GameObject _leftOvers;
	public GameObject _rightOvers;
	public Text _msgArea;
	public Text _btnRedText;

	[Header("SITUATION VARIABLES")]
	public int simulatorInputs = 16;
	public int simulatorOutputs = 16;

	public int usedInputs = 0;
	public int usedOutputs = 0;
	public GameObject[] inputElements;
	public GameObject[] outputsElements;

	public bool[] outputsValues;

	public Image Fundido_;

	[HideInInspector]
	public GameObject _background;
	[HideInInspector]
	public string DescriptionPractice;
	[HideInInspector]
	public string var_scene;

	private CalCient ScriptCalCient;
	private Text TextBoton;

	private bool upperIndicatorUpdated = false;

	void Start () {
		outputsValues = new bool[simulatorOutputs];
	}

	public void checkLanguageComponent(){
		if (Manager.Instance.globalLanguageEnabled == false && Manager.Instance.globalGraphicInterface == baseSystem.graphicInterfaces.pneumatic_bench) {
			GameObject _langBtToHide = GameObject.Find ("lang_bt");
			_langBtToHide.SetActive (false);
		}
	}

	public void setTexts (){
		_upperBars.GetComponent<upperBars> ().setTexts ();
		_btnRedText.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='delete']").InnerText;
	}

	public void loadSituation(){
		_cam1.enabled = true;
		_cam2.enabled = false;
		//TODO: check report name, for situation 1 is name_report_1

		base.StartBaseSituation();

		if (Manager.Instance.recoveryUpper == true) {
			//Manager.Instance.recoveryUpper = false;
		}

		setTexts ();
		//configure situation

		inputElements = new GameObject[simulatorInputs];
		outputsElements = new GameObject[simulatorOutputs];
		usedInputs = 0;
		usedOutputs = 0;

		GameObject infoBt = GameObject.Find("info_bt_pneu");
		Button btn1 = infoBt.GetComponent<Button>();
		btn1.onClick.AddListener(callInfoAlert);

		GameObject _help_obj = GameObject.Find("help_bt_pneu");
		_help_obj.GetComponent<Button>().onClick.AddListener(callHelpAlert);

		GameObject trashBt = GameObject.Find("trash_bt_pneu");
		trashBt.GetComponent<Button> ().onClick.AddListener (callTrashAction);

		GameObject _goto_obj = GameObject.Find("goto_bt_pneu");
		_goto_obj.GetComponent<Button>().onClick.AddListener(callGoToAction);

		GameObject _language_obj = GameObject.Find("language_bt_pneu");
		if(_language_obj != null){
			_language_obj.GetComponent<Button> ().onClick.AddListener (callLanguageAction);	
		}

		//Fundido_ = GameObject.Find("Fundido").GetComponent<Image>();

		//Fundido_.transform.SetAsLastSibling ();
		//Fundido_.CrossFadeAlpha(0, 0.5f, false);
		//Fundido_.raycastTarget = false;

		switch (Manager.Instance.globalController) {
		case baseSystem.Controllers.Grafcet:
			Manager.Instance.currentPDFName = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='name_report_2']").InnerText;
			break;
		case baseSystem.Controllers.Ladder:
			Manager.Instance.currentPDFName = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='name_report_4']").InnerText;
			break;
		}
	}

	public int addInputElement(GameObject _element){
		int _result = 0;

		for(int i = 0; i < simulatorInputs; i++){
			if (inputElements [i]) {

			} else {
				inputElements [i] = _element;
				_result = i;
				break;
			}
		}

		usedInputs++;
		_upperBars.GetComponent<upperBars> ().updateInputsDropdown ();

		return _result;
	}

	public int addOutputElement(GameObject _element){
		int _result = 0;

		for(int i = 0; i < simulatorOutputs; i++){
			if (outputsElements [i]) {

			} else {
				outputsElements [i] = _element;
				_result = i;
				break;
			}
		}

		usedOutputs++;
		_upperBars.GetComponent<upperBars> ().updateOutputsDropdown ();

		return _result;
	}

	public void redButtonAction(){
		_deleteGrid.GetComponent<gridDelete> ().showGrid ();
	}

	public void callBack()
	{
		var_scene = "menu";
		_alert = Instantiate(_alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
		_alert.transform.localPosition = new Vector3(-700, 700, 0);
		_alert.GetComponent<Alert>().showAlert(2, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='simulator_title']").InnerText, Manager.Instance.globalInfo.SelectSingleNode("/data/" + base.situationTag + "/name_practice").InnerText, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='desea_salir']").InnerText, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='accept']").InnerText, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='cancel']").InnerText, acceptback);
	}

	private void acceptback(){

		SceneManager.LoadScene(var_scene);
	}

	private void callInfoAlert()
	{
		_alert = Instantiate(_alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
		_alert.transform.localPosition = new Vector3(-700, 700, 0);
		DescriptionPractice = Manager.Instance.globalInfo.SelectSingleNode("/data/" + base.situationTag + "/description_practice").InnerText;
		_alert.GetComponent<Alert>().showAlert(7, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='situation']").InnerText + "|" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='procedure']").InnerText, Manager.Instance.globalInfo.SelectSingleNode("/data/" + base.situationTag + "/name_practice").InnerText, DescriptionPractice + "|" + Manager.Instance.globalInfo.SelectSingleNode("/data/" + base.situationTag + "/procedure").InnerText);
	}

	private void callGoToAction(){
		Fundido_.transform.SetAsLastSibling ();
		Fundido_.CrossFadeAlpha(1, 0.5f, false);
		Fundido_.raycastTarget = true;

		GameObject.Find ("SystemController").GetComponent<baseSystem>().goToController();
	}

	private void callTrashAction(){
		int _row = -1;
		int _column = -1;

		for (int i = 0; i < inputElements.Length; i++) {
			if (inputElements [i] != null) {

				_row = inputElements [i].GetComponent<dragElementPneumatic> ().rowUsed;
				_column = inputElements [i].GetComponent<dragElementPneumatic> ().columnUsed;

				if (inputElements [i].GetComponent<dragElementPneumatic> ()._typeInput == dragElementPneumatic.typesInputs.Left) {
					_leftOvers.GetComponent<benchAvailability> ().setAreaAvailable (_row, _column);	
				} else {
					_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaAvailable (_row, _column);	
				}

				if(inputElements[i].GetComponent<dragElementPneumatic>()._leftCable){
					Destroy (inputElements[i].GetComponent<dragElementPneumatic>()._leftCable);
				}
				if(inputElements[i].GetComponent<dragElementPneumatic>()._rightCable){
					Destroy (inputElements[i].GetComponent<dragElementPneumatic>()._rightCable);
				}
				if(inputElements[i].GetComponent<dragElementPneumatic>()._topCable){
					Destroy (inputElements[i].GetComponent<dragElementPneumatic>()._topCable);
				}

				Destroy(inputElements[i]);
				inputElements [i] = null;
				usedInputs--;
			}
		}

		for(int i = 0; i < outputsElements.Length; i++){
			if (outputsElements [i] != null && outputsElements [i].GetComponent<dragElementPneumatic> () != null) {
				_row = outputsElements [i].GetComponent<dragElementPneumatic> ().rowUsed;
				_column = outputsElements [i].GetComponent<dragElementPneumatic> ().columnUsed;

				if (outputsElements [i].GetComponent<dragElementPneumatic> ()._leftCable) {
					Destroy (outputsElements [i].GetComponent<dragElementPneumatic> ()._leftCable);
				}
				if (outputsElements [i].GetComponent<dragElementPneumatic> ()._rightCable) {
					Destroy (outputsElements [i].GetComponent<dragElementPneumatic> ()._rightCable);
				}
				if (outputsElements [i].GetComponent<dragElementPneumatic> ()._topCable) {
					Destroy (outputsElements [i].GetComponent<dragElementPneumatic> ()._topCable);
				}

				Destroy (outputsElements [i]);
				outputsElements [i] = null;
				usedOutputs--;

				_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaAvailable (_row, _column);

			} else if (outputsElements [i] != null && outputsElements [i].GetComponent<solenoidClass> () != null) {
				if (outputsElements [i].GetComponent<solenoidClass> ()._parent != null && outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> () != null) {
					//activate availability in elements
					for(int j = 0; j < outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ().rowsColumnsUsed.Length; j++){
						int _rowN = (int)outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ().rowsColumnsUsed [j].x;
						int _columnN = (int)outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement> ().rowsColumnsUsed [j].y;
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaAvailable (_rowN, _columnN);
					}

					//delete parent element
					Destroy(outputsElements [i].GetComponent<solenoidClass> ()._parent);

					//delete element
					Destroy (outputsElements [i]);
					outputsElements [i] = null;
					usedOutputs--;
				} else if (outputsElements [i].GetComponent<solenoidClass> ()._parent != null && outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> () != null) {
					for(int j = 0; j < outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> ().rowsColumnsUsed.Length; j++){
						int _rowN = (int)outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> ().rowsColumnsUsed [j].x;
						int _columnN = (int)outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<motorActuatorElement> ().rowsColumnsUsed [j].y;
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaAvailable (_rowN, _columnN);
					}

					//delete parent element
					Destroy(outputsElements [i].GetComponent<solenoidClass> ()._parent);

					//delete element
					Destroy (outputsElements [i]);
					outputsElements [i] = null;
					usedOutputs--;
				}
			}
		}
		_upperBars.GetComponent<upperBars> ().updateOutputsDropdown ();
		_upperBars.GetComponent<upperBars> ().updateInputsDropdown ();
		_upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
	}

	private void callHelpAlert()
	{
		_alert = Instantiate(_alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
		_alert.transform.localPosition = new Vector3(-700, 700, 0);
		_alert.GetComponent<Alert>().showAlert(4, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='help']").InnerText, "", Manager.Instance.globalInfo.SelectSingleNode("/data/" + base.situationTag + "/tips").InnerText);
	}

	private void callLanguageAction()
	{
		GameObject _langBt = GameObject.Find ("lang_bt");
		_langBt.GetComponent<languageBt> ().showMenu (setNewLanguageFromMenu);
	}

	public void setNewLanguageFromMenu(){
		GameObject.Find ("SystemController").GetComponent<baseSystem>().Language = Manager.Instance.globalLanguage;
		StartCoroutine (GameObject.Find ("SystemController").GetComponent<baseSystem> ().loadLanguageXML (false, false));
		setTextsAfterChangeLanguage ();
		GameObject.Find ("SystemController").GetComponent<baseSystem> ()._grafcet.GetComponent<GrafcetMainClass> ().setTexts ();
	}

	public void setTextsAfterChangeLanguage(){
		_upperBars.GetComponent<upperBars> ().setTexts ();
		_btnRedText.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='delete']").InnerText;
		_upperBars.GetComponent<upperBars> ().updateInputsDropdown ();
		_upperBars.GetComponent<upperBars> ().updateOutputsDropdown ();
	}

	public void activateBackground(){
		_background = Instantiate(_backgroundPref, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_background.name = "SituationBg";
		_background.transform.SetParent (GameObject.Find("Canvas/PneumaticBench").transform, false);
		_background.transform.localPosition = new Vector3(0,0,0);
		_background.transform.SetAsLastSibling ();
	}

	public void deactivateBackground(){
		Image _bg = _background.GetComponent<Image>();
		_bg.CrossFadeAlpha (0f, 1f, false);
		Destroy(_background, 1.2f);
	}

	void Update(){


	}
}
