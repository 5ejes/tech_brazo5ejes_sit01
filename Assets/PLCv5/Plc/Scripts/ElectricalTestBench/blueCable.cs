﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class blueCable : MonoBehaviour {

	public Image _initialPoint;
	public Image _line1;
	public Image _line2;
	public Image _line3;
	public Image _line4;
	public Image _line5;
	public Image _endPoint;

	public GameObject _parentElement;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setPosition(int _row, int _column, int _input){

		Quaternion _angleRotation = new Quaternion ();

		float _lineWidth = 0;
		float _offsetLevel = 0;
		float _xInitial = 0;
		float _width = 0;
		float _height = 0;
		float _angleLine2 = 0;
		float _firstLineWidth = 0;
		float _topLine2 = 0;
		float _xOffset = 0;
		float _widthOffset = 0;
		float _heightOffset = 0;

		_lineWidth = 2f;

		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			if(_parentElement){
				switch(_parentElement.GetComponent<dragElement>()._typeComponent){
				case dragElement.typesComponents.limit_switch_sensor:
					_heightOffset = 2.08f;
					break;
				case dragElement.typesComponents.green_button_sensor:
				case dragElement.typesComponents.red_button_sensor:
				case dragElement.typesComponents.switch_sensor:
					_xOffset = 2;
					_widthOffset = -2;
					break;
				}
			}

			switch(_row){
			case 1:
				switch(_column){
				case 1:
					_offsetLevel = 3f;
					_xInitial = 1545.71f + _xOffset;
					_width = 493.7f + _widthOffset;
					_height = 51.22f + _heightOffset;
					_firstLineWidth = 33f;
					_angleLine2 = 12f;
					_topLine2 = 1.1f;
					break;
				case 2:
					_offsetLevel = 7f;
					_xInitial = 1410f + _xOffset;
					_width = 358.3f + _widthOffset;
					_height = 55.15f + _heightOffset;
					_firstLineWidth = 33f;
					_angleLine2 = 8.7f;
					_topLine2 = 0.5f;
					break;
				case 3:
					_offsetLevel = 11f;
					_xInitial = 1273.48f + _xOffset;
					_width = 221f + _widthOffset;
					_height = 60.88f + _heightOffset;
					_firstLineWidth = 36f;
					_angleLine2 = 6.6f;
					_topLine2 = 0.3f;
					break;
				case 4:
					_offsetLevel = 15f;
					_xInitial = 1137.5f + _xOffset;
					_width = 85.2f + _widthOffset;
					_height = 65f + _heightOffset;
					_firstLineWidth = 36f;
					_angleLine2 = 4f;
					_topLine2 = 0.12f;
					break;
				}
				break;
			case 2:
				switch (_column) {
				case 1:
					_offsetLevel = 2f;
					_xInitial = 1563f + _xOffset;
					_width = 511.43f + _widthOffset;
					_height = 131f + _heightOffset;
					_firstLineWidth = 32f;
					_angleLine2 = 12f;
					_topLine2 = 1.1f;
					break;
				case 2:
					_offsetLevel = 6f;
					_xInitial = 1424f + _xOffset;
					_width = 372.557f + _widthOffset;
					_height = 135f + _heightOffset;
					_firstLineWidth = 32f;
					_angleLine2 = 8.7f;
					_topLine2 = 1.5f;
					break;
				case 3:
					_offsetLevel = 10f;
					_xInitial = 1284f + _xOffset;
					_width = 232.91f + _widthOffset;
					_height = 139.28f + _heightOffset;
					_firstLineWidth = 34f;
					_angleLine2 = 6.6f;
					_topLine2 = 0.87f;
					break;
				case 4:
					_offsetLevel = 14f;
					_xInitial = 1144f + _xOffset;
					_width = 92.204f + _widthOffset;
					_height = 143.077f + _heightOffset;
					_firstLineWidth = 34f;
					_angleLine2 = 4f;
					_topLine2 = 0.32f;
					break;
				}
				break;
			case 3:
				switch (_column) {
				case 1:
					_offsetLevel = 1f;
					_xInitial = 1582f + _xOffset;
					_width = 530.9f + _widthOffset;
					_height = 214f + _heightOffset;
					_firstLineWidth = 31f;
					_angleLine2 = 12f;
					_topLine2 = 3.8f;
					break;
				case 2:
					_offsetLevel = 5f;
					_xInitial = 1438f + _xOffset;
					_width = 385.79f + _widthOffset;
					_height = 220.09f + _heightOffset;
					_firstLineWidth = 31f;
					_angleLine2 = 8.7f;
					_topLine2 = 2.5f;
					break;
				case 3:
					_offsetLevel = 9f;
					_xInitial = 1294f + _xOffset;
					_width = 242.16f + _widthOffset;
					_height = 222.36f + _heightOffset;
					_firstLineWidth = 32f;
					_angleLine2 = 6.6f;
					_topLine2 = 1.4f;
					break;
				case 4:
					_offsetLevel = 13f;
					_xInitial = 1149f + _xOffset;
					_width = 97.61f + _widthOffset;
					_height = 226.39f + _heightOffset;
					_firstLineWidth = 32f;
					_angleLine2 = 4f;
					_topLine2 = 0.52f;
					break;
				}
				break;
			case 4:
				switch (_column) {
				case 1:
					_offsetLevel = 0f;
					_xInitial = 1603f + _xOffset;
					_width = 551.25f + _widthOffset;
					_height = 305.12f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 12f;
					_topLine2 = 6.8f;
					break;
				case 2:
					_offsetLevel = 4f;
					_xInitial = 1452f + _xOffset;
					_width = 400.4f + _widthOffset;
					_height = 310.92f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 8.7f;
					_topLine2 = 3.5f;
					break;
				case 3:
					_offsetLevel = 8f;
					_xInitial = 1304f + _xOffset;
					_width = 252.245f + _widthOffset;
					_height = 314.035f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 6.6f;
					_topLine2 = 2f;
					break;
				case 4:
					_offsetLevel = 12f;
					_xInitial = 1154f + _xOffset;
					_width = 102.983f + _widthOffset;
					_height = 317.752f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 4f;
					_topLine2 = 0.75f;
					break;
				}
				break;
			}

			gameObject.transform.Rotate(new Vector3(0,180,0));
			gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xInitial, -10f + _offsetLevel);
			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, _height);
			_angleRotation.eulerAngles = new Vector3 (0, 0, _angleLine2);
			_initialPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3f, 0f);
			_line1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 0.6f);
			_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_firstLineWidth, _lineWidth);
			_line2.transform.rotation = _angleRotation;
			_line2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width, 0f);
			_line2.GetComponent<RectTransform> ().offsetMax = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMax.x, _topLine2);
			_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth, 0.0963f * (float)_input * (float)_input + 15.672f * (float)_input + 30.99f + _offsetLevel - _lineWidth / 2f);
			_line4.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (((-4.8f / 15f) * (float)_input - 4.5f) - (12f + (float)_input) - (Mathf.Tan (_line4.transform.rotation.eulerAngles.z * Mathf.PI / 180f) * _line4.GetComponent<RectTransform> ().rect.height), 0f);
			_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width + gameObject.GetComponent<RectTransform> ().rect.height * Mathf.Abs (Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth, -_lineWidth / 2f);
			_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (gameObject.GetComponent<RectTransform> ().rect.width - _line3.GetComponent<RectTransform> ().anchoredPosition.x + _line4.GetComponent<RectTransform> ().anchoredPosition.x, _lineWidth);
			_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 ((-4.8f / 15f) * (float)_input - 4.5f, -0.0963f * (float)_input * (float)_input - 15.672f * (float)_input - 28.99f - _offsetLevel);
			_line5.GetComponent<RectTransform> ().sizeDelta = new Vector2 (12f + (float)_input, _lineWidth);
			_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 ((-4.8f / 15f) * (float)_input, -0.0963f * (float)_input * (float)_input - 15.672f * (float)_input - 28.99f - _offsetLevel);
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			if (_parentElement) {
				switch (_parentElement.GetComponent<dragElementPneumatic> ()._typeComponent) {
				case dragElementPneumatic.typesComponents.limit_switch_sensor:
					//_heightOffset = 2.08f;
					break;
				case dragElementPneumatic.typesComponents.green_button_sensor:
				case dragElementPneumatic.typesComponents.red_button_sensor:
				case dragElementPneumatic.typesComponents.switch_sensor:
					//_xOffset = 2;
					//_widthOffset = -2;
					break;
				}
			}

			switch (_row) {
			case 1:
				switch (_column) {
				case 1:
					_offsetLevel = 3f;
					_xInitial = 828.5f + _xOffset;
					_width = 92f + _widthOffset;
					_height = 55.7f + _heightOffset;
					_firstLineWidth = 31f;
					_angleLine2 = -3f;
					_topLine2 = 0f;
					break;
				case 2:
					_offsetLevel = 7f;
					_xInitial = 970 + _xOffset;
					_width = 234f + _widthOffset;
					_height = 59.5f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = -2f;
					_topLine2 = 0f;
					break;
				case 3:
					_offsetLevel = 11f;
					_xInitial = 1111.7f + _xOffset;
					_width = 375f + _widthOffset;
					_height = 63f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 1f;
					_topLine2 = 0f;
					break;
				case 4:
					_offsetLevel = 15f;
					_xInitial = 1253f + _xOffset;
					_width = 516.8f + _widthOffset;
					_height = 67.4f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 4f;
					_topLine2 = 0f;
					break;
				case 5:
					_offsetLevel = 15f;
					_xInitial = 1392.5f + _xOffset;
					_width = 656.5f + _widthOffset;
					_height = 67.4f + _heightOffset;
					_firstLineWidth = 36f;
					_angleLine2 = 8f;
					_topLine2 = 0f;
					break;
				case 6:
					_offsetLevel = 15f;
					_xInitial = 1533f + _xOffset;
					_width = 796.6f + _widthOffset;
					_height = 67.4f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 9f;
					_topLine2 = 0f;
					break;
				}
				break;
			case 2:
				switch (_column) {
				case 1:
					_offsetLevel = 2f;
					_xInitial = 824.6f + _xOffset;
					_width = 88.3f + _widthOffset;
					_height = 136.7f + _heightOffset;
					_firstLineWidth = 29f;
					_angleLine2 = -3f;
					_topLine2 = 0f;
					break;
				case 2:
					_offsetLevel = 6f;
					_xInitial = 970.7f + _xOffset;
					_width = 234.5f + _widthOffset;
					_height = 141.1f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = -2f;
					_topLine2 = 0f;
					break;
				case 3:
					_offsetLevel = 10f;
					_xInitial = 1115.5f + _xOffset;
					_width = 379f + _widthOffset;
					_height = 145f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 1f;
					_topLine2 = 0f;
					break;
				case 4:
					_offsetLevel = 14f;
					_xInitial = 1261f + _xOffset;
					_width = 524.7f + _widthOffset;
					_height = 149f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 4f;
					_topLine2 = 0f;
					break;
				case 5:
					_offsetLevel = 14f;
					_xInitial = 1404f + _xOffset;
					_width = 667.7f + _widthOffset;
					_height = 149f + _heightOffset;
					_firstLineWidth = 34f;
					_angleLine2 = 8f;
					_topLine2 = 0f;
					break;
				case 6:
					_offsetLevel = 14f;
					_xInitial = 1549f + _xOffset;
					_width = 813f + _widthOffset;
					_height = 149f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 9f;
					_topLine2 = 0f;
					break;
				}
				break;
			case 3:
				switch (_column) {
				case 1:
					_offsetLevel = 1f;
					_xInitial = 821f + _xOffset;
					_width = 85f + _widthOffset;
					_height = 222.7f + _heightOffset;
					_firstLineWidth = 27f;
					_angleLine2 = -3f;
					_topLine2 = 0f;
					break;
				case 2:
					_offsetLevel = 5f;
					_xInitial = 970.8f + _xOffset;
					_width = 234.6f + _widthOffset;
					_height = 226.8f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = -2f;
					_topLine2 = 0f;
					break;
				case 3:
					_offsetLevel = 9f;
					_xInitial = 1120.4f + _xOffset;
					_width = 384.1f + _widthOffset;
					_height = 230.5f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 1f;
					_topLine2 = 0f;
					break;
				case 4:
					_offsetLevel = 13f;
					_xInitial = 1269.7f + _xOffset;
					_width = 533.7f + _widthOffset;
					_height = 234.7f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 4f;
					_topLine2 = 0f;
					break;
				case 5:
					_offsetLevel = 13f;
					_xInitial = 1417.2f + _xOffset;
					_width = 681.25f + _widthOffset;
					_height = 234.7f + _heightOffset;
					_firstLineWidth = 32f;
					_angleLine2 = 8f;
					_topLine2 = 0f;
					break;
				case 6:
					_offsetLevel = 13f;
					_xInitial = 1566f + _xOffset;
					_width = 830.15f + _widthOffset;
					_height = 234.7f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 9f;
					_topLine2 = 0f;
					break;
				}
				break;
			case 4:
				switch (_column) {
				case 1:
					_offsetLevel = 0f;
					_xInitial = 817.1f + _xOffset;
					_width = 80.4f + _widthOffset;
					_height = 313.7f + _heightOffset;
					_firstLineWidth = 25f;
					_angleLine2 = -3f;
					_topLine2 = 0f;
					break;
				case 2:
					_offsetLevel = 4f;
					_xInitial = 971.5f + _xOffset;
					_width = 235.1f + _widthOffset;
					_height = 317.6f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = -2f;
					_topLine2 = 0f;
					break;
				case 3:
					_offsetLevel = 8f;
					_xInitial = 1125.2f + _xOffset;
					_width = 389.1f + _widthOffset;
					_height = 321.6f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 1f;
					_topLine2 = 0f;
					break;
				case 4:
					_offsetLevel = 12f;
					_xInitial = 1279f + _xOffset;
					_width = 542.4f + _widthOffset;
					_height = 325.8f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 4f;
					_topLine2 = 0f;
					break;
				case 5:
					_offsetLevel = 12f;
					_xInitial = 1431.5f + _xOffset;
					_width = 695f + _widthOffset;
					_height = 325.8f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = 8f;
					_topLine2 = 0f;
					break;
				case 6:
					_offsetLevel = 12f;
					_xInitial = 1585.5f + _xOffset;
					_width = 849.1f + _widthOffset;
					_height = 325.8f + _heightOffset;
					_firstLineWidth = 31f;
					_angleLine2 = 9f;
					_topLine2 = 0f;
					break;
				}
				break;
			}

			gameObject.transform.Rotate (new Vector3 (0, 180, 0));
			gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xInitial, -82f + _offsetLevel);
			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, _height);
			_angleRotation.eulerAngles = new Vector3 (0, 0, _angleLine2);
			_initialPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3f, 0f);
			_line1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 0.6f);
			_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_firstLineWidth, _lineWidth);
			_line2.transform.rotation = _angleRotation;
			_line2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width, 0f);
			_line2.GetComponent<RectTransform> ().offsetMax = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMax.x, _topLine2);
			_line4.GetComponent<RectTransform> ().pivot = new Vector2 (0, 1);
			_angleRotation.eulerAngles = new Vector3 (0, 0, -3.2f);

			float endXLine = 1.2032f * (float)_input - 0.0421f;
			float endYLine = -0.0871f * (float)_input * (float)_input - 15.171f * (float)_input - 29.028f;

			_line4.GetComponent<RectTransform> ().rotation = _angleRotation;
			_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth, Mathf.Abs (endYLine) + _offsetLevel + _lineWidth - _lineWidth / 2f);
			_line4.GetComponent<RectTransform> ().anchoredPosition = new Vector2 ((endXLine - 4.5f) - (12f + (float)_input) + (Mathf.Tan (_line4.transform.rotation.eulerAngles.z * Mathf.PI / 180f) * _line4.GetComponent<RectTransform> ().rect.height), 0f);
			_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width + gameObject.GetComponent<RectTransform> ().rect.height * (Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth, -_lineWidth / 2f);
			_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (gameObject.GetComponent<RectTransform> ().rect.width - _line3.GetComponent<RectTransform> ().anchoredPosition.x + _line4.GetComponent<RectTransform> ().anchoredPosition.x, _lineWidth);
			//_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (endXLine - 4.5f, endYLine);
			_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (endXLine - 4.5f, endYLine - _offsetLevel);
			_line5.GetComponent<RectTransform> ().sizeDelta = new Vector2 (12f + (float)_input, _lineWidth);
			//_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (endXLine, endYLine);
			_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (endXLine, endYLine - _offsetLevel);
			break;
		}
	}
}
