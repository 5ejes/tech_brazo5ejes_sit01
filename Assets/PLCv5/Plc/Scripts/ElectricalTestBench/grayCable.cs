﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class grayCable : MonoBehaviour {

	public Image _initialPoint;
	public Image _line1;
	public Image _line2;
	public Image _line3;
	public Image _line4;
	public Image _line5;
	public Image _endPoint;

	public GameObject _parentElement;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void setPosition(int _row, int _column, int _input){

		Quaternion _angleRotation = new Quaternion ();

		float _lineWidth = 0;
		float _offsetLevel = 0;
		float _xInitial = 0;
		float _yInitial = 30f;
		float _width = 0;
		float _height = 0;
		float _angleLine2 = 0;
		float _firstLineWidth = 0;
		float _topLine2 = 0;
		float _bottomLine2 = 0;
		float _xOffset = 0;
		float _widthOffset = 0;
		float _heightOffset = 0;

		_lineWidth = 2f;

		if(_parentElement){
			switch(_parentElement.GetComponent<dragElement>()._typeComponent){
			case dragElement.typesComponents.limit_switch_sensor:
				//_heightOffset = 2.08f;
				break;
			case dragElement.typesComponents.green_button_sensor:
			case dragElement.typesComponents.red_button_sensor:
			case dragElement.typesComponents.switch_sensor:
				//_xOffset = 2;
				//_widthOffset = -2;
				break;
			}
		}

		switch(_row){
		case 1:
			switch(_column){
			case 1:
				_offsetLevel = 3f;
				_xInitial = 399f + _xOffset;
				_width = 333.72f + _widthOffset;
				_height = 66.17f + _heightOffset;
				_firstLineWidth = 55f;
				_angleLine2 = -12f;
				_topLine2 = 1.1f;
				break;
			case 2:
				_offsetLevel = 7f;
				_xInitial = 535.4f + _xOffset;
				_width = 247.7f + _widthOffset;
				_height = 66f + _heightOffset;
				_firstLineWidth = 53f;
				_angleLine2 = -8.7f;
				_topLine2 = 0.5f;
				break;
			case 3:
				_offsetLevel = 11f;
				_xInitial = 671f + _xOffset;
				_width = 162.16f + _widthOffset;
				_height = 67.15f + _heightOffset;
				_firstLineWidth = 56f;
				_angleLine2 = -6.6f;
				_topLine2 = 0.3f;
				break;
			case 4:
				_offsetLevel = 15f;
				_xInitial = 808f + _xOffset;
				_width = 75f + _widthOffset;
				_height = 67f + _heightOffset;
				_firstLineWidth = 52f;
				_angleLine2 = -4f;
				_topLine2 = 0.12f;
				break;
			}
			break;
		case 2:
			switch (_column) {
			case 1:
				_offsetLevel = 2f;
				_xInitial = 383.41f + _xOffset;
				_width = 349.44f + _widthOffset;
				_height = 146.56f + _heightOffset;
				_firstLineWidth = 54f;
				_angleLine2 = -12f;
				_topLine2 = 1.1f;
				break;
			case 2:
				_offsetLevel = 6f;
				_xInitial = 522.88f + _xOffset;
				_width = 260.31f + _widthOffset;
				_height = 146.34f + _heightOffset;
				_firstLineWidth = 52f;
				_angleLine2 = -8.7f;
				_topLine2 = 1.5f;
				break;
			case 3:
				_offsetLevel = 10f;
				_xInitial = 662.56f + _xOffset;
				_width = 170.66f + _widthOffset;
				_height = 146.8f + _heightOffset;
				_firstLineWidth = 54f;
				_angleLine2 = -6.6f;
				_topLine2 = 0.87f;
				break;
			case 4:
				_offsetLevel = 14f;
				_xInitial = 803.11f + _xOffset;
				_width = 79.919f + _widthOffset;
				_height = 146.86f + _heightOffset;
				_firstLineWidth = 50f;
				_angleLine2 = -4f;
				_topLine2 = 0.32f;
				break;
			}
			break;
		case 3:
			switch (_column) {
			case 1:
				_offsetLevel = 1f;
				_xInitial = 365.22f + _xOffset;
				_width = 367.63f + _widthOffset;
				_height = 230.7f + _heightOffset;
				_firstLineWidth = 53f;
				_angleLine2 = -12f;
				_topLine2 = 3.8f;
				break;
			case 2:
				_offsetLevel = 5f;
				_xInitial = 507.41f + _xOffset;
				_width = 275.79f + _widthOffset;
				_height = 233.3f + _heightOffset;
				_firstLineWidth = 51f;
				_angleLine2 = -8.7f;
				_topLine2 = 2.5f;
				break;
			case 3:
				_offsetLevel = 9f;
				_xInitial = 653.21f + _xOffset;
				_width = 181.49f + _widthOffset;
				_height = 231.76f + _heightOffset;
				_firstLineWidth = 52f;
				_angleLine2 = -6.6f;
				_topLine2 = 1.4f;
				break;
			case 4:
				_offsetLevel = 13f;
				_xInitial = 798.5f + _xOffset;
				_width = 84.531f + _widthOffset;
				_height = 231.21f + _heightOffset;
				_firstLineWidth = 48f;
				_angleLine2 = -4f;
				_topLine2 = 0.52f;
				break;
			}
			break;
		case 4:
			switch (_column) {
			case 1:
				_offsetLevel = 0f;
				_xInitial = 344.75f + _xOffset;
				_width = 388.11f + _widthOffset;
				_height = 323.4f + _heightOffset;
				_firstLineWidth = 52f;
				_angleLine2 = -12f;
				_topLine2 = 6.8f;
				break;
			case 2:
				_offsetLevel = 4f;
				_xInitial = 495.13f + _xOffset;
				_width = 288.08f + _widthOffset;
				_height = 324.8f + _heightOffset;
				_firstLineWidth = 50f;
				_angleLine2 = -8.7f;
				_topLine2 = 3.5f;
				break;
			case 3:
				_offsetLevel = 8f;
				_xInitial = 643.52f + _xOffset;
				_width = 189.71f + _widthOffset;
				_height = 324.1f + _heightOffset;
				_firstLineWidth = 50f;
				_angleLine2 = -6.6f;
				_topLine2 = 2f;
				break;
			case 4:
				_offsetLevel = 12f;
				_xInitial = 793f + _xOffset;
				_width = 90f + _widthOffset;
				_height = 324f + _heightOffset;
				_firstLineWidth = 46f;
				_angleLine2 = -4f;
				_topLine2 = 0.75f;
				break;
			}
			break;
		}
			
		gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xInitial, _yInitial + _offsetLevel);
		gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, _height + _offsetLevel);
		_angleRotation.eulerAngles = new Vector3 (0, 0, _angleLine2);
		_initialPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3f, 0f);
		_line1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 0.6f);
		_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_firstLineWidth, _lineWidth);
		_line2.transform.rotation = _angleRotation;
		_line2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width - _lineWidth, 0f);
		_line2.GetComponent<RectTransform> ().offsetMax = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMax.x, _topLine2);
		_line2.GetComponent<RectTransform> ().offsetMin = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMin.x, _bottomLine2);

		float _endX = 0;
		float _endY = 0;
		float _heightOffset4 = 0;
		float _lineWidth4 = 0;

		switch (_row) {
		case 1:
			_endX = -1.5f;
			_endY = 24f;
			_heightOffset4 = 1;
			_lineWidth4 = 24f;
			break;
		case 2:
			_endX = -25.8f;
			_endY = 128f;
			_heightOffset4 = 3.5f;
			_lineWidth4 = 24f;
			break;
		case 3:
			_endX = -48.5f;
			_endY = 225f;
			_heightOffset4 = 7f;
			_lineWidth4 = 24f;
			break;
		case 4:
			_endX = -70f;
			_endY = 316f;
			_heightOffset4 = 8f;
			_lineWidth4 = 24f;
			break;
		}

		//_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX, _endY + _offsetLevel);

		//_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX - 6.5f, _endY + _offsetLevel);
		//_line5.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth4 + (float)_input, _lineWidth);

		_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth, 43f - _offsetLevel);
		_line4.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 41.5f - _offsetLevel);

		_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width + gameObject.GetComponent<RectTransform> ().rect.height * Mathf.Abs (Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth, -_lineWidth / 2f);
		_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (gameObject.GetComponent<RectTransform> ().rect.width - _line3.GetComponent<RectTransform> ().anchoredPosition.x - _line4.GetComponent<RectTransform> ().rect.height * Mathf.Abs (Mathf.Tan (_line4.transform.rotation.eulerAngles.z * Mathf.PI / 180f)), _lineWidth);


	}
}
