﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AulaSelector : MonoBehaviour {

	public GameObject baseSituation;

	public Text titleText;
	public Text messageText;
	public Dropdown ddlAula;
	public Button acceptBt;

	public GameObject _backgroundPref;

	private GameObject _background;
	// Use this for initialization
	void Start () {
		acceptBt.onClick.AddListener (setAulaPractice);
	}

	public void showSelector(){

		titleText.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='aula_selector_title']").InnerText;
		messageText.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='aula_selector_message']").InnerText;
		acceptBt.gameObject.GetComponentInChildren<Text>().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText;

		//option list
		int countOptions = 0;
		List<Dropdown.OptionData> dataDDL = new List<Dropdown.OptionData> ();

		switch(Manager.Instance.globalGraphicInterface){
		case baseSystem.graphicInterfaces.electrical_bench:
			switch(Manager.Instance.globalController){
			case baseSystem.Controllers.Grafcet:
				countOptions = baseSituation.GetComponent<baseSystem> ().grafcet_electric_practices;
				for (int i = 0; i < countOptions; i++) {
					Dropdown.OptionData aulaOption = new Dropdown.OptionData ();
					aulaOption.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='aula_grafcet_electric_" + i.ToString () + "']").InnerText;
					dataDDL.Add (aulaOption);
				}
				ddlAula.options = dataDDL;
				break;
			case baseSystem.Controllers.Ladder:
				countOptions = baseSituation.GetComponent<baseSystem> ().ladder_electric_practices;
				for (int i = 0; i < countOptions; i++) {
					Dropdown.OptionData aulaOption = new Dropdown.OptionData ();
					aulaOption.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='aula_ladder_electric_" + i.ToString () + "']").InnerText;
					dataDDL.Add (aulaOption);
				}
				ddlAula.options = dataDDL;
				break;
			}
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			switch(Manager.Instance.globalController){
			case baseSystem.Controllers.Grafcet:
				countOptions = baseSituation.GetComponent<baseSystem> ().grafcet_pneumatic_practices;
				for (int i = 0; i < countOptions; i++) {
					Dropdown.OptionData aulaOption = new Dropdown.OptionData ();
					aulaOption.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='aula_grafcet_pneumatic_" + i.ToString () + "']").InnerText;
					dataDDL.Add (aulaOption);
				}
				ddlAula.options = dataDDL;
				break;
			case baseSystem.Controllers.Ladder:
				countOptions = baseSituation.GetComponent<baseSystem> ().ladder_pneumatic_practices;
				for (int i = 0; i < countOptions; i++) {
					Dropdown.OptionData aulaOption = new Dropdown.OptionData ();
					aulaOption.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='aula_ladder_pneumatic_" + i.ToString () + "']").InnerText;
					dataDDL.Add (aulaOption);
				}
				ddlAula.options = dataDDL;
				break;
			}
			break;
		}

		_background = Instantiate(_backgroundPref, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_background.name = "aulaSelectorBg";
		_background.transform.SetParent (GameObject.Find("Canvas").transform, false);
		_background.transform.localPosition = new Vector3(0,0,0);
		_background.transform.SetAsLastSibling ();

		gameObject.transform.SetAsLastSibling ();

		iTween.MoveTo (
			gameObject,
			iTween.Hash (
				"position", new Vector3 (0, 0, 0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal", true
			)
		);
	}

	private void setAulaPractice(){

		int practiceValue = ddlAula.value;

		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			switch (Manager.Instance.globalController) {
			case baseSystem.Controllers.Grafcet:
				baseSituation.GetComponent<baseSystem> ()._electricalBench.GetComponent<ElectricalTestBenchMainClass> ().situationTag = "Electric_with_Grafcet_" + practiceValue.ToString ();
				break;
			case baseSystem.Controllers.Ladder:
				baseSituation.GetComponent<baseSystem> ()._electricalBench.GetComponent<ElectricalTestBenchMainClass> ().situationTag = "Electric_with_Ladder_" + practiceValue.ToString ();
				break;
			}
			baseSituation.GetComponent<baseSystem> ()._electricalBench.GetComponent<ElectricalTestBenchMainClass> ().loadSituation ();
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			switch (Manager.Instance.globalController) {
			case baseSystem.Controllers.Grafcet:
				baseSituation.GetComponent<baseSystem> ()._pneumaticBench.GetComponent<PnematicTestBenchClass> ().situationTag = "Pneumatic_with_Grafcet_" + practiceValue.ToString ();
				break;
			case baseSystem.Controllers.Ladder:
				baseSituation.GetComponent<baseSystem> ()._pneumaticBench.GetComponent<PnematicTestBenchClass> ().situationTag = "Pneumatic_with_Ladder_" + practiceValue.ToString ();
				break;
			}
			baseSituation.GetComponent<baseSystem> ()._pneumaticBench.GetComponent<PnematicTestBenchClass> ().loadSituation ();
			break;
		}

		iTween.MoveTo (
			gameObject,
			iTween.Hash (
				"position", new Vector3 (0, 1400, 0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal", true
			)
		);

		_background.GetComponent<Image> ().CrossFadeAlpha (0, 1f, false);
		Invoke ("destroyBgAction", 1.1f);

	}

	private void destroyBgAction(){
		Destroy (_background);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
