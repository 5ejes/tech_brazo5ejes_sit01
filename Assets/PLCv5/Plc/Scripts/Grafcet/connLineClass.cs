﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class connLineClass : MonoBehaviour {

	public Image line;
	public Image _workbenchReference;
	public GameObject startConnector;
	public GameObject endConnector;
	public int _id = 0;

	private Vector3 initialPosition;
	private Vector3 endPosition;
	private bool movementActive = false;
	private bool followActive = false;
	private bool onlyReference = false;
	// Use this for initialization
	void Start () {
		_workbenchReference = GameObject.Find ("workbenchReference").GetComponent<Image>();
		gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0f, 3f);
	}

	public void startConnection(GameObject _startConnector, Vector3 _initialPosition, bool _onlyReference = false, bool loadedType = false ){
		startConnector = _startConnector;
		initialPosition = _initialPosition;
		onlyReference = _onlyReference;

		if (_onlyReference == false) {
			_id = _startConnector.GetComponent<connModuleClass> ()._situation.GetComponent<GrafcetMainClass> ().lineModules;
			_startConnector.GetComponent<connModuleClass> ()._situation.GetComponent<GrafcetMainClass> ().lineModules++;
			this.name = "controller_lin_" + this._id.ToString ();
			if (loadedType == true) {
				movementActive = false;
			} else {
				movementActive = true;
			}

		} else {
			this._id =_startConnector.GetComponent<connModuleClass> ()._situation.GetComponent<GrafcetMainClass> ().lineModules;
			this.name = "controller_lin_inv_" + this._id.ToString ();
			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 0);
		}

		gameObject.transform.position = startConnector.transform.position;
	}

	public void startFollow(GameObject _endConnector){
		endConnector = _endConnector;

		if (endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("divergence") == 0 && endConnector.name == "bottomDivGreenConnector") {
			endPosition = new Vector3 (_endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.x + _endConnector.transform.parent.localPosition.x + _endConnector.transform.localPosition.x, _endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.y + _endConnector.transform.parent.localPosition.y + _endConnector.transform.localPosition.y, 0);
		} else if(endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("convergence") == 0 && endConnector.name == "topDivGreenConnector"){
			endPosition = new Vector3 (_endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.x + _endConnector.transform.parent.localPosition.x + _endConnector.transform.localPosition.x, _endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.y + _endConnector.transform.parent.localPosition.y + _endConnector.transform.localPosition.y, 0);
		} 
		else {
			endPosition = new Vector3 (_endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.x + _endConnector.transform.localPosition.x, _endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.y + _endConnector.transform.localPosition.y, 0);
		}

		followActive = true;
		setSiblingAction ();
	}

	public void stopDragLine(){
		movementActive = false;
	}

	private void setSiblingAction(){
		if (startConnector.GetComponent<connModuleClass>()._parentElement.transform.GetSiblingIndex () > endConnector.GetComponent<connModuleClass> ()._parentElement.transform.GetSiblingIndex ()) {
			gameObject.transform.SetSiblingIndex (endConnector.GetComponent<connModuleClass> ()._parentElement.transform.GetSiblingIndex () - 1);
		} else {
			gameObject.transform.SetSiblingIndex (startConnector.GetComponent<connModuleClass>()._parentElement.transform.GetSiblingIndex () - 1);
		}
	}

	public connLineStructure getJSONData(){
		connLineStructure _result = new connLineStructure ();

		_result._id = this._id;
		_result.name = this.name;
		if(startConnector != null){
			if (startConnector.name == "topDivGreenConnector" || startConnector.name == "bottomDivGreenConnector") {
				int _indexEle = int.Parse (startConnector.transform.parent.name.Replace ("conn_", "")) + 1;
				_result.startConnector = "conn_" + _indexEle.ToString ();
			} else {
				if (startConnector.name == "rightGreenConnector") {
					_result.startConnector = "_r_conn";	
				} else if (startConnector.name == "leftGreenConnector") {
					_result.startConnector = "_l_conn";	
				} else if (startConnector.name == "topGreenConnector") {
					_result.startConnector = "_u_conn";	
				} else if (startConnector.name == "bottomGreenConnector") {
					_result.startConnector = "_d_conn";	
				} else {
					_result.startConnector = startConnector.name;
				}
			}
			_result.startConnectorParent = startConnector.GetComponent<connModuleClass> ()._parentElement.name;
		}
		if(endConnector != null){
			if (endConnector.name == "topDivGreenConnector" || endConnector.name == "bottomDivGreenConnector") {
				int _indexEle = int.Parse (endConnector.transform.parent.name.Replace ("conn_", "")) + 1;
				_result.endConnector = "conn_" + _indexEle.ToString ();
			} else {
				if (endConnector.name == "rightGreenConnector") {
					_result.endConnector = "_r_conn";	
				} else if (endConnector.name == "leftGreenConnector") {
					_result.endConnector = "_l_conn";	
				} else if (endConnector.name == "topGreenConnector") {
					_result.endConnector = "_u_conn";	
				} else if (endConnector.name == "bottomGreenConnector") {
					_result.endConnector = "_d_conn";	
				} else {
					_result.endConnector = endConnector.name;
				}
			}
			_result.endConnectorParent = endConnector.GetComponent<connModuleClass> ()._parentElement.name;
		}
		_result.lineStaged = !onlyReference;

		if (startConnector != null && endConnector != null) {
			_result.lineName = "line_" + this._id.ToString ();
		} else {
			_result.lineName = "";
		}

		return _result;
	}
	// Update is called once per frame
	void Update() {
		if(movementActive == true){
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			_workbenchReference.transform.position = mousePos;

			//calculate distance:
			float _distance = Mathf.Sqrt(Mathf.Pow(initialPosition.x - _workbenchReference.transform.localPosition.x,2f) + Mathf.Pow(initialPosition.y - _workbenchReference.transform.localPosition.y,2f)) - 2;
			float _angle = Mathf.Atan ((_workbenchReference.transform.localPosition.y - initialPosition.y) / (_workbenchReference.transform.localPosition.x - initialPosition.x)) * 180f / Mathf.PI;

			if (_workbenchReference.transform.localPosition.x < initialPosition.x) {
				_angle = _angle + 180f;
			}

			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_distance, 3f);

			Quaternion _angleQuaternion = new Quaternion ();
			_angleQuaternion.eulerAngles = new Vector3 (0, 0, _angle);
			gameObject.GetComponent<RectTransform> ().rotation = _angleQuaternion;
		}

		if(followActive == true){
			if (onlyReference == false) {
				if (startConnector && endConnector) {

					if (startConnector.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("divergence") == 0 && startConnector.name == "bottomDivGreenConnector") {
						initialPosition = new Vector3 (startConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.x + startConnector.transform.parent.localPosition.x + startConnector.transform.localPosition.x, startConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.y + startConnector.transform.parent.localPosition.y + startConnector.transform.localPosition.y, 0);
					} else if (startConnector.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("convergence") == 0 && startConnector.name == "topDivGreenConnector") {
						initialPosition = new Vector3 (startConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.x + startConnector.transform.parent.localPosition.x + startConnector.transform.localPosition.x, startConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.y + startConnector.transform.parent.localPosition.y + startConnector.transform.localPosition.y, 0);
					} else {
						initialPosition = new Vector3 (startConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.x + startConnector.transform.localPosition.x, startConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.y + startConnector.transform.localPosition.y, 0);
					}

					if (endConnector.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("divergence") == 0 && endConnector.name == "bottomDivGreenConnector") {
						endPosition = new Vector3 (endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.x + endConnector.transform.parent.localPosition.x + endConnector.transform.localPosition.x, endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.y + endConnector.transform.parent.localPosition.y + endConnector.transform.localPosition.y, 0);
					} else if (endConnector.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("convergence") == 0 && endConnector.name == "topDivGreenConnector") {
						endPosition = new Vector3 (endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.x + endConnector.transform.parent.localPosition.x + endConnector.transform.localPosition.x, endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.y + endConnector.transform.parent.localPosition.y + endConnector.transform.localPosition.y, 0);
					} else {
						endPosition = new Vector3 (endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.x + endConnector.transform.localPosition.x, endConnector.GetComponent<connModuleClass> ()._parentElement.transform.localPosition.y + endConnector.transform.localPosition.y, 0);
					}

					float _distance = Mathf.Sqrt(Mathf.Pow(initialPosition.x - endPosition.x,2f) + Mathf.Pow(initialPosition.y - endPosition.y,2f));
					float _angle = Mathf.Atan ((endPosition.y - initialPosition.y) / (endPosition.x - initialPosition.x)) * 180f / Mathf.PI;

					if (endPosition.x < initialPosition.x) {
						_angle = _angle + 180f;
					}

					gameObject.transform.position = startConnector.transform.position;

					gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_distance, 3f);

					Quaternion _angleQuaternion = new Quaternion ();
					_angleQuaternion.eulerAngles = new Vector3 (0, 0, _angle);
					gameObject.GetComponent<RectTransform> ().rotation = _angleQuaternion;	
				} else {
					followActive = false;
					Destroy (gameObject);
				}
			} else {
				if (startConnector && endConnector) {
				} else {
					followActive = false;
					Destroy (gameObject);
				}
			}

		}
	}
}
