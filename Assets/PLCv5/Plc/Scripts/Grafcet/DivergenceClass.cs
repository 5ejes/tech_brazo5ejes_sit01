﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DivergenceClass : MonoBehaviour {

	public GameObject _situation;

	public bool _active = false;
	public int _id = 0;
	public bool _onSim = false;
	public string _type = "AND"; //AND, OR
	public int _editorType = 3;
	public int _branches = 2;

	public GameObject _ANDBg;
	public GameObject _ORBg;

	public Image _topConnector;
	//public Image _bottomConnector;
	public Button _deleteBt;
	public Button _editBt;
	public Button _deleteTopBt;

	public List<GameObject> _bottomConnectors;
	public List<float> _widthBottomConnectors;

	public bool dragEnabled = true;
	private bool objectDragged;
	private Vector3 initialPosition;
	private Vector3 startPosition;
	private int initialIndex;
	private int startIndex;

	private EventTrigger.Entry entry_0;
	private EventTrigger.Entry entry_1;
	private EventTrigger.Entry entry_2;
	private EventTrigger.Entry entry_3;

	public bool _menuActive = false;

	private float x_offset = 0;
	private float y_offset = 0;

	private bool _mouseOver = false;

	// Use this for initialization
	void Start () {
		_situation = GameObject.Find ("Canvas/Grafcet");

		initialPosition = gameObject.transform.position;

		_topConnector.enabled = false;
		//_bottomConnector.enabled = false;

		_deleteBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_editBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_deleteTopBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);

		_deleteBt.onClick.AddListener (deleteAction);
		_editBt.onClick.AddListener (editAction);
		_deleteTopBt.onClick.AddListener (deleteTopAction);

		gameObject.AddComponent<EventTrigger> ();

		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.BeginDrag;
		entry_0.callback.AddListener((data) => { BeginDrag_((PointerEventData)data); });

		entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.Drag;
		entry_1.callback.AddListener((data) => { ObjectDrag_((PointerEventData)data); });

		entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.EndDrag;
		entry_2.callback.AddListener((data) => { ObjectDrop_((PointerEventData)data); });

		EventTrigger.Entry entry_4 = new EventTrigger.Entry();
		entry_4.eventID = EventTriggerType.PointerEnter;
		entry_4.callback.AddListener((data) => { ObjectMouseOver_((PointerEventData)data); });

		EventTrigger.Entry entry_5 = new EventTrigger.Entry();
		entry_5.eventID = EventTriggerType.PointerExit;
		entry_5.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);
		trigger_Object.triggers.Add(entry_4);
		trigger_Object.triggers.Add(entry_5);

		gameObject.name = "divergence_base";
	}

	public void deleteAction(){
		for(int i = this._id + 1; i < _situation.GetComponent<GrafcetMainClass> ().divergenceModules; i++){
			GameObject _element = GameObject.Find ("divergence_" + i.ToString());
			_element.GetComponent<DivergenceClass> ().updateNameAndId (i - 1);
		}
		_situation.GetComponent<GrafcetMainClass> ().divergenceModules--;

		for(int i = 0; i < _bottomConnectors.Count; i++){
			deleteBottomAction (_bottomConnectors [i]);
		}
		Destroy (gameObject);
	}

	private void editAction(){
		_situation.GetComponent<GrafcetMainClass> ().activateBackground ();
		_situation.GetComponent<GrafcetMainClass> ()._editorPanel.transform.SetParent (GameObject.Find ("Canvas").transform, true);
		_situation.GetComponent<GrafcetMainClass> ()._editorPanel.GetComponent<editorPanelClass> ().configureEditor (gameObject, "divergence");

		iTween.MoveTo(
			_situation.GetComponent<GrafcetMainClass> ()._editorPanel,
			iTween.Hash(
				"position", new Vector3(0,0,0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);
	}

	private void deleteTopAction(){
		if(_topConnector.GetComponent<connModuleClass>()._line){
			GameObject _remoteStartElement = _topConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().startConnector;
			GameObject _remoteEndElement = _topConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector;

			if (_topConnector.name == _remoteStartElement.name) {
				Destroy (_remoteEndElement.GetComponent<connModuleClass> ()._line);
				_remoteEndElement.GetComponent<connModuleClass> ()._line = null;

			} else {
				Destroy (_remoteStartElement.GetComponent<connModuleClass> ()._line);
				_remoteStartElement.GetComponent<connModuleClass> ()._line = null;
			}

			Destroy (_topConnector.GetComponent<connModuleClass>()._line);
			_topConnector.GetComponent<connModuleClass> ()._line = null;
			iTween.ScaleTo (_deleteTopBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
	}

	private void deleteBottomAction(GameObject _elementClicked){
		if(_elementClicked.GetComponentInChildren<connModuleClass>()._line){
			GameObject _remoteStartElement = _elementClicked.GetComponentInChildren<connModuleClass> ()._line.GetComponent<connLineClass> ().startConnector;
			GameObject _remoteEndElement = _elementClicked.GetComponentInChildren<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector;

			if (_elementClicked.GetComponentInChildren<connModuleClass>().gameObject.name == _remoteStartElement.name) {
				if(_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("transition") == 0){
					_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<TransitionClass> ().dragEnabled = true;
				}
				else if(_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("step") == 0){
					_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<StepClass> ().dragEnabled = true;
				}

				Destroy (_remoteEndElement.GetComponent<connModuleClass> ()._line);
				_remoteEndElement.GetComponent<connModuleClass> ()._line = null;

			} else {
				if(_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("transition") == 0){
					_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<TransitionClass> ().dragEnabled = true;
				} else if(_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("step") == 0){
					_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<StepClass> ().dragEnabled = true;
				}

				Destroy (_remoteStartElement.GetComponent<connModuleClass> ()._line);
				_remoteStartElement.GetComponent<connModuleClass> ()._line = null;
			}

			Destroy (_elementClicked.GetComponentInChildren<connModuleClass>()._line);
			_elementClicked.GetComponentInChildren<connModuleClass> ()._line = null;
			iTween.ScaleTo (_elementClicked.GetComponentInChildren<Button>().gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
	}

	public void updateNameAndId(int _id){
		this._id = _id;
		this.name = "divergence_" + this._id.ToString ();
	}

	private void BeginDrag_(PointerEventData data)
	{
		if (dragEnabled == true) {
			objectDragged = true;

			if (_active == false) {

				gameObject.transform.SetParent (GameObject.Find ("Canvas/Grafcet").transform, true);

				startPosition = gameObject.transform.position;
				startIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

				gameObject.GetComponent<RectTransform> ().SetAsLastSibling ();

				_situation.GetComponent<GrafcetMainClass> ().activateWorkbenchCollider ();

				x_offset = 0;
				y_offset = 0;
			} else {
				Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				x_offset = gameObject.GetComponent<RectTransform> ().position.x - mousePos.x;
				y_offset = gameObject.GetComponent<RectTransform> ().position.y - mousePos.y;
			}
		}
	}

	private void ObjectDrag_(PointerEventData data)
	{
		if (objectDragged == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			gameObject.GetComponent<RectTransform> ().position = new Vector3 (mousePos.x + x_offset, mousePos.y + y_offset, 0);
		}
	}

	private void ObjectDrop_(PointerEventData data)
	{
		if (objectDragged == true) {
			objectDragged = false;

			_situation.GetComponent<GrafcetMainClass> ().deactivateWorkbenchCollider ();

			if (_active == false) {
				if (data != null && data.pointerEnter != null && data.pointerEnter.name == "workbenchCollider") {
					gameObject.transform.SetParent (_situation.GetComponent<GrafcetMainClass> ()._workbenchArea.transform, true);

					cloneElement ();

					initializeElement ();
				} else {
					iTween.MoveTo(
						gameObject,
						iTween.Hash(
							"position", startPosition,
							"looktarget", Camera.main,
							"easeType", iTween.EaseType.easeOutExpo,
							"time", 0.2f,
							"islocal",false
						)
					);

					Invoke ("goToInitialState", 0.2f);
				}
			}
		}
	}

	private void ObjectMouseOver_(PointerEventData data){
		if(_active == false){
			_mouseOver = true;
			_situation.GetComponent<GrafcetMainClass> ().showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='divergence']").InnerText);
		}
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(_mouseOver == true){
			_mouseOver = false;
			_situation.GetComponent<GrafcetMainClass> ().hideInfoText ();
		}
	}

	private void goToInitialState(){
		//inScenario = false;
		gameObject.GetComponent<RectTransform> ().SetSiblingIndex (initialIndex);
		gameObject.transform.SetParent(GameObject.Find("space7").transform, true);
	}

	public void cloneElement(){
		GameObject _clone = Instantiate(_situation.GetComponent<GrafcetMainClass>().divergencePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_clone.transform.SetParent (GameObject.Find("Canvas/Grafcet/leftMenu/containerElements/Viewport/Content/space7").transform, false);
		_clone.transform.localPosition = new Vector3 (0,0,0);
	}

	public void initializeElement(bool fromOpenAction = false){
		this._id = _situation.GetComponent<GrafcetMainClass> ().divergenceModules;
		_situation.GetComponent<GrafcetMainClass> ().divergenceModules++;
		this.name = "divergence_" + this._id.ToString ();

		if (fromOpenAction == true) {
			Vector2 _size = gameObject.GetComponent<RectTransform> ().sizeDelta;
			_size = new Vector2 (200f, _size.y);
			gameObject.GetComponent<RectTransform> ().sizeDelta = _size;
			initialAction ();
		} else {
			StartCoroutine (increaseWidth ());
		}

		entry_3 = new EventTrigger.Entry();
		entry_3.eventID = EventTriggerType.PointerClick;
		entry_3.callback.AddListener((data) => { ObjectClicked_((PointerEventData)data); });
		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();
		trigger_Object.triggers.Add(entry_3);

	}

	public void ObjectClicked_(PointerEventData data){
		if (objectDragged == false && _active == true) {
			if (_menuActive == false) {
				_situation.GetComponent<GrafcetMainClass> ().deactivateEditWithoutElement (gameObject.name);
				_menuActive = true;
				dragEnabled = false;
				iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0.6f, 0.6f, 0.6f), 0.3f);
				iTween.ScaleTo (_editBt.gameObject, new Vector3 (0.6f, 0.6f, 0.6f), 0.3f);
				if(_topConnector.GetComponent<connModuleClass>().lineIsBusy()){
					iTween.ScaleTo (_deleteTopBt.gameObject, new Vector3 (0.4f, 0.4f, 0.4f), 0.3f);
				}

				for(int i = 0; i < _bottomConnectors.Count; i++){
					if(_bottomConnectors[i].GetComponentInChildren<connModuleClass>().lineIsBusy()){
						iTween.ScaleTo (_bottomConnectors[i].GetComponentInChildren<Button> ().gameObject, new Vector3 (0.4f, 0.4f, 0.4f), 0.3f);
					}
				}

			} else {
				closeEditElements ();
			}
		}
	}

	public void closeEditElements(){
		iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		iTween.ScaleTo (_editBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		iTween.ScaleTo (_deleteTopBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);

		for (int i = 0; i < _bottomConnectors.Count; i++) {
			iTween.ScaleTo (_bottomConnectors[i].GetComponentInChildren<Button> ().gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
		_menuActive = false;
		dragEnabled = true;
	}

	public void initialAction(){
		this._active = true;
		//TODO: check if element is used;
		_topConnector.enabled = true;

		_bottomConnectors = new List<GameObject> ();

		for(int i = 0; i < _branches; i++){
			GameObject _downElement = Instantiate(_situation.GetComponent<GrafcetMainClass>().connBottomDivPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_downElement.transform.SetParent (gameObject.transform, false);
			float _width = gameObject.GetComponent<RectTransform> ().rect.width;
			float _x = -_width / 2f + (float)i * _width / ((float)_branches - 1f);
			_downElement.transform.localPosition = new Vector3 (_x, -65f, 0);

			_downElement.GetComponentInChildren<connModuleClass> ()._parentElement = gameObject;
			_downElement.GetComponentInChildren<connModuleClass> ()._id = i;
			_downElement.name = "conn_" + i.ToString();

			_downElement.GetComponentInChildren<Button> ().onClick.AddListener(() => deleteBottomAction(_downElement));
			_downElement.GetComponentInChildren<Button> ().gameObject.transform.localScale = new Vector3 (0, 0, 0);

			_bottomConnectors.Add (_downElement);
			_widthBottomConnectors.Add (0);
		}

		if (_type == "AND") {
			_ANDBg.SetActive (true);
			_ORBg.SetActive (false);
		} else {
			_ANDBg.SetActive (false);
			_ORBg.SetActive (true);
		}
	}

	public void updateConnections(){
		if (_type == "AND") {
			_ANDBg.SetActive (true);
			_ORBg.SetActive (false);
		} else {
			_ANDBg.SetActive (false);
			_ORBg.SetActive (true);
		}

		if(_branches != _bottomConnectors.Count){
			if (_bottomConnectors.Count < _branches) {
				//is required add elements
				for(int i = _bottomConnectors.Count; i < _branches; i++){
					GameObject _downElement = Instantiate(_situation.GetComponent<GrafcetMainClass>().connBottomDivPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
					_downElement.transform.SetParent (gameObject.transform, false);

					_downElement.GetComponentInChildren<connModuleClass> ()._parentElement = gameObject;
					_downElement.GetComponentInChildren<connModuleClass> ()._id = i;
					_downElement.name = "conn_" + (i).ToString ();

					_downElement.GetComponentInChildren<Button> ().onClick.AddListener(() => deleteBottomAction(_downElement));
					_downElement.GetComponentInChildren<Button> ().gameObject.transform.localScale = new Vector3 (0, 0, 0);

					_bottomConnectors.Add (_downElement);
					_widthBottomConnectors.Add (0);
				}

				updateBottomPositions ();
			} else {
				for(int i = (_bottomConnectors.Count-1); i >= _branches; i--){
					
					deleteBottomAction (_bottomConnectors [i]);
					Destroy (_bottomConnectors[i]);
					_bottomConnectors.RemoveAt (i);
					_widthBottomConnectors.RemoveAt (i);
				}
				updateBottomPositions ();
			}
		}

        //update connections
        if (_type == "AND")
        {
            //TOP -> transition
            if (_topConnector.GetComponent<connModuleClass>()._line)
            {
                if (_topConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("transition") == -1)
                {
                    deleteTopAction();
                }
            }

            //BOTTOM -> STEP
            for (int i = 0; i < _bottomConnectors.Count; i++)
            {
                if (_bottomConnectors[i].GetComponentInChildren<connModuleClass>().lineIsBusy())
                {
                    if (_bottomConnectors[i].GetComponentInChildren<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("step") == -1)
                    {
                        deleteBottomAction(_bottomConnectors[i]);
                    }
                }
                else
                {
                    Debug.Log("line empty " + i.ToString());
                }
            }
        }
        else
        {
            //TOP -> step
            if (_topConnector.GetComponent<connModuleClass>()._line)
            {
                if (_topConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("step") == -1)
                {
                    deleteTopAction();
                }
            }

            //BOTTOM -> transition
            for (int i = 0; i < _bottomConnectors.Count; i++)
            {
                if (_bottomConnectors[i].GetComponentInChildren<connModuleClass>().lineIsBusy())
                {
                    if (_bottomConnectors[i].GetComponentInChildren<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("transition") == -1)
                    {
                        deleteBottomAction(_bottomConnectors[i]);
                    }
                }
                else
                {
                    Debug.Log("line empty " + i.ToString());
                }
            }
        }
    }

	private void updateBottomPositions(){
		float _width = 0;
		for(int i = 0; i < (_bottomConnectors.Count - 1); i++){
			if (_widthBottomConnectors [i] > 0) {
				_width = _width + _widthBottomConnectors [i] + 350f;
			} else {
				_width = _width + 200;
			}
		}
		//_width = _width + 150;

		gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, gameObject.GetComponent<RectTransform> ().rect.height);
		for (int i =0; i < (_bottomConnectors.Count); i++) {
			float _x = 0;
			if (i == 0) {
				_x = -_width / 2f;
			} else if (i == (_bottomConnectors.Count - 1)) {
				_x = _width / 2f;
			} else {
				if (_widthBottomConnectors [i - 1] > 0) {
					_x = _bottomConnectors [i - 1].transform.localPosition.x + _widthBottomConnectors [i - 1] + 350f;
				} else {
					_x = _bottomConnectors [i - 1].transform.localPosition.x + 200f;
				}

			}

			//_x = -_width / 2f + (float)i * _width / ((float)_branches - 1f);
			_bottomConnectors [i].transform.localPosition = new Vector3 (_x, -65f, 0);
		}
	}

	private IEnumerator increaseWidth(){
		Vector2 _size = gameObject.GetComponent<RectTransform> ().sizeDelta;
		_size = new Vector2 (_size.x + 50f, _size.y);
		gameObject.GetComponent<RectTransform> ().sizeDelta = _size;
		yield return new WaitForSeconds (1f / 36f);
		if (gameObject.GetComponent<RectTransform> ().rect.width < 200) {
			StartCoroutine (increaseWidth ());
		} else {
			initialAction ();
		}
	}

	public divergenceStructure getJSONData(){
		divergenceStructure _result = new divergenceStructure ();
		_result.x = gameObject.transform.localPosition.x;
		_result.y = gameObject.transform.localPosition.y;
		_result._id = this._id;
		_result.name = this.name;
		_result._type = this._type;
		_result._editorType = this._editorType;
		_result._branches = this._branches;
		_result._u_conn = _topConnector.GetComponent<connModuleClass> ().getJSONData ();
		if(_bottomConnectors.Count > 0){
			connModuleStructure[] _conns = new connModuleStructure[_bottomConnectors.Count];
			for(int i = 0; i < _bottomConnectors.Count; i++){
				_conns [i] = _bottomConnectors [i].GetComponentInChildren<connModuleClass> ().getJSONData ();
			}
			_result.conns = _conns;
		}
		return _result;
	}

	// Update is called once per frame
	void Update () {
		if(_topConnector.GetComponent<connModuleClass>()._line){
			if(_topConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().startConnector && _topConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().endConnector){
				dragEnabled = false;

				Vector3 _newPosition = new Vector3 (0,0,0);
				GameObject _parentElement = _topConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement;

				float _vOffset = 0;
				if(_parentElement.name.IndexOf("step") == 0){
					_vOffset = 115;
				}
				else if(_parentElement.name.IndexOf("transition") == 0){
					_vOffset = 85;
				}

				_newPosition.x = _parentElement.transform.localPosition.x;
				_newPosition.y = _parentElement.transform.localPosition.y - _vOffset;

				gameObject.transform.localPosition = _newPosition;


			}
		}

		if(_active == true){
			for(int i = 0; i < _bottomConnectors.Count; i++){
				_widthBottomConnectors [i] = 0;
				connModuleClass _connection = _bottomConnectors [i].GetComponentInChildren<connModuleClass> ();
				bool parentActive = true;

				while(parentActive == true){
					if (_connection.lineIsBusy ()) {
						if(_connection._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("step") == 0){
							//in step
							if(_widthBottomConnectors [i] < _connection._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.GetComponent<StepClass>()._totalWidth){
								_widthBottomConnectors [i] = _connection._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement.GetComponent<StepClass> ()._totalWidth;
							}
							_connection = _connection._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement.GetComponent<StepClass> ()._bottomConnector.GetComponent<connModuleClass> ();
						}
						else if(_connection._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("transition") == 0){
							//in transition
							_connection = _connection._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement.GetComponent<TransitionClass> ()._bottomConnector.GetComponent<connModuleClass> ();
						}
						else if(_connection._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("convergence") == 0){
							//in convergence
							parentActive = false;
						}
						else if(_connection._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("divergence") == 0){
							//in divergence
							DivergenceClass _divergence = _connection._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.GetComponent<DivergenceClass>();
							if (i == 0) {
								//first
								float _divergence_width = _divergence.gameObject.GetComponent<RectTransform> ().rect.width / 2f + _divergence._widthBottomConnectors [_divergence._widthBottomConnectors.Count - 1];
								if(_widthBottomConnectors [i] < _divergence_width){
									_widthBottomConnectors [i] = _divergence_width;
								}
							} else if (i == (_bottomConnectors.Count - 1)) {
								//last
								float _divergence_width = _divergence.gameObject.GetComponent<RectTransform> ().rect.width / 2f;
								_widthBottomConnectors [i - 1] = _widthBottomConnectors [i - 1] + _divergence_width + 52f;
							} else {
								//middle
								float _divergence_width = _divergence.gameObject.GetComponent<RectTransform> ().rect.width / 2f + _divergence._widthBottomConnectors [_divergence._widthBottomConnectors.Count - 1];
								if(_widthBottomConnectors [i] < _divergence_width){
									_widthBottomConnectors [i] = _divergence_width;
								}

								_divergence_width = _divergence.gameObject.GetComponent<RectTransform> ().rect.width / 2f;
								_widthBottomConnectors [i - 1] = _widthBottomConnectors [i - 1] + _divergence_width + 52f;
							}

							parentActive = false;
						}
					} else {
						parentActive = false;
					}
				}
			}

			updateBottomPositions ();	
		}
	}
}
