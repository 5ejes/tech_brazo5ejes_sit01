﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;


public class connModuleClass : MonoBehaviour {

	public Sprite offImage;
	public Sprite onImage;

	public Sprite offImageMobile;
	public Sprite onImageMobile;

	public string _location = "";
	//public var _jumpElements:Array = new Array();
	public int _id = 0;

	public GameObject _situation;
	public GameObject _parentElement;
	public GameObject _line;

	private EventTrigger.Entry entry_0; //over
	private EventTrigger.Entry entry_1; //down
	private EventTrigger.Entry entry_2; //out
	private EventTrigger.Entry entry_3; //up

	private bool overEventActive = false;
	private bool downEventActive = false;
	private bool outEventActive = false;
	private bool upEventActive = false;

	private bool jump_mode = false;

	// Use this for initialization
	void Start () {

		_situation = GameObject.Find ("Canvas/Grafcet");

		gameObject.AddComponent<EventTrigger> ();
		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.PointerEnter;
		entry_0.callback.AddListener((data) => { BeginOver_((PointerEventData)data); });

		entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.PointerDown;
		entry_1.callback.AddListener((data) => { BeginDown_((PointerEventData)data); });

		entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.PointerExit;
		entry_2.callback.AddListener((data) => { BeginOut_((PointerEventData)data); });

		entry_3 = new EventTrigger.Entry();
		entry_3.eventID = EventTriggerType.PointerUp;
		entry_3.callback.AddListener((data) => { BeginUp_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);
		trigger_Object.triggers.Add(entry_3);

		overEventActive = true;
		downEventActive = false;
		outEventActive = false;
		upEventActive = false;

		if ((Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)) {
			gameObject.GetComponent<Image> ().sprite = offImageMobile;
			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (60, 56.16f);
		} else {
			gameObject.GetComponent<Image> ().sprite = offImage;
			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (24, 12);
		}
	}

	private void BeginOver_(PointerEventData _data)
	{
		if(overEventActive == true){
			if (_situation.GetComponent<GrafcetMainClass> ().jump_mode == true) {
				if(_parentElement.name.IndexOf("transition") != -1){
					if (this.name == "bottomGreenConnector") {
						if ((Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)) {
							gameObject.GetComponent<Image> ().sprite = onImageMobile;
							gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (60, 56.16f);
						} else {
							gameObject.GetComponent<Image> ().sprite = onImage;
							gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (24, 12);
						}

						overEventActive = false;
						outEventActive = true;
					}
				}
                else if (_parentElement.name.IndexOf("convergence") != -1 && _parentElement.GetComponent<ConvergenceClass>()._type == "OR")
                {
                    if (this.name == "bottomGreenConnector")
                    {
                        if ((Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer))
                        {
                            gameObject.GetComponent<Image>().sprite = onImageMobile;
                            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(60, 56.16f);
                        }
                        else
                        {
                            gameObject.GetComponent<Image>().sprite = onImage;
                            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(24, 12);
                        }

                        overEventActive = false;
                        outEventActive = true;
                    }
                }
            } else if(_situation.GetComponent<GrafcetMainClass> ().jump_connecting == true){
				if (_parentElement.name.IndexOf ("step") != -1) {
					if (this.name == "topGreenConnector") {
						if ((Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)) {
							gameObject.GetComponent<Image> ().sprite = onImageMobile;
							gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (60, 56.16f);
						} else {
							gameObject.GetComponent<Image> ().sprite = onImage;
							gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (24, 12);
						}
						overEventActive = false;
						outEventActive = true;
					}
				}
			} else {
				if ((Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)) {
					gameObject.GetComponent<Image> ().sprite = onImageMobile;
					gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (60, 56.16f);
				} else {
					gameObject.GetComponent<Image> ().sprite = onImage;
					gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (24, 12);
				}
				overEventActive = false;
				downEventActive = true;
				outEventActive = true;
			}
		}
	}

	private void BeginDown_(PointerEventData _data){
		if(downEventActive == true){
			
			createLine ();

			overEventActive = false;
			downEventActive = false;
			outEventActive = false;
			upEventActive = true;

		}
	}

	private void BeginUp_(PointerEventData _data){
		if(upEventActive == true){
			upEventActive = false;
			overEventActive = true;
			if ((Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)) {
				gameObject.GetComponent<Image> ().sprite = offImageMobile;
				gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (60, 56.16f);
			} else {
				gameObject.GetComponent<Image> ().sprite = offImage;
				gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (24, 12);
			}

			_line.GetComponent<connLineClass> ().stopDragLine ();

			if (_data != null && _data.pointerEnter != null && _data.pointerEnter.name.Contains("GreenConnector") && _parentElement ) {
                if ( _parentElement.name.IndexOf("step") != -1 ){
					checkStepDestination(_data.pointerEnter);
				} else if( _parentElement.name.IndexOf("transition") != -1 ){
					checkTransitionDestination (_data.pointerEnter);
				} else if( _parentElement.name.IndexOf("action") != -1 ){
					checkActionDestination (_data.pointerEnter);
				} else if( _parentElement.name.IndexOf("divergence") != -1 ){
					checkDivergenceDestination (_data.pointerEnter);
				} else if ( _parentElement.name.IndexOf ("convergence") != -1 ) {
					checkConvergenceDestination (_data.pointerEnter);
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}
		}
	}

	private void BeginOut_(PointerEventData _data){
		if(outEventActive == true){
			if ((Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)) {
				gameObject.GetComponent<Image> ().sprite = offImageMobile;
				gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (60, 56.16f);
			} else {
				gameObject.GetComponent<Image> ().sprite = offImage;
				gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (24, 12);
			}


			overEventActive = true;
			downEventActive = false;
			outEventActive = false;
		}
	}

	private void createLine(bool loadedType = false){
		_line = Instantiate(_situation.GetComponent<GrafcetMainClass>().connLinePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_line.transform.SetParent (_situation.GetComponent<GrafcetMainClass> ()._workbenchArea.transform, false);
		Vector3 _initialPosition = new Vector3 ();

		if (gameObject.transform.parent.gameObject.name.IndexOf ("conn_") == 0) {
			_initialPosition = new Vector3 (_parentElement.transform.localPosition.x + gameObject.transform.parent.transform.localPosition.x + gameObject.transform.localPosition.x, _parentElement.transform.localPosition.y + gameObject.transform.parent.transform.localPosition.y + gameObject.transform.localPosition.y, 0);
		} else {
			_initialPosition = new Vector3 (_parentElement.transform.localPosition.x + gameObject.transform.localPosition.x,_parentElement.transform.localPosition.y + gameObject.transform.localPosition.y,0);
		}

		_line.GetComponent<connLineClass> ().startConnection (gameObject, _initialPosition, false, loadedType);
	}

	private void deleteEvent(EventTriggerType _typeEvent){
		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		List<EventTrigger.Entry> entriesToRemove = new List<EventTrigger.Entry>();
		foreach (var entry in trigger_Object.triggers)
		{        
			if (entry.eventID == _typeEvent)
			{
				//remove listener from entry
				entry.callback.RemoveAllListeners();
				//add entry to transitional list
				entriesToRemove.Add(entry);
			}
		}

		foreach(var entry in entriesToRemove)
		{
			Debug.Log (entry.eventID);
			try{
				trigger_Object.triggers.Remove(entry);
			}catch(Exception e){}
		}
	}

	private void checkStepDestination(GameObject _destination){
		switch (this.name) {
		case "rightGreenConnector":
			if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("action") != -1) {
				if (_destination.name == "leftGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
						this._location = this._parentElement.name;
						_destination.GetComponent<connModuleClass> ()._location = _destination.GetComponent<connModuleClass> ()._parentElement.name;
						_destination.GetComponent<connModuleClass> ().createReferenceLine (gameObject);
						_line.GetComponent<connLineClass> ().startFollow (_destination);
						gameObject.GetComponent<Image> ().enabled = false;
						_destination.GetComponent<Image> ().enabled = false;
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}
			break;
		case "bottomGreenConnector":
			if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("transition") != -1) {
				if (_destination.name == "topGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
						this._location = this._parentElement.name;
						_destination.GetComponent<connModuleClass> ()._location = _destination.GetComponent<connModuleClass> ()._parentElement.name;
						_destination.GetComponent<connModuleClass> ().createReferenceLine (gameObject);
						_line.GetComponent<connLineClass> ().startFollow (_destination);
						gameObject.GetComponent<Image> ().enabled = false;
						_destination.GetComponent<Image> ().enabled = false;
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("divergence") != -1) {
				if (_destination.name == "topGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (_destination.GetComponent<connModuleClass>()._parentElement.GetComponent<DivergenceClass>()._type == "OR")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        } else
                        {
                            deleteLine();
                        }
                    } else {
					    deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("convergence") != -1) {
				if (_destination.name == "topDivGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (_destination.GetComponent<connModuleClass>()._parentElement.GetComponent<ConvergenceClass>()._type == "AND")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}
			break;
		case "topGreenConnector":
			if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("transition") != -1) {
				if (_destination.name == "bottomGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
						this._location = this._parentElement.name;
						_destination.GetComponent<connModuleClass> ()._location = _destination.GetComponent<connModuleClass> ()._parentElement.name;
						_destination.GetComponent<connModuleClass> ().createReferenceLine (gameObject);
						_line.GetComponent<connLineClass> ().startFollow (_destination);
						gameObject.GetComponent<Image> ().enabled = false;
						_destination.GetComponent<Image> ().enabled = false;
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("divergence") != -1) {
				if (_destination.name == "bottomDivGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (_destination.GetComponent<connModuleClass>()._parentElement.GetComponent<DivergenceClass>()._type == "AND")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }

                    } else {
					    deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("convergence") != -1) {
				if (_destination.name == "bottomGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (_destination.GetComponent<connModuleClass>()._parentElement.GetComponent<ConvergenceClass>()._type == "OR")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}

			break;
		}
	}

	private void checkTransitionDestination(GameObject _destination){
		switch (this.name) {
		case "topGreenConnector":
			if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("step") != -1) {
				if (_destination.name == "bottomGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
						this._location = this._parentElement.name;
						_destination.GetComponent<connModuleClass> ()._location = _destination.GetComponent<connModuleClass> ()._parentElement.name;
						_destination.GetComponent<connModuleClass> ().createReferenceLine (gameObject);
						_line.GetComponent<connLineClass> ().startFollow (_destination);
						gameObject.GetComponent<Image> ().enabled = false;
						_destination.GetComponent<Image> ().enabled = false;
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("divergence") != -1) {
				if (_destination.name == "bottomDivGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (_destination.GetComponent<connModuleClass>()._parentElement.GetComponent<DivergenceClass>()._type == "OR")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
                    } else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("convergence") != -1) {
				if (_destination.name == "bottomGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (_destination.GetComponent<connModuleClass>()._parentElement.GetComponent<ConvergenceClass>()._type == "AND")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}
			break;
		case "bottomGreenConnector":
			if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("step") != -1) {
				if (_destination.name == "topGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
						this._location = this._parentElement.name;
						_destination.GetComponent<connModuleClass> ()._location = _destination.GetComponent<connModuleClass> ()._parentElement.name;
						_destination.GetComponent<connModuleClass> ().createReferenceLine (gameObject);
						_line.GetComponent<connLineClass> ().startFollow (_destination);
						gameObject.GetComponent<Image> ().enabled = false;
						_destination.GetComponent<Image> ().enabled = false;
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("divergence") != -1) {
				if (_destination.name == "topGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (_destination.GetComponent<connModuleClass>()._parentElement.GetComponent<DivergenceClass>()._type == "AND")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("convergence") != -1) {
				if (_destination.name == "topDivGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (_destination.GetComponent<connModuleClass>()._parentElement.GetComponent<ConvergenceClass>()._type == "OR")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
                    } else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}
			break;
		}
	}

	private void checkActionDestination(GameObject _destination){
		switch (this.name) {
		case "leftGreenConnector":
			if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("step") != -1) {
				if (_destination.name == "rightGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
						this._location = this._parentElement.name;
						_destination.GetComponent<connModuleClass> ()._location = _destination.GetComponent<connModuleClass> ()._parentElement.name;
						_destination.GetComponent<connModuleClass> ().createReferenceLine (gameObject);
						_line.GetComponent<connLineClass> ().startFollow (_destination);
						gameObject.GetComponent<Image> ().enabled = false;
						_destination.GetComponent<Image> ().enabled = false;
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("action") != -1) {
				if (_destination.name == "rightGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
						this._location = this._parentElement.name;
						_destination.GetComponent<connModuleClass> ()._location = _destination.GetComponent<connModuleClass> ()._parentElement.name;
						_destination.GetComponent<connModuleClass> ().createReferenceLine (gameObject);
						_line.GetComponent<connLineClass> ().startFollow (_destination);
						gameObject.GetComponent<Image> ().enabled = false;
						_destination.GetComponent<Image> ().enabled = false;
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}
			break;
		case "rightGreenConnector":
			if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("action") != -1) {
				if (_destination.name == "leftGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
						this._location = this._parentElement.name;
						_destination.GetComponent<connModuleClass> ()._location = _destination.GetComponent<connModuleClass> ()._parentElement.name;
						_destination.GetComponent<connModuleClass> ().createReferenceLine (gameObject);
						_line.GetComponent<connLineClass> ().startFollow (_destination);
						gameObject.GetComponent<Image> ().enabled = false;
						_destination.GetComponent<Image> ().enabled = false;
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}
			break;
		}
	}

	private void checkDivergenceDestination(GameObject _destination){
		if (this.name == "topGreenConnector") {
			if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("step") != -1) {
				if (_destination.name == "bottomGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (this._parentElement.GetComponent<DivergenceClass>()._type == "OR")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
                    } else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("transition") != -1) {
				if (_destination.name == "bottomGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (this._parentElement.GetComponent<DivergenceClass>()._type == "AND")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}
		}
		else if(this.name == "bottomDivGreenConnector"){
			if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("step") != -1) {
				if (_destination.name == "topGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (this._parentElement.GetComponent<DivergenceClass>()._type == "AND")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("transition") != -1) {
				if (_destination.name == "topGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (this._parentElement.GetComponent<DivergenceClass>()._type == "OR")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}
		}
	}

	private void checkConvergenceDestination(GameObject _destination){
		if (this.name == "bottomGreenConnector") {
			if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("step") != -1) {
				if (_destination.name == "topGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (this._parentElement.GetComponent<ConvergenceClass>()._type == "OR")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("transition") != -1) {
				if (_destination.name == "topGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (this._parentElement.GetComponent<ConvergenceClass>()._type == "AND")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}
		}
		else if(this.name == "topDivGreenConnector"){
			if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("step") != -1) {
				if (_destination.name == "bottomGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (this._parentElement.GetComponent<ConvergenceClass>()._type == "AND")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else if (_destination.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("transition") != -1) {
				if (_destination.name == "bottomGreenConnector") {
					if (_location == "" && _destination.GetComponent<connModuleClass> ()._location == "") {
                        if (this._parentElement.GetComponent<ConvergenceClass>()._type == "OR")
                        {
                            this._location = this._parentElement.name;
                            _destination.GetComponent<connModuleClass>()._location = _destination.GetComponent<connModuleClass>()._parentElement.name;
                            _destination.GetComponent<connModuleClass>().createReferenceLine(gameObject);
                            _line.GetComponent<connLineClass>().startFollow(_destination);
                            gameObject.GetComponent<Image>().enabled = false;
                            _destination.GetComponent<Image>().enabled = false;
                        }
                        else
                        {
                            deleteLine();
                        }
					} else {
						deleteLine ();
					}
				} else {
					deleteLine ();
				}
			} else {
				deleteLine ();
			}
		}
	}

	public void createReferenceLine(GameObject _origin){
		_line = Instantiate(_situation.GetComponent<GrafcetMainClass>().connLinePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_line.transform.SetParent (_situation.GetComponent<GrafcetMainClass> ()._workbenchArea.transform, false);

		Vector3 _initialPosition = new Vector3 (_parentElement.transform.localPosition.x + gameObject.transform.localPosition.x,_parentElement.transform.localPosition.y + gameObject.transform.localPosition.y,0);
		_line.GetComponent<connLineClass> ().startConnection (gameObject, _initialPosition, true);
		_line.GetComponent<connLineClass> ().startFollow (_origin);
	}

	public void createLoadedLine(bool _lineStaged, string _endConnectorName, string _endConnectorParentName, int _index = -1){
		if (_lineStaged == true) {
			//visible
			createLine(true);
		}

		GameObject _parentToConnect = GameObject.Find (_endConnectorParentName);
		GameObject _toConnect = null;
        try
        {
            if (_endConnectorParentName.IndexOf("step_0") == 0)
            {
                if (_endConnectorName.IndexOf("rightGreenConnector") == 0 || _endConnectorName.IndexOf("_r_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<InitialStepClass>()._rightConnector.gameObject;
                }
                else if (_endConnectorName.IndexOf("topGreenConnector") == 0 || _endConnectorName.IndexOf("_u_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<InitialStepClass>()._topConnector.gameObject;
                }
                else if (_endConnectorName.IndexOf("bottomGreenConnector") == 0 || _endConnectorName.IndexOf("_d_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<InitialStepClass>()._bottomConnector.gameObject;
                }
            }
            else if (_endConnectorParentName.IndexOf("step") == 0 && _endConnectorParentName.IndexOf("step_0") == -1)
            {
                if (_endConnectorName.IndexOf("rightGreenConnector") == 0 || _endConnectorName.IndexOf("_r_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<StepClass>()._rightConnector.gameObject;
                }
                else if (_endConnectorName.IndexOf("topGreenConnector") == 0 || _endConnectorName.IndexOf("_u_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<StepClass>()._topConnector.gameObject;
                }
                else if (_endConnectorName.IndexOf("bottomGreenConnector") == 0 || _endConnectorName.IndexOf("_d_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<StepClass>()._bottomConnector.gameObject;
                }
            }
            else if (_endConnectorParentName.IndexOf("action") == 0)
            {
                if (_endConnectorName.IndexOf("rightGreenConnector") == 0 || _endConnectorName.IndexOf("_r_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<ActionClass>()._rightConnector.gameObject;
                }
                else if (_endConnectorName.IndexOf("leftGreenConnector") == 0 || _endConnectorName.IndexOf("_l_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<ActionClass>()._leftConnector.gameObject;
                }
            }
            else if (_endConnectorParentName.IndexOf("transition") == 0)
            {
                if (_endConnectorName.IndexOf("topGreenConnector") == 0 || _endConnectorName.IndexOf("_u_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<TransitionClass>()._topConnector.gameObject;
                }
                else if (_endConnectorName.IndexOf("bottomGreenConnector") == 0 || _endConnectorName.IndexOf("_d_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<TransitionClass>()._bottomConnector.gameObject;
                }
            }
            else if (_endConnectorParentName.IndexOf("divergence") == 0)
            {
                if (_endConnectorName.IndexOf("topGreenConnector") == 0 || _endConnectorName.IndexOf("_u_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<DivergenceClass>()._topConnector.gameObject;
                }
                else if (_endConnectorName.IndexOf("conn_") == 0)
                {
                    int _indexEle = int.Parse(_endConnectorName.Replace("conn_", "")) - 1;
                    _toConnect = _parentToConnect.GetComponent<DivergenceClass>()._bottomConnectors[_indexEle].GetComponentInChildren<connModuleClass>().gameObject;
                }
            }
            else if (_endConnectorParentName.IndexOf("convergence") == 0)
            {
                if (_endConnectorName.IndexOf("bottomGreenConnector") == 0 || _endConnectorName.IndexOf("_d_conn") == 0)
                {
                    _toConnect = _parentToConnect.GetComponent<ConvergenceClass>()._bottomConnector.gameObject;
                }
                else if (_endConnectorName.IndexOf("conn_") == 0)
                {
                    int _indexEle = int.Parse(_endConnectorName.Replace("conn_", "")) - 1;
                    _toConnect = _parentToConnect.GetComponent<ConvergenceClass>()._topConnectors[_indexEle].GetComponentInChildren<connModuleClass>().gameObject;
                }
            }
        } catch (Exception e)
        {
            Debug.Log(e);
        }

		if(_toConnect != null){
			if (_lineStaged == true) {
				_line.GetComponent<connLineClass> ().startFollow (_toConnect);
				gameObject.GetComponent<Image> ().enabled = false;
				_toConnect.GetComponent<Image> ().enabled = false;	
			} else {
				createReferenceLine (_toConnect);
			}
		}
	}

	private void deleteLine(){
		if(_line != null){
			Destroy (_line);
			_line = null;
		}
	}

	public void deactivateIfAvailable(){
		if (lineIsBusy () == false) {
			gameObject.GetComponent<Image> ().enabled = true;
		} else {
			gameObject.GetComponent<Image> ().enabled = false;
		}
	}

	public bool lineIsBusy(){
		bool _result = false;

		if(_line && _line.GetComponent<connLineClass>().startConnector && _line.GetComponent<connLineClass>().endConnector){
			_result = true;
		}

		return _result;
	}

	public connModuleStructure getJSONData(){
		connModuleStructure _result = new connModuleStructure ();
		_result.name = this.name;
		_result._location = this._location;
		_result._id = this._id;
		if (lineIsBusy () == true) {
			_result._line = _line.GetComponent<connLineClass> ().getJSONData ();
		} else {
			_result._line = new connLineStructure();
			_result._line._id = -1;
		}
		return _result;
	}
	// Update is called once per frame
	void Update () {
		if(_location != ""){
			if(!_line){
				_location = "";
				gameObject.GetComponent<Image> ().enabled = true;
			}
		}

		if (_situation.GetComponent<GrafcetMainClass> ().jump_connecting == true) {
			if (_parentElement.name.IndexOf ("step_") == 0 && _parentElement.name != "step_base" && _parentElement.name != "initial_step_base" && this.name == "topGreenConnector") {
				gameObject.GetComponent<Image> ().enabled = true;
				jump_mode = true;
			}
		} else {
			if(jump_mode == true){
				jump_mode = false;
				if (_parentElement.name == "step_0") {
					gameObject.GetComponent<Image> ().enabled = false;
				} else {
					if(lineIsBusy()){
						gameObject.GetComponent<Image> ().enabled = false;
					}
				}
			}
		}
	}
}
