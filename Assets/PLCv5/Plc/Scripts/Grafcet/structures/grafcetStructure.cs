﻿using System;

[Serializable]
public struct grafcetStructure
{
	public workAreaStructure work_area;
	public stepStructure[] steps;
	public actionStructure[] actions;
	public transitionStructure[] transitions;
	public divergenceStructure[] divergences;
	public convergenceStructure[] convergences;
	public jumpStructure[] jumps;
}