﻿using System;

[Serializable]
public struct stepStructure
{
	public float x;
	public float y;
	public int _id;
	public string name;
	public string _title;
	public bool _onSim;
	public connModuleStructure _r_conn;
	public connModuleStructure _u_conn;
	public connModuleStructure _d_conn;
}