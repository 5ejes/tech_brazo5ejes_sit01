﻿using System;

[Serializable]
public struct connLineStructure
{
	public int _id;
	public string name;
	public string startConnector;
	public string startConnectorParent;
	public string endConnector;
	public string endConnectorParent;
	public bool lineStaged;
	public string lineName;
}

