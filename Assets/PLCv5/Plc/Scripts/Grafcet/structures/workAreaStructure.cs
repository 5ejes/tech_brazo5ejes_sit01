﻿using System;

[Serializable]
public struct workAreaStructure
{
	public float width;
	public float height;
	public int stepsModules;
	public int actionModules;
	public int transitionModules;
	public int convergenceModules;
	public int divergenceModules;
	public int jumpModules;
	public int lineModules;
}

