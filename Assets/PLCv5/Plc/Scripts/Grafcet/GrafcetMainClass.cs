﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Crosstales.FB;
using System.IO;
using System;


public class GrafcetMainClass : MonoBehaviour {

	[System.Serializable] public class GrafcetControllerEvent : UnityEngine.Events.UnityEvent<grafcetController> {}

	[Header("Event")]
	[SerializeField] private GrafcetControllerEvent _grafcetCompile = null;
	public Text infoText;
	public Text titleLeftMenu;

	public Button _infoBt;
	public Button _openBt;
	public Button _saveBt;
	public Button _compileBt;
	public Button _helpBt;
	public Button _reportBt;
	public Button _trashBt;
	public Button _goToBt;
	public Button _languageBt;

	public Button _menuLeftBt;
	public GameObject _leftMenu;

	public GameObject _workbenchContainer;
	public GameObject _workbenchArea;
	public GameObject _workbenchCollider;
	public GameObject _loadingElement;

	public GameObject _situation;
    public GameObject RoboticSituation;

	public GameObject _editorPanel;
	public GameObject _editorTransPanel;

	public int stepsModules = 0;
	public int actionModules = 0;
	public int transitionModules = 0;
	public int convergenceModules = 0;
	public int divergenceModules = 0;
	public int jumpModules = 0;
	public int lineModules = 0;

	public GameObject initialStepPrefab;
	public GameObject stepPrefab;
	public GameObject actionPrefab;
	public GameObject transitionPrefab;
	public GameObject jumpPrefab;
	public GameObject divergencePrefab;
	public GameObject convergencePrefab;
	public GameObject connLinePrefab;
	public GameObject connBottomDivPrefab;
	public GameObject connTopDivPrefab;

	public GameObject conditionElementPrefab;
	public GameObject operationElementPrefab;
	public GameObject groupElementPrefab;

	public Sprite _showBtImage;
	public Sprite _hideBtImage;

	public bool jump_mode = false;
	public bool jump_connecting = false;

	public bool simulationRunning = false;

	[Header("CONFIG VALUES")]
	public int digitalOutputs = 16;
	public int digitalInputs = 16;
	public int analogInputs = 0;
	public int analogOutputs = 0;
	public int memoryElements = 20;
	public int timerElements = 20;
	public int counterElements = 10;
	public List<string> timerTypes;
	public List<string> counterTypes;
	public List<string> aritmethicOperators;
	public List<string> logicOperators;
	public List<string> logicTransOperators;
	public List<string> logicComparators;

	public bool leftMenuHidden = false;

	private Vector3 _leftMenuActive;
	private Vector3 _leftMenuInactive;

	private float _initialAreaHeight;

	public grafcetController grafcetEngine;
	private bool[] _inputsBool;
	private byte[] _inputsByte;
	private bool[] _outputsBool;
	private byte[] _outputsByte;

	private bool overBtActive = false;

	private string path;

	// Use this for initialization
	void Start () {
		infoText.text = "";

		_openBt.onClick.AddListener (openAction);
		_saveBt.onClick.AddListener (saveActionPrev);
		_compileBt.onClick.AddListener (compileAction);
		_helpBt.onClick.AddListener (helpAction);
		_trashBt.onClick.AddListener (trashAction);
		// _goToBt.onClick.AddListener (goToAction);
		_languageBt.onClick.AddListener (languageAction);

		_menuLeftBt.onClick.AddListener (leftMenuAction);

		_leftMenuActive = _leftMenu.transform.localPosition;
		_leftMenuInactive = new Vector3 (_leftMenuActive.x - 170, _leftMenuActive.y, _leftMenuActive.z);

		deactivateWorkbenchCollider ();

        if(Manager.Instance.globalRoboticsMode == true)
        {
            analogInputs = 5;
        }

        _inputsBool = new bool[digitalInputs];
		_inputsByte = new byte[analogInputs];
		_outputsBool = new bool[digitalOutputs];
		_outputsByte = new byte[analogOutputs];
	
		_openBt.gameObject.AddComponent<EventTrigger> ();
		_saveBt.gameObject.AddComponent<EventTrigger> ();
		_compileBt.gameObject.AddComponent<EventTrigger> ();
		_helpBt.gameObject.AddComponent<EventTrigger> ();
		_trashBt.gameObject.AddComponent<EventTrigger> ();
		// _goToBt.gameObject.AddComponent<EventTrigger> ();
		_languageBt.gameObject.AddComponent<EventTrigger> ();

		EventTrigger.Entry entry_1_0 = new EventTrigger.Entry();
		entry_1_0.eventID = EventTriggerType.PointerEnter;
		entry_1_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "info"); });

		EventTrigger.Entry entry_1_1 = new EventTrigger.Entry();
		entry_1_1.eventID = EventTriggerType.PointerExit;
		entry_1_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "info"); });

		EventTrigger.Entry entry_2_0 = new EventTrigger.Entry();
		entry_2_0.eventID = EventTriggerType.PointerEnter;
		entry_2_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "open"); });

		EventTrigger.Entry entry_2_1 = new EventTrigger.Entry();
		entry_2_1.eventID = EventTriggerType.PointerExit;
		entry_2_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "open"); });

		EventTrigger.Entry entry_2_2 = new EventTrigger.Entry();
		entry_2_2.eventID = EventTriggerType.PointerDown;
		entry_2_2.callback.AddListener((data) => { openDownAction((PointerEventData)data); });

		EventTrigger _infoTrigger2 = _openBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger2.triggers.Add(entry_2_0);
		_infoTrigger2.triggers.Add(entry_2_1);
		_infoTrigger2.triggers.Add(entry_2_2);

		EventTrigger.Entry entry_3_0 = new EventTrigger.Entry();
		entry_3_0.eventID = EventTriggerType.PointerEnter;
		entry_3_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "save"); });

		EventTrigger.Entry entry_3_1 = new EventTrigger.Entry();
		entry_3_1.eventID = EventTriggerType.PointerExit;
		entry_3_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "save"); });

		EventTrigger _infoTrigger3 = _saveBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger3.triggers.Add(entry_3_0);
		_infoTrigger3.triggers.Add(entry_3_1);

		EventTrigger.Entry entry_4_0 = new EventTrigger.Entry();
		entry_4_0.eventID = EventTriggerType.PointerEnter;
		entry_4_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "compile"); });

		EventTrigger.Entry entry_4_1 = new EventTrigger.Entry();
		entry_4_1.eventID = EventTriggerType.PointerExit;
		entry_4_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "compile"); });

		EventTrigger _infoTrigger4 = _compileBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger4.triggers.Add(entry_4_0);
		_infoTrigger4.triggers.Add(entry_4_1);

		EventTrigger.Entry entry_5_0 = new EventTrigger.Entry();
		entry_5_0.eventID = EventTriggerType.PointerEnter;
		entry_5_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "help"); });

		EventTrigger.Entry entry_5_1 = new EventTrigger.Entry();
		entry_5_1.eventID = EventTriggerType.PointerExit;
		entry_5_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "help"); });

		EventTrigger _infoTrigger5 = _helpBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger5.triggers.Add(entry_5_0);
		_infoTrigger5.triggers.Add(entry_5_1);

		EventTrigger.Entry entry_6_0 = new EventTrigger.Entry();
		entry_6_0.eventID = EventTriggerType.PointerEnter;
		entry_6_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "report"); });

		EventTrigger.Entry entry_6_1 = new EventTrigger.Entry();
		entry_6_1.eventID = EventTriggerType.PointerExit;
		entry_6_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "report"); });

		EventTrigger.Entry entry_7_0 = new EventTrigger.Entry();
		entry_7_0.eventID = EventTriggerType.PointerEnter;
		entry_7_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "trash"); });

		EventTrigger.Entry entry_7_1 = new EventTrigger.Entry();
		entry_7_1.eventID = EventTriggerType.PointerExit;
		entry_7_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "trash"); });

		EventTrigger _infoTrigger7 = _trashBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger7.triggers.Add(entry_7_0);
		_infoTrigger7.triggers.Add(entry_7_1);

		EventTrigger.Entry entry_8_0 = new EventTrigger.Entry();
		entry_8_0.eventID = EventTriggerType.PointerEnter;
		entry_8_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "goto"); });

		EventTrigger.Entry entry_8_1 = new EventTrigger.Entry();
		entry_8_1.eventID = EventTriggerType.PointerExit;
		entry_8_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "goto"); });

		// EventTrigger _infoTrigger8 = _goToBt.gameObject.GetComponent<EventTrigger> ();
		// _infoTrigger8.triggers.Add(entry_8_0);
		// _infoTrigger8.triggers.Add(entry_8_1);

		EventTrigger.Entry entry_9_0 = new EventTrigger.Entry();
		entry_9_0.eventID = EventTriggerType.PointerEnter;
		entry_9_0.callback.AddListener((data) => { overButtonAction_((PointerEventData)data, "lang"); });

		EventTrigger.Entry entry_9_1 = new EventTrigger.Entry();
		entry_9_1.eventID = EventTriggerType.PointerExit;
		entry_9_1.callback.AddListener((data) => { outButtonAction_((PointerEventData)data, "lang"); });

		EventTrigger _infoTrigger9 = _languageBt.gameObject.GetComponent<EventTrigger> ();
		_infoTrigger9.triggers.Add(entry_9_0);
		_infoTrigger9.triggers.Add(entry_9_1);

	}

	public void initialAction(){
		float _aspectRatio = 0;

		switch(Manager.Instance.globalGraphicInterface){
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation = GameObject.Find ("Canvas/ElectricalBench");
			_aspectRatio = (float)_situation.GetComponent<ElectricalTestBenchMainClass> ()._cam1.pixelWidth / (float)_situation.GetComponent<ElectricalTestBenchMainClass> ()._cam1.pixelHeight;
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation = GameObject.Find ("Canvas/PneumaticBench");
			_aspectRatio = (float)_situation.GetComponent<PnematicTestBenchClass> ()._cam1.pixelWidth / (float)_situation.GetComponent<PnematicTestBenchClass> ()._cam1.pixelHeight;
			break;
		case baseSystem.graphicInterfaces.none:
			GameObject _langBt = GameObject.Find ("lang_bt_grafcet");
			_langBt.transform.localPosition = new Vector3 (_langBt.transform.localPosition.x - 105f, _langBt.transform.localPosition.y, _langBt.transform.localPosition.z);
			_langBt.GetComponent<languageBt> ().setInitialPosition ();
			// _goToBt.gameObject.SetActive (false);
			_situation = GameObject.Find ("Canvas/OnlyControllerMain");
			_aspectRatio = (float)_situation.GetComponent<onlyControllerMainClass> ()._cam1.pixelWidth / (float)_situation.GetComponent<onlyControllerMainClass> ()._cam1.pixelHeight;

			break;
        case baseSystem.graphicInterfaces.roboticInterface:
            GameObject _langBt1 = GameObject.Find("lang_bt_grafcet");
            _langBt1.transform.localPosition = new Vector3(_langBt1.transform.localPosition.x - 105f, _langBt1.transform.localPosition.y, _langBt1.transform.localPosition.z);
            _langBt1.GetComponent<languageBt>().setInitialPosition();
            // _goToBt.gameObject.SetActive(true);
            _situation = GameObject.Find("Canvas/OnlyControllerMain");
            _aspectRatio = (float)_situation.GetComponent<onlyControllerMainClass>()._cam1.pixelWidth / (float)_situation.GetComponent<onlyControllerMainClass>()._cam1.pixelHeight;
            RoboticSituation = GameObject.Find("Canvas/PLCViewer");
            break;
		}

		float _heightTotal = 1080f * ((1920f / 1080f) / _aspectRatio);

		_initialAreaHeight = _heightTotal - 114f;
		_workbenchArea.GetComponent<RectTransform> ().sizeDelta = new Vector2 (1720f, _initialAreaHeight);

		if (Manager.Instance.globalLanguageEnabled == false) {
			GameObject _langBtToHide = GameObject.Find ("lang_bt_grafcet");
			_langBtToHide.SetActive (false);
		}

		#if UNITY_WEBGL
		Application.ExternalEval(
			@"
document.addEventListener('click', function() {

    var fileuploader = document.getElementById('fileuploader');
    if (!fileuploader) {
        fileuploader = document.createElement('input');
        fileuploader.setAttribute('style','display:none;');
        fileuploader.setAttribute('type', 'file');
        fileuploader.setAttribute('id', 'fileuploader');
		fileuploader.setAttribute('accept', '.vgraf');
        fileuploader.setAttribute('class', '');
        document.getElementsByTagName('body')[0].appendChild(fileuploader);

        fileuploader.onchange = function(e) {
        var files = e.target.files;
            for (var i = 0, f; f = files[i]; i++) {
				var reader = new FileReader();
			    reader.readAsText(f,'UTF-8');
			    reader.onload = readerEvent => {
			       	var content = readerEvent.target.result; // this is the content!
					SendMessage('" + gameObject.name +@"', 'FileDialogResult', content);
			    }
            }
        };
    }
    if (fileuploader.getAttribute('class') == 'focused') {
        fileuploader.setAttribute('class', '');
        fileuploader.click();
    }
});
            ");

		#endif
	}

	private void overButtonAction_(PointerEventData data, string _element)
	{
		overBtActive = true;
		string _text = "";
		switch(_element){
		case "info": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='info_over']").InnerText;
			break;
		case "open": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='open_over']").InnerText;
			break;
		case "save": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='save_over']").InnerText;
			break;
		case "compile": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='compile_over']").InnerText;
			break;
		case "help": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='help_over']").InnerText;
			break;
		case "report": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='report_over']").InnerText;
			break;
		case "trash": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='trash_over']").InnerText;
			break;
		case "goto": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='bench_over']").InnerText;
			break;
		case "lang": _text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='lang_over']").InnerText;
			break;
		}

		CancelInvoke ("hideInfoText");
		infoText.text = _text;

	}

	private void outButtonAction_(PointerEventData data, string _element)
	{
		if(overBtActive == true){
			overBtActive = false;
			if(infoText.text != Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='successful_comp']").InnerText){
				infoText.text = "";
			}
		}
	}

	public void setTexts(){
		titleLeftMenu.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='grafcet_functions']").InnerText;
	}

	public void activateWorkbenchCollider(){
		_workbenchCollider.SetActive (true);
		_workbenchCollider.transform.SetAsLastSibling ();
	}

	public void deactivateWorkbenchCollider(){
		_workbenchCollider.SetActive (false);
	}

	public void showInfoText(string _text){
		CancelInvoke ("hideInfoText");
		infoText.text = _text;
		Invoke ("hideInfoText", 3f);
	}

	public void showInfoAlert(string _text){
		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert = Instantiate(_situation.GetComponent<ElectricalTestBenchMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.GetComponent<Alert>().showAlert(1, "", "", _text, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='accept']").InnerText );
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass>()._alert = Instantiate(_situation.GetComponent<PnematicTestBenchClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<PnematicTestBenchClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<PnematicTestBenchClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			_situation.GetComponent<PnematicTestBenchClass>()._alert.GetComponent<Alert>().showAlert(1, "", "", _text, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='accept']").InnerText );
			break;
		case baseSystem.graphicInterfaces.none:
        case baseSystem.graphicInterfaces.roboticInterface:
			_situation.GetComponent<onlyControllerMainClass>()._alert = Instantiate(_situation.GetComponent<onlyControllerMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<onlyControllerMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<onlyControllerMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			_situation.GetComponent<onlyControllerMainClass>()._alert.GetComponent<Alert>().showAlert(1, "", "", _text, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='accept']").InnerText );
			break;
		}
	}

	public void activateBackground(){
		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass> ()._background = Instantiate (_situation.GetComponent<ElectricalTestBenchMainClass> ()._backgroundPref, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<ElectricalTestBenchMainClass> ()._background.name = "SituationBg";
			_situation.GetComponent<ElectricalTestBenchMainClass> ()._background.transform.SetParent (GameObject.Find ("Canvas").transform, false);
			_situation.GetComponent<ElectricalTestBenchMainClass> ()._background.transform.localPosition = new Vector3 (0, 0, 0);
			_situation.GetComponent<ElectricalTestBenchMainClass> ()._background.transform.SetAsLastSibling ();
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass> ()._background = Instantiate (_situation.GetComponent<PnematicTestBenchClass> ()._backgroundPref, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<PnematicTestBenchClass> ()._background.name = "SituationBg";
			_situation.GetComponent<PnematicTestBenchClass> ()._background.transform.SetParent (GameObject.Find ("Canvas").transform, false);
			_situation.GetComponent<PnematicTestBenchClass> ()._background.transform.localPosition = new Vector3 (0, 0, 0);
			_situation.GetComponent<PnematicTestBenchClass> ()._background.transform.SetAsLastSibling ();
			break;
		case baseSystem.graphicInterfaces.none:
        case baseSystem.graphicInterfaces.roboticInterface:
			_situation.GetComponent<onlyControllerMainClass> ()._background = Instantiate (_situation.GetComponent<onlyControllerMainClass> ()._backgroundPref, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<onlyControllerMainClass> ()._background.name = "SituationBg";
			_situation.GetComponent<onlyControllerMainClass> ()._background.transform.SetParent (GameObject.Find ("Canvas").transform, false);
			_situation.GetComponent<onlyControllerMainClass> ()._background.transform.localPosition = new Vector3 (0, 0, 0);
			_situation.GetComponent<onlyControllerMainClass> ()._background.transform.SetAsLastSibling ();
			break;
		}
	}

	public void deactivateBackground(){
		Image _bg;
		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			_bg = _situation.GetComponent<ElectricalTestBenchMainClass>()._background.GetComponent<Image>();
			_bg.CrossFadeAlpha (0f, 1f, false);
			Destroy(_situation.GetComponent<ElectricalTestBenchMainClass>()._background, 1.2f);
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_bg = _situation.GetComponent<PnematicTestBenchClass>()._background.GetComponent<Image>();
			_bg.CrossFadeAlpha (0f, 1f, false);
			Destroy(_situation.GetComponent<PnematicTestBenchClass>()._background, 1.2f);
			break;
		case baseSystem.graphicInterfaces.none:
        case baseSystem.graphicInterfaces.roboticInterface:
			_bg = _situation.GetComponent<onlyControllerMainClass>()._background.GetComponent<Image>();
			_bg.CrossFadeAlpha (0f, 1f, false);
			Destroy(_situation.GetComponent<onlyControllerMainClass>()._background, 1.2f);
			break;
		}

	}

	public void hideInfoText(){
		infoText.text = "";
	}

	private void infoAction(){
		string DescriptionPractice = "";
		switch(Manager.Instance.globalGraphicInterface){
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert = Instantiate(_situation.GetComponent<ElectricalTestBenchMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			DescriptionPractice = Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<ElectricalTestBenchMainClass>().situationTag + "/description_practice").InnerText;
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.GetComponent<Alert>().showAlert(7, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='situation']").InnerText + "|" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='procedure']").InnerText, Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<ElectricalTestBenchMainClass>().situationTag + "/name_practice").InnerText, DescriptionPractice + "|" + Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<ElectricalTestBenchMainClass>().situationTag + "/procedure").InnerText);	
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass>()._alert = Instantiate(_situation.GetComponent<PnematicTestBenchClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<PnematicTestBenchClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<PnematicTestBenchClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			DescriptionPractice = Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<PnematicTestBenchClass>().situationTag + "/description_practice").InnerText;
			_situation.GetComponent<PnematicTestBenchClass>()._alert.GetComponent<Alert>().showAlert(7, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='situation']").InnerText + "|" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='procedure']").InnerText, Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<PnematicTestBenchClass>().situationTag + "/name_practice").InnerText, DescriptionPractice + "|" + Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<PnematicTestBenchClass>().situationTag + "/procedure").InnerText);	
			break;
		case baseSystem.graphicInterfaces.none:
			_situation.GetComponent<onlyControllerMainClass>()._alert = Instantiate(_situation.GetComponent<onlyControllerMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<onlyControllerMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<onlyControllerMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			DescriptionPractice = Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/description_practice").InnerText;
			_situation.GetComponent<onlyControllerMainClass>()._alert.GetComponent<Alert>().showAlert(7, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='situation']").InnerText + "|" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='procedure']").InnerText, Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/name_practice").InnerText, DescriptionPractice + "|" + Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/procedure").InnerText);	
			break;
        case baseSystem.graphicInterfaces.roboticInterface:
            _situation.GetComponent<onlyControllerMainClass>()._alert = Instantiate(_situation.GetComponent<onlyControllerMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            _situation.GetComponent<onlyControllerMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
            _situation.GetComponent<onlyControllerMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
            DescriptionPractice = Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/description_practice").InnerText;
            _situation.GetComponent<onlyControllerMainClass>()._alert.GetComponent<Alert>().showAlert(7, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='situation']").InnerText + "|" + Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='procedure']").InnerText, Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/name_practice").InnerText, DescriptionPractice + "|" + Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/procedure").InnerText);
            break;
		}
	}

    private void openAction()
    {
        string extensions = "vgraf";
        string _dataJSON = "";

        if (_loadingElement.GetComponent<loadingClass>().isActive == false)
        {
#if !UNITY_WEBGL
            if (Application.platform == RuntimePlatform.Android)
            {
                GameObject openWindow = GameObject.Find("openWindow");
                if (openWindow != null)
                {
                    openWindow.GetComponent<OpenWindow>().showWindow(gameObject);
                }
            }
            else
            {
                string _path = FileBrowser.OpenSingleFile(Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='openFileDialog']").InnerText, "", extensions);
                if (_path != "")
                {
                    StreamReader reader = new StreamReader(_path);
                    _dataJSON = reader.ReadToEnd();
                    reader.Close();

                    _dataJSON = _dataJSON.Replace("\"_line\":null", "\"_line\":{\"_id\":-1,\"name\":\"\",\"startConnector\":\"\",\"startConnectorParent\":\"\",\"endConnector\":\"\",\"endConnectorParent\":\"\",\"lineStaged\":false,\"lineName\":\"\"}");

                    //restart scenario
                    resetScenario();
                    //load new elements.
                    grafcetStructure _data = JsonUtility.FromJson<grafcetStructure>(_dataJSON);

                    StartCoroutine(loadElements(_data));

                }
            }
#endif
        }
    }

	public void openFromAndroid(string pathFile){
		string _dataJSON = "";
		if(pathFile != ""){
			StreamReader reader = new StreamReader(pathFile);
			_dataJSON = reader.ReadToEnd ();
			reader.Close();

			_dataJSON = _dataJSON.Replace ("\"_line\":null", "\"_line\":{\"_id\":-1,\"name\":\"\",\"startConnector\":\"\",\"startConnectorParent\":\"\",\"endConnector\":\"\",\"endConnectorParent\":\"\",\"lineStaged\":false,\"lineName\":\"\"}");

			//restart scenario
			resetScenario();
			//load new elements.
			grafcetStructure _data = JsonUtility.FromJson<grafcetStructure>(_dataJSON);

			StartCoroutine (loadElements (_data));

		}
	}

	private void openDownAction(PointerEventData data){
		#if UNITY_WEBGL
		Application.ExternalEval(
			@"
var fileuploader = document.getElementById('fileuploader');
if (fileuploader) {
    fileuploader.setAttribute('class', 'focused');
}
            ");
		#endif
	}

	public void FileDialogResult(string fileData) {
		//restart scenario
		resetScenario();
		//load new elements.
		grafcetStructure _data = JsonUtility.FromJson<grafcetStructure>(fileData);

		StartCoroutine (loadElements (_data));
	}

	private IEnumerator loadElements(grafcetStructure _data){

		_loadingElement.SetActive (true);
		_loadingElement.GetComponent<loadingClass> ().isActive = true;

		yield return new WaitForSeconds (0.5f);

		_workbenchArea.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_data.work_area.width, _data.work_area.height);

		if(_data.steps.Length > 0){
			for(int i = 0; i < _data.steps.Length; i++){
				if (_data.steps [i]._id == 0) {
					GameObject _initialStep = GameObject.Find ("initial_step_base");
					_initialStep.transform.SetParent ( _workbenchArea.transform , true);
					_initialStep.GetComponent<InitialStepClass> ().initializeElement ();
					_initialStep.transform.localPosition = new Vector3 (_data.steps [i].x, _data.steps [i].y, 0);
				} else {
					yield return new WaitForSeconds (0.01f);
					GameObject _initialStep = GameObject.Find ("step_base");
					_initialStep.transform.SetParent ( _workbenchArea.transform , true);
					_initialStep.GetComponent<StepClass> ().cloneElement ();
					_initialStep.GetComponent<StepClass> ().initializeElement ();
					_initialStep.transform.localPosition = new Vector3 (_data.steps [i].x, _data.steps [i].y, 0);
				}
			}
		}

		if (_data.actions.Length > 0) {
			for (int i = 0; i < _data.actions.Length; i++) {
				yield return new WaitForSeconds (0.01f);
				GameObject _initialAction = GameObject.Find ("action_base");
				_initialAction.transform.SetParent ( _workbenchArea.transform , true);
				_initialAction.GetComponent<ActionClass> ().cloneElement ();
				_initialAction.GetComponent<ActionClass> ().initializeElement ();

				_initialAction.transform.localPosition = new Vector3 (_data.actions [i].x, _data.actions [i].y, 0);
				_initialAction.GetComponent<ActionClass> ()._titleElement.text = _data.actions [i]._title;
				_initialAction.GetComponent<ActionClass> ()._textElement.text = _data.actions [i]._msg;
				_initialAction.GetComponent<ActionClass> ()._type = _data.actions [i]._type;
				_initialAction.GetComponent<ActionClass> ()._value = _data.actions [i]._value;
				_initialAction.GetComponent<ActionClass> ()._index = _data.actions [i]._index;
				_initialAction.GetComponent<ActionClass> ()._value2 = _data.actions [i]._value2;
				_initialAction.GetComponent<ActionClass> ()._index2 = _data.actions [i]._index2;
				_initialAction.GetComponent<ActionClass> ()._value3 = _data.actions [i]._value3;
				_initialAction.GetComponent<ActionClass> ()._index3 = _data.actions [i]._index3;
				_initialAction.GetComponent<ActionClass> ()._value4 = _data.actions [i]._value4;
				_initialAction.GetComponent<ActionClass> ()._index4 = _data.actions [i]._index4;
				_initialAction.GetComponent<ActionClass> ()._scale = _data.actions [i]._scale;
				_initialAction.GetComponent<ActionClass> ()._constant1 = _data.actions [i]._constant1;
				_initialAction.GetComponent<ActionClass> ()._constant2 = _data.actions [i]._constant2;
				_initialAction.GetComponent<ActionClass> ()._editorType = _data.actions [i]._editorType;
				_initialAction.GetComponent<ActionClass> ()._isInverse = _data.actions [i]._isInverse;
				_initialAction.GetComponent<ActionClass> ()._isReset = _data.actions [i]._isReset;
                //if(_data.actions[i]._typeRobotic != null){
                _initialAction.GetComponent<ActionClass>()._typeRobotic = _data.actions[i]._typeRobotic;
                if (_data.actions[i]._childActions != null && _data.actions[i]._childActions.Length > 0)
                {
                    _initialAction.GetComponent<ActionClass>().childActions = new ActionClass[_data.actions[i]._childActions.Length];
                    for (int j = 0; j < _initialAction.GetComponent<ActionClass>().childActions.Length; j++)
                    {
                        _initialAction.GetComponent<ActionClass>().childActions[j] = new ActionClass();
                        _initialAction.GetComponent<ActionClass>().childActions[j]._type = _data.actions[i]._childActions[j]._type;
                        _initialAction.GetComponent<ActionClass>().childActions[j].childName = _data.actions[i]._childActions[j].name;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._value = _data.actions[i]._childActions[j]._value;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._index = _data.actions[i]._childActions[j]._index;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._value2 = _data.actions[i]._childActions[j]._value2;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._index2 = _data.actions[i]._childActions[j]._index2;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._value3 = _data.actions[i]._childActions[j]._value3;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._index3 = _data.actions[i]._childActions[j]._index3;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._value4 = _data.actions[i]._childActions[j]._value4;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._index4 = _data.actions[i]._childActions[j]._index4;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._scale = _data.actions[i]._childActions[j]._scale;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._constant1 = _data.actions[i]._childActions[j]._constant1;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._constant2 = _data.actions[i]._childActions[j]._constant2;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._editorType = _data.actions[i]._childActions[j]._editorType;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._isInverse = _data.actions[i]._childActions[j]._isInverse;
                        _initialAction.GetComponent<ActionClass>().childActions[j]._isReset = _data.actions[i]._childActions[j]._isReset;

                        if (j < (_initialAction.GetComponent<ActionClass>().childActions.Length-1))
                        {
                            _initialAction.GetComponent<ActionClass>().childActions[j].nextAction = _data.actions[i]._childActions[j + 1].name;
                        }
                        
                    }

                }
                
                //}
            }
		}

		if(_data.transitions.Length > 0){
			for (int i = 0; i < _data.transitions.Length; i++) {
				yield return new WaitForSeconds (0.01f);
				GameObject _initialTransition = GameObject.Find ("transition_base");
				_initialTransition.transform.SetParent ( _workbenchArea.transform , true);
				_initialTransition.GetComponent<TransitionClass> ().cloneElement ();
				_initialTransition.GetComponent<TransitionClass> ().initializeElement ();

				_initialTransition.transform.localPosition = new Vector3 (_data.transitions [i].x, _data.transitions [i].y, 0);
				_initialTransition.GetComponent<TransitionClass> ()._titleElement.text = _data.transitions [i]._title;
				if( _data.transitions [i]._elements.Length > 0){
					List<operatorStructure> _opElements = new List<operatorStructure> ();
					for(int j = 0; j < _data.transitions [i]._elements.Length; j++){
						_opElements.Add (_data.transitions [i]._elements [j]);
					}
					_initialTransition.GetComponent<TransitionClass> ()._elements = _opElements;
				}
			}
		}

		if(_data.convergences.Length > 0){
			for (int i = 0; i < _data.convergences.Length; i++) {
				yield return new WaitForSeconds (0.01f);
				GameObject _initialConvergence = GameObject.Find ("convergence_base");
				_initialConvergence.transform.SetParent ( _workbenchArea.transform , true);
				_initialConvergence.GetComponent<ConvergenceClass> ().cloneElement ();

				_initialConvergence.GetComponent<ConvergenceClass> ()._type = _data.convergences [i]._type;
				_initialConvergence.GetComponent<ConvergenceClass> ()._editorType = _data.convergences [i]._editorType;
				_initialConvergence.GetComponent<ConvergenceClass> ()._branches = _data.convergences [i]._branches;

				_initialConvergence.GetComponent<ConvergenceClass> ().initializeElement (true);
				_initialConvergence.transform.localPosition = new Vector3 (_data.convergences [i].x, _data.convergences [i].y, 0);
			}
		}

		if (_data.divergences.Length > 0) {
			for (int i = 0; i < _data.divergences.Length; i++) {
				yield return new WaitForSeconds (0.01f);
				GameObject _initialDivergence = GameObject.Find ("divergence_base");
				_initialDivergence.transform.SetParent ( _workbenchArea.transform , true);
				_initialDivergence.GetComponent<DivergenceClass> ().cloneElement ();

				_initialDivergence.GetComponent<DivergenceClass> ()._type = _data.divergences [i]._type;
				_initialDivergence.GetComponent<DivergenceClass> ()._editorType = _data.divergences [i]._editorType;
				_initialDivergence.GetComponent<DivergenceClass> ()._branches = _data.divergences [i]._branches;

				_initialDivergence.GetComponent<DivergenceClass> ().initializeElement (true);
				_initialDivergence.transform.localPosition = new Vector3 (_data.divergences [i].x, _data.divergences [i].y, 0);
			}
		}

		if (_data.jumps.Length > 0) {
			for (int i = 0; i < _data.jumps.Length; i++) {
				yield return new WaitForSeconds (0.01f);
				GameObject _initialJump = GameObject.Find ("jump_base");
				_initialJump.transform.SetParent ( _workbenchArea.transform , true);
				_initialJump.GetComponent<JumpClass> ().cloneElement ();
				_initialJump.GetComponent<JumpClass> ().initializeElement ();

				_initialJump.transform.localPosition = new Vector3 (_data.jumps [i].x, _data.jumps [i].y, 0);

				GameObject _startObject = GameObject.Find (_data.jumps[i].startObject);
				GameObject _endObject = GameObject.Find (_data.jumps[i].endObject);
                if (_data.jumps[i].startObject.IndexOf("convergence") != -1)
                {
                    _initialJump.GetComponent<JumpClass>().objectToFollow = _startObject.GetComponent<ConvergenceClass>()._bottomConnector.gameObject;
                }
                else
                {
                    _initialJump.GetComponent<JumpClass>().objectToFollow = _startObject.GetComponent<TransitionClass>()._bottomConnector.gameObject;
                }

				_initialJump.GetComponent<JumpClass> ().startObject = _startObject;
				_initialJump.GetComponent<JumpClass> ().endObject = _endObject;
				_initialJump.GetComponent<JumpClass> ().activateLines ();
				_initialJump.GetComponent<JumpClass> ().setlookingForNewLeft ();
			}
		}

		yield return new WaitForSeconds (0.1f);

		//line connections
		if (_data.steps.Length > 0) {
			for(int i = 0; i < _data.steps.Length; i++){
				if(_data.steps[i]._u_conn._id != -1){
					GameObject _element = GameObject.Find ("step_" + i.ToString());
					if (i == 0) {
						if(_data.steps [i]._u_conn._line.endConnector != null){
							_element.GetComponent<InitialStepClass> ()._topConnector.GetComponent<connModuleClass> ()._location = _data.steps [i]._u_conn._location;
							_element.GetComponent<InitialStepClass> ()._topConnector.GetComponent<connModuleClass> ()._id = _data.steps [i]._u_conn._id;
							_element.GetComponent<InitialStepClass> ()._topConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.steps [i]._u_conn._line.lineStaged, _data.steps [i]._u_conn._line.endConnector, _data.steps [i]._u_conn._line.endConnectorParent);
						}
					} else {
						_element.GetComponent<StepClass> ()._topConnector.GetComponent<connModuleClass> ()._location = _data.steps [i]._u_conn._location;
						_element.GetComponent<StepClass> ()._topConnector.GetComponent<connModuleClass> ()._id = _data.steps [i]._u_conn._id;
						_element.GetComponent<StepClass> ()._topConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.steps [i]._u_conn._line.lineStaged, _data.steps [i]._u_conn._line.endConnector, _data.steps [i]._u_conn._line.endConnectorParent);
					}
				}

				if(_data.steps[i]._d_conn._id != -1){
					GameObject _element = GameObject.Find ("step_" + i.ToString());
					if (i == 0) {
						_element.GetComponent<InitialStepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._location = _data.steps [i]._d_conn._location;
						_element.GetComponent<InitialStepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._id = _data.steps [i]._d_conn._id;
						_element.GetComponent<InitialStepClass> ()._bottomConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.steps [i]._d_conn._line.lineStaged, _data.steps [i]._d_conn._line.endConnector, _data.steps [i]._d_conn._line.endConnectorParent);
					} else {
						_element.GetComponent<StepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._location = _data.steps [i]._d_conn._location;
						_element.GetComponent<StepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._id = _data.steps [i]._d_conn._id;
						_element.GetComponent<StepClass> ()._bottomConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.steps [i]._d_conn._line.lineStaged, _data.steps [i]._d_conn._line.endConnector, _data.steps [i]._d_conn._line.endConnectorParent);
					}
				}

				if(_data.steps[i]._r_conn._id != -1){
					GameObject _element = GameObject.Find ("step_" + i.ToString());
					if (i == 0) {
						_element.GetComponent<InitialStepClass> ()._rightConnector.GetComponent<connModuleClass> ()._location = _data.steps [i]._r_conn._location;
						_element.GetComponent<InitialStepClass> ()._rightConnector.GetComponent<connModuleClass> ()._id = _data.steps [i]._r_conn._id;
						_element.GetComponent<InitialStepClass> ()._rightConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.steps [i]._r_conn._line.lineStaged, _data.steps [i]._r_conn._line.endConnector, _data.steps [i]._r_conn._line.endConnectorParent);
					} else {
						_element.GetComponent<StepClass> ()._rightConnector.GetComponent<connModuleClass> ()._location = _data.steps [i]._r_conn._location;
						_element.GetComponent<StepClass> ()._rightConnector.GetComponent<connModuleClass> ()._id = _data.steps [i]._r_conn._id;
						_element.GetComponent<StepClass> ()._rightConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.steps [i]._r_conn._line.lineStaged, _data.steps [i]._r_conn._line.endConnector, _data.steps [i]._r_conn._line.endConnectorParent);
					}
				}
			}
		}

		if (_data.actions.Length > 0) {
			for (int i = 0; i < _data.actions.Length; i++) {
				if (_data.actions [i]._l_conn._id != -1) {
					GameObject _element = GameObject.Find ("action_" + i.ToString());
					_element.GetComponent<ActionClass> ()._leftConnector.GetComponent<connModuleClass> ()._location = _data.actions [i]._l_conn._location;
					_element.GetComponent<ActionClass> ()._leftConnector.GetComponent<connModuleClass> ()._id = _data.actions [i]._l_conn._id;
					_element.GetComponent<ActionClass> ()._leftConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.actions [i]._l_conn._line.lineStaged, _data.actions [i]._l_conn._line.endConnector, _data.actions [i]._l_conn._line.endConnectorParent);
				}

				if (_data.actions [i]._r_conn._id != -1) {
					GameObject _element = GameObject.Find ("action_" + i.ToString());
					_element.GetComponent<ActionClass> ()._rightConnector.GetComponent<connModuleClass> ()._location = _data.actions [i]._r_conn._location;
					_element.GetComponent<ActionClass> ()._rightConnector.GetComponent<connModuleClass> ()._id = _data.actions [i]._r_conn._id;
					_element.GetComponent<ActionClass> ()._rightConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.actions [i]._r_conn._line.lineStaged, _data.actions [i]._r_conn._line.endConnector, _data.actions [i]._r_conn._line.endConnectorParent);
				}
			}
		}

		if (_data.transitions.Length > 0) {
			for (int i = 0; i < _data.transitions.Length; i++) {
				if (_data.transitions [i]._u_conn._id != -1) {
					GameObject _element = GameObject.Find ("transition_" + i.ToString());
					_element.GetComponent<TransitionClass> ()._topConnector.GetComponent<connModuleClass> ()._location = _data.transitions [i]._u_conn._location;
					_element.GetComponent<TransitionClass> ()._topConnector.GetComponent<connModuleClass> ()._id = _data.transitions [i]._u_conn._id;
					_element.GetComponent<TransitionClass> ()._topConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.transitions [i]._u_conn._line.lineStaged, _data.transitions [i]._u_conn._line.endConnector, _data.transitions [i]._u_conn._line.endConnectorParent);
				}

				if (_data.transitions [i]._d_conn._id != -1) {
					GameObject _element = GameObject.Find ("transition_" + i.ToString());
					_element.GetComponent<TransitionClass> ()._bottomConnector.GetComponent<connModuleClass> ()._location = _data.transitions [i]._d_conn._location;
					_element.GetComponent<TransitionClass> ()._bottomConnector.GetComponent<connModuleClass> ()._id = _data.transitions [i]._d_conn._id;
					_element.GetComponent<TransitionClass> ()._bottomConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.transitions [i]._d_conn._line.lineStaged, _data.transitions [i]._d_conn._line.endConnector, _data.transitions [i]._d_conn._line.endConnectorParent);
				}
			}
		}

		if (_data.convergences.Length > 0) {
			for (int i = 0; i < _data.convergences.Length; i++) {
				if (_data.convergences [i]._d_conn._id != -1) {
					GameObject _element = GameObject.Find ("convergence_" + i.ToString());
					_element.GetComponent<ConvergenceClass> ()._bottomConnector.GetComponent<connModuleClass> ()._location = _data.convergences [i]._d_conn._location;
					_element.GetComponent<ConvergenceClass> ()._bottomConnector.GetComponent<connModuleClass> ()._id = _data.convergences [i]._d_conn._id;
					_element.GetComponent<ConvergenceClass> ()._bottomConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.convergences [i]._d_conn._line.lineStaged, _data.convergences [i]._d_conn._line.endConnector, _data.convergences [i]._d_conn._line.endConnectorParent);
				}

				if (_data.convergences [i].conns.Length > 0) {
					GameObject _element = GameObject.Find ("convergence_" + i.ToString());
					for(int j = 0; j < _data.convergences [i].conns.Length; j++){
						if(_data.convergences [i].conns[j]._id != -1){
							_element.GetComponent<ConvergenceClass> ()._topConnectors [j].GetComponentInChildren<connModuleClass> ()._location = _data.convergences [i].conns [j]._location;
							_element.GetComponent<ConvergenceClass> ()._topConnectors [j].GetComponentInChildren<connModuleClass> ()._id = _data.convergences [i].conns [j]._id;
							_element.GetComponent<ConvergenceClass> ()._topConnectors [j].GetComponentInChildren<connModuleClass> ().createLoadedLine (_data.convergences [i].conns [j]._line.lineStaged, _data.convergences [i].conns [j]._line.endConnector, _data.convergences [i].conns [j]._line.endConnectorParent);
						}
					}
				}
			}
		}

		if (_data.divergences.Length > 0) {
			for (int i = 0; i < _data.divergences.Length; i++) {
				if (_data.divergences [i]._u_conn._id != -1) {
					GameObject _element = GameObject.Find ("divergence_" + i.ToString());
					_element.GetComponent<DivergenceClass> ()._topConnector.GetComponent<connModuleClass> ()._location = _data.divergences [i]._u_conn._location;
					_element.GetComponent<DivergenceClass> ()._topConnector.GetComponent<connModuleClass> ()._id = _data.divergences [i]._u_conn._id;
					_element.GetComponent<DivergenceClass> ()._topConnector.GetComponent<connModuleClass> ().createLoadedLine (_data.divergences [i]._u_conn._line.lineStaged, _data.divergences [i]._u_conn._line.endConnector, _data.divergences [i]._u_conn._line.endConnectorParent);
				}

				if (_data.divergences [i].conns.Length > 0) {
					GameObject _element = GameObject.Find ("divergence_" + i.ToString());
					for(int j = 0; j < _data.divergences [i].conns.Length; j++){
						if(_data.divergences [i].conns[j]._id != -1){
							_element.GetComponent<DivergenceClass> ()._bottomConnectors [j].GetComponentInChildren<connModuleClass> ()._location = _data.divergences [i].conns [j]._location;
							_element.GetComponent<DivergenceClass> ()._bottomConnectors [j].GetComponentInChildren<connModuleClass> ()._id = _data.divergences [i].conns [j]._id;
							_element.GetComponent<DivergenceClass> ()._bottomConnectors [j].GetComponentInChildren<connModuleClass> ().createLoadedLine (_data.divergences [i].conns [j]._line.lineStaged, _data.divergences [i].conns [j]._line.endConnector, _data.divergences [i].conns [j]._line.endConnectorParent);
						}
					}
				}
			}
		}

		yield return new WaitForSeconds (0.5f);
		_loadingElement.GetComponent<loadingClass> ().isActive = false;
		_loadingElement.SetActive (false);
	}

	public void resetScenario(){
		if(simulationRunning == true){
			simulationRunning = false;
            grafcetEngine.ResetIO();
            grafcetEngine = null;
		}

		int _stepsModules = stepsModules;
		int _actionModules = actionModules;
		int _transitionModules = transitionModules;
		int _convergenceModules = convergenceModules;
		int _divergenceModules = divergenceModules;
		int _jumpModules = jumpModules;

		//jumps
		for (int i = (_jumpModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("jump_" + i.ToString ());
			_element.GetComponent<JumpClass> ().deleteAction ();
		}

		//convergences
		for (int i = (_convergenceModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("convergence_" + i.ToString ());
			_element.GetComponent<ConvergenceClass> ().deleteAction ();
		}

		//divergences
		for (int i = (_divergenceModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("divergence_" + i.ToString ());
			_element.GetComponent<DivergenceClass> ().deleteAction ();
		}

		//transitions
		for (int i = (_transitionModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("transition_" + i.ToString ());
			_element.GetComponent<TransitionClass> ().deleteAction ();
		}

		//actions
		for (int i = (_actionModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("action_" + i.ToString ());
			_element.GetComponent<ActionClass> ().deleteAction ();
		}

		//steps
		for (int i = (_stepsModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("step_" + i.ToString ());
			if (i == 0) {
				_element.GetComponent<InitialStepClass> ().deleteAction ();
			} else {
				_element.GetComponent<StepClass> ().deleteAction ();
			}

		}

		lineModules = 0;
	}

	public void deactivateEditWithoutElement(string _nameNoDeactivate = ""){
		int _stepsModules = stepsModules;
		int _actionModules = actionModules;
		int _transitionModules = transitionModules;
		int _convergenceModules = convergenceModules;
		int _divergenceModules = divergenceModules;
		int _jumpModules = jumpModules;

		//jumps
		for (int i = (_jumpModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("jump_" + i.ToString ());
			if(_element.name != _nameNoDeactivate){
				if(_element.GetComponent<JumpClass> ()._menuActive == true){
					_element.GetComponent<JumpClass> ().ObjectClick_ (null);
				}
			}
		}

		//convergences
		for (int i = (_convergenceModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("convergence_" + i.ToString ());
			if(_element.name != _nameNoDeactivate){
				if(_element.GetComponent<ConvergenceClass> ()._menuActive == true){
					_element.GetComponent<ConvergenceClass> ().ObjectClicked_ (null);
				}
			}
		}

		//divergences
		for (int i = (_divergenceModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("divergence_" + i.ToString ());
			if(_element.name != _nameNoDeactivate){
				if(_element.GetComponent<DivergenceClass> ()._menuActive == true){
					_element.GetComponent<DivergenceClass> ().ObjectClicked_ (null);
				}
			}
		}

		//transitions
		for (int i = (_transitionModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("transition_" + i.ToString ());
			if (_element.name != _nameNoDeactivate) {
				if(_element.GetComponent<TransitionClass> ()._menuActive == true){
					_element.GetComponent<TransitionClass> ().ObjectClicked_ (null);
				}
			}
		}

		//actions
		for (int i = (_actionModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("action_" + i.ToString ());
			if (_element.name != _nameNoDeactivate) {
				if(_element.GetComponent<ActionClass> ()._menuActive == true){
					_element.GetComponent<ActionClass> ().ObjectClicked_ (null);
				}
			}
		}

		//steps
		for (int i = (_stepsModules - 1); i >= 0; i--) {
			GameObject _element = GameObject.Find ("step_" + i.ToString ());
			if (i == 0) {
				if (_element.name != _nameNoDeactivate) {
					if(_element.GetComponent<InitialStepClass> ()._menuActive == true){
						_element.GetComponent<InitialStepClass> ().ObjectClicked_ (null);
					}
				}
			} else {
				if (_element.name != _nameNoDeactivate) {
					if(_element.GetComponent<StepClass> ()._menuActive == true){
						_element.GetComponent<StepClass> ().ObjectClicked_ (null);
					}
				}
			}

		}
	}

	private void saveActionPrev(){
        //TODO: check exists data
        if (
            stepsModules != 0 ||
            actionModules != 0 ||
            transitionModules != 0 ||
            convergenceModules != 0 ||
            divergenceModules != 0 ||
            jumpModules != 0 ||
            lineModules != 0
        )
        {
            StartCoroutine(saveAction());
        }
	}

	private IEnumerator saveAction(){
		string _data = generateExportData ();
		_data = _data.Replace ("\"_line\":{\"_id\":-1,\"name\":\"\",\"startConnector\":\"\",\"startConnectorParent\":\"\",\"endConnector\":\"\",\"endConnectorParent\":\"\",\"lineStaged\":false,\"lineName\":\"\"}", "\"_line\":null");


		if ( Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer) {
			string extensions = "vgraf";

			path = FileBrowser.SaveFile (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='fileDialog']").InnerText, "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='fileDefaultName']").InnerText, extensions);

			//Debug.Log("Save file: " + path);

			if (path == "") {
				yield break;
			}

		}
		else if( Application.platform == RuntimePlatform.Android){

			GameObject saveAsWindow = GameObject.Find ("saveWindow");
			if(saveAsWindow != null){
				saveAsWindow.GetComponent<SaveWindow> ().showWindow (gameObject, _data);
			}
		}
		else if(Application.platform == RuntimePlatform.IPhonePlayer){
			path = Application.persistentDataPath + "/" + Manager.Instance.currentPDFName + ".vgraf";
		}

		byte[] bytes = System.Text.Encoding.UTF8.GetBytes (_data);

		if (Application.platform == RuntimePlatform.WebGLPlayer) {
			saveFiletoWebGL (_data);
		} else if (Application.platform != RuntimePlatform.Android) {
			File.WriteAllBytes (path, bytes);
			closeFile ();
		}
	}

	void closeFile(){
		if (Application.platform != RuntimePlatform.WebGLPlayer) {
		}
	}

	private void saveFiletoWebGL(string dataToSave){
		Application.ExternalCall ("saveToFile", dataToSave, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='fileDefaultName']").InnerText + ".vgraf");
	}

	private void compileAction(){
		string _data = generateExportData (Manager.Instance.globalRoboticsMode);
		//Debug.Log (_data);

		grafcetEngine = new grafcetController(_inputsBool, _inputsByte, _outputsBool, _outputsByte, _data);

		// Invoco el evento de compilación
		_grafcetCompile?.Invoke(grafcetEngine);

		simulationRunning = true;

		showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='successful_comp']").InnerText);
	}

	private string generateExportData(bool compileForRoboticsMode = false){
		string _result = "";

		grafcetStructure _grafcetData = new grafcetStructure ();
		_grafcetData.work_area = getJSONData ();

		if(stepsModules > 0){
			stepStructure[] _steps = new stepStructure[stepsModules];
			for(int i = 0; i < stepsModules; i++){
				if (i == 0) {
					_steps [i] = GameObject.Find ("step_" + i.ToString ()).GetComponent<InitialStepClass> ().getJSONData ();
				} else {
					_steps [i] = GameObject.Find ("step_" + i.ToString ()).GetComponent<StepClass> ().getJSONData ();
				}
			}
			_grafcetData.steps = _steps;
		}

		if(actionModules > 0){
            if (compileForRoboticsMode == false)
            {
                actionStructure[] _actions = new actionStructure[actionModules];
                for (int i = 0; i < actionModules; i++)
                {
                    ActionClass actionElement = GameObject.Find("action_" + i.ToString()).GetComponent<ActionClass>();
                    _actions[i] = actionElement.getJSONData();

                }
                _grafcetData.actions = _actions;
            }
            else
            {
                List<actionStructure> listActionRobotic = new List<actionStructure>();
                for (int i = 0; i < actionModules; i++)
                {
                    ActionClass actionElement = GameObject.Find("action_" + i.ToString()).GetComponent<ActionClass>();
                    //Debug.Log(actionElement.name);
                    //Debug.Log(actionElement.childActions.Length);
                    for (int j = 0; j < actionElement.childActions.Length; j++)
                    {
                        listActionRobotic.Add(actionElement.childActions[j].getJSONData());
                    }
                }
                //Debug.Log(listActionRobotic.Count);
                actionStructure[] _actions = new actionStructure[listActionRobotic.Count];
                for (int i = 0; i < listActionRobotic.Count; i++)
                {
                    _actions[i] = listActionRobotic[i];
                }
                _grafcetData.actions = _actions;
            }
        }

		if(transitionModules > 0){
			transitionStructure[] _transitions = new transitionStructure[transitionModules];
			for (int i = 0; i < transitionModules; i++) {
                TransitionClass transitionElement = GameObject.Find("transition_" + i.ToString()).GetComponent<TransitionClass>();

                _transitions[i] = transitionElement.getJSONData(compileForRoboticsMode);

            }
			_grafcetData.transitions = _transitions;
		}

		if(divergenceModules > 0){
			divergenceStructure[] _divergences = new divergenceStructure[divergenceModules];
			for (int i = 0; i < divergenceModules; i++) {
				_divergences [i] = GameObject.Find ("divergence_" + i.ToString ()).GetComponent<DivergenceClass> ().getJSONData ();
			}
			_grafcetData.divergences = _divergences;
		}

		if(convergenceModules > 0){
			convergenceStructure[] _convergences = new convergenceStructure[convergenceModules];
			for (int i = 0; i < convergenceModules; i++) {
				_convergences [i] = GameObject.Find ("convergence_" + i.ToString ()).GetComponent<ConvergenceClass> ().getJSONData ();
			}
			_grafcetData.convergences = _convergences;
		}

		if(jumpModules > 0){
			jumpStructure[] _jumps = new jumpStructure[jumpModules];
			for (int i = 0; i < jumpModules; i++) {
				_jumps [i] = GameObject.Find ("jump_" + i.ToString ()).GetComponent<JumpClass> ().getJSONData ();
			}
			_grafcetData.jumps = _jumps;
		}

		_result = JsonUtility.ToJson (_grafcetData);
		return _result;
	}

	private void helpAction(){

		switch(Manager.Instance.globalGraphicInterface){
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert = Instantiate(_situation.GetComponent<ElectricalTestBenchMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._alert.GetComponent<Alert>().showAlert(4, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='help']").InnerText, "", Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<ElectricalTestBenchMainClass>().situationTag + "/tips").InnerText);
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass>()._alert = Instantiate(_situation.GetComponent<PnematicTestBenchClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<PnematicTestBenchClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<PnematicTestBenchClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			_situation.GetComponent<PnematicTestBenchClass>()._alert.GetComponent<Alert>().showAlert(4, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='help']").InnerText, "", Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<PnematicTestBenchClass>().situationTag + "/tips").InnerText);
			break;
		case baseSystem.graphicInterfaces.none:
			_situation.GetComponent<onlyControllerMainClass>()._alert = Instantiate(_situation.GetComponent<onlyControllerMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<onlyControllerMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<onlyControllerMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
			_situation.GetComponent<onlyControllerMainClass>()._alert.GetComponent<Alert>().showAlert(4, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='help']").InnerText, "", Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/tips").InnerText);
			break;
        case baseSystem.graphicInterfaces.roboticInterface:
            _situation.GetComponent<onlyControllerMainClass>()._alert = Instantiate(_situation.GetComponent<onlyControllerMainClass>()._alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            _situation.GetComponent<onlyControllerMainClass>()._alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
            _situation.GetComponent<onlyControllerMainClass>()._alert.transform.localPosition = new Vector3(-700, 700, 0);
            _situation.GetComponent<onlyControllerMainClass>()._alert.GetComponent<Alert>().showAlert(4, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='help']").InnerText, "", Manager.Instance.globalInfo.SelectSingleNode("/data/" + _situation.GetComponent<onlyControllerMainClass>().situationTag + "/tips").InnerText);
            break;
		}
	}

	private void reportAction(){

		List<reportImgElement> _list = new List<reportImgElement>();
		List<reportExtraPageStructure> _extraPagesData = null;

		//check if extra pages is required
		float _widthMaxSize = 0;
		float _heightMaxSize = _initialAreaHeight;
		if (leftMenuHidden == true) {
			_widthMaxSize = 1885;
		} else {
			_widthMaxSize = 1720;
		}

		if (_workbenchArea.GetComponent<RectTransform> ().rect.width > _widthMaxSize || _workbenchArea.GetComponent<RectTransform> ().rect.height > _heightMaxSize) {
			_extraPagesData = new List<reportExtraPageStructure> ();

			//decide number of pages to take
			int widthDivisions = Mathf.CeilToInt(_workbenchArea.GetComponent<RectTransform> ().rect.width / _widthMaxSize);
			int heightDivisions = Mathf.CeilToInt(_workbenchArea.GetComponent<RectTransform> ().rect.height / _heightMaxSize);

			Vector3 _initialPosition = _workbenchArea.transform.localPosition;
			_workbenchContainer.GetComponent<ScrollRect> ().enabled = false;
			GameObject.Find ("Canvas/Grafcet/workbench/Scrollbar Horizontal").SetActive(false);
			GameObject.Find ("Canvas/Grafcet/workbench/Scrollbar Vertical").SetActive(false);

			Vector3 iniPositionIndicator = new Vector3();
			switch (Manager.Instance.globalGraphicInterface) {
			case baseSystem.graphicInterfaces.electrical_bench:
				iniPositionIndicator = _situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x + 1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.pneumatic_bench:
				iniPositionIndicator = _situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x + 1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.none:
            case baseSystem.graphicInterfaces.roboticInterface:
				iniPositionIndicator = _situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x + 1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			}
			_menuLeftBt.gameObject.SetActive (false);

			reportExtraPageStructure _extraPageData = new reportExtraPageStructure ();

			for(int i = 0; i < widthDivisions; i++){
				for(int j = 0; j < heightDivisions; j++){
					if (i == 0 && j == 0) {
						//First page
						_workbenchArea.transform.localPosition = new Vector3(0,0,0);

						reportImgElement _img1 = new reportImgElement ();
						Texture2D _screenShot1 = new Texture2D (100, 100);

						_screenShot1 = screenCapture.captureImage (new Rect (1920 - _widthMaxSize + 1f, (_initialAreaHeight + 114f) / 2f, _widthMaxSize - 1f, _initialAreaHeight), "Main Camera");

						_img1.type = 1;
						_img1.title1 = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='grafcet_report_title']").InnerText;
						_img1.height1 = 900;
						_img1.image1 = _screenShot1;
						_list.Add (_img1);
					} else {
						//Extra pages
						_workbenchArea.transform.localPosition = new Vector3(-_widthMaxSize*(float)i,_heightMaxSize*(float)j,0);

						reportImgElement _imgExtra = new reportImgElement ();
						Texture2D _screenShotExtra = new Texture2D (100, 100);
						_screenShotExtra = screenCapture.captureImage (new Rect (1920 - _widthMaxSize + 1f, (_initialAreaHeight + 114f) / 2f, _widthMaxSize - 1f, _initialAreaHeight), "Main Camera");
						_imgExtra.type = 1;
						_imgExtra.title1 = "";
						_imgExtra.height1 = 900;
						_imgExtra.image1 = _screenShotExtra;

						if(_extraPageData._listElements == null){
							_extraPageData._listElements = new List<reportImgElement> ();
						}

						_extraPageData._listElements.Add (_imgExtra);

						if(_extraPageData._listElements.Count == 2 || (i == (widthDivisions-1) && j == (heightDivisions-1))){
							_extraPagesData.Add (_extraPageData);
							_extraPageData = new reportExtraPageStructure ();
						}
					}
				}
			}
			_workbenchArea.transform.localPosition = _initialPosition;
			GameObject.Find ("Canvas/Grafcet/workbench/Scrollbar Horizontal").SetActive(true);
			GameObject.Find ("Canvas/Grafcet/workbench/Scrollbar Vertical").SetActive(true);
			_workbenchContainer.GetComponent<ScrollRect> ().enabled = true;

			switch (Manager.Instance.globalGraphicInterface) {
			case baseSystem.graphicInterfaces.electrical_bench:
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.pneumatic_bench:
				_situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.none:
            case baseSystem.graphicInterfaces.roboticInterface: 
				_situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			}
			_menuLeftBt.gameObject.SetActive (true);

		} else {

			Vector3 _initialPosition = _workbenchArea.transform.localPosition;
			_workbenchContainer.GetComponent<ScrollRect> ().enabled = false;
			GameObject.Find ("Canvas/Grafcet/workbench/Scrollbar Horizontal").SetActive(false);
			GameObject.Find ("Canvas/Grafcet/workbench/Scrollbar Vertical").SetActive(false);

			reportImgElement _img1 = new reportImgElement ();
			Texture2D _screenShot1 = new Texture2D (100, 100);

			Vector3 iniPositionIndicator = new Vector3 ();
			switch (Manager.Instance.globalGraphicInterface) {
			case baseSystem.graphicInterfaces.electrical_bench:
				iniPositionIndicator = _situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x+1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.pneumatic_bench:
				iniPositionIndicator = _situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x+1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.none:
            case baseSystem.graphicInterfaces.roboticInterface:
				iniPositionIndicator = _situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition;
				_situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x+1000f, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			}

			_menuLeftBt.gameObject.SetActive (false);
			if (leftMenuHidden == true) {
				_screenShot1 = screenCapture.captureImage (new Rect (35, (_initialAreaHeight + 114f) / 2f, 1885, _initialAreaHeight), "Main Camera");
			} else {
				_screenShot1 = screenCapture.captureImage (new Rect (201, (_initialAreaHeight + 114f) / 2f, 1719, _initialAreaHeight), "Main Camera");
			}

			switch (Manager.Instance.globalGraphicInterface) {
			case baseSystem.graphicInterfaces.electrical_bench:
				_situation.GetComponent<ElectricalTestBenchMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.pneumatic_bench:
				_situation.GetComponent<PnematicTestBenchClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			case baseSystem.graphicInterfaces.none:
            case baseSystem.graphicInterfaces.roboticInterface:
				_situation.GetComponent<onlyControllerMainClass> ()._upperIndicator.transform.localPosition = new Vector3 (iniPositionIndicator.x, iniPositionIndicator.y, iniPositionIndicator.z);
				break;
			}

			_menuLeftBt.gameObject.SetActive (true);

			_img1.type = 1;
			_img1.title1 = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='grafcet_report_title']").InnerText;
			_img1.height1 = 900;
			_img1.image1 = _screenShot1;
			_list.Add (_img1);

			_workbenchArea.transform.localPosition = _initialPosition;
			GameObject.Find ("Canvas/Grafcet/workbench/Scrollbar Horizontal").SetActive(true);
			GameObject.Find ("Canvas/Grafcet/workbench/Scrollbar Vertical").SetActive(true);
			_workbenchContainer.GetComponent<ScrollRect> ().enabled = true;
		}

		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass>()._evaluation = Instantiate(_situation.GetComponent<ElectricalTestBenchMainClass>()._evaluationPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<ElectricalTestBenchMainClass>()._evaluation.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._evaluation.transform.localPosition = new Vector3(0, 1200, 0);
			_situation.GetComponent<ElectricalTestBenchMainClass>()._evaluation.name = "SitEvaluation";
			_situation.GetComponent<ElectricalTestBenchMainClass> ()._evaluation.GetComponent<Evaluation> ().startEvaluation (3, 1, _situation.GetComponent<ElectricalTestBenchMainClass> ().orderOptions, 0f, _list, _extraPagesData);
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass>()._evaluation = Instantiate(_situation.GetComponent<PnematicTestBenchClass>()._evaluationPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<PnematicTestBenchClass>()._evaluation.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<PnematicTestBenchClass>()._evaluation.transform.localPosition = new Vector3(0, 1200, 0);
			_situation.GetComponent<PnematicTestBenchClass>()._evaluation.name = "SitEvaluation";
			_situation.GetComponent<PnematicTestBenchClass> ()._evaluation.GetComponent<Evaluation> ().startEvaluation (3, 1, _situation.GetComponent<PnematicTestBenchClass> ().orderOptions, 0f, _list, _extraPagesData);
			break;
		case baseSystem.graphicInterfaces.none:
        case baseSystem.graphicInterfaces.roboticInterface:
			_situation.GetComponent<onlyControllerMainClass>()._evaluation = Instantiate(_situation.GetComponent<onlyControllerMainClass>()._evaluationPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_situation.GetComponent<onlyControllerMainClass>()._evaluation.transform.SetParent(GameObject.Find("Canvas").transform, false);
			_situation.GetComponent<onlyControllerMainClass>()._evaluation.transform.localPosition = new Vector3(0, 1200, 0);
			_situation.GetComponent<onlyControllerMainClass>()._evaluation.name = "SitEvaluation";
			_situation.GetComponent<onlyControllerMainClass> ()._evaluation.GetComponent<Evaluation> ().startEvaluation (3, 1, _situation.GetComponent<onlyControllerMainClass> ().orderOptions, 0f, _list, _extraPagesData);
			break;
		}
	}

	private void trashAction(){
		resetScenario ();
	}

	private void goToAction(){
		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass>().Fundido_.transform.SetAsLastSibling ();
			_situation.GetComponent<ElectricalTestBenchMainClass>().Fundido_.CrossFadeAlpha(1, 0.5f, false);
			_situation.GetComponent<ElectricalTestBenchMainClass>().Fundido_.raycastTarget = true;
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass>().Fundido_.transform.SetAsLastSibling ();
			_situation.GetComponent<PnematicTestBenchClass>().Fundido_.CrossFadeAlpha(1, 0.5f, false);
			_situation.GetComponent<PnematicTestBenchClass>().Fundido_.raycastTarget = true;
			break;
		}

		GameObject.Find ("SystemController").GetComponent<baseSystem>().goToBench();
	}

	private void languageAction(){
		GameObject _langBt = GameObject.Find ("lang_bt_grafcet");
		_langBt.GetComponent<languageBt> ().showMenu (setNewLanguageFromMenu);
	}

	public void setNewLanguageFromMenu(){
		GameObject.Find ("SystemController").GetComponent<baseSystem>().Language = Manager.Instance.globalLanguage;
		StartCoroutine (GameObject.Find ("SystemController").GetComponent<baseSystem> ().loadLanguageXML (false, false));
		setTexts ();
		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			_situation.GetComponent<ElectricalTestBenchMainClass> ().setTextsAfterChangeLanguage ();
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			_situation.GetComponent<PnematicTestBenchClass> ().setTextsAfterChangeLanguage ();
			break;
		case baseSystem.graphicInterfaces.none:
        case baseSystem.graphicInterfaces.roboticInterface:
			break;
		}
	}

	private void leftMenuAction(){

		_workbenchContainer.GetComponent<ScrollRect> ().horizontal = false;
		_workbenchContainer.GetComponent<ScrollRect> ().vertical = false;

		if (leftMenuHidden == false) {
			leftMenuHidden = true;
			_menuLeftBt.GetComponent<Image> ().sprite = _hideBtImage;
			iTween.MoveTo(
				_leftMenu,
				iTween.Hash(
					"position", _leftMenuInactive,
					"looktarget", Camera.main,
					"easeType", iTween.EaseType.linear,
					"time", 0.3f,
					"islocal",true
				)
			);

			StartCoroutine (increaseWorkbenchArea ());

		} else {
			leftMenuHidden = false;

			_menuLeftBt.GetComponent<Image> ().sprite = _showBtImage;
			iTween.MoveTo(
				_leftMenu,
				iTween.Hash(
					"position", _leftMenuActive,
					"looktarget", Camera.main,
					"easeType", iTween.EaseType.linear,
					"time", 0.3f,
					"islocal",true
				)
			);

			StartCoroutine (reduceWorkbenchArea ());

		}

		Invoke ("activateScroll", 0.5f);
	}

	private void activateScroll(){
		_workbenchContainer.GetComponent<ScrollRect> ().horizontal = true;
		_workbenchContainer.GetComponent<ScrollRect> ().vertical = true;
	}

	private IEnumerator increaseWorkbenchArea(){
		Vector2 _data = _workbenchContainer.GetComponent<RectTransform> ().offsetMin;
		_data.x = _data.x - 17;
		_workbenchContainer.GetComponent<RectTransform> ().offsetMin = _data;
		yield return new WaitForSeconds (0.03f);
		if(_data.x > 30){
			StartCoroutine (increaseWorkbenchArea ());
		}
	}

	private IEnumerator reduceWorkbenchArea(){
		Vector2 _data = _workbenchContainer.GetComponent<RectTransform> ().offsetMin;
		_data.x = _data.x + 17;
		_workbenchContainer.GetComponent<RectTransform> ().offsetMin = _data;
		yield return new WaitForSeconds (0.03f);
		if(_data.x < 200){
			StartCoroutine (reduceWorkbenchArea ());
		}
	}

	public void activateJumpListenersAction(){
		jump_mode = true;

		GameObject[] _transitions = GameObject.FindGameObjectsWithTag ("transitionElementTag");

        GameObject[] _convergences = GameObject.FindGameObjectsWithTag("convergenceElementTag");

        for (int i = 0; i < _transitions.Length; i++){
			_transitions [i].GetComponent<TransitionClass> ().activateJumpListenerAction ();
		}

        for (int i = 0; i < _convergences.Length; i++)
        {
            _convergences[i].GetComponent<ConvergenceClass>().activateJumpListenerAction();
        }

    }

	public void deactivateJumpListenersAction(){
		jump_mode = false;

		GameObject[] _transitions = GameObject.FindGameObjectsWithTag ("transitionElementTag");
        GameObject[] _convergences = GameObject.FindGameObjectsWithTag("convergenceElementTag");

        for (int i = 0; i < _transitions.Length; i++){
			_transitions [i].GetComponent<TransitionClass> ().deactivateJumpListenerAction ();
		}

        for (int i = 0; i < _convergences.Length; i++)
        {
            _convergences[i].GetComponent<ConvergenceClass>().deactivateJumpListenerAction();
        }
    }

	public List<GameObject> getNextElements(GameObject _topElement){
		List<GameObject> _result = new List<GameObject> ();

		if(_topElement.GetComponent<InitialStepClass>() != null){
			//is initial step
			if(_topElement.GetComponent<InitialStepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line != null && _topElement.GetComponent<InitialStepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector != null){
				_result.Add(_topElement.GetComponent<InitialStepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement);	
			}
		} else if(_topElement.GetComponent<StepClass>() != null){
			//is step
			if(_topElement.GetComponent<StepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line != null && _topElement.GetComponent<StepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector != null){
				_result.Add(_topElement.GetComponent<StepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement);	
			}
		} else if(_topElement.GetComponent<TransitionClass>() != null){
			//is transition
			if(_topElement.GetComponent<TransitionClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line != null && _topElement.GetComponent<TransitionClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector != null){
				_result.Add(_topElement.GetComponent<TransitionClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement);	
			}
		} else if(_topElement.GetComponent<DivergenceClass>() != null){
			//is divergence
			for(int i = 0; i < _topElement.GetComponent<DivergenceClass> ()._bottomConnectors.Count; i++){
				if(_topElement.GetComponent<DivergenceClass> ()._bottomConnectors[i].GetComponentInChildren<connModuleClass> ()._line != null && _topElement.GetComponent<DivergenceClass> ()._bottomConnectors[i].GetComponentInChildren<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector != null){
					_result.Add(_topElement.GetComponent<DivergenceClass> ()._bottomConnectors[i].GetComponentInChildren<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement);	
				}
			}
		} else if(_topElement.GetComponent<ConvergenceClass>() != null){
			//is convergence
			if(_topElement.GetComponent<ConvergenceClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line != null && _topElement.GetComponent<ConvergenceClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector != null){
				_result.Add(_topElement.GetComponent<ConvergenceClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement);	
			}
		}

		return _result;
	}

	public GameObject getNextElement(GameObject _topElement){
		GameObject _result = null;

		if(_topElement && _topElement.GetComponent<InitialStepClass>() != null){
			//is initial step
			if(_topElement.GetComponent<InitialStepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line != null && _topElement.GetComponent<InitialStepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector != null){
				_result = _topElement.GetComponent<InitialStepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement;	
			}
		} else if(_topElement && _topElement.GetComponent<StepClass>() != null){
			//is step
			if(_topElement.GetComponent<StepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line != null && _topElement.GetComponent<StepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector != null){
				_result = _topElement.GetComponent<StepClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement;	
			}
		} else if(_topElement && _topElement.GetComponent<TransitionClass>() != null){
			//is transition
			if(_topElement.GetComponent<TransitionClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line != null && _topElement.GetComponent<TransitionClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector != null){
				_result = _topElement.GetComponent<TransitionClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement;	
			}
		} else if(_topElement && _topElement.GetComponent<DivergenceClass>() != null){
			//is divergence
		} else if(_topElement && _topElement.GetComponent<ConvergenceClass>() != null){
			//is convergence
			if(_topElement.GetComponent<ConvergenceClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line != null && _topElement.GetComponent<ConvergenceClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector != null){
				_result = _topElement.GetComponent<ConvergenceClass> ()._bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement;	
			}
		}

		return _result;
	}

	public workAreaStructure getJSONData(){
		workAreaStructure _result = new workAreaStructure ();
		_result.width = _workbenchArea.GetComponent<RectTransform> ().rect.width;
		_result.height = _workbenchArea.GetComponent<RectTransform> ().rect.height;
		_result.stepsModules = stepsModules;
		_result.actionModules = actionModules;
		_result.transitionModules = transitionModules;
		_result.convergenceModules = convergenceModules;
		_result.divergenceModules = divergenceModules;
		_result.jumpModules = jumpModules;
		_result.lineModules = lineModules;
		return _result;
	}

    private void FixedUpdate()
    {
        if (simulationRunning == true)
        {
            if (grafcetEngine != null)
            {
                bool[] _digitalInputsArray;
                bool[] _outputVaues;
                switch (Manager.Instance.globalGraphicInterface)
                {
                    case baseSystem.graphicInterfaces.electrical_bench:
                        _digitalInputsArray = new bool[_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements.Length];
                        for (int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().inputElements.Length; i++)
                        {
                            if (_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i])
                            {
                                if (_situation.GetComponent<ElectricalTestBenchMainClass>().inputElements[i].GetComponent<dragElement>().actualValue == 1)
                                {
                                    _digitalInputsArray[i] = true;
                                }
                                else
                                {
                                    _digitalInputsArray[i] = false;
                                }
                            }
                            else
                            {
                                _digitalInputsArray[i] = false;
                            }
                        }

                        grafcetEngine.EntradasBool = _digitalInputsArray;

                        if (analogInputs > 0)
                        {
                            byte[] _analogInputsArray = new byte[analogInputs];
                            grafcetEngine.EntradasByte = _analogInputsArray;
                        }

                        grafcetEngine.runProgram();

                        _outputVaues = grafcetEngine.SalidasBool;
                        _situation.GetComponent<ElectricalTestBenchMainClass>().outputsValues = _outputVaues;

                        for (int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements.Length; i++)
                        {
                            if (_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i])
                            {
                                if (_outputVaues[i] == true)
                                {
                                    _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>().setValue(1);
                                }
                                else
                                {
                                    _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>().setValue(0);
                                }
                            }
                        }
                        break;
                    case baseSystem.graphicInterfaces.pneumatic_bench:
                        _digitalInputsArray = new bool[_situation.GetComponent<PnematicTestBenchClass>().inputElements.Length];
                        for (int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().inputElements.Length; i++)
                        {
                            if (_situation.GetComponent<PnematicTestBenchClass>().inputElements[i])
                            {
                                if (_situation.GetComponent<PnematicTestBenchClass>().inputElements[i].GetComponent<dragElementPneumatic>().actualValue == 1)
                                {
                                    _digitalInputsArray[i] = true;
                                }
                                else
                                {
                                    _digitalInputsArray[i] = false;
                                }
                            }
                            else
                            {
                                _digitalInputsArray[i] = false;
                            }
                        }

                        grafcetEngine.EntradasBool = _digitalInputsArray;

                        if (analogInputs > 0)
                        {
                            byte[] _analogInputsArray = new byte[analogInputs];
                            grafcetEngine.EntradasByte = _analogInputsArray;
                        }

                        grafcetEngine.runProgram();

                        _outputVaues = grafcetEngine.SalidasBool;
                        _situation.GetComponent<PnematicTestBenchClass>().outputsValues = _outputVaues;

                        for (int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().outputsElements.Length; i++)
                        {
                            if (_situation.GetComponent<PnematicTestBenchClass>().outputsElements[i])
                            {

                                if (_situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<dragElementPneumatic>() != null)
                                {
                                    if (_outputVaues[i] == true)
                                    {
                                        _situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<dragElementPneumatic>().setValue(1);
                                    }
                                    else
                                    {
                                        _situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<dragElementPneumatic>().setValue(0);
                                    }
                                }
                                else if (_situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<solenoidClass>() != null)
                                {
                                    if (_outputVaues[i] == true)
                                    {
                                        _situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<solenoidClass>().setValue(1);
                                    }
                                    else
                                    {
                                        _situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<solenoidClass>().setValue(0);
                                    }
                                }
                            }
                        }
                        break;
                    case baseSystem.graphicInterfaces.roboticInterface:
                        _digitalInputsArray = RoboticSituation.GetComponent<RoboticViewer>().digitalInputs;

                        grafcetEngine.EntradasBool = _digitalInputsArray;
                        grafcetEngine.Memorias = RoboticSituation.GetComponent<RoboticViewer>().memoryData;

                        if (analogInputs > 0)
                        {
                            byte[] _analogInputsArray = RoboticSituation.GetComponent<RoboticViewer>().analogInputs;
                            grafcetEngine.EntradasByte = _analogInputsArray;
                        }

                        grafcetEngine.runProgram();

                        _outputVaues = grafcetEngine.SalidasBool;
                        RoboticSituation.GetComponent<RoboticViewer>().digitalOutputs = _outputVaues;
                        RoboticSituation.GetComponent<RoboticViewer>().memoryData = grafcetEngine.Memorias;
                        RoboticSituation.GetComponent<RoboticViewer>().timersData = grafcetEngine.Temporizadores;
                        RoboticSituation.GetComponent<RoboticViewer>().countersData = grafcetEngine.Contadores;

                        break;
                }

                //TODO: switch between pneumatic and electric bench
            }
        }
        else
        {
            switch (Manager.Instance.globalController)
            {
                case baseSystem.Controllers.Grafcet:
                    switch (Manager.Instance.globalGraphicInterface)
                    {
                        case baseSystem.graphicInterfaces.electrical_bench:
                            bool[] _outputValues = new bool[_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements.Length];
                            _situation.GetComponent<ElectricalTestBenchMainClass>().outputsValues = _outputValues;

                            for (int i = 0; i < _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements.Length; i++)
                            {
                                if (_situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i])
                                {
                                    if (_outputValues[i] == true)
                                    {
                                        _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>().setValue(1);
                                    }
                                    else
                                    {
                                        _situation.GetComponent<ElectricalTestBenchMainClass>().outputsElements[i].GetComponent<dragElement>().setValue(0);
                                    }
                                }
                            }
                            break;
                        case baseSystem.graphicInterfaces.pneumatic_bench:
                            bool[] _outputValues1 = new bool[_situation.GetComponent<PnematicTestBenchClass>().outputsElements.Length];
                            _situation.GetComponent<PnematicTestBenchClass>().outputsValues = _outputValues1;

                            for (int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().outputsElements.Length; i++)
                            {
                                if (_situation.GetComponent<PnematicTestBenchClass>().outputsElements[i])
                                {

                                    if (_situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<dragElementPneumatic>() != null)
                                    {
                                        if (_outputValues1[i] == true)
                                        {
                                            _situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<dragElementPneumatic>().setValue(1);
                                        }
                                        else
                                        {
                                            _situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<dragElementPneumatic>().setValue(0);
                                        }
                                    }
                                    else if (_situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<solenoidClass>() != null)
                                    {
                                        if (_outputValues1[i] == true)
                                        {
                                            _situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<solenoidClass>().setValue(1);
                                        }
                                        else
                                        {
                                            _situation.GetComponent<PnematicTestBenchClass>().outputsElements[i].GetComponent<solenoidClass>().setValue(0);
                                        }
                                    }
                                }
                            }
                            break;
                    }
                    break;
            }
        }
    }

    void Update () {
		if(stepsModules > 0 || transitionModules> 0 || divergenceModules > 0 || convergenceModules > 0 || actionModules > 0){
			float _maxStepPosition = 0;
			float _maxYPosition = 0;
			float _height = 0;
            if (GameObject.Find("step_0") && GameObject.Find("step_0").GetComponent<InitialStepClass>()._active == true)
            {
                _maxStepPosition = GameObject.Find("step_0").transform.localPosition.x + GameObject.Find("step_0").GetComponent<InitialStepClass>()._totalWidth;
                _maxYPosition = GameObject.Find("step_0").transform.localPosition.y;
            }

			for(int i = 1; i < stepsModules; i++){
				StepClass _step = GameObject.Find ("step_" + i.ToString ()).GetComponent<StepClass> ();
				if((_maxStepPosition) < (_step.gameObject.transform.localPosition.x + _step._totalWidth)){
					_maxStepPosition = _step.gameObject.transform.localPosition.x + _step._totalWidth;
				}

				if(_maxYPosition > _step.gameObject.transform.localPosition.y){
					_maxYPosition = _step.gameObject.transform.localPosition.y;
				}
			}

			for(int i = 0; i < transitionModules; i++){
				GameObject _trans_ele = GameObject.Find ("transition_" + i.ToString());
				if(_maxYPosition > _trans_ele.transform.localPosition.y){
					_maxYPosition = _trans_ele.transform.localPosition.y;
				}

                if ((_maxStepPosition) < (_trans_ele.gameObject.transform.localPosition.x + 100))
                {
                    _maxStepPosition = _trans_ele.gameObject.transform.localPosition.x + 100;
                }
            }

			for(int i = 0; i < divergenceModules; i++){
				GameObject _div_ele = GameObject.Find ("divergence_" + i.ToString());
				if(_maxYPosition > _div_ele.transform.localPosition.y){
					_maxYPosition = _div_ele.transform.localPosition.y;
				}
			}

			for(int i = 0; i < convergenceModules; i++){
				GameObject _con_ele = GameObject.Find ("convergence_" + i.ToString());
				if(_maxYPosition > _con_ele.transform.localPosition.y){
					_maxYPosition = _con_ele.transform.localPosition.y;
				}
			}

            for (int i = 0; i < actionModules; i++)
            {
                GameObject _act_ele = GameObject.Find("action_" + i.ToString());
                if (_maxYPosition > _act_ele.transform.localPosition.y)
                {
                    _maxYPosition = _act_ele.transform.localPosition.y;
                }

                if ((_maxStepPosition) < (_act_ele.gameObject.transform.localPosition.x + 100))
                {
                    _maxStepPosition = _act_ele.gameObject.transform.localPosition.x + 100;
                }
            }

            if (_workbenchArea.GetComponent<RectTransform> ().rect.height < (Mathf.Abs (_maxYPosition) + 100f)) {
				_height = Mathf.Abs (_maxYPosition) + 100f;
			} else {
				if (Mathf.Abs (_maxYPosition) > _initialAreaHeight) {
					if (_workbenchArea.GetComponent<RectTransform> ().rect.height > (Mathf.Abs (_maxYPosition) + 150f)) {
						_height = Mathf.Abs (_maxYPosition) + 150f;
					} else {
						_height = _workbenchArea.GetComponent<RectTransform> ().rect.height;
					}
				} else {
					_height = _initialAreaHeight;
				}
			}

			if (_maxStepPosition > _workbenchArea.GetComponent<RectTransform> ().rect.width) {
				_workbenchArea.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_maxStepPosition + 20f, _height);
			} else {
				if (_maxStepPosition > 1720) {
					if (_workbenchArea.GetComponent<RectTransform> ().rect.width > (_maxStepPosition + 100f)) {
						_workbenchArea.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_maxStepPosition + 100f, _height);
					} else {
						_workbenchArea.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_workbenchArea.GetComponent<RectTransform> ().rect.width, _height);
					}
				} else {
					_workbenchArea.GetComponent<RectTransform> ().sizeDelta = new Vector2 (1720f, _height);
				}
			}
		}
        else
        {
            _workbenchArea.GetComponent<RectTransform>().sizeDelta = new Vector2(1720f, _initialAreaHeight);
        }
    }
}
