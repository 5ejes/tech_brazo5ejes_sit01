﻿using NSUtilities;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;

public class PanelPlayHandler : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private Image _playButtonImage = null;
	[Header("Data")]
	[SerializeField] private Sprite _playSprite = null;
	[SerializeField] private Sprite _pauseSprite = null;
	[Header("Read-Only Values")]
	[SerializeField] private bool _pausedWorkspaceState;

	[Header("Simple Events")]
	[SerializeField] private UnityEvent _onStartButtonPress = null;
	[SerializeField] private UnityEvent _onPauseButtonPress = null;
	public bool PausedWorkspaceState
	{
		get => _pausedWorkspaceState;
		set
		{
			_pausedWorkspaceState = value;

			// Defino la imagen al botón
			SetImageToButton(value);
		}
	}

	private void Awake()
	{
		// Inicializo el estado de la pausa en true
		PausedWorkspaceState = false;
	}

	private void SetImageToButton(bool pause)
	{
		if (_playButtonImage == null) return;
		if (!_playSprite || !_pauseSprite) return;

		_playButtonImage.sprite = pause ? _playSprite : _pauseSprite;
	}

	public void StateUIHandler()
	{
		StateUIHandler(PausedWorkspaceState);
	}

	public void StateUIHandler(bool isPaused)
	{

		// Si la pausa está activa, el botón funciona como un start
		if (isPaused) _onStartButtonPress.WrapInvoke();
		else _onPauseButtonPress.WrapInvoke();  // Si la pausa no está activa, el botón funciona como una pausa

		// Invierto el valor de pausa
		PausedWorkspaceState = !isPaused;
	}

}
