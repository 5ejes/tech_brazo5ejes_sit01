﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEditorAcciones
{
	public abstract class Conditional : LogicBlock
	{

		private Condition _condition;

		public Condition CurrentCondition
		{
			get => _condition;
			set => _condition = value;
		}

		public bool EvaluateCondition()
		{
			// Evaluo la condición actual
			return _condition.Evaluate();
		}
	}

}