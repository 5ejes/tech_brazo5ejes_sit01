﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEditorAcciones
{
	public class IfElseStatement : IfStatement
	{

		public IfElseStatement(Condition argCondition) : base(argCondition)
		{
			// Defino la condición de la base

			// Defino el ID del bloque
			BlockID = IDTable.IF_ELSE_STATEMENT_ID;
		}

		private LogicBlock[] _elseExecutionBlocks;
		public int ElseSkipIndex
		{
			get
			{
				// Retorno el índice del skip IF más la longitud de bloque ELSE anidados
				return IfSkipIndex + GetExecutionBlocks(false).Length;
			}
		}

		public int SkipAtElseIndex
		{
			get
			{
				// Retorno el ultimo elemento de la cadena de ejecución If para saltar el Else
				return ExecutionOrder + GetExecutionBlocks(true).Length;
			}
		}

		public LogicBlock[] ElseExecutionBlocks
		{
			get => _elseExecutionBlocks;
			set => _elseExecutionBlocks = value;
		}

		public LogicBlock[] GetExecutionBlocks(bool argEvaluation)
		{
			// Retorno los bloques según la evaluación dada
			return argEvaluation ? _ifExecutionBlocks : _elseExecutionBlocks;
		}

		public void SetExecutionBlocks(LogicBlock[] argBlocks, bool argEvaluation)
		{
			// Defino los bloques según la evaluación dada
			if (argEvaluation)
				_ifExecutionBlocks = argBlocks;
			else
				_elseExecutionBlocks = argBlocks;
		}

	}
}