﻿public static class IDTable
{
	public const string IF_STATEMENT_ID = "IF";
	public const string IF_ELSE_STATEMENT_ID = "IF_ELSE";
	public const string COLOR_LSENSOR_ID = "COLOR_SENSOR";
	public const string BARRIER_LSENSOR_ID = "BARRIER_SENSOR";
	public const string MOVE_OPERATION_ID = "MOVE_OP";
	public const string GRIP_OPERATION_ID = "GRIP_OP";
}