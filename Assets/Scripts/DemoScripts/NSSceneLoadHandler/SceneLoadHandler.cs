﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace NSSceneLoadHandler
{
	/// <summary>
	/// Clase encargada de cargar y descargar tanto escenas como escenas aditivas
	/// </summary>
	public class SceneLoadHandler : MonoBehaviour
	{

		[Header("Eventos")]
		[Tooltip("Al cargar una escena aditiva")]
		[SerializeField] private UnityEvent _onAdditiveSceneLoad = new UnityEvent();
		[Tooltip("Al descargar una escena aditiva")]
		[SerializeField] private UnityEvent _onAdditiveSceneUnload = new UnityEvent();

		/// <summary>
		/// Lista de los nombres de las escenas aditivas cargadas
		/// </summary>
		private List<string> _loadedScenes = new List<string>();

		/// <summary>
		/// Obtengo la escena activa actual
		/// </summary>
		/// <returns>Escena activa en la aplicación de Unity</returns>
		public Scene GetCurrentScene()
		{
			// Obtengo la escena activa actual
			return SceneManager.GetActiveScene();
		}

		/// <summary>
		/// Cargo una escena aditivamente
		/// </summary>
		/// <param name="name">Nombre de la escena a cargar</param>
		public void LoadSceneAdditively(string name)
		{
			// Comprueblo si el string es válido
			if (!StringCheckIsValid(name)) return;

			// Cargo la escena aditivamente
			var sceneLoadOperation = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);

			// Cuando se complete ejecuto el callback
			sceneLoadOperation.completed += (AsyncOperation operation) =>
			{
				// Anexo el nombre de la escena actual a la lista de escenas cargadas
				_loadedScenes.Add(name);

				// Invoco el evento
				_onAdditiveSceneLoad.WrapInvoke();
			};

			// Permito anexar la escena al momento de ser descargada
			sceneLoadOperation.allowSceneActivation = true;
		}

		private bool StringCheckIsValid(string name)
		{
			// Compruebo si el string está vacío o es nulo
			if (string.IsNullOrEmpty(name))
			{

				// Lanzo un error y retorno del proceso
				Debug.LogError($"El string especificado es nulo o está vacío : {gameObject}");
				return false;
			}
			else
			{
				// El string es válido
				return true;
			}
		}

		/// <summary>
		/// Descarga una escena aditiva previamente registrada
		/// </summary>
		public void UnloadScene(string name)
		{
			// Comprueblo si el string es válido
			if (!StringCheckIsValid(name)) return;

			// Obtengo la escena
			var sceneNameToUnload = _loadedScenes.Find(scene => scene.Equals(name));

			// Si la escena a descargar no es nula
			if (sceneNameToUnload != null)
			{
				// Descargo la escena
				var sceneUnloadOperation = SceneManager.UnloadSceneAsync(sceneNameToUnload);

				// Suscribo el evento al finalizar
				sceneUnloadOperation.completed += (AsyncOperation operation) =>
				{
					// Elimino la escena de la lista
					_loadedScenes.Remove(sceneNameToUnload);

					// Ejecuto el evento
					_onAdditiveSceneUnload.WrapInvoke();
				};

			}
		}

		/// <summary>
		/// Carga una escena de forma individual
		/// </summary>
		/// <param name="name">El nombre de la escena a cargar</param>
		public void LoadSceneSingle(string name)
		{
			// Compruebo la validez del nombre
			if (!StringCheckIsValid(name)) return;

			// Cargo la escena
			SceneManager.LoadScene(name);
		}
	}
}
