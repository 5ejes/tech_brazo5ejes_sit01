﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using UnityEngine;

public class OnGUISendVector3 : MonoBehaviour
{

	private string xValue, yValue, zValue;

	[SerializeField] private Vector3 vectorValue;
	[SerializeField] private Vector3Event _onVector3DataSend;

	private void OnGUI()
	{
		GUILayout.Label("Vector3");

		xValue = GUILayout.TextField(vectorValue.x.ToString("0.00"));
		yValue = GUILayout.TextField(vectorValue.y.ToString("0.00"));
		zValue = GUILayout.TextField(vectorValue.z.ToString("0.00"));

		if (!float.TryParse(xValue, out vectorValue.x)) vectorValue.x = 0;
		if (!float.TryParse(yValue, out vectorValue.y)) vectorValue.y = 0;
		if (!float.TryParse(zValue, out vectorValue.z)) vectorValue.z = 0;

		if (GUILayout.Button("Translate to"))
		{
			_onVector3DataSend.Invoke(vectorValue);
		}
	}
}