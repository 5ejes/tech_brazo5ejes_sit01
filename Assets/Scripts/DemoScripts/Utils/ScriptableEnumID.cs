﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Simulador Brazo/Enum ID", fileName = "New Enum ID")]
[System.Serializable]
public class ScriptableEnumID : ScriptableObject
{

	[Multiline()]
	[SerializeField] private string _devDescription = string.Empty;

}