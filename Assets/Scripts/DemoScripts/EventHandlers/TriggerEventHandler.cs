﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;

public class TriggerEventHandler : BaseColliderEventImpl
{

  [Header("Events")]
  [SerializeField] private Modular.ColliderEvent _onColliderEnterEvent;
  [SerializeField] private Modular.ColliderEvent _onColliderStayEvent;
  [SerializeField] private Modular.ColliderEvent _onColliderExitEvent;

  public Modular.ColliderEvent OnColliderEnterEvent => _onColliderEnterEvent;
  public Modular.ColliderEvent OnColliderStayEvent => _onColliderStayEvent;
  public Modular.ColliderEvent OnColliderExitEvent => _onColliderExitEvent;

  public void OnTriggerEnter(Collider other)
  {
    // Compruebo los filtros, si el objeto no supera los filtros, retorno
    if (!CheckFilters(other.gameObject)) return;

    // Si el evento no es nulo
    _onColliderEnterEvent.WrapInvoke(other);
  }

  public void OnTriggerStay(Collider other)
  {
    // Compruebo los filtros, si el objeto no supera los filtros, retorno
    if (!CheckFilters(other.gameObject)) return;

    // Si el evento no es nulo
    _onColliderStayEvent.WrapInvoke(other);
  }

  public void OnTriggerExit(Collider other)
  {
    // Compruebo los filtros, si el objeto no supera los filtros, retorno
    if (!CheckFilters(other.gameObject)) return;

    // Si el evento no es nulo
    _onColliderExitEvent.WrapInvoke(other);
  }

  [ContextMenu("Execute OnTriggerEnter Response")]
  public void ExecuteEnterResponse()
  {
    _onColliderEnterEvent.WrapInvoke(null);
  }

  
  [ContextMenu("Execute OnTriggerStay Response")]
  public void ExecuteStayResponse()
  {
    _onColliderStayEvent.WrapInvoke(null);
  }
  [ContextMenu("Execute OnTriggerExit Response")]
  public void ExecuteExitResponse()
  {
    _onColliderExitEvent.WrapInvoke(null);
  }
}