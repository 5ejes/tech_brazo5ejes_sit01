﻿using System;
using System.Collections;
using System.Collections.Generic;
using NSTraduccionIdiomas;
using UnityEngine;

[System.Serializable]
public class ErrorDefinition
{
	public ErrorDefinition(string code, string nonDefaultHandledescription)
	{
		_errorCode = code;
		_errorDesc = nonDefaultHandledescription;
	}

	public static readonly string TRANSLATION_TEXT_CAPTION = "TextErrorCode";

	[Header("Settings")]
	[SerializeField] private string _errorCode;
	[SerializeField] private string _errorDesc;

	public string DisplayDescription
	{
		get
		{
			// Defino la variable de salida
			var output = string.Empty;

			// Trato de traducir la description
			var translation = DiccionarioIdiomas.Instance.Traducir(TRANSLATION_TEXT_CAPTION + ErrorCode);

			// Si el texto de traducción contiene
			output = translation.EndsWith("sin traducción.") ? $"ERROR NOT TRANSLATED {ErrorCode}: \n{NonDefaultHandleDescription}\n{TRANSLATION_TEXT_CAPTION + ErrorCode}" : translation;

			// Retorno la variable de salida
			return output;
		}
	}
	public string NonDefaultHandleDescription => _errorDesc;
	public string ErrorCode => _errorCode;
}

public class ErrorManager : MonoBehaviour
{
	private static readonly float MIN_CUBE_COLLISION_SQR_THRESHOLD = 1f;
	private static ErrorDefinition ERROR_ARM_WRONG_COLLISION;
	private static ErrorDefinition ERROR_ARM_INVALID_COORDINATE;
	private static ErrorDefinition ERROR_ARM_INVALID_COORDINATE_HORIZONTAL;
	private static ErrorDefinition ERROR_ARM_INVALID_HORIZONTAL_TRANSITION;
	private static ErrorDefinition ERROR_ARM_INVALID_CUBE_GRIP;
	private static ErrorDefinition ERROR_CUBE_COLLISION_WITH_TABLE;
	private static ErrorDefinition ERROR_CUBE_COLLISION_WITH_OTHER_CUBE;
	private static ErrorDefinition ERROR_GRIP_CUBE_COLLISION_WITH_INVALID_OBJECT;
	private static ErrorDefinition ERROR_CUBE_OUT_OF_BOUNDS;
	private static ErrorDefinition ERROR_CUBE_OUT_OF_BELT;
	private static ErrorDefinition ERROR_CUBE_PASS_THROUGH_ENVIRONMENT;

	[Header("Error Handling Settings")]
	[SerializeField] private ScriptableEnumID _cubeEnum;
	[SerializeField] private ArmGripController _gripController;
	[Space]
	[SerializeField] private float _cubeStopListeningTimeout = 5f;

	[Header("Actions")]
	[SerializeField] private Modular.ErrorDefinitionEvent _errorEvent;

	public Transform[] RegisteredCubes { get; set; } = null;

	public static ErrorDefinition LastError { get; private set; } = null;

	private static float _heightThreshold = -5f;
	private bool _Co_ListeningBeltCubes = false;
	private List<Cube> _listeningCubes = new List<Cube>();
	private List<float> _listeningTimeouts = new List<float>();

	private List<Transform> _tableCubeCollisionListeners = new List<Transform>();
	private static bool STOP_ERROR_REPORTING = false;

	private ErrorDefinition[] _registeredErrors;

	private void Start()
	{
		// Defino los errores 
		_registeredErrors = new ErrorDefinition[]
		{
			ERROR_ARM_WRONG_COLLISION = new ErrorDefinition("A001", "El brazo colisiono de una forma inesperada."),
		ERROR_ARM_INVALID_COORDINATE = new ErrorDefinition("A002", "Una coordenada inválida ha sido programada para el brazo."),
		ERROR_ARM_INVALID_COORDINATE_HORIZONTAL = new ErrorDefinition("A003", "Una coordenada inválida ha sido programada para el brazo en modo horizontal."),
		ERROR_ARM_INVALID_HORIZONTAL_TRANSITION = new ErrorDefinition("A004", "No se puede realizar la transición del brazo al modo horizontal."),
		ERROR_ARM_INVALID_CUBE_GRIP = new ErrorDefinition("A005", "Se ha sujetado el cilindro en un lugar no permitido, recuerde ajustar el grip al centro del cilindro para poder sujetarlo satisfactoriamente."),
		ERROR_CUBE_COLLISION_WITH_TABLE = new ErrorDefinition("C001", "Uno de los cubos, tuvo una colisión con la mesa de forma inesperada."),
		ERROR_CUBE_COLLISION_WITH_OTHER_CUBE = new ErrorDefinition("C002", "Uno de los cubos, tuvo una colisión con otro cubo de forma inválida."),
		ERROR_CUBE_OUT_OF_BOUNDS = new ErrorDefinition("C003", "Uno de los cubos, ha salido de la zona permitida; Fuera de los límites."),
		ERROR_CUBE_OUT_OF_BELT = new ErrorDefinition("C004", "Uno de los cubos ha caido de la banda."),
		ERROR_GRIP_CUBE_COLLISION_WITH_INVALID_OBJECT = new ErrorDefinition("C005", "El cubo sujetado por la grip ha detectado una colisión inválida con otro objeto."),
		ERROR_CUBE_PASS_THROUGH_ENVIRONMENT= new ErrorDefinition("C006", "El cubo sujetado por la grip ha detectado una colisión con el entorno")
		};
	}

	/// <summary>
	/// Reporta los errores registrados por el ID
	/// </summary>
	/// <param name="id"></param>
	public void ReportErrorID(string id)
	{
		// Si el id es nulo o está vacío
		if (string.IsNullOrEmpty(id)) return;

		// Busco el error con el ID
		var errorDef = System.Array.Find<ErrorDefinition>(_registeredErrors, err => err.ErrorCode.Equals(id));

		// Si el error existe
		if (errorDef != null)
		{
			// Reporto el error encontrado
			ReportError(errorDef);
		}
	}

	/// <summary>
	/// Imprime el error a través del Debug de Unity
	/// </summary>
	/// <param name="argError">Error a imprimir</param>
	public void DebugError(ErrorDefinition argError)
	{
		// Si el error está definido
		if (argError != null)
		{
			// Describo el error a través de el Debug de Unity
			Debug.LogError($"{argError.ErrorCode} : {argError.DisplayDescription}: {this}");
		}
	}
	public void FireInvalidCollisionError()
	{
		// Invoco el error de coordenada inválida
		ReportError(ERROR_ARM_WRONG_COLLISION);
	}

	public void FireInvalidCoordinateError()
	{
		// Invoco el error de coordenada inválida
		ReportError(ERROR_ARM_INVALID_COORDINATE);
	}

	public void FireInvalidHorizontalModeCoordinateError()
	{
		// Invoco el error de coordenada inválida en modo horizontal
		ReportError(ERROR_ARM_INVALID_COORDINATE_HORIZONTAL);
	}

	public void FireInvalidHorizontalModeTransition()
	{
		// Invoco el error de transición horizontal
		ReportError(ERROR_ARM_INVALID_HORIZONTAL_TRANSITION);
	}

	public void FireInvalidGripToCubePosition()
	{
		ReportError(ERROR_ARM_INVALID_CUBE_GRIP);
	}

	public void HandleCubeTableDropHorizontal(Collision collision)
	{
		// obtengo el identificador desde el objeto de la colisión
		var identificator = collision.gameObject.GetComponent<EnumIdentificator>();

		// Si el identificador no existe
		if (identificator == null)
		{
			// Lanzo un error y cancelo la operación
			Debug.LogWarning($"The GameObject '{collision.gameObject} doesn't have an identificator : {gameObject}");
			return;
		}

		// Si el objeto de colisión no está identificado con un número
		if (!_cubeEnum.Equals(identificator.EnumID)) return;

		Transform collisionTransform = collision.transform;
		if (_tableCubeCollisionListeners.Contains(collisionTransform)) return;
		_tableCubeCollisionListeners.Add(collisionTransform);

		StartCoroutine(WaitFor(0.5f,
		async () =>
		{

			for (int i = 0; i < 3; i++)
			{

				// Hallo el producto punto entre la vertical del cilindro y la vertical del mundo
				float dot = Vector3.Dot(collisionTransform.rotation * Vector3.up, Vector3.up);

				// Verifico si el cilindro está horizontalmente sobre el plano
				if (Mathf.Abs(dot) < 0.85f)
				{
					// Reporto un error
					ReportError(ERROR_CUBE_COLLISION_WITH_TABLE);
					break;
				}

				await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(0.5f));
			}

			if (_tableCubeCollisionListeners.Contains(collisionTransform))
				_tableCubeCollisionListeners.Remove(collisionTransform);
		}));
	}

	private IEnumerator WaitFor(float seconds, Action callback)
	{
		if (callback == null) yield break;

		yield return null;
		if (seconds >= 0)
		{
			yield return new WaitForSeconds(seconds);
		}

		callback?.Invoke();
	}

	public void HandleArmWrongCollisionError(Collision collision)
	{
		// obtengo el identificador desde el objeto de la colisión
		var identificator = collision.gameObject.GetComponent<EnumIdentificator>();

		// Si el identificador no existe
		if (identificator == null)
		{
			// Lanzo un error y cancelo la operación
			Debug.LogWarning($"The GameObject '{collision.gameObject} doesn't have an identificator : {gameObject}");
			return;
		}

		// Si el filtro de enumeración coincide
		if (_cubeEnum.Equals(identificator.EnumID))
		{
			// Si el grip Controller está definido
			if (_gripController == null)
			{
				// Lanzo un error y cancelo la operación
				Debug.LogError($"The Grip Controller is not asigned : {gameObject}");
				return;
			}

			// Verifico si el grip ha sido activado
			var gripOpen = !_gripController.GripHold;

			// Si el grip no está abierto
			if (!gripOpen)
			{
				// Ejecuto el evento de error
				ReportError(ERROR_ARM_WRONG_COLLISION);
			}
			else
			{
				// Si el grip está abierto

				// Si hay más de un contacto en la colisión
				if (collision.contactCount > 0)
				{

					// Defino el angulo relativo del vector
					float _upFaceThreshold = 0.2f;

					// Obtengo la normal del primer contacto
					var normal = collision.contacts[0].normal;

					// Obtengo el producto punto, para determinar si el objeto efectivamente fue golpeado desde arriba (1)
					float dot = Vector3.Dot(Vector3.up, normal);

					// Si el producto punto entre los vectores es menor al límite menos una margen de error
					if (dot < 1 - _upFaceThreshold)
					{
						// No es por encima y ejecuto una rutina de error
						Debug.Log($"Not upwards : {collision.rigidbody.name}");

						// Ejecuto el evento de error
						ReportError(ERROR_ARM_WRONG_COLLISION);
					}
					else
					{
						// Si es por encima, ignoro el error
						Debug.Log($"Upwards : {collision.rigidbody.name}");

					}
				}
			}

		}
	}

	/// <summary>
	/// Maneja las colisiones de los cubos en contra de la mesa
	/// </summary>
	/// <param name="collision">Collision ocurrida para ser evaluada</param>
	public void HandleTableCubeCollision(Collision collision)
	{
		// Obtengo el identificador de enumeración de la colisión del cubo
		var enumIdentificator = collision.gameObject.GetComponent<EnumIdentificator>();

		// Si existe el identificador
		if (enumIdentificator != null)
		{
			// Obtengo el enum de la colisión
			var collisionEnum = enumIdentificator.EnumID;

			// Obtengo la velocidad relativa de la colisión
			var collisionRelativeVelocity = collision.relativeVelocity.sqrMagnitude;

			// El límite permitido de la colisión contra la mesa
			float velocityThreshold = 0.1f;

			// Si la enumeración coincide con el filtro y la velocidad supera el límite de velocidad
			if (collisionEnum.Equals(_cubeEnum) && collisionRelativeVelocity > velocityThreshold)
			{
				// Ejecuto el evento de error
				ReportError(ERROR_CUBE_COLLISION_WITH_TABLE);

			}
		}
	}

	private void ReportError(ErrorDefinition definition)
	{

		// Mientras esté activa el flag de reporte no reporto nada
		if (STOP_ERROR_REPORTING) return;

		// Si el error actual es diferente al ultimo error reportado
		if (!definition.Equals(LastError))
		{
			// Defino el ultimo error
			LastError = definition;

			// Invoco la definición del error
			if (_errorEvent != null) _errorEvent.Invoke(definition);
		}
		else
		{
			// Es un error repetido

			// Levanto el flag de reporte
			Debug.Log($"Reporting error flag active! : {this}", this);
			STOP_ERROR_REPORTING = true;
		}

	}

	/// <summary>
	/// Proceso la colisión entregada de un cubo contra otros objetos
	/// </summary>
	/// <param name="collision">Colision potencial de un cubo contra otro cubo</param>
	public void HandleCubeCubeCollision(Collision collision)
	{
		// Filtro los cubos
		if (RegisteredCubes != null)
		{

			// Si no hay cubos registrados
			if (RegisteredCubes.Length < 0)
			{
				Debug.LogWarning($"There are 0 registered cubes for collision : {gameObject}");
				return;
			}
			else
			{
				// Hay al menos un cubo registrado

				// Obtengo el identificador de enumeración
				var _collisionEnumIdentificator = collision.gameObject.GetComponent<EnumIdentificator>();

				// Si hay un identificador de enumeración
				if (_collisionEnumIdentificator)
				{
					// Si el enumerador de colisión corresponde a un cubo
					if (_collisionEnumIdentificator.EnumID.Equals(_cubeEnum))
					{
						// Si el cubo identificado existe en el registro
						var cubeRegistered = (System.Array.Find(RegisteredCubes, cube => cube.name.Equals(collision.gameObject.name)) != null);

						// Si el cubo ha sido registrado
						if (cubeRegistered)
						{
							//Proceso su colisión

							// Controlo la magnitud de la colisión a cierto threshold
							var collisionVelocity = collision.relativeVelocity.sqrMagnitude;

							// Si la magnitud supera n
							if (collisionVelocity > MIN_CUBE_COLLISION_SQR_THRESHOLD)
							{
								// Reporto el error
								ReportError(ERROR_CUBE_COLLISION_WITH_OTHER_CUBE);
							}
						}
					}
				}
			}
		}
	}

	public void HandleBeltDropCube(Collision collision)
	{
		// Si hay cubos registrados
		if (_listeningCubes != null)
		{
			// Si al menos hay un cubo
			if (_listeningCubes.Count > 0)
			{
				// Si la collisión corresponde a un cubo registrado
				var isRegistered = _listeningCubes.Find(cube => cube.gameObject.Equals(collision.gameObject));

				// Si está registrado
				if (isRegistered)
				{
					// Lanzo un error
					ReportError(ERROR_CUBE_OUT_OF_BELT);
				}
			}
		}
	}
	public void ListenCubeError(Cube cube)
	{
		// Si la referencia al cubo no es nula
		if (cube)
		{
			// Si la lista de cubos no es nula
			if (_listeningCubes != null)
			{
				// Si el cubo no está contenido en la lista
				if (!_listeningCubes.Contains(cube))
				{
					// Añado el cubo a la lista de listening
					_listeningCubes.Add(cube);

					// Añado el temporizador
					_listeningTimeouts.Add(0f);

					// Inicio la rutina para escuchar los errores del cubo y la banda
					StartCoroutine(ListenForCubes());
				}
				else
				{
					// El cubo está contenido en la lista
					int index = _listeningCubes.IndexOf(cube);

					// Reinicio el temporizador del cubo
					_listeningTimeouts[index] = 0f;
				}
			}
		}
	}

	[ContextMenu("Reset Error Reporting Flag")]
	public void ResetStopErrorReportingFlag()
	{
		Debug.Log($"Reseting Error Flag...", this);
		// Restablezco el flag
		STOP_ERROR_REPORTING = false;

		// Restablezco el ultimo error registrado
		LastError = null;
	}

	public void ClearListenCubeBeltErrorList()
	{
		_listeningCubes.Clear();
		_listeningTimeouts.Clear();
	}

	private IEnumerator ListenForCubes()
	{
		// Si ya está la corutina ejecutándose
		if (_Co_ListeningBeltCubes) yield break;
		_Co_ListeningBeltCubes = true;

		// Si en la cuenta hay al menos un elemento
		int count = _listeningCubes.Count;
		while (count > 0)
		{
			// Por cada cubo
			for (int i = count - 1; i >= 0; i--)
			{
				try
				{
					// Obtengo el timeout actual y lo incremento
					_listeningTimeouts[i] += Time.deltaTime;

					// Obtengo el cubo actual
					var currentCube = _listeningCubes[i];

					// Si el estado del cubo es diferente del estado inicial o el timeout se ha cumplido
					if (currentCube.State != CubeState.Inicial || _listeningTimeouts[i] > _cubeStopListeningTimeout)
					{
						// Elimino los registros del cubo
						_listeningCubes.RemoveAt(i);
						_listeningTimeouts.RemoveAt(i);

						// Si hay cero cubos registrados
						if (_listeningCubes.Count == 0 || _listeningTimeouts.Count == 0)
						{
							// Rompo el for loop
							break;
						}

						continue;

					}

				}
				catch
				{
					_Co_ListeningBeltCubes = false;
					yield break;
				}


			}

			// Espero un frame
			yield return null;
		}

		// Finalizo la rutina
		_Co_ListeningBeltCubes = false;
	}

}