﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;
using static NSScriptableEvent.ScriptableEventBool;

public class TransformBoundSingleInspector : MonoBehaviour
{
	[Header("Events")]
	[SerializeField] private UnityEvent _onPointInside;
	[SerializeField] private BoolEvent _onPointEvaluationResult;
	[Header("Debug")]
	[SerializeField] private bool _drawDebug;
	[SerializeField] private bool _drawAsSphere = false;
	[SerializeField] private Color _drawColor = Color.red;

	private TransformBound _bound;
	public TransformBound Bound
	{
		get
		{
			// Si el bound es nulo, lo genero
			if (_bound == null) _bound = transform;
			return _bound;
		}
	}

	public bool DrawDebug { get => _drawDebug; set => _drawDebug = value; }
	public Color DrawColor { get => _drawColor; set => _drawColor = value; }

	/// <summary>
	/// Verifica si el punto está dentro de los límites definidos
	/// </summary>
	/// <param name="point">El punto a verificar</param>
	/// <returns>true: El punto está dentro de los límites</returns>
	public bool IsPointInside(Vector3 point)
	{
		// Evaluo si el punto se encuentra dentro
		// Retorno el resultado
		return Bound.IsInside(point);

	}

	public void CheckPointInside(Vector3 point)
	{
		// Evaluo el punto
		var isInside = IsPointInside(point);

		// Si el punto está adentro
		if (isInside)
		{
			// Ejecuto el evento
			_onPointInside.WrapInvoke();
		}

		// Ejecuto el evento
		_onPointEvaluationResult.WrapInvoke(isInside);
	}

	private void OnDrawGizmos()
	{

		// Si dibujo los datos del Debug
		if (!DrawDebug) return;

		// Defino el valor de los colores
		Color gizmosLine, gizmosFill = gizmosLine = DrawColor;

		gizmosLine.a = 0.8f;
		gizmosFill.a = 0.2f;

		if (_drawAsSphere)
		{
			float magnitude = transform.lossyScale.magnitude;
			Vector3 position = transform.position;
			
			Gizmos.color = gizmosFill;
			Gizmos.DrawSphere(position, magnitude);

			Gizmos.color = gizmosLine;
			Gizmos.DrawWireSphere(position, magnitude);
		}
		else
		{
			Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.localScale);
			// Dibujo los indicadores
			Gizmos.color = gizmosFill;
			Gizmos.DrawCube(Vector3.zero, Vector3.one);

			Gizmos.color = gizmosLine;
			Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
		}

	}
}

