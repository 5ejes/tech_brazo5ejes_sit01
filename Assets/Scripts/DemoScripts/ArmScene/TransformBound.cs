﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformBound
{

	/// <summary>
	/// Referencia a la posición, rotación y la escala del transform bound
	/// </summary>
	/// <value></value>
	public Transform SceneTransform { get; set; } = null;
	public TransformBound(Transform transform)
	{
		// Asigno el transform a referenciar
		SceneTransform = transform;
	}

	/// <summary>
	/// Genera los límites del transform, con escala y rotación
	/// </summary>
	/// <returns>Un arreglo de Vector3 con los límites</returns>
	public Vector3[] Get3DBoundaries()
	{
		// Obtengo el parámetro de escala dividido 2
		var halfScale = SceneTransform.localScale / 2f;

		// Obtengo las posición global
		var position = SceneTransform.position;

		// Obtengo la rotación
		var rotation = SceneTransform.rotation;

		// Obtengo las coordenadas de cada lìmite
		var localPositions = new Vector3[]
		{
			rotation * new Vector3(+halfScale.x, +halfScale.y, +halfScale.z) + position,
			rotation * new Vector3(-halfScale.x, +halfScale.y, +halfScale.z) + position,
			rotation * new Vector3(-halfScale.x, -halfScale.y, +halfScale.z) + position,
			rotation * new Vector3(+halfScale.x, -halfScale.y, +halfScale.z) + position,
			rotation * new Vector3(+halfScale.x, +halfScale.y, -halfScale.z) + position,
			rotation * new Vector3(-halfScale.x, +halfScale.y, -halfScale.z) + position,
			rotation * new Vector3(-halfScale.x, -halfScale.y, -halfScale.z) + position,
			rotation * new Vector3(+halfScale.x, -halfScale.y, -halfScale.z) + position
		};

		return localPositions;
	}

	/// <summary>
	/// Verifica si un punto tridimensional se encuentra entre este límite
	/// </summary>
	/// <param name="point">Punto a evaluar dentro del límite</param>
	/// <returns>'true' si el punto está dentro del límite, de lo contrario 'false'</returns>
	public bool IsInside(Vector3 point)
	{
		// Evaluo la posición

		// Genero una booleana por cada dimensión
		bool insideX, insideY, insideZ;

		// Obtengo los límites
		var limites = Get3DBoundaries();

		// Obtengo los mínimos y máximos de cada dimensión
		Vector3 min, max = min = Vector2.zero;

		// Dimension x = 0
		min.x = GetMinFromVector3Index(0, limites);
		max.x = GetMaxFromVector3Index(0, limites);

		// Dimension y = 1
		min.y = GetMinFromVector3Index(1, limites);
		max.y = GetMaxFromVector3Index(1, limites);

		// Dimension z = 2
		min.z = GetMinFromVector3Index(2, limites);
		max.z = GetMaxFromVector3Index(2, limites);

		// Por cada posición en límites
		for (int limitIndex = 0; limitIndex < limites.Length; limitIndex++)
		{
			// Obtengo la posición del límite	
			var pos = limites[limitIndex];

			// Dibujo un rayo con la posición
			Debug.DrawRay(pos, Vector3.one.normalized, Color.green);
		}

		// Verifico si los puntos están dentro de los elementos designados
		insideX = (point.x > min.x && point.x < max.x);
		insideY = (point.y > min.y && point.y < max.y);
		insideZ = (point.z > min.z && point.z < max.z);

		// Retorno el resultado de la evaluación
		return (insideX && insideY && insideZ);
	}


	/// <summary>
	/// Genera los límites locales solo del eje X y z
	/// </summary>
	/// <returns>Un arreglo de Vector3 con los límites</returns>
	public Vector3[] Get3DLocalBoundariesXZ()
	{
		// Obtengo la escala
		var halfScale = SceneTransform.localScale / 2f;

		// Obtengo la rotación
		var rotation = SceneTransform.rotation;

		var localPositions = new Vector3[]
		{
		 rotation * new Vector3(+halfScale.x,0f,+halfScale.z),
		 rotation * new Vector3(-halfScale.x,0f,+halfScale.z),
		 rotation * new Vector3(+halfScale.x,0f,-halfScale.z),
		 rotation * new Vector3(-halfScale.x,0f,-halfScale.z)
		};

		return localPositions;
	}

	/// <summary>
	/// Genera los límites solo del eje X y z
	/// </summary>
	/// <returns>Un arreglo de Vector3 con los límites</returns>
	public Vector3[] Get3DBoundariesXZ()
	{
		// Obtengo la escala
		var halfScale = SceneTransform.localScale / 2f;

		// Obtengo la posición
		var position = SceneTransform.position;

		// Obtengo la rotación
		var rotation = SceneTransform.rotation;

		// Genero las posiciones
		var localPositions = new Vector3[]
		{
		 rotation * new Vector3(+halfScale.x,0f,+halfScale.z) + position,
		 rotation * new Vector3(-halfScale.x,0f,+halfScale.z) + position,
		 rotation * new Vector3(+halfScale.x,0f,-halfScale.z) + position,
		 rotation * new Vector3(-halfScale.x,0f,-halfScale.z) + position
		};

		return localPositions;
	}


	/// <summary>
	/// Genera los límites locales del transform, con escala y rotación
	/// </summary>
	/// <returns>Un arreglo de Vector3 con los límites</returns>
	public Vector3[] Get3DLocalBoundaries()
	{

		var halfScale = SceneTransform.localScale / 2f;
		var rotation = SceneTransform.rotation;
		// Obtengo las coordenadas de cada lìmite
		var localPositions = new Vector3[]
		{
			rotation * new Vector3(+halfScale.x, +halfScale.y, +halfScale.z),
			rotation * new Vector3(-halfScale.x, +halfScale.y, +halfScale.z),
			rotation * new Vector3(-halfScale.x, -halfScale.y, +halfScale.z),
			rotation * new Vector3(+halfScale.x, -halfScale.y, +halfScale.z),
			rotation * new Vector3(+halfScale.x, +halfScale.y, -halfScale.z),
			rotation * new Vector3(-halfScale.x, +halfScale.y, -halfScale.z),
			rotation * new Vector3(-halfScale.x, -halfScale.y, -halfScale.z),
			rotation * new Vector3(+halfScale.x, -halfScale.y, -halfScale.z)
		};

		return localPositions;
	}

	/// <summary>
	/// Valida si un punto está dentro de los límites tanto en X como en Z
	/// </summary>
	/// <param name="point">Punto a evaluar en coordenadas globales</param>
	/// <returns>'true' si está dentro de los puntos, de lo contrario 'false'</returns>
	public bool IsInsideXZ(Vector3 point)
	{
		// Evaluo la posición

		// Genero una booleana por cada dimensión
		bool insideX, insideZ;

		// Obtengo los límites
		var limites = Get3DBoundariesXZ();

		// Obtengo los mínimos y máximos de cada dimensión
		Vector2 min, max = min = Vector2.zero;

		// Dimension x = 0
		min.x = GetMinFromVector3Index(0, limites);
		max.x = GetMaxFromVector3Index(0, limites);

		// Dimension z = 2
		min.y = GetMinFromVector3Index(2, limites);
		max.y = GetMaxFromVector3Index(2, limites);

		foreach (var pos in limites)
		{
			Debug.DrawRay(pos, Vector3.one.normalized, Color.green);
		}

		// Verifico si los puntos están dentro de los elementos designados
		insideX = (point.x > min.x && point.x < max.x);
		insideZ = (point.z > min.y && point.z < max.y);

		// Retorno el resultado de la evaluación
		return (insideX && insideZ);
	}

	private float GetMinFromVector3Index(int dimensionIndex, params Vector3[] argBounds)
	{

		// Si supera las dimensiones establecidas por el Vector3
		if (dimensionIndex > 3) return 0f;

		// Defino la variable para el valor mínimo
		var minValue = float.MaxValue;

		// Recorro cada posición
		foreach (Vector3 position in argBounds)
		{
			// Evaluo el x de cada posición
			minValue = Mathf.Min(position[dimensionIndex], minValue);
		}

		// Retorno el menor en X
		return minValue;
	}

	private float GetMaxFromVector3Index(int dimensionIndex, params Vector3[] argBounds)
	{

		// Si supera las dimensiones establecidas por el Vector3
		if (dimensionIndex > 3) return 0f;

		// Defino la variable para el valor mínimo
		var maxValue = float.MinValue;

		// Recorro cada posición
		foreach (Vector3 position in argBounds)
		{
			// Evaluo el x de cada posición
			maxValue = Mathf.Max(position[dimensionIndex], maxValue);
		}

		// Retorno el menor en X
		return maxValue;
	}

	/// <summary>
	/// Retorna la referencia transform via una conversión implícita
	/// </summary>
	/// <param name="bound">Objectivo a convertir</param>
	public static implicit operator Transform(TransformBound bound)
	{
		// Retorno la referencia al transform
		return bound.SceneTransform;
	}

	/// <summary>
	/// Retorna un nuevo objeto a partir del Transform referenciado
	/// </summary>
	/// <param name="sceneTransform">Transform referenciado</param>
	public static implicit operator TransformBound(Transform sceneTransform)
	{
		// Retorno un nuevo objeto
		return new TransformBound(sceneTransform);
	}
}
