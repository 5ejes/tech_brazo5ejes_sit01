﻿using UnityEngine;

public class CubeFunctionHandler : MonoBehaviour
{
	[Header("Settings")]
	[SerializeField] private int _executeAfterMillis = 0;
	public void Reset(GameObject GO)
	{
		HandleMillisDelayedOperation(GO, cube => cube.Reset());
	}

	public void ResetToKinematicState(GameObject GO)
	{
		HandleMillisDelayedOperation(GO, cube =>
		{
			// Si la referencia al cubo no existe, retorno
			if (!cube) return;

			// Reseteo el cubo
			cube.Reset();

			// Defino el rigidbody como kinemático si existe
			Rigidbody cubeRigidbody = cube.Rigidbody;
			if (cubeRigidbody)
				cubeRigidbody.isKinematic = true;
		});
	}

	public void SetToKinematic(GameObject GO)
	{
		HandleMillisDelayedOperation(GO, cube => cube.Rigidbody.isKinematic = true);
	}

	public void SetToNonKinematic(GameObject GO)
	{
		HandleMillisDelayedOperation(GO, cube => cube.Rigidbody.isKinematic = false);
	}

	private void DoCubeOperation(GameObject GO, System.Action<Cube> onCubeCallback)
	{
		if (onCubeCallback == null) return;

		// Si no existe el GameObject, retorno
		if (!GO) return;

		// Obtengo el component cubo
		var cube = GO.GetComponent<Cube>();

		// Si existe el cubo ejecuto el callback
		if (cube) onCubeCallback(cube);
	}

	private void DelayOperation(int millis, System.Action operation)
	{
		if (operation == null) return;
		if (millis == 0) operation();
		// Creo una tarea asíncrona
		System.Action asyncDelay = async () =>
		{
			// Espero los milisegundos
			await System.Threading.Tasks.Task.Delay(millis);

			// Invoco la operación
			operation();
		};

		// Invoco la tarea
		asyncDelay();
	}

	private void HandleMillisDelayedOperation(GameObject GO, System.Action<Cube> operation)
	{
		ClampMillis();
		if (_executeAfterMillis == 0)
			DoCubeOperation(GO, cube => cube.Reset());
		else
			DelayOperation(
				_executeAfterMillis,
				() => DoCubeOperation(GO, cube => operation(cube))
			);
	}

	private void ClampMillis()
	{
		if (_executeAfterMillis < 0) _executeAfterMillis = 0;
	}
}
