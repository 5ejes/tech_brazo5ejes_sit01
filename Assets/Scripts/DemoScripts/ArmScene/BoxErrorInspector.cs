﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NSBoxMessage.Inspector
{
  public class BoxErrorInspector : BoxMessageInspector
  {
    [Header("Settings")]
    [SerializeField] private float _autoCleanLastErrorTimeout = 5f;
    [SerializeField] private string[] _ignoreErrorCodes;
    private ErrorDefinition _lastComunicatedError = null;
    private float _currentTimeElapsed = 0f;
    private bool _Co_Timer = false;

    public float AutoCleanLastErrorTimeout { get => _autoCleanLastErrorTimeout; set => _autoCleanLastErrorTimeout = value; }
    public ErrorDefinition LastComunicatedError { get => _lastComunicatedError; private set => _lastComunicatedError = value; }
    public float CurrentTimeElapsed { get => _currentTimeElapsed; private set => _currentTimeElapsed = value; }

    public void ComunicateError(ErrorDefinition errorDefinition)
    {
      // Defino un variable de control para la comunicación
      var canComunicate = false;

      // Si el ultimo error comunicado no es nulo
      if (_lastComunicatedError != null)
      {
        // Si el error no se ha comunicado antes
        if (!_lastComunicatedError.Equals(errorDefinition))
        {
          // marco como valida la posibilidad de comunicación del error
          canComunicate = true;
        }
      }
      else
      {
        // Si el ultimo error no existe puedo comunicarlo
        canComunicate = true;
      }

      // Si puedo comunicar el error
      if (canComunicate)
      {
        // Comunico el error
        ShowInfoBox($"{errorDefinition.DisplayDescription}");

        // Defino el error como el ultimo comunicado
        _lastComunicatedError = errorDefinition;

        // Defino el temporizador
        StartCoroutine(Timer(_autoCleanLastErrorTimeout, () =>
        {
          // Al terminar la cuenta, elimino la referencia al ultimo error
          _lastComunicatedError = null;
        }));
      }
    }

		
    public void ComunicateErrorOverride(ErrorDefinition errorDefinition, UnityEvent acceptAction)
    {
      // Defino un variable de control para la comunicación
      var canComunicate = false;

      // Si el ultimo error comunicado no es nulo
      if (_lastComunicatedError != null)
      {
        // Si el error no se ha comunicado antes
        if (!_lastComunicatedError.Equals(errorDefinition))
        {
          // marco como valida la posibilidad de comunicación del error
          canComunicate = true;
        }
      }
      else
      {
        // Si el ultimo error no existe puedo comunicarlo
        canComunicate = true;
      }

      // Si puedo comunicar el error
      if (canComunicate)
      {
        // Comunico el error
        ShowInfoBoxOverride($"{errorDefinition.DisplayDescription}", acceptAction);

        // Defino el error como el ultimo comunicado
        _lastComunicatedError = errorDefinition;

        // Defino el temporizador
        StartCoroutine(Timer(_autoCleanLastErrorTimeout, () =>
        {
          // Al terminar la cuenta, elimino la referencia al ultimo error
          _lastComunicatedError = null;
        }));
      }
    }

    /// <summary>
    /// Verifica si el parámetro dado hace parte de la lista
    /// </summary>
    /// <param name="errorCode">Código de error a evaluar</param>
    /// <returns></returns>
    private bool IgnoreErrorCode(string errorCode)
    {

      // Genero una variable para la salida del resultado
      bool output = false;

      // Si la variable tiene un valor definido
      if (!string.IsNullOrEmpty(errorCode))
      {
        // Si existe la lista a ignorar
        if (_ignoreErrorCodes != null)
        {
          // Si hay más de un elemento en la lista
          if (_ignoreErrorCodes.Length > 0)
          {
            // Por cada código de error
            foreach (var codeIgnore in _ignoreErrorCodes)
            {
              // Escribo el output con los valores en la comprobación
              output = codeIgnore.ToLower().Equals(errorCode.ToLower());

              // Si el parámetro coincide con el codigo a ignorar
              if (output)
              {
                // Salgo de la comprobación
                break;
              }
            }
          }
        }
      }

      // Retorno el valor del output
      return output;
    }

    private IEnumerator Timer(float timeout, System.Action callbackOnFinished = null, System.Action<float> callbackOnSecondsPassed = null)
    {

      // Si la rutina ya se está ejecutando
      if (_Co_Timer) yield break;
      _Co_Timer = true;

      // Defino una variable del tiempo actual 
      float elapsed = 0f;

      // Comunico los segundos transcurridos
      if (callbackOnSecondsPassed != null) callbackOnSecondsPassed(elapsed);

      // Mientras el timeout no se haya cumplido
      while (elapsed < timeout)
      {
        // Incremento el tiempo transcurrido
        elapsed += Time.deltaTime;

        // Comunico los segundos transcurridos
        if (callbackOnSecondsPassed != null) callbackOnSecondsPassed(elapsed);

        // Espero un frame
        yield return null;
      }

      // Comunico el timeout, como tiempo de finalización
      if (callbackOnSecondsPassed != null) callbackOnSecondsPassed(timeout);

      // Comunico el callback de finalización
      if (callbackOnFinished != null) callbackOnFinished();

      // Finalizo la ejecución de la corutina
      _Co_Timer = false;

    }
  }
}
