﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using NSUtilities;
using UnityEngine;

public class TransformInsideBoundChecker : MonoBehaviour
{
	[SerializeField] private TransformEvent _onTransformInside;
	[Header("Settings")]
	[SerializeField] private Transform[] _transformBounds;
	[SerializeField] private Transform[] _transformPoints;

	private TransformBound[] _bounds;

	private void OnStart()
	{
		// Genero los bounds
		GenerateBounds();
	}

	private void Update()
	{
		// Si puedo generar los bounds
		if (CanGenerateBounds())
			// Genero los bounds
			GenerateBounds();

		if (_bounds != null && _bounds.Length > 0)
		{
			// Por cada punto
			int pointsLength = _transformPoints.Length;
			int boundsLength = _bounds.Length;
			for (int pointIndex = 0; pointIndex < pointsLength; pointIndex++)
			{
				// Obtengo el punto actual
				var currentPoint = _transformPoints[pointIndex];

				for (int boundIndex = 0; boundIndex < boundsLength; boundIndex++)
				{
					// Obtengo el bound actual
					var currentBound = _bounds[boundIndex];

					// Verifico el punto sobre el current bound
					if (currentBound.IsInside(currentPoint.position))
					{
						_onTransformInside.WrapInvoke(currentPoint);
					}
				}
			}
		}
	}

	/// <summary>
	/// Verifica si es necesario generar nuevos límites
	/// </summary>
	/// <returns>'true' si el transform y el bound tienen cuentas diferentes, de lo contrario 'false'</returns>
	public bool CanGenerateBounds()
	{
		// Si los limites y los transform están definidos
		if (_transformBounds != null && _bounds != null)
		{
			// Obtengo las dos cuentas
			int transformBoundCount = _transformBounds.Length;
			int boundCount = _bounds.Length;

			// Retorno el resultado de la evaluación
			return !transformBoundCount.Equals(boundCount);
		}
		else
		{
			// Si son nulos
			if (_bounds == null) return true;

			return false;
		}
	}

	/// <summary>
	/// Genera las bounds a partir de los transform almacenados
	/// </summary>
	public void GenerateBounds()
	{
		// Inicializo una variable de almacenamiento
		var tempList = new List<TransformBound>();

		// Recorro cada punto
		for (int i = 0; i < _transformBounds.Length; i++)
		{
			// Obtengo el transform actual
			var currentTransform = _transformBounds[i];

			// Anexo el transform actual a la lista
			tempList.Add(currentTransform);
		}

		// Defino lo bounds
		_bounds = tempList.ToArray();
	}
}
