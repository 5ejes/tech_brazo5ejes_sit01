﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmPoleHandler : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private IKSolver _refSolver;
	[Space]
	[SerializeField] private Transform _min;
	[SerializeField] private Transform _max;

	[Space]
	[SerializeField] private Transform _leafEnd;
	[SerializeField] private Transform _pole;

	[Header("Settings and Values")]
	[SerializeField] private float _horizontalOffset;
	[SerializeField] private float _verticalOffset;

	[Header("Debug")]
	[SerializeField] private Transform _debugDrawBaseOverride = null;

	private float[] boneHeights;

	public IKSolver RefSolver { get => _refSolver; set => _refSolver = value; }
	public Transform Min { get => _min; set => _min = value; }
	public Transform Max { get => _max; set => _max = value; }
	public Transform LeafEnd { get => _leafEnd; set => _leafEnd = value; }
	public Transform Pole { get => _pole; set => _pole = value; }
	public float HorizontalOffset { get => _horizontalOffset; set => _horizontalOffset = value; }
	public float VerticalOffset { get => _verticalOffset; set => _verticalOffset = value; }
	public Transform DebugDrawBaseOverride { get => _debugDrawBaseOverride; set => _debugDrawBaseOverride = value; }
	public Vector3 TargetDirection { get; set; } = -Vector3.one;

	// Start is called before the first frame update
	void Start()
	{
		boneHeights = new float[RefSolver.bones.Length];
		UpdateBoneHeights();
	}

	// Update is called once per frame
	void Update()
	{
		ProcessPosition(Pole, RefSolver);
	}

	private void ProcessPosition(Transform pole, IKSolver ikTarget)
	{
		int boneCount = ikTarget.bones.Length;
		// Obtengo la dirección hacia donde apunta el IK
		var firstBone = ikTarget.bones[boneCount - 1].bone;
		var directionTowardsTarget = ikTarget.transform.position - firstBone.position;

		// Normalizo la dirección a la punta
		directionTowardsTarget.y = 0f;
		directionTowardsTarget.Normalize();

		// Defino la posición según el offset horizontal, desde el primer hueso
		Vector3 horizontalPosition = directionTowardsTarget * HorizontalOffset;
		Vector3 verticalPosition = Vector3.up * (VerticalOffset);

		// El vector horizontal es totalmente plano, significa que no tiene influencia alguna sobre Y (Verticalidad)

		// Realizo una copia de los datos de la posición para poder modificarlos libremente
		var finalPolePosition = pole.position;

		// Aplico la posición relativa a los offset definidos
		finalPolePosition = firstBone.position + horizontalPosition;

		// Aplico la posición del hueso superior
		// finalPolePosition.y = GetMaxBonePosition();

		// Aplico el offset vertical
		finalPolePosition += verticalPosition;

		// Vector3 difference = (pole.position - finalPolePosition);
		// Vector2 horizontalPlaneDiff = new Vector2(difference.x, difference.z);
		// 
		// var heightDelta = Mathf.Abs(difference.y);
		// var horizontalDelta = Mathf.Abs(horizontalPlaneDiff.sqrMagnitude);
		//
		// Si la altura supera un límite y el delta horizontal no modifica su posición
		// (El movimiento es meramente horizontal)
		// if (heightDelta > 0.025f && horizontalDelta < Mathf.Pow(0.025f, 2))
		// {
		// 	// Reasigno los datos modificados
		// 	pole.position = Vector3.MoveTowards(pole.position, finalPolePosition, _moveSpeed * Time.deltaTime);
		// }
		// else
		// {
		// 	pole.position = finalPolePosition;
		// }

		pole.position = finalPolePosition;

		Debug.DrawRay(firstBone.position, horizontalPosition, Color.red);
		Debug.DrawRay(firstBone.position, verticalPosition, Color.yellow);
	}
	private float GetMaxBonePosition()
	{
		// Debo actualizar la altura de los huesos
		UpdateBoneHeights();

		// Obtengo la posición del hueso más alto
		return Mathf.Max(boneHeights);
	}

	private void UpdateBoneHeights()
	{
		int count = boneHeights.Length;
		for (int i = 0; i < count; i++)
		{
			boneHeights[i] = RefSolver.bones[i].bone.position.y;
		}
	}

	public Vector3 GetLimitWeights(Vector3 refPosition)
	{

		var output = new Vector3();

		Vector3 minPosition = Min.localPosition;
		Vector3 maxPosition = Max.localPosition;
		Vector3 position = Min.InverseTransformPoint(refPosition);

		// Si los límites están definidos
		if (Min && Max)
		{
			// Calculo la interpolación de 0 a 1 de la referencia entre la posición mínima y la máxima
			output.x = Mathf.InverseLerp(minPosition.x, maxPosition.x, position.x);
			output.y = Mathf.InverseLerp(minPosition.y, maxPosition.y, position.y);
			output.z = Mathf.InverseLerp(minPosition.z, maxPosition.z, position.z);
		}

		return output;
	}

	public Vector3 GetLimitWeightsFromLeafEnd()
	{
		if (LeafEnd == null)
		{
			Debug.LogError($"The LeafEnd is not defined, define the reference before using this method : {this}", this);
			return Vector3.zero;
		}
		return GetLimitWeights(LeafEnd.position);
	}

	public void SetOffsets(float horizontal, float vertical)
	{
		HorizontalOffset = horizontal;
		VerticalOffset = vertical;
	}


	private void OnDrawGizmos()
	{
		if (Pole)
		{

			Transform usedTransform = DebugDrawBaseOverride != null ? DebugDrawBaseOverride : transform;

			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(Pole.position, .2f);

			Gizmos.color = Color.yellow;
			Gizmos.DrawLine(usedTransform.position, Pole.position);
			Gizmos.DrawLine(Pole.position, RefSolver.transform.position);

			Gizmos.color = Color.green;
			Gizmos.DrawLine(LeafEnd.position, LeafEnd.position + Vector3.up * (Pole.position.y - LeafEnd.position.y));
		}
	}
}