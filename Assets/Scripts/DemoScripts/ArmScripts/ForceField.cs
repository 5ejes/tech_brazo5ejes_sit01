﻿using System;
using UnityEngine;

public class ForceField : MonoBehaviour
{

	[Header("Settings")]

	[SerializeField] private Transform[] _forceFieldPoints;
	[SerializeField] private float _radius = 0.05f;
	[SerializeField] private LayerMask _layerMask = Physics.AllLayers;
	[Header("Special Settings")]
	[SerializeField] private bool _moveInsteadOfApplyingForce = false;
	[Header("Force Mode")]
	[SerializeField] private ForceMode _forceMode;
	[SerializeField] private float _multiply = 1f;
	[Header("Debug")]
	[SerializeField] private bool _drawDebug;

	private Vector3[] _lastPositions;
	private Collider[] _castResults = new Collider[8];

	private Transform _transform;
	private float[] _forceFieldRadiai;

	private void Start()
	{
		// Creo un caché para el transform
		_transform = transform;

		// Construyo el vector de posiciones almacenadas
		int length = _forceFieldPoints.Length;
		_lastPositions = new Vector3[length];
		_forceFieldRadiai = new float[length];

		for (int i = 0; i < length; i++)
		{
			_forceFieldRadiai[i] = _forceFieldPoints[i].localScale.magnitude;
		}

		// Actualizo las posiciones de los puntos
		UpdatePointPosition();
	}

	private void UpdatePointPosition()
	{
		int length = _forceFieldPoints.Length;
		for (int i = 0; i < length; i++)
		{
			_lastPositions[i] = _forceFieldPoints[i].position;
		}
	}

	private void FixedUpdate()
	{
		ProcessPoints();
	}

	private void ProcessPoints()
	{
		if (Time.frameCount % 2 != 0) return;

		int length = _forceFieldPoints.Length;
		for (int i = 0; i < length; i++)
		{
			Transform currentPoint = _forceFieldPoints[i];
			Vector3 pointLastPosition = _lastPositions[i];

			// Si el campo no está activo a través del Transform en la escena, continúo
			if (!currentPoint.gameObject.activeInHierarchy) continue;

			Vector3 deltaPosition = currentPoint.position - pointLastPosition;

			ApplyForceField(currentPoint.position, deltaPosition, _radius);
		}
	}

	private void ApplyForceField(Vector3 castPosition, Vector3 deltaPosition, float radius)
	{
		int detectionCount = Physics.OverlapSphereNonAlloc(castPosition, radius, _castResults, _layerMask, QueryTriggerInteraction.Ignore);

		for (int i = 0; i < detectionCount; i++)
		{
			Rigidbody attachedRigidbody = _castResults[i].attachedRigidbody;
			if (attachedRigidbody == null) return;

			// Aseguro la dirección de empuje a la horizontalidad
			var directionToPosition = (attachedRigidbody.transform.position - _transform.position);
			directionToPosition.y = 0f;
			directionToPosition.Normalize();

			var deltaDirection = deltaPosition;
			deltaDirection.y = 0f;
			deltaDirection.Normalize();

			Vector3 force = directionToPosition * _multiply + deltaDirection * _multiply;

			Debug.DrawRay(attachedRigidbody.transform.position, force, Color.yellow, Time.fixedDeltaTime);
			if (_moveInsteadOfApplyingForce)
			{
				Vector3 position = attachedRigidbody.position;
				position += force * Time.fixedDeltaTime;

				attachedRigidbody.MovePosition(position);
			}
			else
			{
				attachedRigidbody.AddForceAtPosition(force, castPosition, ForceMode.VelocityChange);
			}
		}
	}

	private void LateUpdate()
	{
		// Actualizo las ultimas posiciones de los puntos
		UpdatePointPosition();
	}

	private void OnDrawGizmos()
	{
		if (!_drawDebug) return;

		int length = _forceFieldPoints.Length;
		for (int i = 0; i < length; i++)
		{
			Transform fieldPoint = _forceFieldPoints[i];
			if (fieldPoint == null || !fieldPoint.gameObject.activeInHierarchy) continue;
			Gizmos.color = Color.green;

			float radius = 0f;
			
			if (_forceFieldRadiai == null || _forceFieldRadiai.Length != length)
				radius = fieldPoint.localScale.magnitude;
			else if (i >= 0 && i < _forceFieldRadiai.Length)
				radius = _forceFieldRadiai[i];

			if (radius > 0f)
				Gizmos.DrawWireSphere(fieldPoint.position, radius);
		}

	}
}