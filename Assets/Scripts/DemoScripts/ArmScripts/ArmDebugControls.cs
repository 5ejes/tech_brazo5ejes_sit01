﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

public class ArmDebugControls : MonoBehaviour
{


	[Header("Settings")]
	[SerializeField] private Vector3Event _onVector3DataSend;
	[SerializeField] private UnityEvent _onGripButtonPress;
	[Header("Data")]
	[SerializeField] private Vector3 _moveData;

	public Vector3 Data { get => _moveData; set => _moveData = value; }

	public void MoveOrderSend(Vector3 position)
	{
		_onVector3DataSend.WrapInvoke(position);
	}

	public void GripButtonPress()
	{
		_onGripButtonPress.WrapInvoke();
	}
}