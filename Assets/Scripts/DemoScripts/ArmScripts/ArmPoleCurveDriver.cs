﻿using System;
using Modular.CurveProcessor;
using NSInspectorValue;
using NSScriptableValues;
using UnityEngine;

namespace NSBrazo5Ejes
{
	public class ArmPoleCurveDriver : MonoBehaviour
	{
		[Header("Curves")]
		[SerializeField] private ValueCurveProcessor _horizontalCurveProcessor;
		[SerializeField] private ValueCurveProcessor _verticalCurveProcessor;
		[Header("References")]
		[SerializeField] private ArmPoleHandler _poleHandler;
		[SerializeField] private ArmPositionHandler _positionHandler;

		[Space]
		[SerializeField] private Transform _target;

		[Header("Settings")]
		[SerializeField] private bool _callOnUpdate = true;
		[SerializeField] private float _updateSecondsInterval = 0.025f;

		private float _nextUpdateCall = 0f;
		private Vector3 _localPositionRegister = -Vector3.one;

		public bool CallOnUpdate { get => _callOnUpdate; set => _callOnUpdate = value; }
		public float UpdateSecondsInterval { get => _updateSecondsInterval; set => _updateSecondsInterval = value; }
		public Vector3 LocalPositionRegister
		{
			get => _localPositionRegister;
			set => _localPositionRegister = value;
		}
		public ValueCurveProcessor HorizontalCurveProcessor { get => _horizontalCurveProcessor; set => _horizontalCurveProcessor = value; }
		public ValueCurveProcessor VerticalCurveProcessor { get => _verticalCurveProcessor; set => _verticalCurveProcessor = value; }

		private void Update()
		{
			// Si no se usa la llamada en update, retorno
			if (!CallOnUpdate) return;
			if (LocalPositionRegister.Equals(-Vector3.one)) return;

			// Si hay un intérvalo definido
			if (UpdateSecondsInterval > 0f)
			{
				// Si el tiempo para llamar al update no se ha cumplido, retorno
				if (Time.time < _nextUpdateCall) return;

				// Si el tiempo se ha cumplido
				// Incremento el tiempo por el intérvalo
				_nextUpdateCall = Time.time + UpdateSecondsInterval;
			}

			// Proceso los pesos en el update
			ProcessWeight();
		}

		public void ProcessWeight()
		{
			// Si el pole handler no está definido, retorno
			if (_poleHandler == null) return;
			if (_positionHandler == null) return;
			if (LocalPositionRegister.Equals(-Vector3.one)) return;

			// Obtengo el valor local
			// Vector3 worldSpaceValue = _target.position;
			Vector3 worldSpaceValue = _positionHandler.ConvertToWorldPosition(LocalPositionRegister);

			// Obtengo los pesos
			var weights = _poleHandler.GetLimitWeights(worldSpaceValue);

			Debug.Log($"Weights -> {weights}");

			// Verifico si existen las curvas
			if (VerticalCurveProcessor == null || HorizontalCurveProcessor == null) return;

			// Obtengo los valores transformados por las curvas
			float valueX = HorizontalCurveProcessor.GetCurveValue(weights.x);
			float valueY = VerticalCurveProcessor.GetCurveValue(weights.y);

			// Modifico los valores del offset en el pole handler
			_poleHandler.SetOffsets(valueX, valueY);
		}
	}
}
