﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointSampler
{
	public static Vector3[] GenerateInsideXZFixedRadius(int pointCount, Vector3 center, Vector3 halfExtents, float fixedRadius)
	{
		// Genero una lista de puntos
		var pointList = new List<Vector3>();

		// Si la cuenta de los puntos es mayor a 0
		if (pointCount > 0)
		{
			// Genero los vectores limitrofes
			Vector3 max, min;

			// Defino los vectores
			max = min = center;

			max.x += halfExtents.x;
			max.z += halfExtents.z;

			min.x -= halfExtents.x;
			min.z -= halfExtents.z;

			// Genero una variable de salida
			int invalidTries = 0;
			int maxInvalidTries = 100;

			// Por la cuenta de puntos
			for (int i = 0; i < pointCount;)
			{
				// Genero un punto
				var generatedPoint = GeneratePointVector3(min, max);

				// Si hay al menos un elemento en la lista de puntos
				if (pointList.Count > 0)
				{

					// Obtengo el punto anterior
					var previousPoint = pointList[i - 1];

					// Hallo la dirección entre el punto anterior y el punto actual
					var pointDir = (generatedPoint - previousPoint).normalized;

					// Genero la distancia al punto y sobreescribo el punto generado
					generatedPoint = (pointDir * fixedRadius) + previousPoint;

					// Verifico si el punto está entre los vectores
					if (IsPointInBetween(generatedPoint, min, max))
					{

						// Por cada punto existente
						for (int pointIndex = 0; pointIndex < pointList.Count; pointIndex++)
						{

							// Obtengo el punto actual
							var currentPoint = pointList[pointIndex];

							// Obtengo la distancia
							var distance = Vector3.Distance(generatedPoint, currentPoint);

							// Si la distancia es menor al Fixed Radius
							if (distance >= fixedRadius)
							{
								// Si no es menor

								// Agrego la distancia a la lista
								pointList.Add(generatedPoint);

								// Aumento la cuenta
								i++;
							}
							else
							{
								// Aumento el número de intentos inválidos
								invalidTries++;

								// Salgo del bucle
								break;
							}
						}
					}
					else
					{
						// El punto queda por fuera de los límites

						// Aumento la cuenta
						invalidTries++;
					}
				}
				else
				{
					// No hay elementos en la lista

					// Agrego el punto a la lista
					pointList.Add(generatedPoint);

					// Continúo
					i++;
				}

				// Si se supera el límite de cuentas inválidas
				if (invalidTries > maxInvalidTries)
				{
					// Salgo de ciclo
					break;
				}
			}
		}

		// Retorno la lista de puntos
		return pointList.ToArray();
	}

	public static bool IsPointInBetween(Vector3 generatedPoint, Vector3 min, Vector3 max, float minThreshold = 0.01f)
	{

		// Verifico cada dimensión
		bool insideX = (generatedPoint.x > min.x && generatedPoint.x < max.x) || Mathf.Abs(min.x - max.x) < minThreshold;
		bool insideY = (generatedPoint.y > min.y && generatedPoint.y < max.y) || Mathf.Abs(min.y - max.y) < minThreshold;
		bool insideZ = (generatedPoint.z > min.z && generatedPoint.z < max.z) || Mathf.Abs(min.z - max.z) < minThreshold;

		// Retorno el resultado
		return insideX && insideY && insideZ;

	}

	public static Vector3[] GenerateInsideXZ(int pointCount, Vector3 center, Vector3 halfExtents, float minDistanceFraction)
	{
		// Genero una lista de puntos
		var pointList = new List<Vector3>();

		// Si la cuenta de los puntos es mayor a 0
		if (pointCount > 0)
		{

			// Genero los vectores limitrofes
			Vector3 max, min;

			// Defino los vectores
			max = min = center;

			max.x += halfExtents.x;
			max.z += halfExtents.z;

			min.x -= halfExtents.x;
			min.z -= halfExtents.z;

			// Defino la distancia entre los límites
			float limitDistance = Vector3.Distance(min, max);

			// Restrinjo los valores desde 0 hasta 1
			minDistanceFraction = Mathf.Clamp01(minDistanceFraction);

			// Defino la distancia mínima con el valor de la fracción
			float minDistance = limitDistance * minDistanceFraction;

			// Genero una variable de salida
			int invalidTries = 0;
			int maxInvalidTries = 100;

			// Por la cuenta de puntos
			for (int i = 0; i < pointCount;)
			{
				// Genero un punto
				var generatedPoint = GeneratePointVector3(min, max);

				// Si hay al menos un elemento en la lista de puntos
				if (pointList.Count > 0)
				{

					// Genero la variable para puntos válidos
					var isValid = true;

					// Comparo todos los puntos
					for (int pointIndex = 0; pointIndex < pointList.Count; pointIndex++)
					{
						var currentPoint = pointList[pointIndex];

						// Obtengo la distancia entre el punto actual y el punto generado
						float distance = Vector3.Distance(currentPoint, generatedPoint);

						// Si la distancia es menor a la distancia mínima
						if (distance < minDistance)
						{
							// Se marca como inválida la posición
							isValid = false;

							// Rompo el for
							break;
						}
					}

					// Si no es válida la posición
					if (!isValid)
					{

						// Aumento la cuenta
						invalidTries++;

						// Si se supera el límite de cuentas inválidas
						if (invalidTries > maxInvalidTries)
						{
							// Salgo de ciclo
							break;
						}

					}
					else
					{
						// Si es válida

						// Agrego la distancia a la lista
						pointList.Add(generatedPoint);

						// Aumento la cuenta
						i++;
					}
				}
				else
				{
					// No hay elementos en la lista

					// Agrego el punto a la lista
					pointList.Add(generatedPoint);

					// Continúo
					i++;
				}
			}
		}

		// Retorno la lista de puntos
		return pointList.ToArray();
	}

	private static Vector3 GeneratePointVector3(Vector3 min, Vector3 max)
	{
		// Genero el vector de salida
		Vector3 output;

		output.x = UnityEngine.Random.Range(min.x, max.x);
		output.y = UnityEngine.Random.Range(min.y, max.y);
		output.z = UnityEngine.Random.Range(min.z, max.z);

		// Retorno el output
		return output;
	}
}