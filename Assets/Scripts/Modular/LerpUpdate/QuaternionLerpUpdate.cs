﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Modular.LerpUpdate
{

	[System.Serializable]
	public class QuaternionLerpUpdate
	{

		public QuaternionLerpUpdate(float timeout, Quaternion current, Quaternion target)
		{
			// Defino los parámetros
			Timeout = timeout;
			Current = current;
			Target = target;
		}
		public float Elapsed { get; private set; } = 0f;
		public float Timeout { get; set; }
		public Quaternion Current
		{
			get => _current; set
			{
				// Defino el valor actual y registro un valor de inicio
				_current = value;

				// Reinicio la interpolación
				Reset();
			}
		}
		private Quaternion _current;
		public Quaternion LerpValue
		{
			get
			{
				_lerpValue = UseSLerp
				? Quaternion.Slerp(Current, Target, Percent)
				: Quaternion.Lerp(Current, Target, Percent);

				return _lerpValue;
			}
		}
		private Quaternion _lerpValue;

		public Quaternion Target { get; set; }
		public float Percent => Elapsed / (float)Timeout;
		public bool UseSLerp { get; set; } = false;

		public void Update(float timeDelta)
		{

			// Si el tiempo transcurrido es menor al tiempo límite
			if (Elapsed > Timeout) return;

			// Obtengo el valor absoluto
			timeDelta = Mathf.Abs(timeDelta);
			// Aumento el tiempo transcurrido
			Elapsed += Time.deltaTime;

			// Fijo el valor
			Elapsed = Mathf.Clamp(Elapsed, 0, Timeout);
		}

		public void InverseUpdate(float timeDelta)
		{
			// Si el tiempo transcurrido es menor al tiempo límite
			if (Elapsed < 0) return;

			// Obtengo el valor absoluto
			timeDelta = Mathf.Abs(timeDelta);
			// Aumento el tiempo transcurrido
			Elapsed -= Time.deltaTime;

			// Fijo el valor
			Elapsed = Mathf.Clamp(Elapsed, 0, Timeout);
		}

		public void Reset()
		{
			Elapsed = 0f;
		}
	}
}
