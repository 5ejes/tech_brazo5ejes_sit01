﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using NSUtilities;

namespace Modular.Parser
{

	public class SystemObjectToBooleanParse : GenericParser<object>
	{
		[SerializeField] private BoolEvent _onBoolParsed;
		public override void Parse(object valueToParse)
		{
			OnParseAction.WrapInvoke();

			// Convierto el objecto en bool
			var parsedType = Type.GetType("System.Boolean");

			// Si el tipo existe
			if (parsedType != null)
			{
				// Obtengo el valor desde el parámetro
				var boolValue = (bool?)Convert.ChangeType(valueToParse, parsedType);

				// Si la conversión fue exitosa
				if (boolValue.HasValue)
				{
					// Ejecuto el evento
					_onBoolParsed.WrapInvoke(boolValue.Value);
				}
			}
		}
	}
}
