﻿using UnityEngine;

namespace Modular.CurveProcessor
{
	public abstract class AbstractCurveProcessor : MonoBehaviour
	{
		[Header("Bounds")]
		[SerializeField] private float _minValue;
		[SerializeField] private float _maxValue;

		public float MinValue { get => _minValue; set => _minValue = value; }
		public float MaxValue { get => _maxValue; set => _maxValue = value; }
	}
}