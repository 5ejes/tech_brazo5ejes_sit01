﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLimitsTest : MonoBehaviour
{
  [SerializeField] private TransformLimits[] _limits;

  private void OnDrawGizmos()
  {
    if (_limits != null)
    {
      if (_limits.Length > 0)
      {
        foreach (var limit in _limits)
        {
          var bounds = limit.GetLimitBoundsXZ();

          foreach (var position in bounds)
          {
            Gizmos.DrawWireSphere(position, 0.01f);
          }
        }
      }
    }
  }
}