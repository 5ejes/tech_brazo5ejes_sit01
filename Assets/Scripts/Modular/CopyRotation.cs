﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Modular
{
	public class CopyRotation : MonoBehaviour, IManualUpdate
	{
		[Header("Target")]
		[SerializeField] private Transform _target;
		[SerializeField] private Vector3 _offset;
		[Space]
		[SerializeField] private bool _copyX = true;
		[SerializeField] private bool _copyY = true;
		[SerializeField] private bool _copyZ = true;
		
		[Space]
		[SerializeField] private int _updateEach = 0;
		[SerializeField] private bool _runOnUpdate = true;

		public bool hasRigidbody => _rigidbody != null;
		private Rigidbody _rigidbody = null;
		public Vector3 Offset { get => _offset; set => _offset = value; }
		public bool RunOnUpdate { get => _runOnUpdate; set => _runOnUpdate = value; }
		public bool AllowUpdate { get => enabled; set => enabled = value; }

		private void Awake()
		{
			// Obtengo el rigidbody
			_rigidbody = GetComponent<Rigidbody>();
		}

		private void LateUpdate()
		{
			// Si no está permitido ejecutar en Update
			if (!RunOnUpdate) return;

			// Si tiene un rigidbody, no haga ninguna operación
			if (hasRigidbody) return;

			// Si el target no está definido
			if (_target == null)
			{
				Debug.LogError($"Target is not defined : {gameObject.name}");
				return;
			}

			CallUpdate();

		}

		private void FixedUpdate()
		{
			// Si no está permitido ejecutar en Update
			if (!RunOnUpdate) return;

			// Si tiene un Rigidbody
			if (hasRigidbody)
			{

				// Si tiene un rigidbody, no haga ninguna operación
				if (hasRigidbody) return;

				// Si el target no está definido
				if (_target == null)
				{
					Debug.LogError($"Target is not defined : {gameObject.name}");
					return;
				}

				CallUpdate();

			}
		}

		public static void TrackRotation(Transform self, Transform target, Quaternion offset = default, bool copyX = true, bool copyY = true, bool copyZ = true)
		{

			Vector3 targetRotation = CopyRotationVector(self, target, copyX, copyY, copyZ);

			// Defino la rotación del transform actual
			self.rotation = Quaternion.Euler(targetRotation) * offset;
		}

		public static void TrackRotation(Rigidbody self, Transform target, Quaternion offset = default, bool copyX = true, bool copyY = true, bool copyZ = true)
		{

			Vector3 targetRotation = CopyRotationVector(self.transform, target, copyX, copyY, copyZ);

			// Defino la rotación del Rigidbody actual
			self.MoveRotation(Quaternion.Euler(targetRotation) * offset);
		}

		private static Vector3 CopyRotationVector(Transform self, Transform target, bool copyX = true, bool copyY = true, bool copyZ = true)
		{
			Vector3 targetRotation = new Vector3();

			// Uso la copia y el offset para procesar
			targetRotation.x = copyX ? target.rotation.eulerAngles.x : self.rotation.eulerAngles.x;
			targetRotation.y = copyY ? target.rotation.eulerAngles.y : self.rotation.eulerAngles.y;
			targetRotation.z = copyZ ? target.rotation.eulerAngles.z : self.rotation.eulerAngles.z;

			// Retorno la rotación objetivo
			return targetRotation;
		}

		public void CallUpdate()
		{
			if (!hasRigidbody)
			{
				// Si updateEach tiene un valor no válido
				if (_updateEach <= 0)
				{
					// Cada frame
					TrackRotation(transform, _target, Quaternion.Euler(Offset), _copyX, _copyY, _copyZ);
				}
				else if (Time.frameCount % _updateEach == 0) // Si llego a un multiplo del frame definido
				{
					// Cada n frame
					TrackRotation(transform, _target, Quaternion.Euler(Offset), _copyX, _copyY, _copyZ);
				}
			}
			else
			{// Si updateEach tiene un valor no válido
				if (_updateEach <= 0)
				{
					// Cada frame
					TrackRotation(_rigidbody, _target, Quaternion.Euler(Offset), _copyX, _copyY, _copyZ);
				}
				else if (Time.frameCount % _updateEach == 0) // Si llego a un multiplo del frame definido
				{
					// Cada n frame
					TrackRotation(_rigidbody, _target, Quaternion.Euler(Offset), _copyX, _copyY, _copyZ);
				}

			}
		}
	}
}