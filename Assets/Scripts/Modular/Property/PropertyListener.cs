﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static NSScriptableEvent.ScriptableEventSystemObject;
using Object = UnityEngine.Object;
using NSUtilities;

namespace Modular.Property
{

	public class PropertyListener : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private Object _value;
		[SerializeField] private string _parseType;
		[SerializeField] private string _propertyName;
		[Space]
		[SerializeField] private float _listeningPeriod = 0.5f;

		[Header("Events")]
		[SerializeField] private SystemObjectEvent _onValueChange;

		public Object ObjectValue { get => _value; set => _value = value; }
		public string ParseType { get => _parseType; set => _parseType = value; }
		public string PropertyName { get => _propertyName; set => _propertyName = value; }

		private PropertyInfo _property = null;
		private object _storedPropertyValue = null;
		private string _storedPropertyName = null;

		private float _elapsed;

		private object ParsedObject { get; set; }
		public float ListeningPeriod { get => _listeningPeriod; set => _listeningPeriod = value; }

		private void Update()
		{
			// Si no está en el tiempo de actualización
			if (!IsInUpdateTime(ListeningPeriod)) return;

			// Si el objeto no es nulo
			if (ObjectValue != null)
			{
				// Obtengo la propiedad

				// Si la propiedad no está definida
				if (_property == null || !PropertyName.Equals(_storedPropertyName))
				{
					// Defino el nombre de la propiedad
					_storedPropertyName = PropertyName;

					// Obtengo el tipo
					var typeParsed = Type.GetType(ParseType);

					// Defino el objeto convertido
					ParsedObject = Convert.ChangeType(ObjectValue, typeParsed);

					// Si el tipo convertido no es nulo
					if (typeParsed != null)
					{
						// Obtengo el nombre de la propiedad
						_property = typeParsed.GetProperty(PropertyName);

						// Si la propiedad no se ha definido
						if (_property == null)
						{
							// Lanzo un error
							Debug.LogError($"The property named as {PropertyName} wasn't found : {gameObject}");
							return;
						}
					}
					else
					{
						// Si es nulo

						// Lanzo un error
						Debug.LogError($"The parsed type {ParseType} wasn't found : {gameObject}");
						return;
					}
				}

				// Si la propiedad no es nula
				if (_property != null)
				{
					// Si la propiedad se puede leer
					if (_property.CanRead)
					{
						// Si el objeto convertido no es nulo
						if (ParsedObject != null)
						{
							// Defino el valor de la propiedad
							var currentPropertyValue = _property.GetValue(ParsedObject);

							// Si el valor de la propiedad es diferente al valor almacenado
							if (!currentPropertyValue.Equals(_storedPropertyValue))
							{
								// Almaceno el nuevo valor
								_storedPropertyValue = currentPropertyValue;

								// Ejecuto el evento
								_onValueChange.WrapInvoke(_storedPropertyValue);

								Debug.Log(_property.PropertyType.ToString());
							}
						}
						else
						{
							// Si es nulo

							// Lanzo un error
							Debug.LogError($"The parsed object of type {ParseType} is null: {gameObject}");
							return;
						}
					}
				}
			}
		}

		private bool IsInUpdateTime(float desiredTimeout)
		{
			// Aumento el elapsed
			_elapsed += Time.deltaTime;

			// Si el elapsed supera al tiempo estimado
			if (_elapsed > desiredTimeout)
			{
				// Reinicio elapsed
				_elapsed = 0f;

				// Retorno true
				return true;
			}
			else
			{
				// Retorno false
				return false;
			}
		}
	}
}