﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Modular;
using NSUtilities;
using UnityEngine.Events;

namespace Modular.Selector
{

	/// <summary>
	/// Lista todos los transform disponibles en el objeto como hijos
	/// </summary>
	public class TransformSelector : GenericSelector<Transform>
	{

		[Header("Events")]
		[SerializeField] private TransformEvent _onSelectItem = new TransformEvent();

		private void OnEnable()
		{
			// Agrego los elementos a la lista
			InitializeSelector(_onSelectItem);

			// Limpio la lista
			Items.Clear();

			// Recorro los hijos
			foreach (Transform child in transform)
			{
				// Agrego el hijo actual a la lista
				Items.Add(child);
			}
		}

		/// <summary>
		/// Selecciona el elemento contenido en la lista que coincida con el parámetro e invoco el evento
		/// </summary>
		/// <param name="name">Nombre del objeto a buscar</param>
		/// <returns>El objeto seleccionado de la lista</returns>
		public void SelectAndInvoke(string name)
		{
			// Selecciono el elemento
			Select(child => child.name.Equals(name));
		}
	}
}