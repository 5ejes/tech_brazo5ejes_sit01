﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace Modular.Selector
{
	public abstract class GenericSelector<T> : MonoBehaviour
	{

		/// <summary>
		/// Lista de los items de este módulo
		/// </summary>
		protected List<T> Items { get; set; }

		private UnityEvent<T> SelectionEvent { get; set; }

		public void InitializeSelector(UnityEvent<T> selectionEvent)
		{
			// Inicializo la lista
			Items = new List<T>();

			// Defino la referencia al evento actual
			SelectionEvent = selectionEvent;
		}


		/// <summary>
		/// Selecciona el elemento contenido en la lista que coincida con el parámetro
		/// </summary>
		/// <param name="predicate">Predicado de filtro para seleccionar Item</param>
		/// <returns>El objeto seleccionado de la lista</returns>
		public T Select(System.Predicate<T> predicate)
		{
			//Compruebo si la lista no es nula
			if (Items != null)
			{
				// Si hay más de un elemento en la lista
				if (Items.Count > 0)
				{
					// Procedo a buscar el elemento que coincide
					var selected = Items.Find(predicate);

					// Si el elemento seleccionado no es nulo
					if (selected != null)
						SelectionEvent.WrapInvoke(selected);

					// Retorno el elemento hallado o null
					return selected;
				}
				else
				{
					// No hay elementos en la lista
					Debug.LogWarning($"The list is not initialized : {gameObject}");
					return default(T);
				}

			}
			else
			{
				// Si es nula
				Debug.LogError($"The list is null : {gameObject}");
				return default(T);
			}
		}

		/// <summary>
		/// Selecciona un item del selector a través de un índice
		/// </summary>
		/// <param name="index">Índice del item a seleccionar</param>
		public void SelectIndex(int index)
		{
			// Si la referencia a la lista no es nula
			if (Items != null)
			{
				// Si el índice es menor a la cantidad de items
				if (index < Items.Count)
				{
					// Selecciona el item en la lista
					var selected = Items[index];

					// Invoco el evento
					SelectionEvent.WrapInvoke(selected);
				}
			}
		}

		/// <summary>
		/// Añade el item a la lista
		/// </summary>
		/// <param name="item">Item a añadir a la lista</param>
		/// <param name="ignoreContained">'true' Si ignora cualquier dato que se encuentre en la lista, 'false' si no se tiene en cuenta</param>
		public void Add(T item, bool ignoreContained = true)
		{
			// Si item no es nulo
			if (item != null)
			{
				// Añado el item
				if (ignoreContained)
				{
					// Agrego el item a la lista de items
					Items.Add(item);
				}
				else
				{
					// Verifico si está el item
					if (!Items.Contains(item))
					{
						// Agrego el item a la lista de items
						Items.Add(item);
					}
				}
			}
		}

		/// <summary>
		/// Remueve el item especificado
		/// </summary>
		/// <param name="item">Item a remover de la lista</param>
		public void Remove(T item)
		{
			// Si contiene el item
			if (Items.Contains(item))
			{
				// Remuevo el item
				Items.Remove(item);
			}
		}

		/// <summary>
		/// Remuevo el item que coincida con el predicado
		/// </summary>
		/// <param name="match">Predicado a coincidir</param>
		public void RemoveMatching(System.Predicate<T> match)
		{
			// Remuevo el item
			Items.RemoveAll(match);
		}
	}
}
