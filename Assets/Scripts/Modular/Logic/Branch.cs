﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace Modular.Logic
{
	public class Branch : MonoBehaviour
	{
		[Header("Event")]
		[SerializeField] private UnityEvent _onTrueEvaluation;
		[SerializeField] private UnityEvent _onFalseEvaluation;
		[Space]
		[SerializeField] private BoolEvent _evaluationValuePassThrough;
		public void Evaluate(bool value)
		{
			// Paso el valor evaluado a través del evento
			_evaluationValuePassThrough.WrapInvoke(value);

			// Evaluo el evento y obtengo una referencia a el evento seleccionado
			var evaluationEvent = value ? _onTrueEvaluation : _onFalseEvaluation;
			
			// Ejecuto el evento seleccionado
			evaluationEvent.WrapInvoke();
		}
	}
}
