﻿using System;
using Modular;
using NSUtilities;
using UnityEngine;

public class MultiValueLerpVector3 : MonoBehaviour
{

	[Header("Settings")]
	[Range(0, 1)]
	[SerializeField] private float _lerpValue;
	[Header("Values")]
	[SerializeField] private bool _readFromChildren;
	[SerializeField] private Transform[] _transformValues;

	[Header("Events")]
	[SerializeField] private Vector3Event _lerpResult;
	[SerializeField]private FloatEvent _xValueResult;
	[SerializeField]private FloatEvent _yValueResult;
	[SerializeField]private FloatEvent _zValueResult;

	private float _previousLerpValue = -1f;

	public float LerpValue { get => _lerpValue; set => _lerpValue = value; }
	public Transform[] TransformValues { get => _transformValues; set => _transformValues = value; }
	public Vector3Event LerpResult { get => _lerpResult; set => _lerpResult = value; }
	public bool ReadFromChildren { get => _readFromChildren; set => _readFromChildren = value; }
	public FloatEvent XValueResult { get => _xValueResult; set => _xValueResult = value; }
	public FloatEvent YValueResult { get => _yValueResult; set => _yValueResult = value; }
	public FloatEvent ZValueResult { get => _zValueResult; set => _zValueResult = value; }

	private void Awake()
	{
		if (ReadFromChildren)
			ResolveTransformValuesFromChildren(transform);
	}


	private void Update()
	{
		// LerpValue = (Mathf.Sin(Time.time) + 1) / 2;
		if (LerpValue != _previousLerpValue)
		{
			_previousLerpValue = LerpValue;

			EmitCurrentLerpValue();
		}
	}
	private void ResolveTransformValuesFromChildren(Transform transformReference)
	{

		if (transformReference == null) return;
		int childCount = transformReference.childCount;
		if (childCount == 0) return;
		_transformValues = new Transform[childCount];

		for (int i = 0; i < childCount; i++)
		{
			_transformValues[i] = transformReference.GetChild(i);
		}
	}
	public Vector3 GetLerpFromValues(Transform[] transformValues, float lerpValue)
	{
		Vector3 output = Vector3.zero;

		int length = transformValues.Length;
		int topIndex = GetTopIndex(lerpValue, length - 1);

		if (topIndex <= 0) return output;
		int previousIndex = topIndex - 1;
		Vector3 aVector, bVector;

		aVector = transformValues[previousIndex].position;
		bVector = transformValues[topIndex].position;

		float partialLerp = 1 / ((float)length - 1);
		float fractionLerp = (lerpValue % (partialLerp + 0.001f)) / partialLerp;

		output = Vector3.Lerp(aVector, bVector, fractionLerp);
		return output;
	}

	private int GetTopIndex(float lerpValue, int length)
	{
		float inverseLerpValue = Mathf.Lerp(1, length, lerpValue);
		return Mathf.CeilToInt(inverseLerpValue);
	}

	public void EmitLerpValue(float value)
	{
		Vector3 lerpResultValue = GetLerpFromValues(TransformValues, value);
		LerpResult.WrapInvoke(lerpResultValue);

		XValueResult.WrapInvoke(lerpResultValue.x);
		YValueResult.WrapInvoke(lerpResultValue.y);
		ZValueResult.WrapInvoke(lerpResultValue.z);
	}

	public void EmitCurrentLerpValue()
	{
		EmitLerpValue(LerpValue);
	}
}
