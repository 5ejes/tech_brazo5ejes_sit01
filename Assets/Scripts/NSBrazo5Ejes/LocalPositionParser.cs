﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSBrazo5Ejes
{
	/// <summary>
	/// Convierte cualquier punto local usado por el brazo en su traducción a punto en espacio global
	/// </summary>
	public class LocalPositionParser : MonoBehaviour
	{
		[Header("Events")]
		[SerializeField] private UnityEvent _onParseAction = new UnityEvent();
		[SerializeField] private Vector3Event _onParsedVector = new Vector3Event();

		[Header("Limits")]
		[SerializeField] private Transform _adjustedMinLimit = null;
		[SerializeField] private Transform _maxLimit = null;

		public void Parse(Vector3 localCoordinates)
		{
			// Genero el vector transformado
			var result = ParseVector(localCoordinates);

			// Invoco el evento
			_onParsedVector.WrapInvoke(result);
		}

		/// <summary>
		/// Convierte las coordenadas locales en coordenadas globales para facil uso
		/// </summary>
		/// <param name="localCoordinates">Coordenadas locales compatibles con el entorno del brazo</param>
		/// <returns>El Vector3 convertido compatible con el brazo 5 ejes</returns>
		public Vector3 ParseVector(Vector3 localCoordinates)
		{
			// Convierto las coordenadas locales

			// Creo los límites para definir la dirección relativa
			var limitDir = (_maxLimit.position - _adjustedMinLimit.position).normalized;

			// Obtengo la diferencia

			// Defino la dirección de movimiento según los límites
			localCoordinates.x *= Mathf.Sign(limitDir.x);
			localCoordinates.y *= Mathf.Sign(limitDir.y);
			localCoordinates.z *= Mathf.Sign(limitDir.z);

			// Inicializo la posición relativa
			localCoordinates += _adjustedMinLimit.position;
      
			// Resuelvo las rotaciones

			// Creo el punto pivote
			var pivotPoint = _adjustedMinLimit.position;

			// Genero un vector que apunte hacia la posición objetivo sin rotar
			var positionDifference = localCoordinates - pivotPoint;

			// Creo una copia del vector
			var newDiff = positionDifference;


			// Multiplico por los límites predefinidos
			newDiff.x *= Mathf.Sign(-limitDir.x);
			newDiff.z *= Mathf.Sign(limitDir.z);

			// Roto el vector
			newDiff = transform.rotation * newDiff;

			// Obtengo la nueva posición objetivo, aplicando el vector sobre el punto pivote
			localCoordinates = newDiff + pivotPoint;

			// Ejecuto el evento
			_onParseAction.WrapInvoke();

			// Retorno la posición final
			return localCoordinates;

		}

		public Vector3 ParseVector(Vector3 localCoordinates, Transform minLimit, Transform maxLimit)
		{
			// Convierto las coordenadas locales

			// Creo los límites para definir la dirección relativa
			var limitDir = (maxLimit.position - minLimit.position).normalized;

			// Obtengo la diferencia

			// Defino la dirección de movimiento según los límites
			localCoordinates.x *= Mathf.Sign(limitDir.x);
			localCoordinates.y *= Mathf.Sign(limitDir.y);
			localCoordinates.z *= Mathf.Sign(limitDir.z);

			// Inicializo la posición relativa
			localCoordinates += minLimit.position;

			// Resuelvo las rotaciones

			// Creo el punto pivote
			var pivotPoint = minLimit.position;

			// Genero un vector que apunte hacia la posición objetivo sin rotar
			var positionDifference = localCoordinates - pivotPoint;

			// Creo una copia del vector
			var newDiff = positionDifference;


			// Multiplico por los límites predefinidos
			newDiff.x *= Mathf.Sign(-limitDir.x);
			newDiff.z *= Mathf.Sign(limitDir.z);

			// Roto el vector
			newDiff = transform.rotation * newDiff;

			// Obtengo la nueva posición objetivo, aplicando el vector sobre el punto pivote
			localCoordinates = newDiff + pivotPoint;

			// Ejecuto el evento
			_onParseAction.WrapInvoke();

			// Retorno la posición final
			return localCoordinates;
		}

		/// <summary>
		/// Define si un punto Vector3 está dentro de los límites establecidos
		/// </summary>
		/// <param name="point">Punto a evaluar</param>
		/// <returns>'true' si el punto está dentro de los límites, ´false´ si ocurre lo contrario</returns>
		public bool IsInside(Vector3 point, float margin = 0f, Transform minLimit = null, Transform maxLimit = null)
		{

			// Si los límites no están definidos, defino los límites desde la propia referencia del parser
			if(minLimit == null) minLimit = _adjustedMinLimit;
			if(maxLimit == null) maxLimit = _maxLimit;

			// Creo las variables de comprobación por cada dimensión
			bool insideX, insideY, insideZ;

			// Creo variables para mayor y menor de cada dimensión
			Vector2 minMax_x, minMax_y, minMax_z;

			// Defino el menor de cada dimensión
			minMax_x.x = (minLimit.position.x < maxLimit.position.x) ? minLimit.position.x : maxLimit.position.x;
			minMax_y.x = (minLimit.position.y < maxLimit.position.y) ? minLimit.position.y : maxLimit.position.y;
			minMax_z.x = (minLimit.position.z < maxLimit.position.z) ? minLimit.position.z : maxLimit.position.z;

			// Defino el mayor de cada dimensión
			minMax_x.y = (minLimit.position.x > maxLimit.position.x) ? minLimit.position.x : maxLimit.position.x;
			minMax_y.y = (minLimit.position.y > maxLimit.position.y) ? minLimit.position.y : maxLimit.position.y;
			minMax_z.y = (minLimit.position.z > maxLimit.position.z) ? minLimit.position.z : maxLimit.position.z;

			// Compruebo cada dimensión

			// Si el min es 0 y el max es 10
			// n > min && n < max

			// Si el min es 0 y el max es -10
			// n > max && n < min

			// Calculo el valor de inside, considerando que el índice 0 corresponde al min y el índice 1 corresponde al max
			insideX = point.x > minMax_x[0] - margin && point.x < margin + minMax_x[1];
			insideY = point.y > minMax_y[0] - margin && point.y < margin + minMax_y[1];
			insideZ = point.z > minMax_z[0] - margin && point.z < margin + minMax_z[1];

			// Evaluo si todos las dimensiones están dentro de los límites
			return insideX && insideY && insideZ;
		}
	}
}