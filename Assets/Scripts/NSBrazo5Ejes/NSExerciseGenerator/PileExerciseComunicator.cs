﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using NSTraduccionIdiomas;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;
using static NSScriptableEvent.ScriptableEventString;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
	public class PileExerciseComunicator : MonoBehaviour
	{
		[Header("References")]
		[SerializeField] private Transform _localPositionParse = null;
		[Header("Events")]
		[SerializeField] private StringEvent _onSendStringInfo;
		public PileExerciseRoutine ExerciseRoutine { get; set; } = null;

		/// <summary>
		/// Obtengo la información de los objetivos de la rutina
		/// </summary>
		public void ComunicateTargetInfo()
		{
			// Comunico la rutina actual
			ComunicateTargetInfo(ExerciseRoutine);
		}

		/// <summary>
		/// Obtengo la información de los objetivos de la rutina, dada la rutina
		/// </summary>
		/// <param name="argRoutine">Rutina a comunicar</param>
		public void ComunicateTargetInfo(PileExerciseRoutine argRoutine)
		{
			// Si existe una rutina
			if (argRoutine != null)
			{

				// Obtengo la cuenta de los evaluadores
				int count = argRoutine.EvaluatorTokens.Count;

				// Creo un arreglo con las posiciones de cada token
				Vector3[] targetTokenPositions = new Vector3[count];

				// Por cada token
				for (int i = 0; i < count; i++)
				{
					// Obtengo el evaluador actual
					var evaluator = argRoutine.EvaluatorTokens[i];

					if (_localPositionParse)
					{
						// Obtengo la posición actual convertida a coordenadas locales
						targetTokenPositions[i] = _localPositionParse.InverseTransformPoint(evaluator.WorldPosition);
					}
					else
					{
						// Obtengo la posición actual
						targetTokenPositions[i] = evaluator.WorldPosition;
					}
				}

				// Texto de la información
				var info = DiccionarioIdiomas.Instance.Traducir("TextDescripcionSituacionP1");

				// Extraigo los parentesis
				var splitted = Regex.Matches(info, @"\(([^)]*)\)");

				// Si hay al menos 4 elementos
				int splittedCount = splitted.Count;

				// creo un arreglo de los elementos modificados
				var stringValues = new List<string>();

				// Compilo los valores a reemplazar en orden
				var floatParams = new List<float>();

				// Anexo la posición objetivo, tanto la posición en x como la posición en z
				floatParams.Add(Mathf.Abs(targetTokenPositions[0].x * 100f));
				floatParams.Add(Mathf.Abs(targetTokenPositions[0].z * 100f));

				// Creo un valor de snap
				float snap = 0.05f;

				// Recorro Cada posición
				foreach (var position in argRoutine.Positions)
				{
					// Hago snap de los valores
					var xSnapValue = Mathf.Round(position.x / snap) * snap;
					var ySnapValue = Mathf.Round(position.z / snap) * snap;

					// Anexo los dos valores correspondiente a los ejes x y z de cada vector
					floatParams.Add(Mathf.Abs(xSnapValue * 100f));
					floatParams.Add(Mathf.Abs(ySnapValue * 100f));
				}

				// Creo un índice para el parametro float
				int indexFloat = 0;

				// Si la cuenta de matches supera el mínimo de 3 posiciones y el objetivo
				if (splittedCount >= 4)
				{
					// Por cada elemento
					for (int i = 0; i < splittedCount; i++)
					{
						// Obtengo el grupo actual
						var currentMatch = splitted[i];

						// Obtengo el valor del match
						var matchValue = currentMatch.Value;

						// Evaluo el match actual para encontrar cada letra
						var lettersParams = Regex.Matches(currentMatch.Value, @"([a-z])");

						// Por cada letra parametro encontrada
						for (int j = 0; j < lettersParams.Count; j++)
						{
							// Obtengo el parámetro letra actual
							var currentParam = lettersParams[j];
							
							// Reemplazo cada valor con el parametro flotante actual
							matchValue = matchValue.Replace(currentParam.Value, $"{floatParams[indexFloat].ToString("0")}");

							// Aumento el índice del parametro flotante
							indexFloat++;
						}

						// Reemplazo el parámetro
						info = info.Replace(splitted[i].Value, matchValue);
					}
				}


				// Invoco el evento output
				_onSendStringInfo.WrapInvoke(info);

			}
		}

	}
}
