﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
  [System.Serializable]
  public class PileExerciseRoutine : AbstractExerciseRoutine
  {
    public PileExerciseRoutine(params PointEvaluationToken[] tokens)
    {

      // Creo la lista que contiene los evaluadores y las posiciones de los cubos
      _evaluators = new List<PointEvaluationToken>();
      _positions = new List<Vector3>();

      // Anexo todos los evaluadores
      _evaluators.AddRange(tokens);
    }
    private List<Vector3> _positions;
    private List<PointEvaluationToken> _evaluators;

    private System.Action _onEvaluationCompleted;

    /// <summary>
    /// Lista de los evaluadores a usar para generar la calificación
    /// </summary>
    public List<PointEvaluationToken> EvaluatorTokens => _evaluators;
    /// <summary>
    /// Posiciones de los cubos
    /// </summary>
    public override List<Vector3> Positions { get => _positions; set => _positions = value; }
    public override Action OnEvaluationCompleted { get => _onEvaluationCompleted; set => _onEvaluationCompleted = value; }

    public override bool EvaluateRoutine()
    {

      // Reinicio la calificación de la rutina
      CurrentQualification = 0f;

      // Por cada evaluador

      // Si la lista no es nula
      if (_evaluators != null)
      {
        // Obtengo la cuenta de elementos en la lista
        int count = _evaluators.Count;

        // Si la lista tiene al menos un elemento
        if (count > 0)
        {
          // Genero un arreglo con los resultado
          bool[] results = new bool[count];

          // Fracción de calificación
          float qualificationFraction = 1 / (float)count;

          // Defino las acciones que me escribiran los resultados
          System.Action succededEval, failedEval = succededEval = null;

          // Por cada evaluador
          for (int i = 0; i < count; i++)
          {
            // Obtengo el evaluador actual
            var currentEvaluator = _evaluators[i];

            succededEval = () =>
              {
                Debug.Log($"Evaluation {i}: Succeded");
                // Modifico los resultados en la posición del arreglo definida
                results[i] = true;
              };

            failedEval = () =>
            {
              Debug.Log($"Evaluation {i}: Failed");
              // Modifico los resultados en la posición del arreglo definida
              results[i] = false;
            };

            // Genero los eventos
            currentEvaluator.OnSuccess += succededEval;
            currentEvaluator.OnFailure += failedEval;

            // Defino el evento de finalizar la evaluación
            currentEvaluator.OnEvaluationEnd += () =>
            {
              // Substraigo las rutinas de evaluación
              currentEvaluator.OnSuccess -= succededEval;
              currentEvaluator.OnFailure -= failedEval;

              // Substraigo esta rutina
              currentEvaluator.OnEvaluationEnd = null;
            };

            // Ejecuto el evaluador
            currentEvaluator.Evaluate();
          }

          // Evaluo los resultados
          // Por cada resultado
          foreach (var result in results)
          {
            // Si el resultado es falso, retorno falso
            if (!result)
            {
              // Continuo con el siguiente resultado
              continue;
            }

            // Incremento la calificación actual
            CurrentQualification += qualificationFraction;
          }

          // Ejecuto la acción, si está definida
          if (_onEvaluationCompleted != null) _onEvaluationCompleted.Invoke();

          // Evaluo si el resultado es mayor al threshold de calificación
          return CurrentQualification > QualificationThreshold;
        }
        else
        {
          // No hay elementos en la lista

          // Ejecuto la acción, si está definida
          if (_onEvaluationCompleted != null) _onEvaluationCompleted.Invoke();

          // Retorno falso
          return false;
        }
      }
      else
      {
        // La referencia a la lista es nula

        // Ejecuto la acción, si está definida
        if (_onEvaluationCompleted != null) _onEvaluationCompleted.Invoke();

        // Retorno falso
        return false;
      }
    }
  }
}
