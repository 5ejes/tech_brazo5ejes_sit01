﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
  public abstract class AbstractEvaluationToken : IEvaluator
  {
    /// <summary>
    /// Enumeración que define el elemento a buscar
    /// </summary>
    /// <value>Enumeración del objetivo</value>
    public ScriptableEnumID TargetEnumID { get; set; }

    /// <summary>
    /// Capa en la cual se hace la evaluación
    /// </summary>
    /// <value>Por defecto es -1 (Todas las capas)</value>
    public LayerMask CheckLayerMask { get; set; }

    /// <summary>
    /// Describe el evento al realizar satisfactoriamente una evaluación
    /// </summary>
    /// <value></value>
    public System.Action OnSuccess { get; set; } = null;

    /// <summary>
    /// Describe el evento al realizar una evaluación y que resulte en fallo
    /// </summary>
    /// <value></value>
    public System.Action OnFailure { get; set; } = null;

    /// <summary>
    /// Describe la acción al realizar una evaluación
    /// </summary>
    public System.Action OnEvaluationEnd;


    /// <summary>
    /// Evalua el token con las propiedades especificadas
    /// </summary>
    public abstract void Evaluate();


    protected void InvokeFailure()
    {
      // Si no hay resultados
      if (OnFailure != null) OnFailure();

      if (OnEvaluationEnd != null)
        // Ejecuto el evento
        OnEvaluationEnd.Invoke();
    }

    protected void InvokeSuccess()
    {
      // Si no hay resultados
      if (OnSuccess != null) OnSuccess();

      if (OnEvaluationEnd != null)
        // Ejecuto el evento
        OnEvaluationEnd.Invoke();
    }

  }
}