﻿using System.Collections;
using System.Collections.Generic;
using NSBrazo5Ejes.NSTransportBelt;
using NSInspectorValue;
using NSUtilities;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
	public class CubeAreaExerciseGenerator : AbstractExerciseGenerator
	{

		[Header("Positions")]
		[SerializeField] private Transform[] _areaTokenReferences;


		[Header("Color ID Array")]
		[Tooltip("Si la lista de Color ID disponibles para la evaluación es nula o su cuenta es 0, se ignora")]
		[SerializeField] private int[] _availableColorID;
		[SerializeField] private int _checkedElementGoal = 1;

		[Header("Events")]
		[SerializeField] private CubeAreaExerciseEvent _onExerciseGenerated;

		[Header("Debug")]
		[SerializeField] private bool _drawDebug = false;
		[SerializeField] private Color _gizmosColor = Color.blue;

		public Transform[] AreaTokenReferences { get => _areaTokenReferences; set => _areaTokenReferences = value; }
		private bool UseRequiredColorID => _availableColorID.Length > 0;

		public override void GenerateRoutine()
		{
			// Genero una variable con las posiciones
			Vector3[] generatedPositions = null;

			// Genero un arreglo con todas las posiciones
			generatedPositions = GenerateCubePositions(GeneratedPositionCount);

			// Creo una lista de tokens
			var tokens = new List<RadialAreaEvaluationToken>();

			// Genero un desplazamiento de 0
			int offset = 0;

			// Si uso los Color ID
			if (UseRequiredColorID)
				// Defino un desplazamiento para el ID
				offset = Random.Range(0, _availableColorID.Length);

			// Creo por cada referencia un token
			int length = AreaTokenReferences.Length;
			for (int tokenIndex = 0; tokenIndex < length; tokenIndex++)
			{
				// Obtengo el token actual
				var tokenReference = AreaTokenReferences[tokenIndex];

				// Configuro la variable de ID por defecto
				int currentColorID = -1;

				// Si genero los color ID requeridos
				if (UseRequiredColorID)
				{
					// Obtengo el ID Actual
					int colorIDIndex = (tokenIndex + offset) % _availableColorID.Length;
					currentColorID = _availableColorID[colorIDIndex];

					// Obtengo el componente de referencia Int
					var intInspectorValue = tokenReference.GetComponent<IntInspectorValue>();

					// Si existe el valor
					if (intInspectorValue)
					{
						// Defino el entero que almacena
						intInspectorValue.Value = currentColorID;
					}
				}

				// Creo el token de evaluación
				RadialAreaEvaluationToken evaluationToken = new RadialAreaEvaluationToken();

				evaluationToken.PositionInfo = tokenReference;
				evaluationToken.CheckRadius = tokenReference.lossyScale.magnitude;

				evaluationToken.RequiredColorID = currentColorID;

				evaluationToken.CheckLayerMask = TokenCheckLayer;
				evaluationToken.TargetEnumID = TargetEnumId;

				evaluationToken.CheckedElementGoal = _checkedElementGoal;

				// Defino los requerimientos especiales
				evaluationToken.SpecialRequeriments = cube =>
				{
					// Obtengo el transform
					Transform cubeTransform = cube.transform;

					// Obtengo el producto punto entre la vertical del mundo y la vertical del transform
					float absDot = Mathf.Abs(Vector3.Dot(cubeTransform.up, Vector3.up));

					// Si es mayor al threshold
					return absDot > 0.7f;
				};

				// Lo agrego a la lista
				tokens.Add(evaluationToken);

			}

			// Creo la rutina de ejercicio
			CubeAreaExerciseRoutine routine = new CubeAreaExerciseRoutine(tokens.ToArray());

			// Anexo las posiciones
			routine.Positions.AddRange(generatedPositions);

			// Ejecuto el evento
			_onExerciseGenerated.WrapInvoke(routine);
		}

		private void OnDrawGizmos()
		{
			// Si puedo dibujar las guías visuales
			if (_drawDebug && AreaTokenReferences != null)
			{
				// Si hay más de un elemento en la lista
				if (AreaTokenReferences.Length > 0)
				{
					// Por cada referencia para el token
					foreach (var tokenReference in AreaTokenReferences)
					{

						// Si el token actual no existe, continúo
						if (tokenReference == null) continue;

						// Defino el color del dibujado
						Gizmos.color = _gizmosColor;

						var rotationMatrix = Matrix4x4.TRS((Vector3)tokenReference.position, (Quaternion)tokenReference.rotation, (Vector3)tokenReference.localScale);
						Gizmos.matrix = rotationMatrix;

						// Dibujo el cubo
						Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
					}
				}
			}
		}
	}
}
