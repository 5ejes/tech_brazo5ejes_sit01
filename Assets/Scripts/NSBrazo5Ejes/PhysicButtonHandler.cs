using System;
using System.Collections;
using UnityEngine;

namespace NSBrazo5Ejes
{
	public class PhysicButtonHandler : MonoBehaviour
	{
		[SerializeField] private RoboticViewer _viewer;

		public void SetStartInput(bool value)
		{
			if (!_viewer) return;

			// Defino el start

			SetViewerStartInput(value);
		}

		public void SetStartInputOneFrame()
		{
			if (!_viewer) return;

			// Defino el start
			SetViewerStartInput(true);

			// Espero un frame
			StartCoroutine(WaitOneFrame(() =>
			{
				// Si el viewer existe
				if (_viewer)
				{
					// Defino el start como falso
					SetViewerStartInput(false);
				}
			}
			));
		}

		private static IEnumerator WaitOneFrame(Action callback)
		{
			yield return null;
			if (callback != null) callback();
		}

		private void SetViewerStartInput(bool value)
		{
			// Si el viewer es nulo
			if (_viewer == null)
			{
				// Lanzo el error
				Debug.LogError("Viewer is not defined", this);
				return;
			}

			// Modifico el valor de start input
			_viewer.digitalInputs[0] = value;
		}
	}
}
