﻿using Modular;
using NSUtilities;
using UnityEngine;

namespace NSBrazo5Ejes
{
	public class MousePositionInput : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private int _mouseButton = 0;
		[Tooltip("Comunica el evento cuando se haga click con el mouse")]
		[SerializeField] private bool _communicateOnUpdateClick = true;
		[Header("Events")]
		[SerializeField] private Vector3Event _onMouseDownPosition;
		[SerializeField] private Vector3Event _onMouseHoldPosition;
		[SerializeField] private Vector3Event _onMouseUpPosition;

		public int MouseButton { get => _mouseButton; set => _mouseButton = value; }
		public bool CommunicateOnUpdateClick { get => _communicateOnUpdateClick; set => _communicateOnUpdateClick = value; }
		public Vector3Event OnMouseDownPosition { get => _onMouseDownPosition; set => _onMouseDownPosition = value; }
		public Vector3Event OnMouseHoldPosition { get => _onMouseHoldPosition; set => _onMouseHoldPosition = value; }
		public Vector3Event OnMouseUpPosition { get => _onMouseUpPosition; set => _onMouseUpPosition = value; }

		private void Update()
		{
			// Si no se permite comunicar el evento en el update
			if (!CommunicateOnUpdateClick) return;

			// Obtengo la posición del mouse, y comunico según el evento
			Vector3 mousePosition = Input.mousePosition;

			if (Input.GetMouseButtonDown(MouseButton))
				CommunicateDown(mousePosition);

			if (Input.GetMouseButton(MouseButton))
				CommunicateDown(mousePosition);

			if (Input.GetMouseButtonUp(MouseButton))
				CommunicateUp(mousePosition);
		}

		private void CommunicateDown(Vector3 mousePosition) => OnMouseDownPosition.WrapInvoke(mousePosition);
		private void CommunicateHold(Vector3 mousePosition) => OnMouseHoldPosition.WrapInvoke(mousePosition);
		private void CommunicateUp(Vector3 mousePosition) => OnMouseUpPosition.WrapInvoke(mousePosition);


	}
}
