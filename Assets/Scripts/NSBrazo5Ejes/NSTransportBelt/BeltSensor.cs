﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using NSUtilities;
using UnityEngine;
using static NSScriptableEvent.ScriptableEventCube;

namespace NSBrazo5Ejes.NSTransportBelt
{
	public class BeltSensor : MonoBehaviour
	{
		[Header("Events")]
		[SerializeField] private BoolEvent _onSensorCheck;
		[SerializeField] private IntEvent _onColorIDCheck;
		[SerializeField] private CubeEvent _onSensorCubePass;

		[Header("Sensor Transform")]
		[SerializeField] private Transform _sensorTransform = null;
		[Header("Check Settings")]
		[SerializeField] private float _checkRadius = 0.05f;
		[SerializeField] private float _checkMinRadius = 0.01f;
		[SerializeField] private float _checkDistance = 0.1f;
		[Range(0f,1.25f), SerializeField] private float _exitCastFraction = 0.25f;

		[SerializeField] private LayerMask _checkLayer = -1;
		[SerializeField] private ScriptableEnumID _checkEnumID = null;
		[Space] [SerializeField] private int _updateFrame = 0;


		[Header("Debug")]
		[SerializeField] private bool _drawDebug = false;
		[SerializeField] private Color _gizmosColor = Color.white;

		private Cube _listeningErrorCubes;

		private bool _validObjectInSensor = false;
		private float _nextWaitIDFlushTImeout = 0f;
		private readonly float _waitIDFlushTimeout = 0.1f;


		private void Awake()
		{
			// Defino la distancia threshold para la detección de los cubos
		}

		private void FixedUpdate()
		{
			// Compruebo el valor del sensor
			CheckSensor();
		}

		public void CheckSensor()
		{
			// Compruebo el Update Frame definido

			// Defino la variable de los colisionadores
			RaycastHit[] results;

			// Si es menor a uno, lo ignoro
			if (_updateFrame < 1)
			{
				// Obtengo los colisionadores afectados
				results = GetSensorData();

				// Compruebo que los resultados no sean nulos
				if (results != null)
				{
					// Comprueblo los resultados obtenidos
					CheckResults(results);
				}
			}
			else
			{
				// Si no es menor a uno

				// Ejecuto el update Frame
				if (Time.frameCount % _updateFrame == 0)
				{
					// Obtengo los colisionadores afectados
					results = GetSensorData();

					// Compruebo que los resultados no sean nulos
					if (results != null)
					{
						// Comprueblo los resultados obtenidos
						CheckResults(results);
					}
				}
			}

		}

		private void CheckResults(RaycastHit[] results)
		{
			// Ejecuto el evento booleano
			_onSensorCheck.WrapInvoke(results.Length > 0);

			// Si hay más de un resultado
			if (results.Length > 0)
			{
				// recorro cada resultado
				foreach (var raycastResult in results)
				{
					// Obtengo el componente cubo del resultado obtenido
					var cubeComponent = raycastResult.collider.GetComponent<Cube>();

					// Ejecuto el evento
					_onSensorCubePass.WrapInvoke(cubeComponent);

					// Si hay un componente cubo
					if (cubeComponent)
					{
						// Obtengo el dato de color ID
						var colorID = cubeComponent.ColorID;

						// Ejecuto el evento con los datos obtenidos
						_onColorIDCheck.WrapInvoke(colorID);

					}

					// Marco la flag de color válido
					_validObjectInSensor = true;

					// Defino el tiempo de espera para el reinicio de los datos
					_nextWaitIDFlushTImeout = Time.time + _waitIDFlushTimeout;
				}
			}
			else
			{

				// Si ha detectado un color válido previamente
				if (_validObjectInSensor)
				{
					// Verifico que el tiempo límite al reinicio del dato se ha cumplido

					// Si no se ha cumplido
					if (Time.time < _nextWaitIDFlushTImeout) return;

					// Si se cumple restauro la flag
					_validObjectInSensor = false;
				}

				// Si no hay ningún resultado llamo la detección por defecto de color
				_onColorIDCheck.WrapInvoke(255);
			}
		}

		private RaycastHit[] GetSensorData()
		{

			// Genero un rayo sensor
			Ray sensorRay = new Ray(_sensorTransform.position, Vector3.up);

			// Obtengo los resultados
			var results = Physics.SphereCastAll(sensorRay, _checkRadius, _checkDistance, _checkLayer, QueryTriggerInteraction.Ignore);

			// Si el debug está habilitado, dibujo el rayo
			if (_drawDebug)
				Debug.DrawRay(sensorRay.origin, sensorRay.direction * _checkDistance, Color.green);

			// Si hay al menos un resultado
			if (results.Length > 0)
			{


				// Si el Enum ID ha sido definido
				if (_checkEnumID)
				{

					// Creo una lista de colisionadores nuevos
					List<RaycastHit> temp = new List<RaycastHit>();

					// Filtro los resultados con el enum ID
					foreach (var result in results)
					{
						// Obtengo el componente de Enumeración ID
						var enumIdentificator = result.transform.GetComponent<EnumIdentificator>();

						// Si hallo el enumIdentificator
						if (enumIdentificator)
						{
							// Comparo el enumID del identificador y mi EnumID
							if (_checkEnumID.Equals(enumIdentificator.EnumID))
							{

								// Obtengo la distancia de la detección actual
								Vector3 position = result.transform.position;
								position.y = _sensorTransform.position.y;
								var distanceToResult = (_sensorTransform.position - position).magnitude;
								// Evaluo la distancia

								//Si hay un objeto en el sensor, uso la fracción adicional

								// Defino una distancia cuando hay un objeto en el sensor y una cuando no hay objetos en el sensor
								float radialValue = _validObjectInSensor
								? Mathf.LerpUnclamped(_checkMinRadius, _checkRadius, _exitCastFraction)
								: _checkMinRadius;

								// Si el valor de distancia mínima es menor a epsilon o mayor al checkRadius lo restrinjo
								float minDistance = Mathf.Clamp(radialValue, float.Epsilon, _checkRadius);

								if (distanceToResult < minDistance)
								{
									// Si son los mismos, incluyo el resultado en la lista
									temp.Add(result);
								}
							}
						}
					}

					// Al terminar, reemplazo los resultados, con los resultados filtrados previamente
					results = temp.ToArray();
				}
			}

			// Retorno los resultados
			return results;
		}

		private void OnDrawGizmos()
		{
			// Si la posición se ha definido
			if (_sensorTransform != null)
			{
				// Defino el color del gizmo
				Gizmos.color = _gizmosColor;

				// Dibujo una esfera con los parámetros dados
				Gizmos.DrawWireSphere(_sensorTransform.position, _checkRadius);
				if (_checkMinRadius > 0 && _checkMinRadius < _checkRadius)
				{
					Gizmos.DrawWireSphere(_sensorTransform.position, _checkMinRadius);

					Gizmos.color = Color.yellow;
					Gizmos.DrawWireSphere(_sensorTransform.position, Mathf.LerpUnclamped(_checkMinRadius, _checkRadius, _exitCastFraction));
				}

			}
		}

	}
}
