﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace NSBrazo5Ejes.NSTransportBelt
{
	public class BeltLedStatusCommunicator : MonoBehaviour
	{
		[Header("Events")]
		[SerializeField] private UnityEvent _activeStateEvent;
		[SerializeField] private UnityEvent _inactiveStateEvent;
		[SerializeField] private UnityEvent _busyStateEvent;

		public void CallActiveEvent()
		{
			_activeStateEvent?.Invoke();
		}

		public void CallInactiveEvent()
		{
			_inactiveStateEvent?.Invoke();
		}

		public void CallBusyEvent()
		{
			_busyStateEvent?.Invoke();
		}
	}
}