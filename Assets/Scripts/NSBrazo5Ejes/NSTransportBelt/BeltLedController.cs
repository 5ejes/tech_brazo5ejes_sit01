﻿using UnityEngine;

namespace NSBrazo5Ejes.NSTransportBelt
{
	public class BeltLedController : MonoBehaviour
	{
		[Header("References")]
		[SerializeField] private MeshRenderer _ledMesh;

		[Header("Settings")]
		[SerializeField] private Color _ledColor;

		[Header("Predefined Colors")]
		[SerializeField] private Color _activeColor = Color.green;
		[SerializeField] private Color _busyColor = Color.yellow;
		[SerializeField] private Color _inactiveColor = Color.red;

		private Material _runtimeMaterial;

		public Color ActiveColor { get => _activeColor; set => _activeColor = value; }
		public Color BusyColor { get => _busyColor; set => _busyColor = value; }
		public Color InactiveColor { get => _inactiveColor; set => _inactiveColor = value; }
		public Color LedColor { get => _ledColor; private set => _ledColor = value; }

		private Color _lastLedColor;

#if UNITY_EDITOR

		private void Update()
		{
			if (!_lastLedColor.Equals(LedColor))
			{
				SetLedColor(LedColor);
			}
		}
#endif
		private void Awake()
		{
			// Si no hay un MeshRenderer definido
			if (_ledMesh)
			{
				// Lo busco en los hijos
				_ledMesh = GetComponentInChildren<MeshRenderer>();

				// Si no existe el mesh aún después de buscarlo
				if (!_ledMesh)
				{
					// Lanzo un error e interrumpo la operación
					Debug.LogError($"Mesh Renderer Reference undefined", this);
					return;
				}
			}

			// Inicializo el material runtime
			_runtimeMaterial = new Material(_ledMesh.material);

			// Defino el material copia
			_ledMesh.material = _runtimeMaterial;
		}

		public void SetLedColor(Color color)
		{
			// Si el material runtime no existe, retorno
			if (!_runtimeMaterial) return;

			// Si el color es el mismo aplicado actualmente, retorno
			if (_lastLedColor.Equals(color)) return;

			// Defino el campo de color previo
			_lastLedColor = color;

			// Defino el color
			LedColor = color;

			// Defino los colores del material
			_runtimeMaterial.color = color;
			_runtimeMaterial.SetColor("_EmissionColor", color);
		}

		public void SetColorToActive()
		{
			// Activo el color del material a la propiedad definida
			SetLedColor(ActiveColor);
		}

		public void SetColorToBusy()
		{
			// Activo el color del material a la propiedad definida
			SetLedColor(BusyColor);
		}

		public void SetColorToInactive()
		{
			// Activo el color del material a la propiedad definida
			SetLedColor(InactiveColor);
		}
	}
}