﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;

namespace NSCamera3DControl
{
	[System.Serializable]
	public class CinemachineVirtualCameraEvent : UnityEvent<CinemachineVirtualCamera> { };
}
