﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using NSScriptableValues;
using NSUtilities;
using UnityEngine;

namespace NSScriptableValueReader
{
	public class ScriptableValueErrorDefinitionReader : AbstractScriptableValueReader
	{
		[SerializeField] private ValueErrorDefinition _valueErrorDefinition;
		[SerializeField] private ErrorDefinitionEvent _readingResult;

		public override void Read()
		{
			_readingResult.WrapInvoke(_valueErrorDefinition);
		}
	}
}
