﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableValueReader.Array
{
	public abstract class GenericValueArrayReader<T> : MonoBehaviour
	{
		public UnityEvent<T> ResponseEvent { get; set; }
		public bool Initialized => ResponseEvent != null;

		public abstract void Read(int index);

	}
}
