﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSInspectorSender
{
	public abstract class BaseInspectorSender : MonoBehaviour
	{
		public abstract void Send();

		[ContextMenu("Send Action")]
		public void SendContextMenuAction()
		{
			Send();
		}
	}
}
