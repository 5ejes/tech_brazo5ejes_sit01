﻿#pragma warning disable 0649
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace NSActiveZones
{
    public class CheckCorrectTogglesActivate : MonoBehaviour
    {
        #region members

        [SerializeField] private List<Toggle> listAllToggles;

        [SerializeField] private List<TogglesGroupVerify> listAllTogglesGroupdVerify;

        [Header("Eventos verificacion"), Space(5)]
        public UnityEvent OnTogglesVerified;

        public UnityEvent OnTogglesIncorrect;

        #endregion

        public bool VerifyGroupToggles(string argNameGroup)
        {
            Debug.Log("listAllTogglesGroupdVerify : " + listAllTogglesGroupdVerify + " : " + argNameGroup);
            Debug.Log("listAllTogglesGroupdVerify.Find(x => x.name.Equals(argNameGroup)) : " + listAllTogglesGroupdVerify.Find(x => x.name.Equals(argNameGroup)));
            var tmpResult = listAllTogglesGroupdVerify.Find(x => x.name.Equals(argNameGroup)).VerifyTogglesActive(listAllToggles);

            if (tmpResult)
                OnTogglesVerified.Invoke();
            else
                OnTogglesIncorrect.Invoke();

            return tmpResult;
        }

        public void ActiveGroupToggles(string argNameGroup)
        {
            listAllTogglesGroupdVerify.Find(x => x.name.Equals(argNameGroup)).ActiveTogglesGroups();
        }

        public void DesactivateAllToggles()
        {
            foreach (var tmpToggle in listAllToggles)
                tmpToggle.isOn = false;
        }

        public bool AnyToggleActive()
        {
            foreach (var tmpToggle in listAllToggles)
                if (tmpToggle.isOn)
                    return true;

            return false;
        }
    }

    [Serializable]
    public class TogglesGroupVerify
    {
        public string name;

        public List<Toggle> listTogglesVerify;

        public UnityEvent OnTogglesActives;

        public bool VerifyTogglesActive(List<Toggle> argListAllToggles)
        {
            var tmpQuantityTogglesCorrectOn = 0;
            var tmpQuantityTogglesIncorrect = 0;

            foreach (var tmpToggle in listTogglesVerify)
            {
                if (tmpToggle.isOn)
                    tmpQuantityTogglesCorrectOn++;
            }

            foreach (var tmpToggle in argListAllToggles)
            {
                if (tmpToggle.isOn)
                    tmpQuantityTogglesIncorrect++;
            }

            if (tmpQuantityTogglesCorrectOn == listTogglesVerify.Count && tmpQuantityTogglesIncorrect == tmpQuantityTogglesCorrectOn)
            {
                OnTogglesActives.Invoke();
                return true;
            }
            else
                return false;
        }

        public void ActiveTogglesGroups(bool argActive = true)
        {
            foreach (var tmpToggle in listTogglesVerify)
                tmpToggle.isOn = argActive;
        }
    }
}