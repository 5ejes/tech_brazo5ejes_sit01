﻿using System.Collections;
using System.Collections.Generic;
using NSContextosLocalesBrazo;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerArmLocalContext : GenericScriptableEventListener<ArmLocalContext>
	{
		[SerializeField] private ScriptableEventArmLocalContext _scriptableEvent;
		[SerializeField] private ScriptableEventArmLocalContext.ArmLocalContextEvent _armLocalContextEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_armLocalContextEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_armLocalContextEvent.Invoke);
	}
}
