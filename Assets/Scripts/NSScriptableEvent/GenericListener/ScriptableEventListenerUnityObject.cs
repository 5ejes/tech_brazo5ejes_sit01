﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerUnityObject : GenericScriptableEventListener<Object>
	{
		[SerializeField] private ScriptableEventUnityObject _scriptableEvent;
		[SerializeField] private ScriptableEventUnityObject.UnityObjectEvent _UnityObjectEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_UnityObjectEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_UnityObjectEvent.Invoke);
	}
}
