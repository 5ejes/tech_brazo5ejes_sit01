﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerInt : GenericScriptableEventListener<int>
	{
		[SerializeField] private ScriptableEventInt _scriptableEvent;
		[SerializeField] private ScriptableEventInt.IntEvent _IntEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_IntEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_IntEvent.Invoke);
	}
}
