﻿using System.Collections;
using System.Collections.Generic;
using NSInterfazControlPLC;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerGrafcetController : GenericScriptableEventListener<grafcetOutputInterface>
	{
		[SerializeField] private ScriptableEventGrafcetController _scriptableEvent;
		[SerializeField] private ScriptableEventGrafcetController.GrafcetControllerEvent _grafcetOutputInterfaceEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_grafcetOutputInterfaceEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_grafcetOutputInterfaceEvent.Invoke);
	}
}
