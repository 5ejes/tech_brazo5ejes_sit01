﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerCollision : GenericScriptableEventListener<Collision>
	{
		[SerializeField] private ScriptableEventCollision _scriptableEvent;
		[SerializeField] private ScriptableEventCollision.CollisionEvent _collisionEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_collisionEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_collisionEvent.Invoke);
	}
}
