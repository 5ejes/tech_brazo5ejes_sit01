using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event Transform Array", menuName = "NSScriptableEvent/Transform Array", order = 0)]
	public class ScriptableEventTransformArray : AbstractScriptableEvent<Transform[]>
	{
		[System.Serializable]
		public class TransformArrayEvent : UnityEvent<Transform[]> { }
	}
}