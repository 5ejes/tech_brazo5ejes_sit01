﻿using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event Color", menuName = "NSScriptableEvent/Color", order = 0)]
	public class ScriptableEventColor : AbstractScriptableEvent<Color>
	{
		[System.Serializable]
		public class ColorEvent : UnityEvent<Color> { }
	}
}