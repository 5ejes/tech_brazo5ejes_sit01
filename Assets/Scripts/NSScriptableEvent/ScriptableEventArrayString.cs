using UnityEngine;

namespace NSScriptableEvent
{
    [CreateAssetMenu(fileName = "Event array string", menuName = "NSScriptableEvent/ArrayString", order = 0)]
    public class ScriptableEventArrayString : AbstractScriptableEvent<string[]>
    {
        
    }
}