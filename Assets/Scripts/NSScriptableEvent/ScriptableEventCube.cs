﻿using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event Cube", menuName = "NSScriptableEvent/Cube", order = 0)]
	public class ScriptableEventCube : AbstractScriptableEvent<Cube>
	{
		[System.Serializable]
		public class CubeEvent : UnityEvent<Cube> { }
	}
}