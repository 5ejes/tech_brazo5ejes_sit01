using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event SystemObject", menuName = "NSScriptableEvent/SystemObject", order = 0)]
	public class ScriptableEventSystemObject : AbstractScriptableEvent<object>
	{
		[System.Serializable]
		public class SystemObjectEvent : UnityEvent<object> { }
	}
}