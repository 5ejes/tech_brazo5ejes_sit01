using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event string", menuName = "NSScriptableEvent/String", order = 0)]
	public class ScriptableEventString : AbstractScriptableEvent<string>
	{
		[System.Serializable] public class StringEvent : UnityEvent<string> { }
	}
}