﻿using NSInterfazControlPLC;
using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event grafcetOutputInterface", menuName = "NSScriptableEvent/grafcetOutputInterface", order = 0)]
	public class ScriptableEventGrafcetOutputInterface : AbstractScriptableEvent<grafcetOutputInterface>
	{
		[System.Serializable]
		public class GrafcetOutputInterfaceEvent : UnityEvent<grafcetOutputInterface> { }
	}
}