﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSInspectorValue
{
	public abstract class GenericInspectorValue<T> : MonoBehaviour, IValue<T>
	{
		[Header("Description")]
		[Multiline(), SerializeField] private string _description;
		[Header("Read-Only Value")]
		[SerializeField] private T _value;
		public T Value
		{
			get
			{
				// Manejo la inicialización
				InitializedHandler();

				// Invoco el evento si está inicializado
				GetResponse.WrapInvoke(_value);

				// Retorno el valor
				return _value;
			}
			set
			{
				// Manejo la inicialización
				InitializedHandler();

				// Defino el valor
				_value = value;

				// Invoco el evento si está inicializado
				SetResponse.WrapInvoke(_value);
			}
		}
		public UnityEvent<T> SetResponse { get; set; }
		public UnityEvent<T> GetResponse { get; set; }
		public bool IsInitialized => SetResponse != null && GetResponse != null;

		private bool InitializedHandler()
		{
			// Lanzo un mensaje de error si no está inicializado el valor
			if (!IsInitialized) Debug.LogError($"{this} is not initialized properly, define the initialization values using Initialize()", this);
			return IsInitialized;
		}

		public abstract void Initialize();
	}
}
