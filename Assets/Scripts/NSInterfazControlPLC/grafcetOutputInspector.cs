using System.Collections;
using System.Collections.Generic;
using Modular;
using NSUtilities;
using UnityEngine;

namespace NSInterfazControlPLC
{
	public class grafcetOutputInspector : MonoBehaviour
	{
#if UNITY_EDITOR
		[Header("Arm Main States")]
		[SerializeField] private int positionX;
		[SerializeField] private int positionY;
		[SerializeField] private int positionZ;
		[Space]
		[SerializeField] private bool gripState;
		[Header("Belt & Sensors")]
		[SerializeField] private bool beltMotor;
		[SerializeField] private bool beltSensor;
		[SerializeField] private int colorSensorId;
		[Space]
		[SerializeField] private bool _eje5;


		public void UpdateInspectorValues()
		{
			if (GrafcetOutput != null)
			{
				positionX = (int)GrafcetOutput.PositionX;
				positionY = (int)GrafcetOutput.PositionY;
				positionZ = (int)GrafcetOutput.PositionZ;
				gripState = GrafcetOutput.GripState;
				beltMotor = GrafcetOutput.BeltMotor;
				beltSensor = GrafcetOutput.BeltSensor;
				colorSensorId = (int)GrafcetOutput.ColorSensorID;
				_eje5 = GrafcetOutput.Eje5;
			}
		}
#endif

		[Header("Events")]
		[SerializeField] private Modular.GrafcetOutputInterfaceEvent _onCreateInterfaceEvent;

		[Header("References")]
		[SerializeField] private RoboticViewer _viewer;
		[SerializeField] private GrafcetMainClass _mainClass;
		public grafcetOutputInterface GrafcetOutput { get; set; }
		public RoboticViewer Viewer { get => _viewer; set => _viewer = value; }

		private void Update()
		{
#if UNITY_EDITOR
			if (GrafcetOutput != null)
			{
				UpdateInspectorValues();
			}
#endif
		}

		private void SetDefaultViewerValues()
		{
			// Recorro cada array de datos y lo reinicio a su valor inicial
			int length = _viewer.digitalOutputs.Length;
			for (int i = 0; i < length; i++)
			{
				_viewer.digitalOutputs[i] = false;
			}

			length = _viewer.digitalInputs.Length;
			for (int i = 0; i < length; i++)
			{
				_viewer.digitalInputs[i] = false;
			}

			length = _viewer.memoryData.Length;
			for (int i = 0; i < length; i++)
			{
				// Si el índice corresponde a las posiciones X,Y o Z
				// O corresponde al valor del Color ID sensado por la banda
				if (i == 8    // X
				 || i == 9    // Y
				 || i == 10   // Z
				 || i == 14)  // Color ID
					_viewer.memoryData[i] = 255;  // Restablezco su valor por defecto
				else
					_viewer.memoryData[i] = 0;  // Defino los valores por defecto como 0
			}
		}

		public void CreateGrafcetOutput(grafcetController controller)
		{
			// Si el parametro no es nulo
			if (controller != null)
			{
				// Creo una instancia de Grafcet Output
				GrafcetOutput = new grafcetOutputInterface(controller);
				GrafcetOutput.Viewer = Viewer;

				// Si existe el evento, llamo el evento
				if (_onCreateInterfaceEvent != null) _onCreateInterfaceEvent.Invoke(GrafcetOutput);
			}
		}

		public void WriteEje5State(bool value)
		{
			if(GrafcetOutput == null) return;
			GrafcetOutput.Eje5 = value;
		}
		public void WriteBeltSensorState(bool value)
		{
			if (GrafcetOutput == null) return;
			GrafcetOutput.BeltSensor = value;
		}

		public void WriteGripperState(bool value)
		{
			if (_viewer == null) return;
			SetViewerGripperInput(value);
		}

		public void WriteColorSensorRead(int colorIdValue)
		{
			if (GrafcetOutput == null) return;
			GrafcetOutput.ColorSensorID = (uint)colorIdValue;
		}

		public void WriteXYZPositionValues(int? x = null, int? y = null, int? z = null)
		{
			if (GrafcetOutput == null) return;

			if (x.HasValue)
				GrafcetOutput.PositionX = (uint)x.Value;

			if (y.HasValue)
				GrafcetOutput.PositionY = (uint)y.Value;

			if (z.HasValue)
				GrafcetOutput.PositionZ = (uint)z.Value;
		}

		public void ResetViewerPositionValues()
		{
			WriteXYZPositionValues(255, 255, 255);
		}

		public void ResetToDefaultValues()
		{
			// Reinicio los valores por defecto del ejercicio

			// Desactivo todos los outputs del viewer
			SetDefaultViewerValues();

			// WriteGripperState(false);
			// ResetViewerPositionValues();
			// WriteColorSensorRead(255);
			// WriteBeltSensorState(false);


		}
		public void RemoveGrafcetController()
		{
			// Remuevo la referencia al output Grafcet
			GrafcetOutput = null;
		}

		public void RemoveGrafcetInternalController()
		{
			if (GrafcetOutput == null) return;
			GrafcetOutput.RemoveController();
		}

		public void PauseGrafcet()
		{
			if (GrafcetOutput == null) return;
			// NOTE: No hay forma de parar los relojes del Grafcet sin editar el código base
			//_mainClass.enabled =_viewer.enabled = false;
			_mainClass.simulationRunning = false;
		}

		public void ResumeGrafcet()
		{
			if (GrafcetOutput == null) return;
			// _mainClass.enabled = _viewer.enabled = true;
			_mainClass.simulationRunning = true;
		}

		private void SetViewerGripperInput(bool gripState)
		{
			// Si el viewer es nulo
			if (_viewer == null)
			{
				// Lanzo el error
				Debug.LogError("Viewer is not defined", this);
				return;
			}

			// Modifico el valor de gripper input
			_viewer.digitalInputs[2] = gripState;
		}

	}
}
