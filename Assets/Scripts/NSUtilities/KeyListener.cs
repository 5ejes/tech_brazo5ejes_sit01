﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSUtilities
{
	public class KeyListener : MonoBehaviour
	{
		public enum KeyListenMode { Press, Hold, Release }
		[Header("Settings")]
		[SerializeField] private KeyCode _keyValue;
		[SerializeField] private KeyListenMode _listenMode = KeyListenMode.Press;
		[Header("Events")]
		[SerializeField] private UnityEvent _response;

		public KeyCode KeyValue { get => _keyValue; set => _keyValue = value; }
		public UnityEvent Response { get => _response; set => _response = value; }
		public KeyListenMode ListenMode { get => _listenMode; set => _listenMode = value; }

		public void Update()
		{
			switch (ListenMode)
			{
				case KeyListenMode.Press:
					if (Input.GetKeyDown(KeyValue)) Response.WrapInvoke();
					break;
				case KeyListenMode.Hold:
					if (Input.GetKey(KeyValue)) Response.WrapInvoke();
					break;
				case KeyListenMode.Release:
					if (Input.GetKeyUp(KeyValue)) Response.WrapInvoke();
					break;
			}
		}
	}
}
