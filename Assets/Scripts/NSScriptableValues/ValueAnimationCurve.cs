﻿using UnityEngine;

namespace NSScriptableValues
{

	[CreateAssetMenu(fileName = "AnimationCurve", menuName = "NSScriptableValue/AnimationCurve", order = 0)]
	public class ValueAnimationCurve : AbstractScriptableValue<AnimationCurve>
	{
		public static implicit operator AnimationCurve(ValueAnimationCurve curve)
		{
			return curve.value;
		}
	}
}
