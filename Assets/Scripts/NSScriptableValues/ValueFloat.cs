#pragma warning disable 0660
#pragma warning disable 0661
using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableValues
{
    [CreateAssetMenu(fileName = "Float", menuName = "NSScriptableValue/Float", order = 0)]
    public class ValueFloat : AbstractScriptableValue<float>
    {
        #region sobre carga operadores

        public static implicit operator float(ValueFloat argValueA)
        {
            return argValueA.value;
        }

        public static ValueFloat operator +(ValueFloat argValueA, ValueFloat argValueB)
        {
            argValueA.Value += argValueB.Value;
            return argValueA;
        }

        public static ValueFloat operator -(ValueFloat argValueA, ValueFloat argValueB)
        {
            argValueA.Value -= argValueB.Value;
            return argValueA;
        }

        public static ValueFloat operator *(ValueFloat argValueA, int argValueB)
        {
            argValueA.Value *= argValueB;
            return argValueA;
        }

        public static ValueFloat operator /(ValueFloat argValueA, int argValueB)
        {
            argValueA.Value /= argValueB;
            return argValueA;
        }

        public static ValueFloat operator ++(ValueFloat argValueA)
        {
            argValueA.Value++;
            return argValueA;
        }

        public static ValueFloat operator --(ValueFloat argValueA)
        {
            argValueA.Value--;
            return argValueA;
        }

        public static bool operator >(ValueFloat argValueA, ValueFloat argValueB)
        {
            return argValueA.value > argValueB.value;
        }

        public static bool operator <(ValueFloat argValueA, ValueFloat argValueB)
        {
            return argValueA.value < argValueB.value;
        }

        public static bool operator ==(ValueFloat argValueA, ValueFloat argValueB)
        {
            return argValueA.value == argValueB.value;
        }

        public static bool operator !=(ValueFloat argValueA, ValueFloat argValueB)
        {
            return argValueA.value != argValueB.value;
        }

        public static bool operator >=(ValueFloat argValueA, ValueFloat argValueB)
        {
            return argValueA.value >= argValueB.value;
        }

        public static bool operator <=(ValueFloat argValueA, ValueFloat argValueB)
        {
            return argValueA.value <= argValueB.value;
        }

        #endregion
    }
}