﻿#pragma warning disable 0649
#pragma warning disable 0660
#pragma warning disable 0661
#pragma warning disable 0618

using System;
using Crosstales.FB;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NSBoxMessage;
using NSSeguridad;
using NSTraduccionIdiomas;
using SimpleJSON;
using System.Collections;
using System.IO;
using System.Text;
using NSCalificacionSituacion;
using NSScriptableValues;
using UnityEngine;

namespace NSCreacionPDF
{
    public class CreateSendPdfArchive : MonoBehaviour
    {
        #region members

        /// <summary>
        /// codigo unico por cada situacion, define a que practica pertenese el reporte 
        /// </summary>
        public string codigoSituacion;

        /// <summary>
        /// es el nombre de la practica con la extenion .pdf
        /// </summary>
        public string NombrePdf;

        public float ValorMayor;

        /// <summary>
        /// direcion donde se ubica el archivo
        /// </summary>
        private string documentPath;

        /// <summary>
        /// tiempo maximo de espera par ala creacion del  pdf
        /// </summary>
        private float maxTiempo = 3;

        private string imagenHoja1;

        private string imagenHoja2;

        private byte[] fileBytesAula;

        private string NombreUsLoguin;

        [SerializeField] private SOSecurityData soSecurityData;

        [SerializeField] private SOSessionData soSessionData;

        [SerializeField] private ValueFloat valFloatScore;

        [SerializeField] private SOSecurityConfiguration soSecurityConfiguration;

        #endregion

        #region Delegate

        public delegate void DelegatetMensaje(string mensage);

        /// <summary>
        ///  delegado el cual se llamda a la hora de reportar un mesaje importante para el usuario, se le da como parametro el mensaje a mostrar
        /// </summary>
        public DelegatetMensaje DltMensajeEnvioPdf = delegate(string mensage) { };

        public delegate void DelegateBotonterminarWebGl();

        /// <summary>
        /// delegado que es llamado una vez se crea el pdf en web para que el ususario pueda continuar, el delagado debe activar el boton
        /// </summary>
        public DelegateBotonterminarWebGl DltBotontTerminarPdf = delegate() { };

        #endregion

        private void Start()
        {
            ValorMayor = 5;
        }
        #region private methods

        /// <summary>
        /// abre el pdf con un visor o en su defecto lo descarga, solo pc
        /// </summary>
        private void OpenPdfStandAlone()
        {
            if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXEditor)
            {
                string pdfRuta = Uri.EscapeUriString("file:///" + documentPath);
                Application.OpenURL(pdfRuta);
            }
        }

        /// <summary>
        /// crea los directorios para guardar el pdf en las diferentes plataformas
        /// </summary>
        /// <param name="fileName"></param>
        private void DefinirPath(string fileName)
        {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {

#if UNITY_ANDROID
                try
                {
                    var _dir = "/storage/emulated/0/";
                    var _dir0 = "/storage/sdcard/";
                    var _dir1 = "/storage/sdcard0/";
                    var _dir2 = "/storage/sdcard1/";
                    var _dirx = "/sdcard/";
                    DirectoryInfo currentDirectory = new DirectoryInfo(_dir);
                    DirectoryInfo currentDirectory_0 = new DirectoryInfo(_dir0);
                    DirectoryInfo currentDirectory_1 = new DirectoryInfo(_dir1);
                    DirectoryInfo currentDirectory_2 = new DirectoryInfo(_dir2);
                    DirectoryInfo currentDirectory_x = new DirectoryInfo(_dirx);

                    if (currentDirectory.Exists)
                    {
                        documentPath = _dir;
                    }
                    else if (currentDirectory_0.Exists)
                    {
                        documentPath = _dir0;
                    }
                    else if (currentDirectory_1.Exists)
                    {
                        documentPath = _dir1;
                    }
                    else if (currentDirectory_2.Exists)
                    {
                        documentPath = _dir2;
                    }
                    else if (currentDirectory_x.Exists)
                    {
                        documentPath = _dirx;
                    }
                }
                catch (Exception error)
                {
                    Debug.Log("no se encontro direccion viable : " + error);
                }

                try
                {
                    if (!Directory.Exists(documentPath + "CloudLabs"))
                        Directory.CreateDirectory(documentPath + "CloudLabs");

                    documentPath = documentPath + "CloudLabs/" + fileName + ".pdf";
                }
                catch (Exception e)
                {
                    Debug.LogError("Error Ruta : " + e);
                }

#elif UNITY_IPHONE
            documentPath = Application.persistentDataPath + "/" + fileName + ".pdf";
#endif
            }
            else
            {
#if UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
            documentPath = FileBrowser.SaveFile("Save File", string.Empty, fileName, "pdf");
#else
                documentPath = Application.dataPath + "/../" + fileName + ".pdf";
#endif
            }
        }

        /// <summary>
        /// metodo que verifica la subscripcion del usuario a la practica a la cual se va a enviar el reporte 
        /// </summary>
        private void leerAulaScore()
        {
            string _data = "{\"user\":\"" + NombreUsLoguin + "\",\"labCode\":\"" + codigoSituacion + "\"}";
            byte[] bytesToEncode = Encoding.UTF8.GetBytes(_data);
            string encodedText = Convert.ToBase64String(bytesToEncode);
            string _url = soSecurityConfiguration.urlClassroom + "/externals/get_lab?data=" + encodedText;
            Debug.Log(_url);
            Debug.Log("esto es lo que se envia antes del reporte= " + _data);
            WWW wwwAulaGet = new WWW(_url);
            StartCoroutine(WaitForRequestAulaGet(wwwAulaGet));
        }

        /// <summary>
        /// metodo que prepara el envio de pdf y activa la corrutina que hace este envio 
        /// </summary>
        private void set_aula_score()
        {
            var tmpUrl = soSecurityConfiguration.urlClassroom + "/externals/put_lab"; // externals/put_lab http://localhost:3000/externals/put_lab
            var tmpDateTimeActual = DateTime.Now.ToString("yyyy/MM/dd");
            var tmpForm = new WWWForm();
            float calificacion = valFloatScore * 5.0f;
            string finalCal = calificacion.ToString("0.0");
   
            //Debug.Log("calificacion y multiplicar por 5----> "+ valFloatScore);
            var tmpParametros = "{\"user\":\"" + NombreUsLoguin + "\",\"labCode\":\"" + codigoSituacion + "\",\"attempts\":" + soSessionData.quantityAttempts.ToString() + ",\"delivery_date\":\"" + tmpDateTimeActual + "\",\"delivery_time\":\"" + soSessionData.TimeSituationString + "\",\"app_score\":" + finalCal.Replace(",", ".") + "}";///  + "}"; //+"1.8"+ "}"; la plataforma solo me esta amitiendo nuemros como notas   soSessionData.quantityAttempts.ToString() 
            var tmpNombrePdf = NombrePdf + ".pdf";
            tmpForm.AddField("data", tmpParametros);
            tmpForm.AddBinaryData("report_file", fileBytesAula, tmpNombrePdf, "application/pdf");
            WWW wwwAulaPut = new WWW(tmpUrl, tmpForm);
            StartCoroutine(WaitForRequestAulaPut(wwwAulaPut));

            Debug.Log("nombre del pdf antes de enviar " + tmpNombrePdf);
            Debug.Log("url a la que se enviar " + tmpUrl);
            Debug.Log("json " + tmpParametros);
        }


        /// <summary>
        /// mentodo encargado de crar el archivo pdf
        /// </summary>
        /// <param name="argHojasPdf"></param>
        private void CrearImagenDos(Texture2D[] argHojasPdf)
        {
            if (Application.platform == RuntimePlatform.WebGLPlayer)
            {
                StartCoroutine(BajarImagenParaWebPlayer(argHojasPdf));

                if (soSecurityConfiguration.isLtiActive)
                {
                    var tmpScore = Convert.ToSingle(valFloatScore.Value) / ValorMayor;
                    var tmpUrl = soSecurityConfiguration.urlScoreLti + "?score=" + tmpScore + "&lti_params=" + soSecurityData.ltiDatos;
                    var tmpLti = new WWW(tmpUrl);
                    Debug.Log(tmpUrl);
                }
            }

#if !UNITY_WEBGL
            try
            {
                var tmpAncho = Convert.ToInt32(argHojasPdf[0].width);
                var tmpAlto = Convert.ToInt32(argHojasPdf[0].height);
                documentPath = Application.persistentDataPath + "/" + DiccionarioIdiomas.Instance.Traducir(NombrePdf) + ".pdf";
                DefinirPath(DiccionarioIdiomas.Instance.Traducir(NombrePdf));

                var tmpRectangulo = new Rectangle(tmpAncho, tmpAlto);
                var tmpDocument = new Document(tmpRectangulo);
                PdfWriter.GetInstance(tmpDocument, new FileStream(documentPath, FileMode.Create));
                tmpDocument.SetMargins(0, 0, 0, 0);
                tmpDocument.Open();

                for (int i = 0; i < argHojasPdf.Length; i++)
                {
                    var tmpBytesImg = argHojasPdf[i].EncodeToPNG();
                    tmpDocument.Add(Image.GetInstance(tmpBytesImg));
                }
                tmpDocument.Close();

                if (soSecurityConfiguration.isModeClassroom && soSecurityConfiguration.isConectionActive)
                {
                    FileStream stream = File.OpenRead(documentPath);
                    fileBytesAula = new byte[stream.Length];
                    stream.Read(fileBytesAula, 0, fileBytesAula.Length);
                    stream.Close();
                    leerAulaScore();
                }
            }
            catch (Exception e)
            {
                Debug.LogError("Error creado PDF : " + e);
            }
#endif
        }

        #endregion

        #region public methods

        /// <summary>
        /// metodo que se llama desde la web, confirma la generacion del pdf
        /// </summary>
        public void ActivarBotonTerminar()
        {
            DltBotontTerminarPdf();
        }

        /// <summary>
        /// metodo utilizado para visualizar el pdf en ios y android unicamente
        /// </summary>
        public void VisualizarPDF()
        {
#if UNITY_ANDROID
            BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("MensajeCarpetaPdf"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
#endif

#if UNITY_IPHONE
        EtceteraBinding.showWebPage (documentPath , true );
#endif
        }

        /// <summary>
        /// metodo para enviar el pdf utilizado solo en android y ios
        /// </summary>
        public void SendPdfByMail()
        {
#if UNITY_ANDROID
            EtceteraAndroid.showEmailComposer("", "Reporte", "Reporte", false, documentPath);
#endif

#if UNITY_IPHONE
		EtceteraBinding.showMailComposerWithAttachment( documentPath , "application/pdf" , "Reporte" , "direccion" ,"Reporte " , "Reporte " , false);
#endif
        }

        /// <summary>
        /// se llama para enviar o crear el pdf
        /// </summary>
        /// <param name="argHojasPdf">Array con las render textures capturadas de las hojas del pdf en la escena</param>
        public void CrearReportePdf(Texture2D[] argHojasPdf)
        {
            Debug.Log("estoy en CrearReportePdf");
            NombreUsLoguin = soSecurityData.inputUser;
            CrearImagenDos(argHojasPdf);
        }
        #endregion

        #region courutines

        /// <summary>
        /// corrutina que se encarga de descargar el pdf en webgl
        /// </summary>
        /// <param name="argHojasPdf"></param>
        private IEnumerator BajarImagenParaWebPlayer(Texture2D[] argHojasPdf)
        {
            for (int i = 0; i < argHojasPdf.Length; i++)
            {
                var tmpJpgEncoder = new JPGEncoder(argHojasPdf[i], 100);
                yield return new WaitUntil(() => tmpJpgEncoder.isDone);
                var tmpStringPdfPage = "data:image/jpeg;base64," + Convert.ToBase64String(tmpJpgEncoder.GetBytes());
                Application.ExternalCall("AgregarPaginaPdf", tmpStringPdfPage, soSecurityConfiguration.isModeClassroom);
            }

            Application.ExternalCall("FinalizarCreacionPdf", DiccionarioIdiomas.Instance.Traducir(NombrePdf));
        }


        /// <summary>
        /// corrutina encargada de subir los datos al servidor asi como el archivo pdf
        /// </summary>
        private IEnumerator WaitForRequestAulaGet(WWW www)
        {
            yield return www;

            if (www.error == null)
            {
                byte[] decodedBytes = Convert.FromBase64String(www.text);
                string decodedText = Encoding.UTF8.GetString(decodedBytes);
                Debug.Log(decodedText);
                var _data = JSON.Parse(decodedText);

                if (_data["state"] != null)
                {
                    if (_data["state"].Value == "true")
                    {
                        if (_data["res_code"] != null)
                        {
                            switch (_data["res_code"].Value)
                            {
                                case "USER_NOT_FOUND":
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeUSER_NOT_FOUND"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;

                                case "LAB_NOT_ASSIGNED":
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeLAB_NOT_ASSIGNED"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;

                                case "LAB_NOT_FOUND":
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeLAB_NOT_FOUND"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;

                                case "STATUS_OK":
                                    if (_data["lab_state"].Value == "0")
                                    {
                                        Debug.Log("OK");
                                        set_aula_score(); // se a validado 
                                    }
                                    else
                                        BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeSTATUS_OKconValorDiferenteDE0"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));

                                    break;

                                case "DB_EXCEPTION":
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeDB_EXCEPTION"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;
                            }
                        }
                        else
                            BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeDelServidorRespuestaInvalida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                    }
                    else
                        BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeDelServidorRespuestaInvalida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                }
                else
                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeDelServidorRespuestaInvalida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
            }
            else
                BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeNosePudoEstableserConexion"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
        }

        /// <summary>
        /// coorrutina a la cual retorna el mensaje de estatus de la practiva subida, es decir si ya exite sino esta inscrito ect
        /// </summary>
        private IEnumerator WaitForRequestAulaPut(WWW www)
        {
            yield return www;

            if (www.error == null)
            {
                byte[] decodedBytes = Convert.FromBase64String(www.text);
                string decodedText = Encoding.UTF8.GetString(decodedBytes);

                Debug.Log("respuesta que da al enviar el pdf" + decodedText);

                var _data = JSON.Parse(decodedText);

                if (_data["state"] != null)
                {
                    if (_data["state"].Value == "true")
                    {
                        if (_data["res_code"] != null)
                        {
                            switch (_data["res_code"].Value)
                            {
                                case "LAB_INSERTED":
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeLAB_INSERTED"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;

                                case "LAB_NOT_ASSIGNED":
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeLAB_INSERTED"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;

                                case "LAB_NOT_FOUND":
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeLAB_NOT_FOUND"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;

                                case "LAB_UPDATED":
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeLAB_UPDATED"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;

                                case "DB_EXCEPTION":
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeDB_EXCEPTION"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;

                                case "LICENSE_EXPIRED":
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeLICENSE_EXPIRED"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;
                            }
                        }
                        else
                            BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeDelServidorRespuestaInvalida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                    }
                    else
                        BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeDelServidorRespuestaInvalida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                }
                else
                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeDelServidorRespuestaInvalida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
            }
            else
                BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeNosePudoEstableserConexion"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
        }

        #endregion
        public void mtdMandarReporteWebAula(string PdfWebAula)
        { 
            fileBytesAula = Convert.FromBase64String(PdfWebAula);
            leerAulaScore();
        }

    }


}