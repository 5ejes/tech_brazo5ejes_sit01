﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BuildWin : EditorWindow {

    string myString = "Hello World";
    bool groupEnabled;
    bool myBool = true;
    float myFloat = 1.23f;
    private BuildWin target;
    private bool buildBtn;
    public Utilidades utilsss;

    //add menu named  - mywindows
    [MenuItem("IE/BuildWizard")]
    static void Init()
    {
        //
        BuildWin wizardWindow = (BuildWin)EditorWindow.GetWindow(typeof(BuildWin));
        wizardWindow.Show();
    }

    void OnGUI()
    {
        //register
        GUILayout.Label("Base settings", EditorStyles.boldLabel);
        myString = EditorGUILayout.TextField("Text Field", myString);

        groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
        myBool = EditorGUILayout.Toggle("Toggle", myBool);
        myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);

        BuildWin myScript = (BuildWin)target;
        if (GUILayout.Button("Build"))
        {
            //utilsss.Electrical_and_Pneuma_Grafcet_Ladder_SPA_PC();
        }

        EditorGUILayout.EndToggleGroup();
    }

}
