﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EraseData : MonoBehaviour {

    [MenuItem("Simulator/Erase All Data")]
    static void DoSomething()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("Data erased...");
    }
}
