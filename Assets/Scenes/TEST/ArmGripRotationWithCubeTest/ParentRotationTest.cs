﻿using UnityEngine;

public class ParentRotationTest : MonoBehaviour
{
	[SerializeField] private Transform _detachedTarget;
	[SerializeField] private Transform _rotationTarget;

	private void Update()
	{
		if(_detachedTarget == null || _rotationTarget == null) return;

		var targetRotation = _rotationTarget.rotation;

		_detachedTarget.rotation = targetRotation;
	}
}
