﻿using System.Collections;
using System.Collections.Generic;
using NSBrazo5Ejes.NSDraggingBehaviour;
using NSUtilities;
using UnityEngine;

public class IsCollidingInspector : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private DragMoveComponent _moveComponent;
	[SerializeField] private DraggableComponent _checkCollisionTransform;
	[SerializeField] private float _cutSpace = 0.05f;
	[SerializeField] private GameObject[] _ignoreGameObjects;

	public void Evaluate()
	{

		var worldPosition = _checkCollisionTransform.Target.position;
		var target = _checkCollisionTransform;

		var IgnoreGameObjects = new List<GameObject>();

		IgnoreGameObjects.AddRange(_ignoreGameObjects);
		IgnoreGameObjects.Add(target.gameObject);

		// Si el draggable actual, está colisionando
		var isColliding = _moveComponent.IsColliding(worldPosition,
										target.GetOffsetToMin().ToAbsolute() * 2f * (1f - _cutSpace),
										target.Target.rotation,
										IgnoreGameObjects.ToArray());

		if (isColliding) Debug.Log("Colliding");
		else Debug.Log("Not Colliding");
	}
}
