﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestDemo
{

	public class RotationDemoManager : MonoBehaviour
	{
		[SerializeField] private Transform _demoRotationTransform;
		[SerializeField] private float _maxDegrees = 10f;
		[Space]
		[SerializeField] private Transform[] _demoTransformArray;

		private Quaternion _lastRotation;
		private IEnumerator _routine;

		private void OnEnable()
		{
			// Inicio la coroutine
			_routine = RotationCoroutine();
			StartCoroutine(_routine);

			_lastRotation = _demoRotationTransform.rotation;
		}

		private void FixedUpdate()
		{
			var difference = Quaternion.Angle(_demoRotationTransform.rotation, _lastRotation);
			if (difference > 1f)
			{
				StopCoroutine(_routine);
				StartCoroutine(_routine);
				_lastRotation = _demoRotationTransform.rotation;
			}
		}

		private void OnDisable()
		{
			// Detengo la coroutine
			StopCoroutine(_routine);
		}

		private IEnumerator RotationCoroutine()
		{
			// Inicializo la corutina
			int length = _demoTransformArray.Length;
			for (int i = 0; i < length; i++)
			{
				var currentTransform = _demoTransformArray[i];

				Vector3 snapRotation = new Vector3();
				Vector3 snapDemo = new Vector3();
				Vector3 currentEuler = currentTransform.rotation.eulerAngles;
				Vector3 demoEuler = _demoRotationTransform.rotation.eulerAngles;

				float snap = 90f;
				snapRotation.x = Mathf.Round(currentEuler.x / snap) * snap;
				snapRotation.y = Mathf.Round(currentEuler.y / snap) * snap;
				snapRotation.z = Mathf.Round(currentEuler.z / snap) * snap;

				snapDemo.x = Mathf.Round(demoEuler.x / snap) * snap;
				snapDemo.y = Mathf.Round(demoEuler.y / snap) * snap;
				snapDemo.z = Mathf.Round(demoEuler.z / snap) * snap;

				Quaternion snapQuaternionEuler = Quaternion.Euler(snapDemo);
				var qDiff = snapQuaternionEuler * (Quaternion.Inverse(snapQuaternionEuler) * _demoRotationTransform.rotation);

				var targetRotation = qDiff * Quaternion.Euler(snapRotation);

				var difference = float.MaxValue;

				float threshold = 1f;
				while (difference > threshold)
				{
					yield return null;


					difference = Quaternion.Angle(currentTransform.rotation, targetRotation);

					Quaternion targetIncrement = Quaternion.RotateTowards(currentTransform.rotation, targetRotation, _maxDegrees * Time.deltaTime);

					currentTransform.rotation = targetIncrement;
				}

				yield return null;
			}
		}
	}
}
