﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class botonIo : MonoBehaviour
{
    [SerializeField]
    private Animator anim;

    private bool stado=false;
    // Start is called before the first frame update
    void Start()
    {
        stado = true;
 
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void DesplegarBoton()
    {
        anim.SetBool("Desplagado", true);
    }


    public void ContraerBoton()
    {
        anim.SetBool("Desplagado", false);
    }


}
